package org.fudaa.fudaa.piv.imageio;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import junit.framework.TestCase;

/**
 * @author bertrand.marchand@deltacad.fr
 */
public class TestJPGM extends TestCase {

  public void testReadWritePGM() {
    try {
      URL url = TestJPGM.class.getResource("image1.pgm");
      File fpgm = new File(url.toURI());
      BufferedImage buf = new PivPGMReader().read(fpgm);
    
      assertEquals(buf.getWidth(), 1024);
      assertEquals(buf.getHeight(), 768);

      new PivPGMWriter().write(File.createTempFile("img",".pgm"), buf);
    }
    catch (IOException _exc) {
      assertFalse(true);
    }
    catch (URISyntaxException e) {
      assertFalse(true);
    }
  }
}
