package org.fudaa.fudaa.piv.io;

import org.fudaa.fudaa.piv.utils.PivUtils;

import junit.framework.TestCase;

/**
 * @author bertrand.marchand@deltacad.fr
 */
public class TestJExeLauncher extends TestCase {

  public void testComputeImgLauncher() {
    assertEquals(PivUtils.formatOn4Chars(1),"0001");
    assertEquals(PivUtils.formatOn4Chars(672),"0672");
    assertEquals(PivUtils.formatOn4Chars(9999),"9999");
  }
}
