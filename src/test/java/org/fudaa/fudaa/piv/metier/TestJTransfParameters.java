package org.fudaa.fudaa.piv.metier;

import org.fudaa.ebli.geometrie.GrPoint;

import junit.framework.TestCase;

/**
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class TestJTransfParameters extends TestCase {

  public void testOnlyRotationLauncher() {
    PivTransformationParameters params = new PivTransformationParameters(0, 0, -2, 90, -3, -3);
    GrPoint pt1 = new GrPoint(-3, -3, 0);
    GrPoint ptCompute1 = pt1.applique(params.getToComputing());
    GrPoint pt2 = new GrPoint(-3, -2, 0);
    GrPoint ptCompute2 = pt2.applique(params.getToComputing());
    
    assertEquals(ptCompute1.x_, -3, 0.01);
    assertEquals(ptCompute1.y_, -3, 0.01);
    assertEquals(ptCompute1.z_, -2, 0.01);
    
    assertEquals(ptCompute2.x_, -4, 0.01);
    assertEquals(ptCompute2.y_, -3, 0.01);
    assertEquals(ptCompute2.z_, -2, 0.01);
    
    GrPoint ptOrig1    = ptCompute1.applique(params.getToInitial());
    GrPoint ptOrig2    = ptCompute2.applique(params.getToInitial());
    
    assertEquals(ptOrig1.x_, -3, 0.01);
    assertEquals(ptOrig1.y_, -3, 0.01);
    assertEquals(ptOrig1.z_, 0, 0.01);
    
    assertEquals(ptOrig2.x_, -3, 0.01);
    assertEquals(ptOrig2.y_, -2, 0.01);
    assertEquals(ptOrig2.z_, 0, 0.01);
  }
  
  public void testAllParametersLauncher() {
    PivTransformationParameters params = new PivTransformationParameters(3, 3, -2, 90, -3, -3);
    GrPoint pt1 = new GrPoint(-6, -6, 0);
    GrPoint ptCompute1 = pt1.applique(params.getToComputing());
    GrPoint pt2 = new GrPoint(-6, -5, 0);
    GrPoint ptCompute2 = pt2.applique(params.getToComputing());
    
    assertEquals(ptCompute1.x_,  -3, 0.01);
    assertEquals(ptCompute1.y_,  -3, 0.01);
    assertEquals(ptCompute1.z_, -2, 0.01);
    
    assertEquals(ptCompute2.x_, -4, 0.01);
    assertEquals(ptCompute2.y_,  -3, 0.01);
    assertEquals(ptCompute2.z_, -2, 0.01);
    
    GrPoint ptOrig1    = ptCompute1.applique(params.getToInitial());
    GrPoint ptOrig2    = ptCompute2.applique(params.getToInitial());
    
    assertEquals(ptOrig1.x_, -6, 0.01);
    assertEquals(ptOrig1.y_, -6, 0.01);
    assertEquals(ptOrig1.z_,  0, 0.01);
    
    assertEquals(ptOrig2.x_, -6, 0.01);
    assertEquals(ptOrig2.y_, -5, 0.01);
    assertEquals(ptOrig2.z_,  0, 0.01);
  }
}
