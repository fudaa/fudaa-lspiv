package org.fudaa.fudaa.piv;

import java.text.DecimalFormat;

import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import org.fudaa.ctulu.CtuluLib;

/**
 * Une classe permettant de lier 2 textfield avec valeur num�riques. 
 * Quand un est modifi� par l'utilisateur, l'ature est automatiquement mis � jour.
 * 
 * @author Bertrand Marchand (marchand@detacad.fr)
 *
 * @param <T> Le type num�rique du premier champ
 * @param <U> Le type num�rique du deuxi�mem champ.
 */
public abstract class PivLinkNumericalTextFields<T extends Number,U extends Number> {
  JTextField tf1_;
  JTextField tf2_;
  boolean eventLocked_ = false;
  
  public PivLinkNumericalTextFields(JTextField _tf1, JTextField _tf2) {
    tf1_ = _tf1;
    tf2_ = _tf2;
    
    tf1_.addCaretListener(new CaretListener() {

      @Override
      public void caretUpdate(CaretEvent e) {
        if (eventLocked_)
          return;

        changeTF2FromTF1();
      }
    });
    
    tf2_.addCaretListener(new CaretListener() {
      @Override
      public void caretUpdate(CaretEvent e) {
        if (eventLocked_)
          return;
        
        changeTF1FromTF2();
      }
    });
  }
  
  /**
   * @param _b True : La liaison est active. False : La liaison est inactive.
   */
  public void setActive(boolean _b) {
    eventLocked_ = !_b;
    
    if (_b)
      changeTF2FromTF1();
  }
  
  private void changeTF2FromTF1() {
    eventLocked_=true;
    try {
      U val = calculateFromTF1();
      if (val instanceof Integer) {
        tf2_.setText("" + val);
      }
      else if (val instanceof Double) {
        tf2_.setText("" + getTF2DecimalFormat().format(val));
      }
    }
    catch (NumberFormatException exc) {
    }
    finally {
      eventLocked_=false;
    }
  }
  
  private void changeTF1FromTF2() {
    eventLocked_ = true;
    try {
      T val = calculateFromTF2();
      if (val instanceof Integer) {
        tf1_.setText("" + val);
      }
      else if (val instanceof Double) {
        tf1_.setText("" + getTF1DecimalFormat().format(val));
      }
    }
    catch (NumberFormatException exc) {}
    finally {
      eventLocked_ = false;
    }
  }
  
  /**
   * @return Le format de formatage de la valeur decimale de Textfield 1 par defaut.
   */
  protected DecimalFormat getTF1DecimalFormat() {
    return CtuluLib.getDecimalFormat();
  }
  
  /**
   * @return Le format de formatage de la valeur decimale de Textfield 2 par defaut.
   */
  protected DecimalFormat getTF2DecimalFormat() {
    return CtuluLib.getDecimalFormat();
  }
  
  /**
   * @return La valeur calcul�e depuis TextField 2 pour Textfield 1
   */
  public abstract T calculateFromTF2();
  
  /**
   * @return La valeur calcul�e depuis TextField 1 pour Textfield 2
   */
  public abstract U calculateFromTF1();
}