/*
 * @creation     18 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.metier.PivStatisticParameters;
import org.fudaa.fudaa.piv.metier.PivStatisticParameters.PivStatisticCalcultationMethod;

import com.memoire.bu.BuVerticalLayout;

/**
 * Un panneau pour moyenner les r�sultats filtr�s instantan�s.
 * Les resultats selectionn�s sont sauv�s dans le projet.
 *  
 * @author marchand@deltacad.fr
 * @version $Id: PivFilterInstantResultsPanel.java 9455 2016-11-18 11:05:02Z bmarchan $
 */
public class PivAverageFilteredResultsPanel extends CtuluDialogPanel {

  PivImplementation impl;
  /** Le mod�le pour la liste des resultats */
  private CtuluListEditorModel mdResults_;
  /** La liste des r�sultats dans l'ordre */
  private CtuluListEditorPanel pnListResults_;
  /** Le choix de la m�thode de calcul. */
  private JComboBox<String> coType;
  /** Le champ de vitesses manuel est il utilis� */
  private JCheckBox cbUseManualVelocities_;
  
  CtuluDialog diProgress_;

  /**
   * Constructeur.
   * @param _ui Le parent pour la boite de dialogue.
   */
  public PivAverageFilteredResultsPanel(PivImplementation _impl) {
    impl = _impl;
    
    // Moyenne
    
    JPanel pnAverage=new JPanel();
    pnAverage.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
    pnAverage.setLayout(new BuVerticalLayout(3, true, true));
    pnAverage.setPreferredSize(new Dimension(400,200));
    
    
    // Choix du calcul
    JPanel pnChoice = new JPanel();
    pnChoice.setLayout(new BorderLayout(3,3));
    pnChoice.add(new JLabel(PivResource.getS("M�thode de calcul:") + " "), BorderLayout.WEST);
    coType = new JComboBox<>();
    coType.addItem(PivResource.getS("R�sultats moyens"));
    coType.addItem(PivResource.getS("R�sultats m�dians"));
    pnChoice.add(coType, BorderLayout.CENTER);
    pnAverage.add(pnChoice);
    
    cbUseManualVelocities_ = new JCheckBox(PivResource.getS("Utiliser les vitesses calcul�es manuellement"));
    pnAverage.add(cbUseManualVelocities_);
    
    // Label r�sultats
    pnAverage.add(new JLabel(PivResource.getS("Liste des r�sultats instantan�s filtr�s"),JLabel.LEFT));
    
    // Liste des r�sultats
    mdResults_=new CtuluListEditorModel(false) {
      @Override
      public boolean isCellEditable(int _rowIndex, int _columnIndex) {
        return false;
      }
      @Override
      public Object createNewObject() {
        return null;
      }
    };
    
    pnListResults_ = new CtuluListEditorPanel(mdResults_, false, false, false, false, false);
    pnAverage.add(pnListResults_);

    setLayout(new BorderLayout());
    add(pnAverage,BorderLayout.CENTER);
  }
  
  /**
   * D�finit les param�tres pour la moyenne.
   * @param _tool L'utilitaire
   */
  public void setStatisticParameters(PivStatisticParameters params) {
    if (params == null) {
      return;
    }
    
    coType.setSelectedIndex(params.getMethod().ordinal());
    cbUseManualVelocities_.setSelected(params.isManualFieldUsed());
    
    boolean[] usedResults = params.getUsedInstantResults();
    
    String[] values=new String[usedResults.length];
    for (int i=0; i<usedResults.length; i++) {
      values[i]=PivResource.getS("R�sultat : {0}", (i+1));
    }
    
    mdResults_.setData(values);
    
    for (int i=0; i<usedResults.length; i++) {
      if (usedResults[i])
        pnListResults_.getTable().getSelectionModel().addSelectionInterval(i, i);
    }
  }
  
  /**
   * @return Les parametres.
   */
  public PivStatisticParameters getStatisticParameters() {
    PivStatisticParameters params = new PivStatisticParameters();

    PivStatisticCalcultationMethod method = PivStatisticCalcultationMethod.ordinal(coType.getSelectedIndex());
    params.setMethod(method);
    params.setManualFieldUsed(cbUseManualVelocities_.isSelected());

    boolean[] idSels=new boolean[mdResults_.getRowCount()];
    int[] sel=pnListResults_.getTable().getSelectedRows();
    for (int i : sel) {
      idSels[i]=true;
    }
    params.setUsedInstantResults(idSels);
    
    return params;
  }
  
  @Override
  public boolean isDataValid() {
    if (pnListResults_.getTable().getSelectedRowCount()==0 && !cbUseManualVelocities_.isSelected()) {
      setErrorText(PivResource.getS("Vous devez s�lectionner 1 r�sultat au moins parmi les vitesses calcul�es manuellement ou les r�sultats filtr�s"));
      return false;
    }
    return true;
  }

  @Override
  public boolean apply() {
    boolean compute = false;
    
    if (!getStatisticParameters().equals(impl.getCurrentProject().getStatisticParameters())) {
      
      if (impl.getCurrentProject().hasFlowResults() && !impl.question(PivResource.getS("Suppression des r�sultats"), 
          PivResource.getS("Attention : des r�sultats existent et seront supprim�s si vous modifiez les param�tres.\nVoulez-vous continuer ?"))) {
        return false;
      }
      
      impl.getCurrentProject().setStatisticParameters(getStatisticParameters());
      compute = true;
    }
    
    else if (!impl.getCurrentProject().hasAverageResults()) {
      compute = true;
    }
    
    if (compute) {
      // La tache a ex�cuter.
      PivTaskAbstract r=new PivTaskAbstract(PivResource.getS("Moyenne des r�sultats instantan�s")) {

        public boolean act(PivTaskObserverI _observer) {
          CtuluLog ana = new CtuluLog();

          // Moyenne
          PivExeLauncher.instance().computeAverageResults(ana, impl.getCurrentProject(), _observer);
          if (ana.containsErrorOrSevereError()) {
            impl.error(ana.getResume());
            return false;
          }

          // Lanc� � la fin, car l'interface se bloque si on ne le fait pas.
          // Probl�me de thread swing probablement...
          SwingUtilities.invokeLater(new Runnable() {
            public void run() {
              impl.message(PivResource.getS("Calcul termin�"), PivResource.getS("Le calcul s'est termin� avec succ�s"), false);
              impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);
              impl.get2dFrame().getVisuPanel().setAveVelocitiesLayerVisible(true);
            }
          });
          
          return true;
        }
      };

      PivProgressionPanel pnProgress_=new PivProgressionPanel(r);
      diProgress_ = pnProgress_.createDialog(impl.getParentComponent());
      diProgress_.setOption(CtuluDialog.ZERO_OPTION);
      diProgress_.setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
      diProgress_.setTitle(r.getName());

      r.start();
      diProgress_.afficheDialogModal();
    }
    
    return true;
  }
}
