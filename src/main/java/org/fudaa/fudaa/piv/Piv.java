/*
 * @creation 7 juin 07
 * @modification $Date: 2008/01/15 14:00:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.ToolTipManager;

import org.fudaa.fudaa.commun.impl.Fudaa;

import com.memoire.bu.BuPreferences;
import com.memoire.fu.FuLog;

/**
 * Classe de lancement de l'application Fudaa-Piv.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public final class Piv {
  private Piv() {}

  /**
   * Lance l'application. Les arguments de lancement possibles sont :
   * <ul>
   * <li>--no_terminal : Les affichage textes se font dans le terminal de lancement</li>
   * <li>--no_splash : Pas de splash screen au lancement de l'application</li>
   * <li>--version : Affiche la version de l'application et sort</li>
   * <li>--exePath {path} : Indique le path pour les exes
   * <li>--docPath {path} : Indique le path pour la doc
   * <li>--templatePath {path} : Indique le path pour les templates
   * </ul>
   * @param _args Les arguments autoris�s par l'applicatif.
   */
  public static void main(String[] _args) {
    ArrayList<String> largs = new ArrayList<String>(Arrays.asList(_args));
    if (!parseArguments(largs)) {
      System.exit(1);
    }
    // On r�cup�re les autres arguments � traiter.
    _args = largs.toArray(new String[0]);
    
    PivPreferences.setRoot("piv");
    // Obligatoire pour que la langue soit prise en charge le plus tot possible
    // et dans tous les composants standards de Java (FileChooser, ...). Il faut
    // le faire avant m�me qu'un composant Swing soit instanci�.
    String defLang=System.getProperty("piv.lang","fr"); // Langage par d�faut � l'installation.
    BuPreferences.BU.applyLanguage("en, fr",BuPreferences.BU.getStringProperty("locale.language",defLang),false);
    
    // Le niveau de log par defaut
    if (System.getProperty("fulog.level") == null) {
      System.setProperty("fulog.level", "trace");
    }
    
    ToolTipManager.sharedInstance().setDismissDelay(100000);

//    try {
//      //pour utiliser les icones crystal.
//      //a voir si cela convient.
//      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//    } catch (Exception ex) {
//      ex.printStackTrace();
//    }
//    BuResource.BU.setIconFamily("crystal");
//
//    JOptionPane.setRootFrame(BuLib.HELPER);
//    BuLib.HELPER.setIconImage(FudaaResource.FUDAA.getImage("fudaa-logo"));
    // pour ne pas noircir une case sur deux
//    UIManager.put("Table.darkerOddLines", Boolean.FALSE);
//    System.setProperty("swing.boldMetal", "false");
//    FuLib.setSystemProperty("lnf.menu.needed", "false");
    Fudaa f = new Fudaa();
    f.launch(_args, PivImplementation.informationsSoftware(), true);
    f.startApp(new PivImplementation());
  }

  /**
   * Parsing des arguments. Les arguments sp�cifiques � Fudaa-LSPIV, qui ne peuvent plus �tre
   * pass� par des properties Java.
   * @param _args
   * @return True : Les arguments sont corrects. False sinon
   * 
   */
  public static boolean parseArguments(ArrayList<String> _args) {
    
    int icpt = 0;
    while (icpt < _args.size()) {
      String option = _args.get(icpt);
      
      // On passe les options autres, elles seront trait�es plus tard.
      if (!option.equals("--exepath") && !option.equals("--docpath") && !option.equals("--templatepath")) {
        icpt++;
        continue;
      }
      _args.remove(icpt);
      
      if (icpt >= _args.size()) {
         System.err.println("Option " + option + ": value expected.");
         showUsage();
         return false;
      }
      
      String path = _args.get(icpt);
      _args.remove(icpt);
      
      File dir = new File(path);
      if (!dir.exists()) {
        System.err.println("Option " + option + ": Path "+ path + " not found");
        showUsage();
        return false;
      }
      else if (option.equals("--exepath")) {
        System.setProperty("piv.exe.path", path);
      }
      else if (option.equals("--docpath")) {
        System.setProperty("piv.doc.path", path);
      }
      else if (option.equals("--templatepath")) {
        System.setProperty("piv.templates.path", path);
      }
      
    }
    
    return true;
  }
  
  public static void showUsage() {
    System.out.println("Usage:");
    System.out.println("Fudaa-LSPIV [options]");
    System.out.println(" options:");
    System.out.println("   --exepath {exepath} :       Path for executables programs");
    System.out.println("   --docpath {docPath} :       Path for documentation");
    System.out.println("   --templatepath {tempPath} : Path for jauging export");
  }
}
