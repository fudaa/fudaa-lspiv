/*
 * @creation     18 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.piv.action.PivExportGaugingReportAction;

import com.memoire.bu.BuVerticalLayout;

/**
 * Un panneau pour remplir les valeurs des cellules nomm�es en vue d'exporter le rapport de jaugeage.
 *  
 * @author marchand@deltacad.fr
 * @version $Id: PivFilterInstantResultsPanel.java 9455 2016-11-18 11:05:02Z bmarchan $
 */
public class PivExportGaugingReportFillValuesPanel extends CtuluDialogPanel {
  
  class ValuesTableModel extends AbstractTableModel {
    List<PivExportGaugingReportAction.UserCellData> cells;
    
    public ValuesTableModel(List<PivExportGaugingReportAction.UserCellData> _cells) {
      cells=_cells;
    }

    @Override
    public int getRowCount() {
      return cells.size();
    }

    @Override
    public int getColumnCount() {
      return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (columnIndex==0) {
        return cells.get(rowIndex).description;
      }
      else {
        return cells.get(rowIndex).getValue(0,0);
      }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      return String.class;
    }

    @Override
    public String getColumnName(int column) {
      if (column == 0) {
        return PivResource.getS("Champ");
      }
      else {
        return PivResource.getS("Valeur");
      }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return columnIndex==1;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      cells.get(rowIndex).setValue(aValue.toString());
    }
  }
  

  /** Le mod�le pour la table des valeurs */
  private AbstractTableModel mdValues_;
  /** La liste des r�sultats dans l'ordre */
  private JTable tbValues_;

  /**
   * Constructeur.
   * @param _ui Le parent pour la boite de dialogue.
   */
  public PivExportGaugingReportFillValuesPanel(CtuluUI _ui, Collection<PivExportGaugingReportAction.UserCellData> _userVars, Set<String> _systemVars) {
    ArrayList<PivExportGaugingReportAction.UserCellData> userVars=new ArrayList<>(_userVars);
    Collections.sort(userVars,new Comparator<PivExportGaugingReportAction.UserCellData>() {
      @Override
      public int compare(PivExportGaugingReportAction.UserCellData c1, PivExportGaugingReportAction.UserCellData c2) {
        if (c1.description == null && c2.description == null) {
          return 0;
        }
        else if (c1.description == null) {
          return -1;
        }
        else if (c2.description == null) {
          return 1;
        }
        else {
          return c1.description.compareTo(c2.description);
        }
      }
    });
    
    ArrayList<String> systemVars=new ArrayList<>(_systemVars);
    Collections.sort(systemVars);
    
    StringBuilder sb=new StringBuilder();
    sb.append(PivResource.getS("Les champs de la table sont les zones nomm�es inconnues du syst�me,\nissues du mod�le Excel de rapport. Les zones nomm�es connues sont :\n\n"));
    for (String s : systemVars) {
      sb.append(s).append("\n");
    }
    setHelpText(sb.toString());
    
    mdValues_=new ValuesTableModel(userVars);
    tbValues_=new JTable(mdValues_);
    // Pour conserver la valeur saisie, m�me lorsqu'on clique sur le bouton Ok
    tbValues_.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);    
    JScrollPane sp=new JScrollPane(tbValues_);
    
    JPanel pnValues=new JPanel();
    pnValues.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
    pnValues.setLayout(new BuVerticalLayout(3, true, true));
    
    // Label valeurs
    pnValues.add(new JLabel(PivResource.getS("Champs � renseigner"),JLabel.LEFT));
    
    // Liste des r�sultats
    pnValues.setPreferredSize(new Dimension(600,300));
    pnValues.add(sp);

    setLayout(new BorderLayout());
    add(pnValues,BorderLayout.CENTER);
  }
  
}
