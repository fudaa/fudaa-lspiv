/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.piv;

import java.awt.Component;
import java.util.Locale;

import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteractionTargetI;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.commun.EbliSelectedChangeListener;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.piv.action.PivEnterGeometryAction;
import org.fudaa.fudaa.piv.layer.PivOrthoPointsModel;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;

import com.memoire.fu.FuLog;

/**
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivCreateGRPInDistanceModePanel extends CtuluDialogPanel {
  /**
   * Pour visualiser les Double de mani�re condens�e. Les autres types sont redu avec le renderer standard.
   * @author Bertrand Marchand
   */
  class DistanceTableCellRenderer extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
        int row, int column) {
      
      Object sval = value;
      if (value instanceof Double) {
        sval = String.format(Locale.US, "%.4G", value);
      }
      
      return super.getTableCellRendererComponent(table, sval, isSelected, hasFocus, row, column);
    }
  }
  
  /**
   * Un modele pour la saisie des distances
   * @author marchand@deltacad.fr
   */
  class DistanceTableModel extends AbstractTableModel {
    
    private String[] headers = new String[] {
        PivResource.getS("Segment"),
        PivResource.getS("Distance")
    };
    
    @Override
    public int getRowCount() {
      return currentPts_ == null ? 0 : 5;
    }

    @Override
    public int getColumnCount() {
      return headers.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (columnIndex == 0) {
        switch (rowIndex) {
        default:
        case 0:
          return "P1-P2";
        case 1:
          return "P1-P3";
        case 2:
          return "P1-P4";
        case 3:
          return "P2-P3";
        case 4:
          return "P2-P4";
        }
      }
      
      else {
        return dists_[rowIndex];
      }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      if (columnIndex == 0) {
        return;
      }
      
      eventEnabled_ = false;
      
      dists_[rowIndex] = getDoubleValue(aValue);
      fireParametersChanged();
      
      eventEnabled_ = true;
    }

    private Double getDoubleValue(Object aValue) {
      if (!aValue.toString().trim().isEmpty()) {
        // Double attendu
        try {
          return Double.parseDouble(aValue.toString().trim());
        }
        catch (NumberFormatException _exc) {
        }
      }
      
      return null;
    }

    @Override
    public String getColumnName(int columnIndex) {
      return headers[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      return columnIndex == 0 ? String.class : Object.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return columnIndex == 1;
    }
    
    public void pointsChanged() {
      dists_ = new Double[5];
      fireTableChanged(null);
    }
  }
  
  /**
   * Un modele pour l'affichage des coordonn�es
   * @author marchand@deltacad.fr
   */
  class CoordTableModel extends AbstractTableModel {
    
    private String[] headers = new String[] {
        PivResource.getS("N�"),
        PivOrthoPointsModel.ATT_IND_XI.getName(),
        PivOrthoPointsModel.ATT_IND_YI.getName(),
        PivOrthoPointsModel.ATT_IND_XR.getName(),
        PivOrthoPointsModel.ATT_IND_YR.getName()
    };
    
    @Override
    public int getRowCount() {
      return currentPts_ == null ? 0 : currentPts_.length;
    }

    @Override
    public int getColumnCount() {
      return headers.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      
      // Le n� de point
      switch (columnIndex) {
      case 0:
        return rowIndex + 1;
      case 1:
        return currentPts_[rowIndex].getImgPoint().x_;
      case 2:
        return currentPts_[rowIndex].getImgPoint().y_;
      case 3:
        double x = currentPts_[rowIndex].getRealPoint().x_;
        return x == Double.POSITIVE_INFINITY ? null : x;
      case 4:
        double y = currentPts_[rowIndex].getRealPoint().y_;
        return y == Double.POSITIVE_INFINITY ? null : y;
      default:
        return null;
      }
    }
    
    public void pointsChanged() {
      computeReelCoords();
      fireTableChanged(null);
    }
    
    protected void computeReelCoords() {
      for (int i = 0; i < currentPts_.length; i++) {
        currentPts_[i].setRealPoint(new GrPoint(notSetPoint));
      }
      
      // Les coordonn�es sont nulles => On sort.
      for (Double dist : dists_) {
        if (dist == null)
          return;
      }
      
      Double[] xr = new Double[4];
      Double[] yr = new Double[4];
      
      xr[0] = 0.;
      yr[0] = 0.;
      xr[1] = dists_[0];
      yr[1] = 0.;
      xr[2] = (dists_[1]*dists_[1]+dists_[0]*dists_[0]-dists_[3]*dists_[3])/(2*dists_[0]);
      yr[2] = Math.sqrt(dists_[1]*dists_[1]-xr[2]*xr[2]);
      xr[3] =(dists_[0]*dists_[0]+dists_[2]*dists_[2]-dists_[4]*dists_[4])/(2*dists_[0]);
      yr[3] = Math.sqrt(dists_[2]*dists_[2]-xr[3]*xr[3]);
      
      for (int i = 0; i < currentPts_.length; i++) {
        currentPts_[i].setRealPoint(new GrPoint(xr[i], yr[i], 0.));
      }
    }

    @Override
    public String getColumnName(int columnIndex) {
      return headers[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      return columnIndex == 0 ? String.class : Object.class;
    }
  }
  
  /**
   * Un editeur qui r�agit � la saisie des points.
   *
   * @author Bertrand Marchand (marchand@deltacad.fr)
   * @version $Id$
   */
  class EditionController implements ZCalqueEditionInteractionTargetI {

    /**
     * Declenche le changement d'�tat du bouton fin.
     */
    public void atomicChanged() {
      // On notifie la palette que la forme a chang�, pour activer ou non le bouton fin d'edition.
      GrPolyligne pl = (GrPolyligne)actDefinePoints_.getCalqueInteraction().getFormeEnCours().getFormeEnCours();
      
      // On stoppe le polygone au bout de 4 clics.
      if (pl != null && pl.nombre() == 4) {
        currentPts_ = new PivOrthoPoint[4];
        for (int i = 0; i<currentPts_.length; i++) {
          currentPts_[i] = new PivOrthoPoint(new GrPoint(notSetPoint), pl.sommet(i));
        }
        fireParametersChanged();
        
        actDefinePoints_.hideWindow();
      }
      
      ((PivSimplifiedEditionPalette)actDefinePoints_.getPaletteContent()).getButton(PivSimplifiedEditionPalette.BUTTON_END).setEnabled(pl != null);
      
      impl_.getCurrentProject().setOrthoPoints(currentPts_);
    }

    public boolean addNewPolygone(GrPolygone _pg, ZEditionAttributesDataI _data) {
      return true;
    }

    /** 
     * Remet a jour la position du point centre.
     */
    public boolean addNewPoint(GrPoint _point, ZEditionAttributesDataI _data) {
//      actEnterDistance_.changeAll();
      return true;
    }

    /** Non utilis� */
    public boolean addNewRectangle(GrPoint _pt, GrPoint _pt2, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewPolyligne(GrPolyligne _pt, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewMultiPoint(GrPolyligne _multi, ZEditionAttributesDataI _data) {
      FuLog.debug("Nombre de points "+_multi.nombre());
//      GrPolygone pg = new GrPolygone();
//      for (int i=0; i<_multi.nombre(); i++) {
//        pg.sommets_.ajoute(_multi.sommet(i));
//      }
//      ptsImg_ = _multi.sommets_.tableau();
//      fireParametersChanged();
//      
//      actDefinePoints_.hideWindow();
      return false;
    }

    /** Non utilis� */
    public void setMessage(String _s) {
    }

    /** Non utilis� */
    public void unsetMessage() {
    }

    /** Non utilis� */
    public void pointMove(double _x, double _y) {
    }
  }

  static GrPoint notSetPoint = new GrPoint(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, 0.);
  PivImplementation impl_;
  PivEnterGeometryAction actDefinePoints_;
  DistanceTableModel mdlDists_;
  CoordTableModel mdlCoords_;
  // Les points conserv�s, si annulation.
  PivOrthoPoint[] origPts_;
  // Les points ortho. null si aucun point saisi. Les coordonn�es reelles peuvent �tre vides.
  PivOrthoPoint[] currentPts_ = null;
  // Une valeur peut �tre null, indiquant que la valeur n'est pas renseign�e.
  private Double[] dists_ = new Double[5];
  boolean eventEnabled_ = true;
  

  /**
   * Creates new form PivCreateGRPInDistanceModePanel
   */
  public PivCreateGRPInDistanceModePanel(PivImplementation _impl) {
    impl_ = _impl;
    origPts_ = _impl.getCurrentProject().getOrthoPoints();
    _impl.getCurrentProject().setOrthoPoints(null);
    
    initComponents();
    customize();
  }

  private void customize() {
//    PivProject prj = impl_.getCurrentProject();
    
    
    actDefinePoints_ = new PivEnterGeometryAction(impl_.get2dFrame().getVisuPanel(), PivResource.getS("Saisie interactive..."), PivResource.getS("Saisir les 4 points"), true);
    actDefinePoints_.setGeometryForm(DeForme.MULTI_POINT);
    actDefinePoints_.setEditionController(new EditionController());
    btDefinePoints.setAction(actDefinePoints_);
    actDefinePoints_.addPropertyChangeListener(new EbliSelectedChangeListener(btDefinePoints));
    
    mdlDists_ = new DistanceTableModel();
    tbDists.setModel(mdlDists_);
    tbDists.setDefaultRenderer(Object.class, new DistanceTableCellRenderer());
    
    mdlCoords_ = new CoordTableModel();
    tbCoords.setModel(mdlCoords_);
    tbCoords.setDefaultRenderer(Object.class, new DistanceTableCellRenderer());
    
    lbInfoDist1.setText(PivResource.getS("Saisissez les 4 points dans le sens trigo"));
    lbInfoDist2.setText(PivResource.getS("Renseignez les distances"));
    
    pnDists.setBorder(new TitledBorder(PivResource.getS("Distances")));
    pnCoords.setBorder(new TitledBorder(PivResource.getS("Coordonn�es �quivalentes")));
  }

  /**
   * Notifie que les param�tres ont chang� (les param�tres peuvent �tre d�grad�s, c'est a dire pas completement remplis)
   */
  public void fireParametersChanged() {
    mdlCoords_.pointsChanged();
    
    if (!eventEnabled_)
      return;
    
    mdlDists_.pointsChanged();
  }
  
  
  @Override
  public boolean isDataValid() {
    if (currentPts_ == null) {
      setErrorText(PivResource.getS("Les 4 points doivent �tre saisis"));
      return false;
    }
    
    boolean bok = true;
    for (Double dist : dists_) {
      if (dist == null) {
        bok = false;
        break;
      }
    }
    if (!bok) {
      setErrorText(PivResource.getS("Les distances doivent toutes �tre renseign�es"));
      return false;
    }

    return true;
  }
  

  @Override
  public boolean apply() {
    boolean b = super.apply();
    
    if (b)
      impl_.getCurrentProject().setOrthoPoints(currentPts_);
    
    return b;
  }

  @Override
  public boolean cancel() {
    // Remise a jour des points de reference
    boolean b =  super.cancel();
    
    if (b)
      impl_.getCurrentProject().setOrthoPoints(origPts_);
    
    return b;
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnDists = new javax.swing.JPanel();
        lbImageDists = new javax.swing.JLabel();
        lbInfoDist1 = new javax.swing.JLabel();
        spDists = new javax.swing.JScrollPane();
        tbDists = new javax.swing.JTable();
        lbInfoDist2 = new javax.swing.JLabel();
        btDefinePoints = new javax.swing.JToggleButton();
        pnCoords = new javax.swing.JPanel();
        spCoords = new javax.swing.JScrollPane();
        tbCoords = new javax.swing.JTable();

        pnDists.setBorder(javax.swing.BorderFactory.createTitledBorder("Distances"));

        lbImageDists.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/fudaa/fudaa/piv/4points_distance.png"))); // NOI18N

        lbInfoDist1.setText("Saisissez les points dans le sens anti horaire ");

        tbDists.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"1-2",  new Double(7.2)},
                {"2-3",  new Double(10.29)},
                {"3-4",  new Double(6.69)},
                {"4-1",  new Double(10.1)}
            },
            new String [] {
                "Segment", "Distance"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        spDists.setViewportView(tbDists);

        lbInfoDist2.setText("Renseignez les distances");

        btDefinePoints.setText("Saisie interactive...");

        javax.swing.GroupLayout pnDistsLayout = new javax.swing.GroupLayout(pnDists);
        pnDists.setLayout(pnDistsLayout);
        pnDistsLayout.setHorizontalGroup(
            pnDistsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnDistsLayout.createSequentialGroup()
                .addGroup(pnDistsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbInfoDist1)
                    .addGroup(pnDistsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(btDefinePoints)
                        .addComponent(lbImageDists)))
                .addGap(18, 18, 18)
                .addGroup(pnDistsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnDistsLayout.createSequentialGroup()
                        .addComponent(lbInfoDist2)
                        .addContainerGap(31, Short.MAX_VALUE))
                    .addComponent(spDists, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        );
        pnDistsLayout.setVerticalGroup(
            pnDistsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnDistsLayout.createSequentialGroup()
                .addGroup(pnDistsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbInfoDist1)
                    .addComponent(lbInfoDist2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnDistsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnDistsLayout.createSequentialGroup()
                        .addComponent(lbImageDists)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btDefinePoints))
                    .addComponent(spDists, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(6, 6, 6))
        );

        pnCoords.setBorder(javax.swing.BorderFactory.createTitledBorder("Coordonn�es �quivalentes"));

        tbCoords.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(1),  new Integer(200),  new Integer(974),  new Double(-1.77),  new Double(20.48)},
                { new Integer(2),  new Integer(339),  new Integer(974),  new Double(0.14),  new Double(20.55)},
                { new Integer(3),  new Integer(855),  new Integer(991),  new Double(7.51),  new Double(20.45)},
                { new Integer(4),  new Integer(1479),  new Integer(915),  new Double(17.74),  new Double(16.0)}
            },
            new String [] {
                "N� point", "I", "J", "X r�el (m)", "Y r�el (m)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        spCoords.setViewportView(tbCoords);

        javax.swing.GroupLayout pnCoordsLayout = new javax.swing.GroupLayout(pnCoords);
        pnCoords.setLayout(pnCoordsLayout);
        pnCoordsLayout.setHorizontalGroup(
            pnCoordsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spCoords, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        pnCoordsLayout.setVerticalGroup(
            pnCoordsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spCoords, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnCoords, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnDists, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnDists, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnCoords, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

  private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton1ActionPerformed
    // TODO add your handling code here:
  }// GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton btDefinePoints;
    private javax.swing.JLabel lbImageDists;
    private javax.swing.JLabel lbInfoDist1;
    private javax.swing.JLabel lbInfoDist2;
    private javax.swing.JPanel pnCoords;
    private javax.swing.JPanel pnDists;
    private javax.swing.JScrollPane spCoords;
    private javax.swing.JScrollPane spDists;
    private javax.swing.JTable tbCoords;
    private javax.swing.JTable tbDists;
    // End of variables declaration//GEN-END:variables
}
