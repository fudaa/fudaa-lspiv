package org.fudaa.fudaa.piv;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteraction;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.geometrie.GrPolyligne;

/**
 * Une palette simplifi�e pour l'�dition (permet la reprise ou la fin d'�dition).
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivSimplifiedEditionPalette extends BuPanel implements BPalettePanelInterface {

  public static final String BUTTON_RESUME="RESUME_CMD";
  public static final String BUTTON_END="END_CMD";
  
  
  /** Le calque de cr�ation g�om�trique */
  protected ZCalqueEditionInteraction cqEdition_;
  /** Le bouton 'continuer'. */
  protected BuButton btContinuer_;
  /** Le bouton fin de saisie */
  protected BuButton btFin_;

  private ZEbliCalquesPanel pnCalques_;

  /**
   * Le constructeur de la palette.
   * @param _pnCalques Le panneau des calques.
   */
  public PivSimplifiedEditionPalette(ZEbliCalquesPanel _pnCalques, String _title) {
    pnCalques_ = _pnCalques;
    // Construction du panel \\
    setLayout(new BuVerticalLayout(2));
    // Le titre, car le titre de la fenetre n'est pas affich�.
    BuLabel lbTitre = new BuLabel("<html><b>" + _title + "</b></html>");
    lbTitre.setHorizontalAlignment(BuLabel.CENTER);
    add(lbTitre);
    // Bouton reprise
    btContinuer_ = new BuButton(PivResource.getS("Reprise"));
    btContinuer_.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        btContinuerActionPerformed(e);
      }
    });
    add(btContinuer_);
    // Bouton de fin de saisie
    btFin_ = new BuButton(PivResource.getS("Fin de saisie"));
    btFin_.setEnabled(false);
    btFin_.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        btFinActionPerformed(e);
      }
    });
    add(btFin_);


    // Affichage
    setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
    setVisible(true);
  }

  public void setButtonVisible(String _cmd, boolean _b) {
    if (BUTTON_RESUME.equals(_cmd)) {
      btContinuer_.setVisible(_b);
    }
    else if (BUTTON_END.equals(_cmd)) {
      btFin_.setVisible(_b);
    }
  }
  
  /**
   * Associe le calque d'interaction � la palette. Le calque d'interaction
   * permet de saisir interactivement le contour de la grille.
   * @param _cqEdition
   */
  public void setCalqueInteraction(ZCalqueEditionInteraction _cqEdition) {
    cqEdition_ = _cqEdition;
    if (_cqEdition==null) return;

    cqEdition_.addPropertyChangeListener("gele", new PropertyChangeListener() {
      public void propertyChange(PropertyChangeEvent evt) {
        cqEditionPropertyChange(evt);
      }
    });
  }

  private void cqEditionPropertyChange(PropertyChangeEvent evt) {
    if (evt.getPropertyName().equals("gele")) {
      btContinuer_.setEnabled((Boolean) evt.getNewValue());
    }
  }

  /**
   * Appel� lorsque le contour de grille est modifi�.
   * @param _form La forme en cours de cr�ation.
   */
//  public void currentFormeChanged(GrPolyligne _form) {
////    btFin_.setEnabled(_form!=null && _form.sommets_.nombre()>3);
//    btFin_.setEnabled(_form!=null);
//  }
  
  /**
   * @return Le bouton sp�cifique, par son nom. Jamais null.
   */
  public BuButton getButton(String _cmd) {
    if (BUTTON_RESUME.equals(_cmd)) {
      return btContinuer_;
    }
    else {
      return btFin_;
    }
  }

  private void btContinuerActionPerformed(ActionEvent e) {
    pnCalques_.setCalqueInteractionActif(cqEdition_);
  }

  private void btFinActionPerformed(ActionEvent e) {
    if (cqEdition_!=null)
      cqEdition_.endEdition();
  }

  public JComponent getComponent() {
    return this;
  }

  public boolean setPalettePanelTarget(Object _target) {
    return false;
  }

  public void paletteDeactivated() {
  }

  public void doAfterDisplay() {
  }
}
