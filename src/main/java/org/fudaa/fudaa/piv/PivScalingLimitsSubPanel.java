package org.fudaa.fudaa.piv;

import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JPanel;

import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;
import org.fudaa.fudaa.piv.utils.PivUtils;

/**
 * Un panneau de saisie des param�tres de scaling.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivScalingLimitsSubPanel extends JPanel {
  PivImplementation impl_;
  PivScalingParamPanel pnParams_;

  /**
   * Constructeur.
   */
  public PivScalingLimitsSubPanel(PivImplementation _impl, PivScalingParamPanel _pnParams) {
    impl_=_impl;
    pnParams_=_pnParams;
    initComponents();
    customize();

    PivOrthoParameters params=impl_.getCurrentProject().getOrthoParameters();
    if (params==null)
      params=new PivOrthoParameters();
    setParams(params);
  }

  private void customize() {
    lbXmin.setText(PivResource.getS("Xmin: "));
    lbYmin.setText(PivResource.getS("Ymin: "));
    lbXmax.setText(PivResource.getS("Xmax: "));
    lbYmax.setText(PivResource.getS("Ymax: "));
    pnPositions.setBorder(javax.swing.BorderFactory.createTitledBorder(PivResource.getS("Position des coins (m)")));
    
    cbDefaut.setText(PivResource.getS("Valeurs automatiques"));
    cbDefaut.addItemListener(new ItemListener() {
      
      @Override
      public void itemStateChanged(ItemEvent e) {
        setAutomaticLimits(cbDefaut.isSelected());
      }
    });
    
    lbNivEau.setText(PivResource.getS("Niveau d'eau") + " (m):");
  }
  
  /**
   * Met a jour le panneau des limites quand il est rendu visible.
   */
  public void updatePanel() {
    setAutomaticLimits(cbDefaut.isSelected());
  }
  
  /**
   * Definit les limites depuis les valeurs par defaut de resolution, de transformation.
   * @param _b True : Automatique, False : Manuel.
   */
  private void setAutomaticLimits(boolean _b) {
    if (_b) {
      double[] autoLimits = getComputedAutomaticLimits();
      tfXmin.setText(PivUtils.DECIMAL_FORMATTER.format(autoLimits[0]));
      tfXmax.setText(PivUtils.DECIMAL_FORMATTER.format(autoLimits[1]));
      tfYmin.setText(PivUtils.DECIMAL_FORMATTER.format(autoLimits[2]));
      tfYmax.setText(PivUtils.DECIMAL_FORMATTER.format(autoLimits[3]));
    }

    tfXmin.setEnabled(!_b);
    tfXmax.setEnabled(!_b);
    tfYmin.setEnabled(!_b);
    tfYmax.setEnabled(!_b);
  }
  
  /**
   * Analyse que les donn�es correspondent � des donn�es automatiques. Le caract�re automatique n'est pas sauv� dans le projet.
   * @return True : Automatiques.
   */
  private boolean isAutomaticLimits() {
    PivOrthoParameters pars = new PivOrthoParameters();
    retrieveParams(pars);
    
    double[] autoLimits = getComputedAutomaticLimits();
    return pars.getXmin() == autoLimits[0] && pars.getXmax() == autoLimits[1] && pars.getYmin() == autoLimits[2] && pars.getYmax() == autoLimits[3];
  }
  
  /**
   * Retourne les valeurs automatiques.
   * @return { Xmin, Xmax, Ymin, Ymax }
   */
  public double[] getComputedAutomaticLimits() {
    double xmin;
    double xmax;
    double ymin;
    double ymax;
    
    if (pnParams_.getResolutionPanel().isDataValid(false) && pnParams_.getTransformationPanel().isDataValid(false)) {
      PivScalingResolutionSubPanel pnResolution = pnParams_.getResolutionPanel();
      PivScalingTransformationSubPanel pnTransf = pnParams_.getTransformationPanel();

      double resolution = pnResolution.getResolution();
      Dimension d = impl_.getCurrentProject().getSrcImageSize();
      GrPoint[] realPoints = pnTransf.getRealPoints();
      GrPoint[] imgPoints = pnTransf.getImgPoints();
      int imin = 0;
      int jmin = 0;

      // Pas de transformation
      if (imgPoints.length == 0) {
        xmin = 0;
        ymin = 0;
        xmax = resolution * d.getWidth();
        ymax = resolution * d.getHeight();
      }

      // Translation / rotation
      else {
        GrPoint p1final = new GrPoint(realPoints[0].x_ / resolution, realPoints[0].y_ / resolution, 0);

        // Calcul de l'angle de rotation depuis les points saisis.
        GrVecteur v1 = new GrVecteur(imgPoints[1].x_ - imgPoints[0].x_, imgPoints[1].y_ - imgPoints[0].y_, 0);
        GrVecteur v2 = new GrVecteur(realPoints[1].x_ - realPoints[0].x_, realPoints[1].y_ - realPoints[0].y_, 0);
        double rotation = v1.getAngleXY(v2);

        // Le premier point saisi dans le repere apr�s rotation
        GrMorphisme toRotation = GrMorphisme.rotationZ(rotation);
        GrPoint p1rotation = imgPoints[0].applique(toRotation);

        // La transformation de l'espace de depart vers l'espace final (le 2ieme point
        // n'est pas forcement la ou c'�tait demand�).
        GrMorphisme toFinal = GrMorphisme.rotationZ(rotation).composition(
            GrMorphisme.translation(p1final.x_ - p1rotation.x_ - imin, p1final.y_ - p1rotation.y_ - jmin, 0));

        GrPoint ptXMinYMin = new GrPoint();
        GrPoint ptXMinYMax = new GrPoint(0, d.getHeight(), 0);
        GrPoint ptXMaxYMin = new GrPoint(d.getWidth(), 0, 0);
        GrPoint ptXMaxYMax = new GrPoint(d.getWidth(), d.getHeight(), 0);

        ptXMinYMin.autoApplique(toFinal);
        ptXMinYMax.autoApplique(toFinal);
        ptXMaxYMin.autoApplique(toFinal);
        ptXMaxYMax.autoApplique(toFinal);

        xmin = Math.min(ptXMinYMin.x_, ptXMinYMax.x_);
        xmax = Math.max(ptXMaxYMin.x_, ptXMaxYMax.x_);
        ymin = Math.min(ptXMinYMin.y_, ptXMaxYMin.y_);
        ymax = Math.max(ptXMinYMax.y_, ptXMaxYMax.y_);
      }
    }
    
    else {
      xmin = 0;
      xmax = 0;
      ymin = 0;
      ymax = 0;
    }
    
    return new double[] { xmin, xmax, ymin, ymax }; 
  }

  /**
   * Rempli le panneau depuis les donn�es du projet.
   * @param _params L'objet m�tier pour l'orthorectification.
   */
  public void setParams(PivOrthoParameters _params) {
    tfXmin.setText("" + _params.getXmin());
    tfXmax.setText("" + _params.getXmax());
    tfYmin.setText("" + _params.getYmin());
    tfYmax.setText("" + _params.getYmax());
    tfNivEau.setText("" + (_params.getWaterElevation()==null ? "":_params.getWaterElevation()));
    
    cbDefaut.setSelected(isAutomaticLimits());
  }

  /**
   * Met a jour les parametres d'ortho rectification donn�s.
   * @param _params Les parametres d'ortho rectification.
   */
  public void retrieveParams(PivOrthoParameters _params) {
    try {
      _params.setXmin(Double.parseDouble(tfXmin.getText().trim()));
      _params.setYmin(Double.parseDouble(tfYmin.getText().trim()));
      _params.setXmax(Double.parseDouble(tfXmax.getText().trim()));
      _params.setYmax(Double.parseDouble(tfYmax.getText().trim()));
      _params.setWaterElevation(getWaterLevel());
    }
    catch (NumberFormatException _exc) {
    }
  }
  
  public Double getWaterLevel() {
    try {
      return Double.parseDouble(tfNivEau.getText().trim());
    }
    catch (NumberFormatException _exc) {}
    
    return null;
  }

  public boolean isDataValid(boolean _withMes) {
    // Quand on teste que les datas sont valides, on remet a jour les valeurs si elles sont en automatique.
    setAutomaticLimits(cbDefaut.isSelected());
    
    boolean bok=false;
    double xmin=0;
    double xmax=0;
    double ymin=0;
    double ymax=0;

    try {
      bok=false;
      xmin=Double.parseDouble(tfXmin.getText().trim());
      bok=true;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      if (_withMes)
        setErrorText(PivResource.getS("Xmin doit �tre un r�el"));
      return false;
    }

    try {
      bok=false;
      xmax=Double.parseDouble(tfXmax.getText().trim());
      bok=true;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      if (_withMes)
        setErrorText(PivResource.getS("Xmax doit �tre un r�el"));
      return false;
    }

    if (xmin>=xmax) {
      if (_withMes)
        setErrorText(PivResource.getS("Xmax doit �tre strictement sup�rieur � Xmin"));
      return false;
    }

    try {
      bok=false;
      ymin=Double.parseDouble(tfYmin.getText().trim());
      bok=true;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      if (_withMes)
        setErrorText(PivResource.getS("Ymin doit �tre un r�el"));
      return false;
    }

    try {
      bok=false;
      ymax=Double.parseDouble(tfYmax.getText().trim());
      bok=true;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      if (_withMes)
        setErrorText(PivResource.getS("Ymax doit �tre un r�el"));
      return false;
    }

    if (ymin>=ymax) {
      if (_withMes)
        setErrorText(PivResource.getS("Ymax doit �tre strictement sup�rieur � Ymin"));
      return false;
    }

    if (getWaterLevel() == null) {
      if (_withMes)
        setErrorText(PivResource.getS("Le niveau d'eau doit �tre un r�el"));
      return false;
    }

    return true;
  }
  
  private void setErrorText(String _error) {
    if (_error.isEmpty()) {
      pnParams_.setErrorText("");
    }
    else {
      pnParams_.setErrorText(PivResource.getS("Autres")+" : "+_error);
    }
  }

  /** This method is called from within the constructor to
   * initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is
   * always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnPositions = new javax.swing.JPanel();
        lbXmin = new javax.swing.JLabel();
        tfXmin = new javax.swing.JTextField();
        lbXmax = new javax.swing.JLabel();
        tfXmax = new javax.swing.JTextField();
        lbYmax = new javax.swing.JLabel();
        tfYmax = new javax.swing.JTextField();
        lbYmin = new javax.swing.JLabel();
        tfYmin = new javax.swing.JTextField();
        cbDefaut = new javax.swing.JCheckBox();
        lbNivEau = new javax.swing.JLabel();
        tfNivEau = new javax.swing.JTextField();

        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        pnPositions.setBorder(javax.swing.BorderFactory.createTitledBorder("Position des coins (m)"));

        lbXmin.setText("Xmin:");

        tfXmin.setPreferredSize(new Dimension(50,tfXmin.getPreferredSize().height));

        lbXmax.setText("Xmax:");

        lbYmax.setText("Ymax:");

        lbYmin.setText("Ymin:");

        cbDefaut.setText("Valeurs automatiques");

        javax.swing.GroupLayout pnPositionsLayout = new javax.swing.GroupLayout(pnPositions);
        pnPositions.setLayout(pnPositionsLayout);
        pnPositionsLayout.setHorizontalGroup(
            pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnPositionsLayout.createSequentialGroup()
                .addGroup(pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnPositionsLayout.createSequentialGroup()
                        .addGroup(pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbXmin)
                            .addComponent(lbYmin))
                        .addGap(8, 8, 8)
                        .addGroup(pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfXmin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(tfYmin))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lbYmax)
                            .addComponent(lbXmax))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfXmax)
                            .addComponent(tfYmax)))
                    .addGroup(pnPositionsLayout.createSequentialGroup()
                        .addComponent(cbDefaut)
                        .addGap(0, 147, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnPositionsLayout.setVerticalGroup(
            pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnPositionsLayout.createSequentialGroup()
                .addComponent(cbDefaut)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbXmin)
                    .addComponent(tfXmin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbXmax)
                    .addComponent(tfXmax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbYmax)
                    .addComponent(tfYmax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbYmin)
                    .addComponent(tfYmin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        lbNivEau.setText("Niveau d'eau (m):");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnPositions, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbNivEau, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tfNivEau)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(pnPositions, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbNivEau, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tfNivEau, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox cbDefaut;
    private javax.swing.JLabel lbNivEau;
    private javax.swing.JLabel lbXmax;
    private javax.swing.JLabel lbXmin;
    private javax.swing.JLabel lbYmax;
    private javax.swing.JLabel lbYmin;
    private javax.swing.JPanel pnPositions;
    private javax.swing.JTextField tfNivEau;
    private javax.swing.JTextField tfXmax;
    private javax.swing.JTextField tfXmin;
    private javax.swing.JTextField tfYmax;
    private javax.swing.JTextField tfYmin;
    // End of variables declaration//GEN-END:variables
}
