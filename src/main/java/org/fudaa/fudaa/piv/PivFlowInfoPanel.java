/*
 * @creation 8 sept. 06
 * @modification $Date: 2009-06-03 15:10:45 +0200 (mer., 03 juin 2009) $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.text.DecimalFormat;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.piv.metier.PivFlowResults;
import org.fudaa.fudaa.piv.metier.PivGlobalFlowResults;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;
import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;
import org.fudaa.fudaa.piv.metier.PivTransect;


/**
 * Un panneau d'information sur le calcul de d�bit effectu�.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivFlowInfoPanel extends CtuluDialogPanel {
  private PivFlowResults[] res_;
  private PivTransect[] transects_;
  private double meanDisch_;
  private double meanArea_;
  private double meanMeanV_;
  private double meanMeasuredDisch_;
  private double meanCoef_;

  /**
   * Une classe pour le modele de la table affichant les r�sultats.
   */
  private class DischargeTableModel implements TableModel {

    public int getRowCount() {
      return res_.length+1;
    }

    public int getColumnCount() {
      return 9;
    }

    public String getColumnName(int columnIndex) {
      switch (columnIndex) {
        default:
        case 0:
          return PivResource.getS("N�");
        case 1:
          return PivResource.getS("<html>Coef. vitesse</html>");
        case 2:
          return PivResource.getS("<html>Q total (m�/s)</html>");
        case 3:
          return PivResource.getS("<html>Ecart (%)</html>");
        case 4:
          return PivResource.getS("<html>Aire mouill�e (m�)</html>");
        case 5:
          return PivResource.getS("<html>Ecart (%)</html>");
        case 6:
          return PivResource.getS("<html>Vit. moy. (m/s)</html>");
        case 7:
          return PivResource.getS("<html>Ecart (%)</html>");
//        case 8:
//          return PivResource.getS("<html>Q mesur� (m�/s)</html>");
        case 8:
          return PivResource.getS("<html>Q mesur� / Q total (%)</html>");
      }
    }

    public Class<?> getColumnClass(int columnIndex) {
      return columnIndex==0 ? String.class:Double.class;
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return false;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
      double val;
      
      switch (columnIndex) {
        default:
        case 0:
          return rowIndex==res_.length? PivResource.getS("Moyenne"):""+(rowIndex+1);
        case 1:
          return rowIndex==res_.length? meanCoef_:res_[rowIndex].getValue(-1,ResultType.COMPUTED_VEL_COEFFICIENT);
        case 2:
          return rowIndex==res_.length? meanDisch_:res_[rowIndex].getDischarge();
        case 3:
          val=rowIndex==res_.length? meanDisch_:res_[rowIndex].getDischarge();
          return (val-meanDisch_)/meanDisch_*100;
        case 4:
          return rowIndex==res_.length? meanArea_:res_[rowIndex].getWettedArea();
        case 5:
          val=rowIndex==res_.length? meanArea_:res_[rowIndex].getWettedArea();
          return (val-meanArea_)/meanArea_*100;
        case 6:
          return rowIndex==res_.length? meanMeanV_:res_[rowIndex].getMeanVelocity();
        case 7:
          val=rowIndex==res_.length? meanMeanV_:res_[rowIndex].getMeanVelocity();
          return (val-meanMeanV_)/meanMeanV_*100;
//        case 8:
//          return rowIndex==res_.length? meanMeasuredDisch_:res_[rowIndex].getValue(-1,ResultType.MEASURED_DISCHARGE);
        case 8:
          val=rowIndex==res_.length? meanMeasuredDisch_/meanDisch_:res_[rowIndex].getValue(-1,ResultType.MEASURED_DISCHARGE)/res_[rowIndex].getDischarge();
          return val*100;
      }
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {}
    public void addTableModelListener(TableModelListener l) {}
    public void removeTableModelListener(TableModelListener l) {}
  }

  /**
   * Une classe pour une repr�sentation des doubles.
   */
  private class DischargeCellRenderer extends DefaultTableCellRenderer {
    DecimalFormat fmt = CtuluLib.getDecimalFormat(3);
    DecimalFormat fmtEcart = CtuluLib.getNoEffectDecimalFormat();
    DecimalFormat fmtQRatio = CtuluLib.getNoEffectDecimalFormat();

    public DischargeCellRenderer() {
      this.setHorizontalAlignment(SwingConstants.RIGHT);
      fmt.setMaximumFractionDigits(3);
      fmt.setMinimumFractionDigits(3);
      fmtEcart.setMaximumFractionDigits(1);
      fmtEcart.setMinimumFractionDigits(1);
      fmtEcart.setPositivePrefix("+");
      fmtQRatio.setMaximumFractionDigits(1);
      fmtQRatio.setMinimumFractionDigits(1);
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
      
      if (value instanceof Double) {
        if (((Double) value).isNaN()) {
          this.setText(value.toString());
        }
        else if (column==9)
          this.setText(fmtQRatio.format((Double)value));
        else if (column==3 || column==5 || column==7)
          this.setText(fmtEcart.format((Double)value));
        else
          this.setText(fmt.format((Double)value));
      }
      return this;
    }
  }
  
  /**
   * Construction du panneau avec les r�sutalts de d�bit
   * @param _res Les r�sultats.
   */
  public PivFlowInfoPanel(PivFlowResults[] _res, PivTransect[] _transects, PivOrthoParameters _orthoParams) {
    setLayout(new BorderLayout(7,7));
    JTable tbResults=new JTable();
    tbResults.setModel(new DischargeTableModel());
    tbResults.setDefaultRenderer(Double.class,new DischargeCellRenderer());
    tbResults.getColumnModel().getColumn(0).setMaxWidth(60);
    tbResults.getColumnModel().getColumn(3).setMaxWidth(70);
    tbResults.getColumnModel().getColumn(5).setMaxWidth(70);
    tbResults.getColumnModel().getColumn(7).setMaxWidth(70);
    // Customization pour avoir le titre sur 2 lignes.
    tbResults.getTableHeader().setPreferredSize(
        new Dimension(tbResults.getColumnModel().getTotalColumnWidth(), 40));
    ((JLabel)tbResults.getTableHeader().getDefaultRenderer()).setVerticalAlignment(JLabel.TOP);
    
    JScrollPane spResults=new JScrollPane();
    spResults.getViewport().add(tbResults);
    spResults.setPreferredSize(new Dimension(850,200));
    
    JPanel pnResults = new JPanel();
    pnResults.setLayout(new BorderLayout(5, 5));
    pnResults.add(new JLabel(PivResource.getS("Liste des transects")),BorderLayout.NORTH);
    pnResults.add(spResults, BorderLayout.CENTER);
    
    JTextField tfWaterLevel = new JTextField(15);
    tfWaterLevel.setEditable(false);
    tfWaterLevel.setText(""+_orthoParams.getWaterElevation());
    
    JPanel pnLevel = new JPanel();
    pnLevel.setLayout(new FlowLayout(FlowLayout.LEFT));
    pnLevel.add(new JLabel(PivResource.getS("Niveau d'eau")+" (m):"));
    pnLevel.add(tfWaterLevel);
    
    add(pnLevel, BorderLayout.NORTH);
    add(pnResults, BorderLayout.CENTER);
    
    res_=_res;
    transects_ = _transects;
    computeMoyennes();
  }
  
  protected void computeMoyennes() {
    PivGlobalFlowResults globRes=new PivGlobalFlowResults(res_);
    meanDisch_=globRes.getAverageScalarResult(ResultType.FULL_DISCHARGE);
    meanArea_=globRes.getAverageScalarResult(ResultType.WETTED_AREA);
    meanMeanV_=globRes.getAverageScalarResult(ResultType.MEAN_VELOCITY);
    meanMeasuredDisch_=globRes.getAverageScalarResult(ResultType.MEASURED_DISCHARGE);
    meanCoef_ = globRes.getAverageScalarResult(ResultType.COMPUTED_VEL_COEFFICIENT);
    
//    meanCoef_ = 0;
//    for (PivTransect trans : transects_) {
//      meanCoef_+=trans.getParams().getSurfaceCoef();
//    }
//    meanCoef_/=transects_.length;
  }

  @Override
  public boolean isDataValid() {
    return true;
  }
}