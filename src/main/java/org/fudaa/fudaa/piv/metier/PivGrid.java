package org.fudaa.fudaa.piv.metier;

import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.locationtech.jts.geom.Coordinate;

/**
 * Les points de grille pour le calcul des vitesses. LA grille est d�finie par des
 * points en espace r�el.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivGrid extends GISZoneCollectionPoint {

  /**
   * Calcule une grille a partir d'un polygone a 4 cotes et d'un nombre de points
   * sur X et Y. X est consid�r� entre les points 1-2/3-4, Y entre les points
   * 2-3/4-1.
   *
   * @param _pg Le polygone a 4 cotes.
   * @param nbXPoints Le nombre de points sur X.
   * @param nbYPoints Le nombre de points sur Y.
   */
  public static PivGrid computeFromShape(GrPolygone _pg, int nbXPoints, int nbYPoints) {
    PivGrid grid=new PivGrid();

    GrPoint pt1=_pg.sommet(0);
    GrPoint pt2=_pg.sommet(1);
    GrPoint pt3=_pg.sommet(2);
    GrPoint pt4=_pg.sommet(3);

    for (int iy = 0; iy < nbYPoints; iy++) {
      double x14=pt1.x_+iy*(pt4.x_-pt1.x_)/(nbYPoints-1);
      double y14=pt1.y_+iy*(pt4.y_-pt1.y_)/(nbYPoints-1);
      double x23=pt2.x_+iy*(pt3.x_-pt2.x_)/(nbYPoints-1);
      double y23=pt2.y_+iy*(pt3.y_-pt2.y_)/(nbYPoints-1);
      for (int ix = 0; ix < nbXPoints; ix++) {
        double x = x14 + ix * (x23 - x14) / (nbXPoints-1);
        double y = y14 + ix * (y23 - y14) / (nbXPoints-1);

        grid.add(x, y, 0);
      }
    }

    return grid;
  }

  /**
   * Concatene la grille donn�e en param�tre � cette grille.
   * @param _grid La grille � concatener.
   */
  public void merge(PivGrid _grid) {
    if (_grid==null) return;
    
    for (int i=0; i<_grid.getNbGeometries(); i++) {
     this.addGeometry(_grid.getGeometry(i), null, null);
    }
  }
  
  /**
   * Modifie l'echelle de la grille. 
   * Sert en particulier en cas de changement de resolution, pour conserver les coordonn�es m�triques.
   * @param _scale
   */
  public void changeScale(double _scale) {
    for (int i=0; i<this.getNbGeometries(); i++) {
      Coordinate coords=this.get(i).getCoordinate();
      GISPoint pt = new GISPoint(coords.x*_scale, coords.y*_scale, coords.z*_scale);
      this.setGeometry(i, pt, null);
    }
  }
}
