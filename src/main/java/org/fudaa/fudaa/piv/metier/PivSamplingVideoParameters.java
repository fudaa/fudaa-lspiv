package org.fudaa.fudaa.piv.metier;

import java.awt.Dimension;
import java.io.File;

/**
 * Contient les parametres d'échantillonnage de la video poour le sauvegarder.
 * @author Bertrand Marchand
 */
public class PivSamplingVideoParameters {
  /** Le nom de la video */
  private File videoName = null;
  /** La dimension de la vidéo echantillonée */
  private Dimension samplingSize;
  /** La periode de conservation des images */
  private int samplingPeriod;
  /** L'instant de début de l'échantillonnage */
  private double samplingBeginTime;
  /** L'instant de fin de l'échantillonnage */
  private double samplingEndTime;
  
  public File getVideoFile() {
    return videoName;
  }
  
  public void setVideoFile(File videoName) {
    this.videoName = videoName;
  }

  public Dimension getSamplingSize() {
    return samplingSize;
  }

  public void setSamplingSize(Dimension samplingSize) {
    this.samplingSize = samplingSize;
  }

  public int getSamplingPeriod() {
    return samplingPeriod;
  }

  public void setSamplingPeriod(int samplingPeriod) {
    this.samplingPeriod = samplingPeriod;
  }

  public double getSamplingBeginTime() {
    return samplingBeginTime;
  }

  public void setSamplingBeginTime(double samplingBeginTime) {
    this.samplingBeginTime = samplingBeginTime;
  }

  public double getSamplingEndTime() {
    return samplingEndTime;
  }

  public void setSamplingEndTime(double samplingEndTime) {
    this.samplingEndTime = samplingEndTime;
  }
}
