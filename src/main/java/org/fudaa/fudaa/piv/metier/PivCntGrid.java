package org.fudaa.fudaa.piv.metier;

import org.fudaa.ebli.geometrie.GrPolygone;

/**
 * Le contour de grille et ses param�tres.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivCntGrid {
  /** Le nombre de points suivant 1-2 et 3-4 */
  protected int nbXPoints;
  /** Le nombre de points suivant 2-3 et 4-1 */
  protected int nbYPoints;
  /** Le contour de la grille */
  protected GrPolygone contour;

  public PivCntGrid() {
  }

  /**
   * Retourne le nombre de points suivant 1-2 et 3-4
   * @return Le nombre de points.
   */
  public int getNbXPoints() {
    return nbXPoints;
  }

  /**
   * Definit le nombre de points de grille suivant la direction X
   * @param nbXPoints Le nombre de points suivant 1-2 et 3-4
   */
  public void setNbXPoints(int nbXPoints) {
    this.nbXPoints = nbXPoints;
  }

  /**
   * Retourne le nombre de points suivant 2-3 et 4-1
   * @return Le nombre de points
   */
  public int getNbYPoints() {
    return nbYPoints;
  }

  /**
   * Definit le nombre de points de grille suivant la direction Y
   * @param nbYPoints Le nombre de points suivant 2-3 et 4-1
   */
  public void setNbYPoints(int nbYPoints) {
    this.nbYPoints = nbYPoints;
  }

  /**
   * Retourne la g�om�trie du contour.
   * @return Le contour
   */
  public GrPolygone getContour() {
    return contour;
  }

  /**
   * Definit la g�om�trie du contour de grille
   * @param contour Le contour
   */
  public void setContour(GrPolygone contour) {
    this.contour = contour;
  }
}
