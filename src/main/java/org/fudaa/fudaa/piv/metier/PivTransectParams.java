package org.fudaa.fudaa.piv.metier;

import org.fudaa.fudaa.piv.utils.PivValueEditorGlobalCoefVelocity;

/**
 * Les parametres pour le calcul de debit suivant un transect.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivTransectParams implements Cloneable {
  /** Coefficient de surface. null signifie que la valeur n'a pas �t� donn�e. */
  protected Double surfaceCoef=0.85;
  /** Rayon de recherche RX des vitesses autour du point bathy. null signifie que la valeur n'a pas �t� donn�e. */
  protected Double radiusX=10.;
  /** Rayon de recherche RY des vitesses autour du point bathy. null signifie que la valeur n'a pas �t� donn�e. */
  protected Double radiusY=10.;
  /** Distance au dela de laquelle on extrapole les vitesses. null signifie que la valeur n'a pas �t� donn�e. */
  protected Double interpolationStep=1000000.;
  /** Nombre maxi de points pour le reconditionnement. 0 : Pas de reconditionnement */
  protected Integer maxPoints = 0;

  public PivTransectParams() {
  }
  
  /**
   * Constructeur copie.
   * @param _o
   */
  public PivTransectParams(PivTransectParams _o) {
    if (_o == null) {
      return;
    }

    surfaceCoef=_o.surfaceCoef;
    radiusX=_o.radiusX;
    radiusY=_o.radiusY;
    interpolationStep=_o.interpolationStep;
    maxPoints = _o.maxPoints;
  }

  /**
   * @return the surfaceCoef
   */
  public Double getSurfaceCoef() {
    return surfaceCoef;
  }

  /**
   * @return True si le coefficient de surface est d�fini globalement.
   */
  public boolean isSetSurfaceCoef() {
    return surfaceCoef != PivValueEditorGlobalCoefVelocity.UNSET_VALUE;
  }
  
  /**
   * @param surfaceCoef the surfaceCoef to set
   */
  public void setSurfaceCoef(Double surfaceCoef) {
    this.surfaceCoef = surfaceCoef;
  }

  /**
   * @return the dmax
   */
  public Double getRadiusX() {
    return radiusX;
  }

  /**
   * @param rad the dmax to set
   */
  public void setRadiusX(Double rad) {
    this.radiusX = rad;
  }

  /**
   * @return the dmax
   */
  public Double getRadiusY() {
    return radiusY;
  }

  /**
   * @param rad the dmax to set
   */
  public void setRadiusY(Double rad) {
    this.radiusY = rad;
  }

  public Integer getMaxPoints() {
    return maxPoints;
  }

  public void setMaxPoints(Integer maxPoints) {
    this.maxPoints = maxPoints;
  }

  /**
   * @return Le pas d'interpolation
   */
  public Double getInterpolationStep() {
    return interpolationStep;
  }

  /**
   * @param _step Le pas d'interpolation
   */
  public void setInterpolationStep(Double _step) {
    this.interpolationStep = _step;
  }

  @Override
  public boolean equals(Object _o) {
    if (!(_o instanceof PivTransectParams)) return false;
    PivTransectParams o=(PivTransectParams)_o;

    return 
        ((o.surfaceCoef == null && surfaceCoef == null) || (surfaceCoef != null && (surfaceCoef.equals(o.surfaceCoef))) || o.surfaceCoef.equals(surfaceCoef)) &&
        ((o.radiusX == null && radiusX == null) || (radiusX != null && (radiusX.equals(o.radiusX))) || o.radiusX.equals(radiusX)) &&
        ((o.radiusY == null && radiusY == null) || (radiusY != null && (radiusY.equals(o.radiusY))) || o.radiusY.equals(radiusY)) &&
        ((o.interpolationStep == null && interpolationStep == null) || (interpolationStep != null && (interpolationStep.equals(o.interpolationStep))) || o.interpolationStep.equals(interpolationStep)) &&
        ((o.maxPoints == null && maxPoints == null) || (maxPoints != null && (maxPoints.equals(o.maxPoints))) || o.maxPoints.equals(maxPoints));
  }
}
