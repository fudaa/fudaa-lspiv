package org.fudaa.fudaa.piv.metier;

/**
 * Une interface � implementer pour notifier l'�tat courant d'un projet
 * (UI modifi�e, donn�es modifi�es, etc.).
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public interface PivProjectStateI {

  /**
   * Ajout d'un listener notifi� en cas de modification des param�tres.
   * @param _l Le listener.
   */
  void addListener(PivProjectStateListener _l);

}
