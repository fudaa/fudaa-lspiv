package org.fudaa.fudaa.piv.metier;

import java.util.Arrays;

import gnu.trove.TIntArrayList;

/**
 * Les param�tres de moyenne/m�diane pour le post calcul PIV.
 */
public class PivStatisticParameters {
  
  /** Les m�thodes de calcul */
  public enum PivStatisticCalcultationMethod {
    MEAN,
    MEDIAN;
    
    public static PivStatisticCalcultationMethod ordinal(int _i) {
      if (_i == 0)
        return MEAN;
      else if (_i == 1)
        return MEDIAN;
      
      throw new IllegalArgumentException();
    }
  }

  /** Les r�sultats instantan�s utilis�s pour le calcul. */
  protected boolean[] usedInstantResults = new boolean[0];
  /** La m�thode de calcul */
  protected PivStatisticCalcultationMethod method = PivStatisticCalcultationMethod.MEDIAN;
  /** Le champ manuel est il utilis� */
  protected boolean isManualFieldUsed = false;

  public boolean isManualFieldUsed() {
    return isManualFieldUsed;
  }

  public void setManualFieldUsed(boolean isManualFieldUsed) {
    this.isManualFieldUsed = isManualFieldUsed;
  }

  public PivStatisticParameters() {
  }

  public PivStatisticParameters(PivStatisticParameters _o) {
    if (_o == null) {
      return;
    }

    method = _o.method;
    usedInstantResults = Arrays.copyOf(_o.usedInstantResults, _o.usedInstantResults.length);
    isManualFieldUsed = _o.isManualFieldUsed;
  }

  public boolean[] getUsedInstantResults() {
    return usedInstantResults;
  }
  
  /**
   * @return Les seuls index des resultats utilis�s.
   */
  public int[] getUsedInstantResultsIndexes() {
    TIntArrayList indexes = new TIntArrayList();
    for (int i=0; i<this.usedInstantResults.length; i++) {
      if (usedInstantResults[i]) {
        indexes.add(i);
      }
    }
    
    return indexes.toNativeArray();
  }
  
  public void setUsedInstantResults(boolean[] usedInstantResults) {
    this.usedInstantResults = usedInstantResults;
  }

  public PivStatisticCalcultationMethod getMethod() {
    return method;
  }

  public void setMethod(PivStatisticCalcultationMethod method) {
    this.method = method;
  }

  @Override
  public boolean equals(Object _o) {
    if (!(_o instanceof PivStatisticParameters))
      return false;
    PivStatisticParameters o = (PivStatisticParameters) _o;

    return method == o.method && isManualFieldUsed == o.isManualFieldUsed && Arrays.equals(usedInstantResults, o.usedInstantResults);
  }
}