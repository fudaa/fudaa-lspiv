package org.fudaa.fudaa.piv.metier;

import java.util.List;

/**
 * Une concat�nation de r�sultats. Les r�sultats doivent tous avoir le m�me nombre de natures de r�sultats. L'ordre des r�sultats dans la liste
 * reflete l'ordre dans lequel les valeurs seront restitu�es.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivResults.java 8097 2012-12-04 16:27:08Z bmarchan $
 */
public class PivConcatenatedResults implements PivResultsI {

  /** La liste des r�sultats constituants */
  private List<PivResultsI> res_;

  /**
   * Constructeur.
   * 
   * @param _res Les resultats � concat�ner. Les points doivent �tre diff�rents, les natures des r�sultats doit �tre les m�mes. Aucun controle n'est
   *             fait.
   */
  public PivConcatenatedResults(List<PivResultsI> _res) {
    res_ = _res;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.fudaa.fudaa.piv.metier.PivResultsI#hasResult(org.fudaa.fudaa.piv.metier.PivResults.TYPE)
   */
  @Override
  public boolean hasResult(ResultType _tpRes) {
    if (res_.get(0) == null) {
      return false;
    }

    return res_.get(0).hasResult(_tpRes);
  }

  @Override
  public ResultType[] getResults() {
    if (res_.get(0) == null) {
      return new ResultType[0];
    }

    return res_.get(0).getResults();
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.fudaa.fudaa.piv.metier.PivResultsI#getNbPoints()
   */
  @Override
  public int getNbPoints() {
    int nb = 0;
    for (PivResultsI res : res_) {
      nb += res.getNbPoints();
    }
    return nb;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.fudaa.fudaa.piv.metier.PivResultsI#getX(int)
   */
  @Override
  public double getX(int _ind) {
    int icpt = 0;
    for (PivResultsI res : res_) {
      if (_ind - icpt < res.getNbPoints()) {
        return res.getX(_ind - icpt);
      }
      icpt += res.getNbPoints();
    }

    return 0;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.fudaa.fudaa.piv.metier.PivResultsI#getY(int)
   */
  @Override
  public double getY(int _ind) {
    int icpt = 0;
    for (PivResultsI res : res_) {
      if (_ind - icpt < res.getNbPoints()) {
        return res.getY(_ind - icpt);
      }
      icpt += res.getNbPoints();
    }

    return 0;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.fudaa.fudaa.piv.metier.PivResultsI#getVal(int, org.fudaa.fudaa.piv.metier.PivResults.TYPE)
   */
  @Override
  public double getValue(int _ind, ResultType _tpRes) {
    int icpt = 0;
    for (PivResultsI res : res_) {
      if (_ind - icpt < res.getNbPoints()) {
        return res.getValue(_ind - icpt, _tpRes);
      }
      icpt += res.getNbPoints();
    }

    return 0;
  }

  @Override
  public double[] getValues(ResultType _tpRes) {
    double[] values;
    if (_tpRes.isVector()) {
      values = new double[getNbPoints()];

      int icpt = 0;
      for (PivResultsI res : res_) {
        System.arraycopy(res.getValues(_tpRes), 0, values, icpt, res.getNbPoints());
        icpt += res.getNbPoints();
      }
    }

    // Option qui peut se discuter : Si le type n'est pas vectoriel, on retourne la moyenne pour tous les r�sultats concat�n�s
    else {
      double sum = 0;
      for (PivResultsI res : res_) {
        sum += res.getValues(_tpRes)[0];
      }

      values = new double[] { sum / getNbPoints() };
    }

    return values;
  }

  @Override
  public void setValue(int _ind, ResultType _tpRes, double _val) {
    int icpt = 0;
    for (PivResultsI res : res_) {
      if (_ind - icpt < res.getNbPoints()) {
        res.setValue(_ind - icpt, _tpRes, _val);
      }
      icpt += res.getNbPoints();
    }
  }
}
