package org.fudaa.fudaa.piv.metier;

import org.fudaa.ebli.geometrie.GrPoint;

/**
 * Les parametres pour le calcul des vitesses par PIV.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivComputeParameters {
  
  // Si une valeur n'a pas encore �t� renseign�e, est elle null.
  
  /** True : Le module de calcul est actif. */
  protected boolean isActive = true;
  /** La taille en pixels de l'aire d'interrogation IA */
  protected Integer iaSize;
  /** Le min en pixels suivant i de l'aire de recherche */
  protected Integer sim;
  /** Le max en pixels suivant i de l'aire de recherche */
  protected Integer sip;
  /** Le min en pixels suivant j de l'aire de recherche */
  protected Integer sjm;
  /** Le max en pixels suivant j de l'aire de recherche */
  protected Integer sjp;
  /** L'intervale de temps entre 2 images en secondes */
  protected Double timeInterval;
  /** La frequence de sous echantillonnage des images transform�es */
  protected Integer underSamplingPeriod = 1;
  /** La position suivant j et i du centre de l'aire. */
  protected GrPoint ptCenter;
  /** Les filtres */
  protected PivFilterParameters filters = new PivFilterParameters();

  public PivComputeParameters() {
  }
  
  /**
   * Constructeur copie.
   * @param _o
   */
  public PivComputeParameters(PivComputeParameters _o) {
    if (_o == null) {
      return;
    }
    
    isActive = _o.isActive;
    iaSize = _o.iaSize;
    sim = _o.sim;
    sip = _o.sip;
    sjm = _o.sjm;
    sjp = _o.sjp;
    timeInterval = _o.timeInterval;
    underSamplingPeriod = _o.underSamplingPeriod;
    ptCenter = _o.ptCenter;
    
    filters = new PivFilterParameters(_o.filters);
  }

  /**
   * @return the iaSize
   */
  public Integer getIASize() {
    return iaSize;
  }

  /**
   * @param iaSize the iaSize to set
   */
  public void setIASize(Integer iaSize) {
    this.iaSize = iaSize;
  }

  /**
   * @return the sim
   */
  public Integer getSim() {
    return sim;
  }

  /**
   * @param sim the sim to set
   */
  public void setSim(Integer sim) {
    this.sim = sim;
  }

  /**
   * @return the sip
   */
  public Integer getSip() {
    return sip;
  }

  /**
   * @param sip the sip to set
   */
  public void setSip(Integer sip) {
    this.sip = sip;
  }

  /**
   * @return the sjm
   */
  public Integer getSjm() {
    return sjm;
  }

  /**
   * @param sjm the sjm to set
   */
  public void setSjm(Integer sjm) {
    this.sjm = sjm;
  }

  /**
   * @return the sjp
   */
  public Integer getSjp() {
    return sjp;
  }

  /**
   * @param sjp the sjp to set
   */
  public void setSjp(Integer sjp) {
    this.sjp = sjp;
  }

  /**
   * @return the timeInterval
   */
  public Double getTimeInterval() {
    return timeInterval;
  }

  /**
   * @param timeInterval the timeInterval to set
   */
  public void setTimeInterval(Double timeInterval) {
    this.timeInterval = timeInterval;
  }

  /**
   * Definit la position de IA suivant J et I
   */
  public void setIACenterPosition(GrPoint _pt) {
    ptCenter=_pt;
  }
  
  /**
   * @return La position du centre de IA/SA. Peut �tre nulle, dans ce cas,
   * l'IA est mise au centre de l'image.
   */
  public GrPoint getIACenterPosition() {
    return ptCenter;
  }
  
  public Integer getUnderSamplingPeriod() {
    return underSamplingPeriod;
  }

  /**
   * D�finit la p�riode de sous echantillonnage.
   * @param _samplingPeriod La p�riode d'�chantillonnage
   */
  public void setUnderSamplingPeriod(Integer _samplingPeriod) {
    this.underSamplingPeriod = _samplingPeriod;
  }

  /**
   * Modifie la p�riode de sous echantillonnage. On conserve les coordonn�es m�triques.
   * @param _samplingPeriod La p�riode d'�chantillonnage
   */
  public void changeUnderSamplingPeriod(Integer _samplingPeriod) {
    if (underSamplingPeriod != _samplingPeriod && underSamplingPeriod != null && _samplingPeriod != null) {
      double ratio=(float)_samplingPeriod/(float)underSamplingPeriod;

      if (sim != null) {
        sim=(int) (sim * ratio + 0.5);
      }
      if (sip != null) {
        sip=(int) (sip * ratio + 0.5);
      }
      if (sjm != null) {
        sjm=(int) (sjm * ratio + 0.5);
      }
      if (sjp != null) {
        sjp=(int) (sjp * ratio + 0.5);
      }
    }
    
    this.underSamplingPeriod = _samplingPeriod;
  }

  public PivFilterParameters getFilters() {
    return filters;
  }

  public void setFilters(PivFilterParameters filters) {
    this.filters = filters;
  }
  
  /**
   * @return true : Le calcul par PIV est actif. false : il est ignor�.
   */
  public boolean isActive() {
    return isActive;
  }

  /**
   * Definit si le calcul par PIV est actif.
   * @param isActive true : Le calcul par PIV est actif. false : il est ignor�.
   */
  public void setActive(boolean isActive) {
    this.isActive = isActive;
  }

  /**
   * @return True si les param�tres sont tous remplis pour le calcul par PIV.
   */
  public boolean isFilledForComputing() {
    return iaSize != null && sim != null && sip != null && sjm != null && sjp != null;
  }
  
  /**
   * Les parametres pour le calcul sont tous mis � null.
   */
  public void clearComputingParameters() {
    iaSize = null;
    sim = null;
    sip = null;
    sjm = null;
    sjp = null;
  }
  
  /**
   * Modifie l'echelle des parametres. 
   * Sert en particulier en cas de changement de resolution, pour conserver les coordonn�es m�triques.
   * @param _scale
   */
  public void changeScale(double _scale) {
    // #5443 : La taille doit rester une valeur paire.
    if (iaSize != null) {
      iaSize = (int) (iaSize * _scale);
      if (iaSize % 2 != 0)
        iaSize++;
    }
    if (ptCenter != null) {
      ptCenter.x_*=_scale;
      ptCenter.y_*=_scale;
    }
    if (sim != null) {
      sim = (int)(sim*_scale+0.5);
    }
    if (sip != null) {
      sip = (int)(sip*_scale+0.5);
    }
    if (sjm != null) {
      sjm = (int)(sjm*_scale+0.5);
    }
    if (sjp != null) {
      sjp = (int)(sjp*_scale+0.5);
    }
  }
  
  /**
   * Modifie l'intervalle de temps. Sert a conserver les coordonn�es m�triques.
   * @param _newTimeInterval Le nouvel intervalle de temps.
   */
  public void changeTimeInterval(Double _newTimeInterval) {
    if (timeInterval != null && _newTimeInterval != null) {
      double ratio=_newTimeInterval/timeInterval;

      if (sim != null) {
        sim=(int) (sim * ratio + 0.5);
      }
      if (sip != null) {
        sip=(int) (sip * ratio + 0.5);
      }
      if (sjm != null) {
        sjm=(int) (sjm * ratio + 0.5);
      }
      if (sjp != null) {
        sjp=(int) (sjp * ratio + 0.5);
      }
    }
    
    timeInterval = _newTimeInterval;
  }

  @Override
  public boolean equals(Object _o) {
    if (!(_o instanceof PivComputeParameters)) return false;
    PivComputeParameters o=(PivComputeParameters)_o;
    
    return solverParametersEquals(o) && 
        !(ptCenter == null ^ o.ptCenter == null) && (ptCenter == null || ptCenter.equals(o.ptCenter)) &&
        !(underSamplingPeriod == null ^ o.underSamplingPeriod == null) && (underSamplingPeriod == null || underSamplingPeriod == o.underSamplingPeriod);
  }
  
  /**
   * Test que les parametres pour le solveur uniquement sont egaux.
   * @param o Les parametres a comparer
   * @return True : Si les parametres sont identiques
   */
  public boolean solverParametersEquals(PivComputeParameters o) {
    
    return isActive == o.isActive &&
           !(iaSize == null ^ o.iaSize == null) && (iaSize == null || iaSize.equals(o.iaSize)) &&
           !(sim == null ^ o.sim == null) && (sim == null || sim.equals(o.sim)) &&
           !(sip == null ^ o.sip == null) && (sip == null || sip.equals(o.sip)) &&
           !(sjm == null ^ o.sjm == null) && (sjm == null || sjm.equals(o.sjm)) &&
           !(sjp == null ^ o.sjp == null) && (sjp == null || sjp.equals(o.sjp)) &&
           !(timeInterval == null ^ o.timeInterval == null) && (timeInterval == null || timeInterval.equals(o.timeInterval)) &&
           !(filters == null ^ o.filters == null) && (filters == null || filters.equals(o.filters));
  }
}
