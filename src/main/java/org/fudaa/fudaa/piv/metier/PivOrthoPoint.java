package org.fudaa.fudaa.piv.metier;

import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.piv.PivResource;

/**
 * Un point de r�f�rence pour l'orthorectification.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivOrthoPoint {
  /** Le point dans l'espace r��l */
  GrPoint realPoint;
  /** Le point dans l'espace image */
  GrPoint imgPoint;
  /** Le point recalcul� dans l'espace r�el */
  GrPoint computeRealPoint;
  /** La distance au point reel donn�. */
  double error;

  /**
   * Creation d'un ortho point avec uniquement les coordonn�es iamges.
   * Les coordonn�es reelles seront donn�es par la suite.
   * @param _imgPt Le point de coordonn�es images.
   */
  public PivOrthoPoint(GrPoint _imgPt) {
    imgPoint = _imgPt;
  }
  
  /**
   * Creation du point d'orthorectification.
   * @param _realPt Le point 3D en coordonn�es r�elles
   * @param _imgPt Le point 2D en coordonn�es image.
   */
  public PivOrthoPoint(GrPoint _realPt, GrPoint _imgPt) {
    realPoint=_realPt;
    imgPoint=_imgPt;
  }

  public PivOrthoPoint(PivOrthoPoint _pt) {
    computeRealPoint=_pt.computeRealPoint==null ? null:new GrPoint(_pt.computeRealPoint);
    imgPoint=_pt.imgPoint==null ? null:new GrPoint(_pt.imgPoint);
    realPoint=_pt.realPoint==null ? null:new GrPoint(_pt.realPoint);
    error=_pt.error;
  }  
  
  /**
   * Definit le point des coordonn�es image.
   * @param _pt Le point
   */
  public void setImgPoint(GrPoint _pt) {
    imgPoint = _pt;
  }
  
  /**
   * Definit le point des coordonn�es image.
   * @param _pt Le point
   */
  public void setRealPoint(GrPoint _pt) {
    realPoint = _pt;
  }
  
  /**
   * Definit le point 2D recalcul� dans l'espace r�el.
   * @param _pt Le point
   */
  public void setComputeRealPoint(GrPoint _pt) {
    computeRealPoint=_pt;
  }

  /**
   * Retourne le point 2D recalcul� dans l'espace r�el.
   * @return Le point.
   */
  public GrPoint getComputeRealPoint() {
    return computeRealPoint;
  }

  /**
   * Definit l'erreur de calcul (distance recalcul�e au point r�el).
   * @param _err L'erreur.
   */
  public void setError(double _err) {
    error=_err;
  }

  /**
   * Retourne l'erreur de calcul
   * @return L'erreur.
   */
  public double getError() {
    return error;
  }

  /**
   * Retourne le point 3D dans l'espace r�el. La coordonn�e Z est indiqu�e.
   * @return Le point, avec le Z.
   */
  public GrPoint getRealPoint() {
    return realPoint;
  }

  /**
   * Retourne le point 2D dans l'espace image.
   * @return Le point
   */
  public GrPoint getImgPoint() {
    return imgPoint;
  }

  /**
   * Controle que les points d'othorectification permettent une transformation d'image.
   * @return null : Les points sont Ok. Sinon message.
   */
  public static String areOrthoPointsOk(PivOrthoPoint[] orthoPoints) {
    
    if (orthoPoints==null || orthoPoints.length < 4) {
      return PivResource.getS("Le nombre de points de r�f�rence doit �tre au minimum de 4.");
    }

    // Controle des valeurs des points r�els.
    for (int i = 0; i < orthoPoints.length; i++) {

      for (int j = 0; j < i; j++) {
        if (orthoPoints[j].getRealPoint().distance(orthoPoints[i].getRealPoint()) == 0) {
          return PivResource.getS("Les points de r�f�rence {0} et {1} ont des coordonn�es r�elles confondues.", j + 1, i + 1);
        }
      }
    }
    
    // Tous les Z doivent �tre �gaux sinon error
    if (orthoPoints.length < 6) {
      for (int j = 0; j < orthoPoints.length-1; j++) {
        if (orthoPoints[j].getRealPoint().z_!=orthoPoints[j+1].getRealPoint().z_) {
          return PivResource.getS("Nombre de points de r�f�rence < 6, tous les points doivent avoir un Z identique.");
        }
      }
    }

    return null;
  }
  
  /**
   * Retourne le Z identique � tous les points.
   * @param orthoPoints Les ortho points.
   * @return La valeur de Z identique pour tous les points, ou null si les points n'ont pas un Z identique.
   */
  public static Double getIdenticalZ(PivOrthoPoint[] orthoPoints) {
    if (orthoPoints==null)
      return null;
    
    for (int j = 0; j < orthoPoints.length-1; j++) {
      if (orthoPoints[j].getRealPoint().z_!=orthoPoints[j+1].getRealPoint().z_) {
        return null;
      }
    }
    
    return orthoPoints[0].getRealPoint().z_;
  }
}
