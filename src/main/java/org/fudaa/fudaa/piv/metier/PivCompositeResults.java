package org.fudaa.fudaa.piv.metier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Les r�sultats scalaires aux points issus du calcul par PIV.
 * Les valeurs sont stock�es dans les tableaux correspondants.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivResults.java 8097 2012-12-04 16:27:08Z bmarchan $
 */
public class PivCompositeResults implements PivResultsI {
  
  /** La liste des r�sultats constituants */
  private List<PivResultsI> res_;
  /** La nature de r�sultat vers le r�sultat */
  private HashMap<ResultType, PivResultsI> type2Res_=new HashMap<>();
  /** Les natures de r�sultats (stock� pour conserver l'ordre) */
  private List<ResultType> types=new ArrayList<>();
  
  /**
   * Constructeur
   * @param _res Les resultats � combiner. Les points doivent �tre les m�mes, la nature des r�sultats
   * doit �tre diff�rente. Aucun controle n'est fait.
   */
  public PivCompositeResults(List<PivResultsI> _res) {
    res_=_res;
    for (PivResultsI res: _res) {
      types.addAll(Arrays.asList(res.getResults()));
      
      for (ResultType type : res.getResults()) {
        type2Res_.put(type, res);
      }
    }
  }
  
  /* (non-Javadoc)
   * @see org.fudaa.fudaa.piv.metier.PivResultsI#hasResult(org.fudaa.fudaa.piv.metier.PivResults.TYPE)
   */
  @Override
  public boolean hasResult(ResultType _tpRes) {
    return type2Res_.containsKey(_tpRes);
  }

  @Override
  public ResultType[] getResults() {
    return types.toArray(new ResultType[0]);
  }
  
  /* (non-Javadoc)
   * @see org.fudaa.fudaa.piv.metier.PivResultsI#getNbPoints()
   */
  @Override
  public int getNbPoints() {
    return res_.get(0).getNbPoints();
  }
  
  /* (non-Javadoc)
   * @see org.fudaa.fudaa.piv.metier.PivResultsI#getX(int)
   */
  @Override
  public double getX(int _ind) {
    return res_.get(0).getX(_ind);
  }
  
  /* (non-Javadoc)
   * @see org.fudaa.fudaa.piv.metier.PivResultsI#getY(int)
   */
  @Override
  public double getY(int _ind) {
    return res_.get(0).getY(_ind);
  }
  
  /* (non-Javadoc)
   * @see org.fudaa.fudaa.piv.metier.PivResultsI#getVal(int, org.fudaa.fudaa.piv.metier.PivResults.TYPE)
   */
  @Override
  public double getValue(int _ind, ResultType _tpRes) {
    return hasResult(_tpRes) ? type2Res_.get(_tpRes).getValue(_ind, _tpRes):0;
  }

  @Override
  public double[] getValues(ResultType _tpRes) {
    return hasResult(_tpRes) ? type2Res_.get(_tpRes).getValues(_tpRes):null;
  }

  @Override
  public void setValue(int _ind, ResultType _tpRes, double _val) {
    if (!hasResult(_tpRes))
      return;
    
    type2Res_.get(_tpRes).setValue(_ind, _tpRes, _val);
  }
}
