package org.fudaa.fudaa.piv.metier;

/**
 * Un listener notifi� en cas de changement d'�tat ou de donn�es du projet en
 * cours.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public interface PivProjectStateListener {

  /**
   * Invoqu� lors d'un changement d'�tat ou de donn�es du projet.
   * @param _prop Le nom de la propri�t� pour laquelle les donn�es ou l'�tat ont
   * �t� modifi�s.
   */
  void projectStateChanged(PivProject _prj, String _prop);

}
