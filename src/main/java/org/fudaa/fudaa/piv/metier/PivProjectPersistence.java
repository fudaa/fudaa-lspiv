package org.fudaa.fudaa.piv.metier;

import java.awt.Dimension;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.interpolation.InterpolationParameters;
import org.fudaa.ctulu.interpolation.InterpolationResultsHolderI;
import org.fudaa.ctulu.interpolation.InterpolationSupportValuesI;
import org.fudaa.ctulu.interpolation.InterpolationTarget;
import org.fudaa.ctulu.interpolation.SupportLocationI;
import org.fudaa.ctulu.interpolation.bilinear.InterpolatorBilinear;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivTaskObserverI;
import org.fudaa.fudaa.piv.io.PivAverageScalReader;
import org.fudaa.fudaa.piv.io.PivAverageScalWriter;
import org.fudaa.fudaa.piv.io.PivAverageVelReader;
import org.fudaa.fudaa.piv.io.PivAverageVelWriter;
import org.fudaa.fudaa.piv.io.PivBathyPReader;
import org.fudaa.fudaa.piv.io.PivBathyReader;
import org.fudaa.fudaa.piv.io.PivBathyWriter;
import org.fudaa.fudaa.piv.io.PivCoeffReader;
import org.fudaa.fudaa.piv.io.PivCoeffWriter;
import org.fudaa.fudaa.piv.io.PivDischargeReader;
import org.fudaa.fudaa.piv.io.PivFlowAreaReader;
import org.fudaa.fudaa.piv.io.PivFlowAreaWriter;
import org.fudaa.fudaa.piv.io.PivGRPReader;
import org.fudaa.fudaa.piv.io.PivGRPWriter;
import org.fudaa.fudaa.piv.io.PivGlobalXmlParam;
import org.fudaa.fudaa.piv.io.PivGlobalXmlReader;
import org.fudaa.fudaa.piv.io.PivGlobalXmlWriter;
import org.fudaa.fudaa.piv.io.PivGridParamReader;
import org.fudaa.fudaa.piv.io.PivGridParamWriter;
import org.fudaa.fudaa.piv.io.PivGridReader;
import org.fudaa.fudaa.piv.io.PivGridWriter;
import org.fudaa.fudaa.piv.io.PivHReader;
import org.fudaa.fudaa.piv.io.PivHWriter;
import org.fudaa.fudaa.piv.io.PivImgRefReader;
import org.fudaa.fudaa.piv.io.PivImgRefWriter;
import org.fudaa.fudaa.piv.io.PivInstantFilteredReader;
import org.fudaa.fudaa.piv.io.PivInstantFilteredWriter;
import org.fudaa.fudaa.piv.io.PivInstantScalReader;
import org.fudaa.fudaa.piv.io.PivInstantScalWriter;
import org.fudaa.fudaa.piv.io.PivInstantVelReader;
import org.fudaa.fudaa.piv.io.PivParamReader;
import org.fudaa.fudaa.piv.io.PivParamWriter;
import org.fudaa.fudaa.piv.io.PivSamplingVideoParamReader;
import org.fudaa.fudaa.piv.io.PivSamplingVideoParamWriter;
import org.fudaa.fudaa.piv.io.PivStabAreasReader;
import org.fudaa.fudaa.piv.io.PivStabAreasWriter;
import org.fudaa.fudaa.piv.io.PivStabParamsReader;
import org.fudaa.fudaa.piv.io.PivStabParamsWriter;
import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.fu.FuLog;

import gnu.trove.TIntArrayList;

/**
 * Un manager de persistence du projet.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 *
 */
public class PivProjectPersistence {
  
  /**
   * Classe support pour le calcul d'interpolation des pointsimg_sta invalides (points � CORREL=-99, VX=0, VY=0)
   * Le support prend en compte tous les points r�sultats, amput�s de ces points invalides.
   * @author marchand
   */
  public class InterpolationSupportResultsAdapter implements SupportLocationI, InterpolationSupportValuesI {

    PivResultsI res_;
    // La correspondance vers les resultats.
    TIntArrayList indexes_=new TIntArrayList();
    
    public InterpolationSupportResultsAdapter(PivResultsI _res) {
      res_ = _res;
      
      for (int i=0; i<_res.getNbPoints(); i++) {
        // Point invalide : on ne le garde pas pour le support.
        if (res_.hasResult(ResultType.CORREL) && res_.getValue(i, ResultType.CORREL)==-99)
          continue;
        
        indexes_.add(i);
      }
    }
    
    public boolean isValid() {
      return indexes_.size()>0;
    }
    
    @Override
    public double getV(CtuluVariable _var, int _ptIdx) {
      return res_.getValue(indexes_.get(_ptIdx), (ResultType)_var);
    }

    @Override
    public int getPtsNb() {
      return indexes_.size();
    }

    @Override
    public double getPtX(int _i) {
      return res_.getX(indexes_.get(_i));
    }

    @Override
    public double getPtY(int _i) {
      return res_.getY(indexes_.get(_i));
    }
  }
  
  /**
   * Classe cible de l'interpolation. Seuls les points invalides sont cibl�s (points � CORREL=-99, VX=0, VY=0)
   * @author marchand
   */
  public class InterpolationResultsTarget implements InterpolationTarget {
    
    PivResultsI res_;
    // La correspondance vers les resultats.
    TIntArrayList indexes_=new TIntArrayList();
    
    public InterpolationResultsTarget(PivResultsI _res) {
      res_ = _res;
      
      // Point invalide : ses valeurs seront interpol�es.
      for (int i=0; i<_res.getNbPoints(); i++) {
        if (!res_.hasResult(ResultType.CORREL) || res_.getValue(i, ResultType.CORREL)!=-99)
          continue;
        
        indexes_.add(i);
      }
    }
    
    public TIntArrayList getIndexes() {
      return indexes_;
    }

    @Override
    public double getPtX(int _i) {
      return res_.getX(indexes_.get(_i));
    }

    @Override
    public double getPtY(int _i) {
      return res_.getY(indexes_.get(_i));
    }

    @Override
    public int getPtsNb() {
      return indexes_.size();
    }

    @Override
    public boolean isInBuffer(double _xToTest, double _yToTest, double _maxDist) {
      return true;
    }
  }
  /** Le projet associ� */
  PivProject prj_;
  
  public PivProjectPersistence(PivProject _prj) {
    prj_=_prj;
  }

  /**
   * Charge le projet depuis le fichier projet donn�. Le fichier projet est
   * d�zipp�, puis il est charg� s'il est conforme.
   * @param _prog L'interface de progression. Peut etre <tt>null</tt>
   * 
   * @return True Si le fichier donn� est bien un fichier projet.
   */
  public boolean load(PivTaskObserverI _prog) {
  //    File transDir=new File(rootPath,TRANSECTS_DIR);
  //    File imgTransfDir=new File(rootPath,IMG_TRANSF_DIR);
    
    // Les images
    prj_.srcFiles_=loadSrcImages(_prog);
    
    // Le fichier des param�tres d'�chantillonnage.
    File samplingParamFile = new File(prj_.getOutputDir(), "user_extract.dat");
    if (samplingParamFile.exists()) {
      CtuluIOResult<PivSamplingVideoParameters> ret = new PivSamplingVideoParamReader().read(samplingParamFile, _prog);
      prj_.samplingParams=ret.getSource();
    }

    // Le fichier des stabilisation d'images
    File stabParamFile = new File(prj_.getOutputDir(), "stab_param.dat");
    if (stabParamFile.exists()) {
      CtuluIOResult<PivStabilizationParameters> ret = new PivStabParamsReader().read(stabParamFile, _prog);
      prj_.stabParameters=ret.getSource();
      prj_.stabParameters.setActive(true);
    }
    
    // Le fichier des zones de stabilisation.
    File stabAreasFile=new File(prj_.getOutputDir(),"stab_areas.dat");
    if (stabAreasFile.exists() && prj_.stabParameters != null) {
      CtuluIOResult<PivStabilizationParameters> ret = new PivStabAreasReader().read(stabAreasFile, _prog);
      
      prj_.stabParameters.setOutlines(ret.getSource().getOutlines());
      prj_.stabParameters.setFlowAreas(ret.getSource().isFlowAreas());
    }
    
    // Le fichier GRP
    File grpFile=new File(prj_.getOutputDir(),"GRP.dat");
    if (grpFile.exists()) {
      CtuluIOResult<PivOrthoPoint[]> ret = new PivGRPReader(prj_.getSrcImageSize()).read(grpFile, _prog);
      prj_.orthoPoints=ret.getSource();
    }
    
    // Le fichier des coefficients ortho
    File coeffFile=new File(prj_.getOutputDir(),"coeff.dat");
    if (coeffFile.exists()) {
      CtuluIOResult<double[]> ret = new PivCoeffReader().read(coeffFile, _prog);
      prj_.coeffs=ret.getSource();
    }
  
    // Les fichiers de parametres ortho
    File imgRefFile=new File(prj_.getOutputDir(),"img_ref.dat");
    File hFile=new File(prj_.getOutputDir(),"h.dat");
    if (imgRefFile.exists() && hFile.exists()) {
      CtuluIOResult<PivOrthoParameters> ret=new PivImgRefReader().read(imgRefFile, _prog);
      prj_.orthoParams=ret.getSource();
  
      CtuluIOResult<Double> ret2 = new PivHReader().read(hFile, null);
      prj_.orthoParams.setWaterElevation((Double)ret2.getSource());
    }
  
    // Le fichier de parametres calcul
    File paramFile=new File(prj_.getOutputDir(),"PIV_param.dat");
    if (paramFile.exists()) {
      CtuluIOResult<Object[]> ret = new PivParamReader().read(paramFile, _prog);
      Object[] params=ret.getSource();
      prj_.computeParams=(PivComputeParameters)params[0];
    }
  
    // Le fichier contour de grille
    File cntGridFile=new File(prj_.getOutputDir(),"grid_param.dat");
    if (cntGridFile.exists()) {
      CtuluIOResult<PivCntGrid> ret = new PivGridParamReader().read(cntGridFile, _prog);
      prj_.cntGrid=(PivCntGrid)ret.getSource();
    }
  
    // Le fichier de grille
    File gridFile=new File(prj_.getOutputDir(),"grid.dat");
    if (gridFile.exists()) {
      CtuluIOResult<PivGrid> ret = new PivGridReader().read(gridFile, _prog);
      prj_.computeGrid=ret.getSource();
    }
  
    // Les r�sultats moyennes
    prj_.averageResults = loadAverageResults(_prog);

    // Les r�sultats instantan�s
    prj_.instantResults = loadInstantRawResults(_prog);
    // Les r�sultats instantan�s filtr�s
    prj_.instantFilteredResults = loadInstantFilteredResults(_prog);
    // Et ceux utilis�s pour le calcul de moyenne
    boolean[] indexes = loadInstantResultsList(_prog);
    prj_.statisticParameters.usedInstantResults = indexes;

    // Les resultats manuels
    prj_.manualResults = loadManualResults(_prog);
    
    // Les transects
    prj_.transects=loadTransects(_prog);
    
    // Les r�sultats de d�bit
    prj_.flowResults=loadFlowResults(_prog);
    
    // Les param�tres globaux
    loadGlobalData(_prog);
  
    // Reconstruction des caches d'image
    prj_.rebuiltAllCacheImagesIfNeeded(_prog);
    
    FuLog.debug("*** Chargement de "+prj_.rootPath+" ***");
    
    if (prj_.stabParameters!=null) {
      FuLog.trace("Param�tres de stabilisation d'images charg�s");
    }
    else {
      FuLog.trace("Pas de param�tres de stabilisation d'images.");
    }
    if (prj_.orthoPoints!=null) {
      FuLog.trace("Points de r�f�rence trouv�s : "+prj_.orthoPoints.length+" points");
    }
    else {
      FuLog.trace("Pas de points de r�f�rence.");
    }
    if (prj_.orthoParams!=null) {
      FuLog.trace("Param�tres d'orthorectification charg�s");
    }
    else {
      FuLog.trace("Pas de param�tres d'orthorectification.");
    }
    if (prj_.computeParams!=null) {
      FuLog.trace("Param�tres de calcul charg�s");
    }
    else {
      FuLog.trace("Pas de param�tres de calcul.");
    }
    if (prj_.cntGrid!=null) {
      FuLog.trace("Contour de grille charg�");
    }
    else {
      FuLog.trace("Pas de contour de grille.");
    }
    if (prj_.computeGrid!=null) {
      FuLog.trace("Points de grille charg�s");
    }
    else {
      FuLog.trace("Pas de points de grille.");
    }
    if (prj_.transects!=null && prj_.transects.length!=0) {
      FuLog.trace("Transects charg�s");
    }
    else {
      FuLog.trace("Pas de transects.");
    }
    if (prj_.hasSrcImages(PivProject.SRC_IMAGE_TYPE.ALL)) {
      FuLog.trace("Images sources : "+prj_.getSrcImageFiles(PivProject.SRC_IMAGE_TYPE.ALL).length+" images");
    }
    else {
      FuLog.trace("Pas d'images sources.");
    }
    if (prj_.hasTransfImages()) {
      FuLog.trace("Images transform�es : "+prj_.getTransfImageFiles().length+" images");
    }
    else {
      FuLog.trace("Pas d'images transform�es.");
    }
    if (prj_.averageResults!=null) {
      FuLog.trace("R�sultats moyenn�s charg�s");
    }
    else {
      FuLog.trace("Pas de r�sultats moyenn�s.");
    }
    if (prj_.instantResults!=null) {
      FuLog.trace("R�sultats instantan�s charg�s");
    }
    else {
      FuLog.trace("Pas de r�sultats instantan�s.");
    }
    if (prj_.flowResults!=null) {
      FuLog.trace("R�sultats de d�bit charg�s");
    }
    else {
      FuLog.trace("Pas de r�sultats de d�bit.");
    }
    FuLog.trace("*** Fin du chargement ***");
  
    prj_.isModified=false;
    
    return true;
  }

  /**
   * Charge les transects avec leurs parametres, dans l'ordre des fichiers de bathy
   * @param _prog L'interface de progression.
   * @return Les resultats ou <tt>null</tt> si aucun r�sultat.
   */
  public PivTransect[] loadTransects(ProgressionInterface _prog) {
    File[] bthFiles=new File(prj_.rootPath,PivProject.TRANSECTS_DIR).listFiles(PivUtils.FILE_FLT_TRANS_BTH);
    
    if (bthFiles!=null && bthFiles.length>0) {
      Arrays.sort(bthFiles);
      
      PivTransect[] trans=new PivTransect[bthFiles.length];
      
      for (int i=0; i<bthFiles.length; i++) {
        CtuluIOResult<PivTransect> ret = new PivBathyReader().read(bthFiles[i], _prog);
        trans[i]=ret.getSource();
        
        CtuluIOResult<Object[]> ret2 = new PivParamReader().read(CtuluLibFile.changeExtension(bthFiles[i], PivUtils.FILE_FLT_TRANS_PAR.getFirstExt()), _prog);
        trans[i].setParams((PivTransectParams)ret2.getSource()[1]);
      }
      
      return trans;
    }
    return null;
  }

  /**
   * Cerrection des valeurs invalides pour certains points.
   * @param _res Les resultats a corriger
   */
  protected void correctInvalidPoints(PivResultsI _res) {
    // On interpole les valeurs des points invalides.
    InterpolationSupportResultsAdapter support=new InterpolationSupportResultsAdapter(_res);
    if (support.isValid()) {
      InterpolationResultsTarget target=new InterpolationResultsTarget(_res);
      List<ResultType> vars=Arrays.asList(_res.getResults());
      InterpolationParameters params=new InterpolationParameters(vars, target, support);
      InterpolatorBilinear interp=new InterpolatorBilinear(support);
      interp.interpolate(params);
      InterpolationResultsHolderI invalidPts=params.getResults();

      // On remplace les valeurs dans les resultats pour les points en -99.
      TIntArrayList indexes=target.getIndexes();
      for (int ind=0; ind < indexes.size(); ind++) {
        double[] vals=invalidPts.getValuesForPt(ind);
        for (int ires=0; ires < vars.size(); ires++) {
          _res.setValue(indexes.get(ind), vars.get(ires), vals[ires]);
        }
      }
    }
    else {
      FuLog.warning("Impossible de corriger les points � CORREL=-99");
    }
  }
  
  /**
   * Charge les resultats moyenn�s dans l'espace r�el.
   * 
   * @param _prog L'interface de progression.
   * @return Les resultats ou <tt>null</tt> si aucun r�sultat.
   */
  public PivCompositeResults loadAverageResults(ProgressionInterface _prog) {
    List<PivResultsI> lres=new ArrayList<PivResultsI>();
    
    File velFile=new File(prj_.getOutputDir(),"average_vel.out");
    if (velFile.exists()) {
      PivResultsDefault aveVelRes = new PivAverageVelReader().read(velFile, _prog).getSource();
      lres.add(aveVelRes);
      
      File scalFile=new File(prj_.getOutputDir(), "average_scal.out");
      if (scalFile.exists()) {
        PivResultsDefault aveScalRes = new PivAverageScalReader().read(scalFile, _prog).getSource();
        lres.add(aveScalRes);
      }
      
      PivCompositeResults res = new PivCompositeResults(lres);
      correctInvalidPoints(res);
      
      return res;
    }
    return null;
  }
  
  /**
   * Charge les resultats manuels dans l'espace reel
   * 
   * @param _prog L'interface de progression.
   * @return Les resultats ou <tt>null</tt> si aucun r�sultat.
   */
  public PivResultsI loadManualResults(ProgressionInterface _prog) {
    
    File velFile=new File(prj_.getOutputDir(),"manual_vel.out");
    if (velFile.exists()) {
      PivResultsDefault velRes = new PivAverageVelReader().read(velFile, _prog).getSource();
//      lres.add(aveVelRes);
      
//      File scalFile=new File(prj_.getOutputDir(), "average_scal.out");
//      if (scalFile.exists()) {
//        PivResultsDefault aveScalRes = new PivAverageScalReader().read(scalFile, _prog).getSource();
//        lres.add(aveScalRes);
//      }
//      
//      PivCompositeResults res = new PivCompositeResults(lres);
//      correctInvalidPoints(res);
      
      return velRes;
    }
    return null;
  }

  /**
   * Charge les resultats instantan�s qui ont servi a calculer les r�sultats moyens.
   * @param _prog L'interface de progression.
   * @return L'utilisation du resultat si l'index correspondant est true.
   */
  public boolean[] loadInstantResultsList(ProgressionInterface _prog) {
    boolean[] res;
    
    File[] instantVelFiles=prj_.getRawVelocityFiles();
    // Pas de r�sultats instantan�s
    if (instantVelFiles.length==0) {
      res=new boolean[0];
      return res;
    }
    
    File list=new File(prj_.getOutputDir(),"list_avg.dat");
    res=new boolean[instantVelFiles.length];
    
    // Pas de liste de r�sultats instantan�s
    if (!list.exists()) {
      Arrays.fill(res, true);
      return res;
    }
    
    // Le fichier existe.
    else {
      LineNumberReader inp=null;
      try {
        inp = new LineNumberReader(new FileReader(list));
        String line;
        while ((line = inp.readLine()) != null) {
          for (int i = 0; i < instantVelFiles.length; i++) {
            if (instantVelFiles[i].getName().equals(line)) {
              res[i] = true;
              break;
            }
          }
        }
      }
      catch (IOException e) {
        Arrays.fill(res, true);
      }
      finally {
        try {
          if (inp != null)
            inp.close();
        }
        catch (IOException e) {
        }
      }
      return res;
    }
  }

  /**
   * Charge un fichier bathy_p.dat
   * 
   * @param _prog L'interface de progression.
   * @return Le transect lu.
   */
  public PivTransect loadBathyPFile(ProgressionInterface _prog) {
    File fbathyp=new File(prj_.getOutputDir(),"bathy_p.dat");
    CtuluIOResult<PivTransect> ret = new PivBathyPReader().read(fbathyp, _prog);
    return ret.getSource();
  }

  /**
   * Charge les resultats instantan�s filtr�s dans l'espace r�el, suivant l'ordre
   * alphab�tique des fichiers.
   * 
   * @param _prog L'interface de progression.
   * @return Les resultats ou <tt>null</tt> si aucun r�sultat.
   */
  public PivResultsI[] loadInstantFilteredResults(ProgressionInterface _prog) {
    File[] instantFltFiles=new File(prj_.rootPath,PivProject.VEL_FILTER_DIR).listFiles();
    File[] instantScalFiles=new File(prj_.rootPath,PivProject.VEL_SCAL_DIR).listFiles();
    
    if (instantFltFiles!=null && instantFltFiles.length>0) {
      Arrays.sort(instantFltFiles);
      // Peut �tre null pour les anciens projets.
      if (instantScalFiles!=null)
        Arrays.sort(instantScalFiles);
      
      PivCompositeResults[] instantRes=new PivCompositeResults[instantFltFiles.length];
      for (int i=0; i<instantFltFiles.length; i++) {
        List<PivResultsI> lres=new ArrayList<PivResultsI>();
        
        PivResultsDefault instantVelRes = new PivInstantFilteredReader().read(instantFltFiles[i], _prog).getSource();
        lres.add(instantVelRes);
        
        // Les scalaires
        PivResultsDefault instantScalRes=null;
        if (instantScalFiles!=null && instantScalFiles.length>0) {
          instantScalRes=new PivInstantScalReader().read(instantScalFiles[i], _prog).getSource();
          // La correlation est supprim�e, elle est issue du fichier filtered plus pr�cis.
          instantScalRes.removeResult(ResultType.CORREL);
          lres.add(instantScalRes);
        }

        PivCompositeResults res = new PivCompositeResults(lres);
        correctInvalidPoints(res);
      
        instantRes[i]=res;
      }
      return instantRes;
    }
    return null;
  }

  /**
   * Charge les resultats de d�bit, suivant l'ordre alphab�tique des fichiers.
   * 
   * @param _prog L'interface de progression.
   * @return Les resultats ou <tt>null</tt> si aucun r�sultat.
   */
  public PivFlowResults[] loadFlowResults(ProgressionInterface _prog) {
    File[] resFlowFiles=new File(prj_.rootPath,PivProject.TRANSECTS_DIR).listFiles(PivUtils.FILE_FLT_TRANS_RES);
  //    File[] instantScalFiles=new File(rootPath,VEL_SCAL_DIR).listFiles();
    
    if (resFlowFiles!=null && resFlowFiles.length>0) {
      Arrays.sort(resFlowFiles);
      
      PivFlowResults[] results=new PivFlowResults[resFlowFiles.length];
      for (int i=0; i<resFlowFiles.length; i++) {
        
        CtuluIOResult<PivFlowResults> ret = new PivDischargeReader().read(resFlowFiles[i], _prog);
        results[i]=ret.getSource();
        
        // Avant la version 1.7.2, le coefficient global calcul� est le m�me que le coefficient global des param�tres.
        if (Double.isNaN(results[i].computedVelCoefficient)) {
          results[i].computedVelCoefficient = prj_.getTransects()[i].getParams().getSurfaceCoef();
        }
      }
      return results;
    }
    return null;
  }

  /**
   * Charge les resultats instantan�es bruts dans l'espace r�el, suivant l'ordre
   * alphab�tique des fichiers. Les points invalides sont interpol�s.
   * 
   * @param _prog L'interface de progression.
   * @return Les resultats ou <tt>null</tt> si aucun r�sultat.
   */
  public PivCompositeResults[] loadInstantRawResults(ProgressionInterface _prog) {
    File[] instantVelFiles=new File(prj_.rootPath,PivProject.VEL_REAL_DIR).listFiles();
  //    File[] instantScalFiles=new File(rootPath,VEL_SCAL_DIR).listFiles();
    
    if (instantVelFiles!=null && instantVelFiles.length>0) {
      Arrays.sort(instantVelFiles);
      // Peut �tre null pour les anciens projets.
  //      if (instantScalFiles!=null)
  //        Arrays.sort(instantScalFiles);
      
      List<PivCompositeResults> instantRes=new ArrayList<PivCompositeResults>();
      for (int i=0; i<instantVelFiles.length; i++) {
        List<PivResultsI> lres=new ArrayList<PivResultsI>();
        
        PivResultsDefault instantVelRes = new PivInstantVelReader().read(instantVelFiles[i], _prog).getSource();
        lres.add(instantVelRes);

        PivCompositeResults res = new PivCompositeResults(lres);
        correctInvalidPoints(res);
        
        instantRes.add(res);
      }
      return instantRes.toArray(new PivCompositeResults[0]);
    }
    return null;
  }

  /**
   * Charge les param�tres globaux du projet.
   * @param _prog L'interface de progression.
   * @return Les param�tres globaux.
   */
  public void loadGlobalData(ProgressionInterface _prog) {
    File globalFile = new File(prj_.getOutputDir(), "global.xml");
    CtuluIOResult<PivGlobalXmlParam> ret = new PivGlobalXmlReader().read(globalFile, _prog);
    if (ret.getAnalyze().containsSevereError()) {
      return;
    }
    PivGlobalXmlParam paramXml=ret.getSource();
    
    if (prj_.computeParams != null) {
      // Affectation du centre de IA/SA.
      prj_.computeParams.setIACenterPosition(paramXml.iaCenter);
    }
    
    // Tri des images suivant leur ordre sauvegard�.
    final List<String> names = Arrays.asList(paramXml.srcImages);
    Collections.sort(prj_.srcFiles_, new Comparator<File>() {
      @Override
      public int compare(File o1, File o2) {
        return names.indexOf(o1.getName()) - names.indexOf(o2.getName());
      }
    });
    
    // Affectation des param�tres de transformation.
    PivTransformationParameters params=new PivTransformationParameters(paramXml.tx, paramXml.ty, paramXml.tz, paramXml.rz, paramXml.xcenter, paramXml.ycenter);
    prj_.setTransformationParameters(params);
    
    // Scaling 
    if (prj_.orthoParams != null) {
      // Les points pour la transformation
      prj_.orthoParams.setScalingTranformationImgPoints(paramXml.scalingImgPoints);
      prj_.orthoParams.setScalingTranformationRealPoints(paramXml.scalingRealPoints);
      
      // Les parametres resolution
      prj_.orthoParams.setScalingResolutionImgPoints(paramXml.scalingResolutionImgPoints);
      prj_.orthoParams.setScalingResolutionRealPoints(paramXml.scalingResolutionRealPoints);
      prj_.orthoParams.setScalingResolutionDistances(paramXml.scalingResolutionDistances);
      prj_.orthoParams.setScalingZSegments(paramXml.scalingZSegments);
      prj_.orthoParams.setScalingZDrone(paramXml.scalingZDrone);
    }
    
    // Vitesses directes.
    if (paramXml.displacementPoints != null) {
      PivManualVelocitiesParameters directVelParams = new PivManualVelocitiesParameters();
      directVelParams.displacementImgPoints = paramXml.displacementPoints;
      directVelParams.imgIndexes = paramXml.imgIndexes;
      prj_.setManualVelocitiesParameters(directVelParams);
    }
    
    // Calcul par PIV
    prj_.computeParams.setActive(paramXml.isPivActive);
    
    // Stabilisation (pour les projet < 1.9)
    if (prj_.stabParameters != null && paramXml.stabilizationFlowArea != null) {
      List<GrPolygone> outlines = new ArrayList<>();
      outlines.add(paramXml.stabilizationFlowArea);
      prj_.stabParameters.setOutlines(outlines);
    }
    
    // Sampling (pour les projet < 1.9)
    if (paramXml.samplingParams != null) {
      prj_.setSamplingParameters(paramXml.samplingParams);
    }
    
    // Filtres (>= 1.10.1)
    if (paramXml.filterParams != null) {
      if (prj_.getComputeParameters() != null) {
        prj_.getComputeParameters().setFilters(paramXml.filterParams);
      }
    }
    
    // Parametres de calcul de moyenne (>= 1.10.1)
    if (paramXml.statisticParams != null) {
      // On ne recupere que le type de calcul de moyenne/mediane (la liste des resultats est stock�e dans list_avg.dat)
      prj_.getStatisticParameters().setMethod(paramXml.statisticParams.method);
      prj_.getStatisticParameters().setManualFieldUsed(paramXml.statisticParams.isManualFieldUsed);
    }
  }
  
  public List<File> loadSrcImages(ProgressionInterface _prog) {
    File imgSrcDir=new File(prj_.rootPath,PivProject.IMG_PGM_DIR);
    File[] srcFiles=imgSrcDir.listFiles(PivProject.FLT_FILES);
    if (srcFiles==null) srcFiles=new File[0];
    
    return Arrays.asList(srcFiles);
  }

  /**
   * Sauvegarde sur le fichier global.xml les donn�es globales.
   * @param _prog La progression
   */
  public void saveGlobalData(ProgressionInterface _prog) {
    PivGlobalXmlParam params = new PivGlobalXmlParam();
    if (prj_.getComputeParameters() != null) {
      params.iaCenter = prj_.getComputeParameters().getIACenterPosition();
    }
    
    // L'ordre des images
    params.srcImages=new String[prj_.srcFiles_.size()];
    for (int i=0; i<prj_.srcFiles_.size(); i++) {
      params.srcImages[i]=prj_.srcFiles_.get(i).getName();
    }
    
    // La matrice de transformation
    params.tx=prj_.getTransformationParameters().getTranslationX();
    params.ty=prj_.getTransformationParameters().getTranslationY();
    params.tz=prj_.getTransformationParameters().getTranslationZ();
    params.rz=prj_.getTransformationParameters().getRotationZ();
    params.xcenter = prj_.getTransformationParameters().getXCenter();
    params.ycenter = prj_.getTransformationParameters().getYCenter();
    
    // Scaling
    PivOrthoParameters orthoParams=prj_.getOrthoParameters();
    if (orthoParams!=null) {
      
      // Les points pour la transformation
      params.scalingImgPoints=orthoParams.getScalingTranformationImgPoints();
      params.scalingRealPoints=orthoParams.getScalingTranformationRealPoints();
      
      // Les parametres resolution
      params.scalingResolutionDistances = orthoParams.getScalingResolutionDistances();
      params.scalingResolutionImgPoints = orthoParams.getScalingResolutionImgPoints();
      params.scalingResolutionRealPoints = orthoParams.getScalingResolutionRealPoints();
      params.scalingZDrone = orthoParams.getScalingZDrone();
      params.scalingZSegments =  orthoParams.getScalingZSegments();
    }
    
    // Vitesses directes
    PivManualVelocitiesParameters directVelParams = prj_.getManualVelocitiesParameters();
    if (directVelParams!=null) {
      params.displacementPoints = directVelParams.getDisplacementImgPoints();
      params.imgIndexes = directVelParams.getImagesIndexes();
    }
    
    // Calcul par PIV
    params.isPivActive = prj_.getComputeParameters() != null ? prj_.getComputeParameters().isActive() : true;
    
//    // Sampling
//    PivSamplingVideoParameters samplingParams = prj_.getSamplingParameters();
//    if (samplingParams!=null) {
//      params.samplingParams = samplingParams;
//    }
    
    // Filters
    params.filterParams = prj_.getComputeParameters() == null ? null : prj_.getComputeParameters().getFilters();
    
    // Parametres de moyenne
    params.statisticParams = prj_.getStatisticParameters();
    
    File globalFile = new File(prj_.getOutputDir(), "global.xml");
    new PivGlobalXmlWriter().write(params, globalFile, _prog);
  }

  /**
   * Sauve le projet sur le fichier projet. Les infos sont sauv�es sur le
   * r�pertoire temporaire, puis zip�es.
   * @param prj_ TODO
   * @param _prog L'interface de progression. Peut etre <tt>null</tt>
   * @return True si le projet a bien �t� sauv�.
   */
  public boolean save(PivProject prj_, ProgressionInterface _prog) {
  
    if (_prog!=null)
      _prog.setDesc(PivResource.getS("Sauvegarde du projet"));
    
    // Suppression du repertoire tmp, qui contient des donn�es temporaires. Devrait �tre vide, mais ce n'est peut �tre pas le cas.
    File tmpDir = prj_.getTempDirectory();
    CtuluLibFile.deleteDir(tmpDir);
    
    // Le fichier des param�tres d'�chantillonnage.
    File samplingParamFile=new File(prj_.getOutputDir(),"user_extract.dat");
    samplingParamFile.delete();
    if (prj_.samplingParams != null) {
      new PivSamplingVideoParamWriter().write(prj_.samplingParams, samplingParamFile, null);
    }
    
    // Le fichier des parametres de stabilisation.
    File stabParamFile = new File(prj_.getOutputDir(), "stab_param.dat");
    stabParamFile.delete();
    if (prj_.stabParameters != null && prj_.stabParameters.isActive_) {
      new PivStabParamsWriter().write(prj_.stabParameters, stabParamFile, null);
    }
    
    // Le fichier des polygones de stabilisation
    File stabAreasFile=new File(prj_.getOutputDir(),"stab_areas.dat");
    stabAreasFile.delete();
    if (prj_.stabParameters != null && prj_.stabParameters.isActive_) {
      new PivStabAreasWriter().write(prj_.stabParameters, stabAreasFile, null);
    }
        
    // Le fichier GRP
    File grpFile=new File(prj_.getOutputDir(),"GRP.dat");
    grpFile.delete();
    if (prj_.orthoPoints!=null) {
      Object[] params=new Object[]{ prj_.orthoPoints, prj_.getSrcImageSize() };
      new PivGRPWriter().write(params, grpFile, null);
    }
    
    // Le fichier des coefficients ortho
    File coeffFile=new File(prj_.getOutputDir(),"coeff.dat");
    coeffFile.delete();
    if (prj_.coeffs!=null) {
      new PivCoeffWriter().write(prj_.coeffs, coeffFile, null);
    }
  
    // Les fichiers de parametres ortho
    File imgRefFile=new File(prj_.getOutputDir(),"img_ref.dat");
    imgRefFile.delete();
    if (prj_.orthoParams!=null) {
      Object[] params=new Object[]{prj_.orthoParams,prj_.getSrcImageSize()};
      new PivImgRefWriter().write(params, imgRefFile, null);
  
      File hFile=new File(prj_.getOutputDir(),"h.dat");
      new PivHWriter().write(prj_.orthoParams.getWaterElevation(), hFile, null);
    }
  
    // Le fichiers de parametres de calcul
    File paramFile=new File(prj_.getOutputDir(),"PIV_param.dat");
    paramFile.delete();
    if (prj_.computeParams!=null) {
      Object[] params=new Object[]{prj_.computeParams,null,prj_.getTransfImageSize()};
      new PivParamWriter().write(params, paramFile, null);
    }
  
    // Le contour de grille
    File cntGridFile=new File(prj_.getOutputDir(),"grid_param.dat");
    cntGridFile.delete();
    if (prj_.cntGrid!=null) {
      new PivGridParamWriter().write(prj_.cntGrid, cntGridFile, null);
    }
  
    // Le fichiers des points de grille
    File gridFile=new File(prj_.getOutputDir(),"grid.dat");
    gridFile.delete();
    if (prj_.computeGrid!=null) {
      new PivGridWriter().write(prj_.computeGrid, gridFile, null);
    }
  
    // Les transects
    saveTransects(prj_, prj_.transects, null);
    
    // Les resultats filtr�s instantan�s
    saveInstantFilteredResults(prj_, prj_.rootPath, prj_.instantFilteredResults, null);
    
    // Les resultats utilis�s pour le calcul de vitesse moyenne.
    saveInstantResultsList(false);
    
    // Les resultats moyenn�s
    saveAverageResults(prj_, prj_.averageResults, null);
    
    // Les donn�es globales
    saveGlobalData(null);
    
    prj_.isModified=false;
    return true;
  }

  /**
   * Sauvegarde sur une liste les resultats instantan�s utilis�s pour le calcul des
   * resultats moyenn�s.
   * @param prj_ TODO
   * @param _all Tous les r�sultats sont list�s, sinon seulement ceux pour le calcul de moyenne.
   */
  public void saveInstantResultsList(boolean _all) {
    File listFile = new File(prj_.getOutputDir(),"list_avg.dat");
    listFile.delete();
    
    File[] instantVelFiles=prj_.getRawVelocityFiles();
    // Pas de r�sultats instantan�s
    if (instantVelFiles.length==0) return;
    
    // Cr�ation du fichier contenant la liste des fichiers piv.dat obtenus par calcul.
    PrintWriter out=null;
    try {
      out = new PrintWriter(listFile);
      // En principe, le nb de fichiers existants dans le repertoire devrait etre le meme
      // que la taille du tableau usedInstantResults. Ce n'est pas toujours le cas,
      // par exemple quand le calcul s'est mal pass�.
      boolean[] usedInstantResults = prj_.getStatisticParameters().getUsedInstantResults();
      for (int i=0; i<usedInstantResults.length; i++) {
        if (_all || usedInstantResults[i])
          out.println(instantVelFiles[i].getName());
      }
    }
    catch (IOException e) {
    }
    finally {
      if (out!=null)
        out.close();
    }
  }

  /**
   * Sauve les transects avec leurs parametres, dans l'ordre des fichiers de bathy
   * @param prj_ TODO
   * @param _trans TODO
   * @param _prog L'interface de progression.
   * @return Les resultats ou <tt>null</tt> si aucun r�sultat.
   */
  public void saveTransects(PivProject prj_, PivTransect[] _trans, ProgressionInterface _prog) {
    File dirTransect=new File(prj_.rootPath,PivProject.TRANSECTS_DIR);
    dirTransect.mkdir();
    
    // Suppression des fichiers bathy
    File[] files=dirTransect.listFiles(PivUtils.FILE_FLT_TRANS_BTH);
    if (files!=null) {
      for (File file : files) {
        file.delete();
      }
    }
    // Suppression des fichiers params
    files=dirTransect.listFiles(PivUtils.FILE_FLT_TRANS_PAR);
    if (files!=null) {
      for (File file : files) {
        file.delete();
      }
    }
    
    if (_trans==null) return;
  
    for (int i = 0; i < _trans.length; i++) {
      File bthFile = new File(dirTransect, "trans" + PivUtils.formatOn4Chars(i + 1) + "."+PivUtils.FILE_FLT_TRANS_BTH.getFirstExt());
      new PivBathyWriter().write(_trans[i], bthFile, _prog);
  
      File parFile = new File(dirTransect, "trans" + PivUtils.formatOn4Chars(i + 1) + "."+PivUtils.FILE_FLT_TRANS_PAR.getFirstExt());
      Object[] params = new Object[] { new PivComputeParameters(), _trans[i].getParams(), new Dimension() };
      new PivParamWriter().write(params, parFile, _prog);
    }
  }

  /**
   * Sauve les resultats filtr�s, dans l'ordre
   * @param prj_ Le projet
   * @param prjRoot Le repertoire racine de sauvegarde.
   * @param _res Les resultats a sauver
   * @param _prog L'interface de progression.
   */
  public void saveInstantFilteredResults(PivProject prj_, File prjRoot, PivResultsI[] _res, ProgressionInterface _prog) {
    
    // Suppression des anciens fichiers vitesses
    File dirVelFilter=new File(prj_.rootPath,PivProject.VEL_FILTER_DIR);
    dirVelFilter.mkdir();
    File[] files=dirVelFilter.listFiles();
    if (files!=null) {
      for (File file : files) {
        file.delete();
      }
    }

    // Suppression des anciens fichiers scalaires
    File dirVelScal=new File(prj_.rootPath,PivProject.VEL_SCAL_DIR);
    dirVelScal.mkdir();
    files=dirVelScal.listFiles();
    if (files!=null) {
      for (File file : files) {
        file.delete();
      }
    }
    
    if (_res==null) return;
  
    for (int i = 0; i < _res.length; i++) {
      File velFile = new File(dirVelFilter, "filter_piv" + PivUtils.formatOn4Chars(i + 1) + ".dat");
      new PivInstantFilteredWriter().write(_res[i], velFile, _prog);
  
      File scalFile = new File(dirVelScal, "scal_piv" + PivUtils.formatOn4Chars(i + 1) + ".dat");
      new PivInstantScalWriter().write(_res[i], scalFile, _prog);
    }
  }

  /**
   * Sauve les resultats moyenn�s dans l'espace r�el
   * @param prj_ Le projet
   * @param _res Les resultats a sauver
   * @param _prog L'interface de progression.
   */
  public void saveAverageResults(PivProject prj_, PivResultsI _res, ProgressionInterface _prog) {

    File velFile=new File(prj_.getOutputDir(), "average_vel.out");
    velFile.delete();
    File scalFile=new File(prj_.getOutputDir(), "average_scal.out");
    scalFile.delete();

    if (_res != null) {
      new PivAverageVelWriter().write(_res, velFile, _prog);
      new PivAverageScalWriter().write(_res, scalFile, _prog);
    }
  }
}
