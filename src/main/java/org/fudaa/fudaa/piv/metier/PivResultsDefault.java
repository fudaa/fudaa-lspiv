package org.fudaa.fudaa.piv.metier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import com.memoire.fu.FuLog;

import gnu.trove.TDoubleArrayList;

/**
 * Un resultat scalaire ou vecteur aux points issus du calcul par PIV.
 * Les valeurs sont stock�es dans les tableaux correspondants.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivResultsDefault implements PivResultsI {
  
  /** Le type de chaque r�sultat. Utilis� pour l'ajout d'un point avec ses valeurs dans l'ordre. */
  private ArrayList<ResultType> types=new ArrayList<>();
  /** Les coordonn�es X des points */
  private ArrayList<Double> x=new ArrayList<Double>();
  /** Les coordonn�es Y des points */
  private ArrayList<Double> y=new ArrayList<Double>();
  /** Les valeurs scalaires pour chaque point (ou global au resultat) */
  private HashMap<ResultType, TDoubleArrayList> vals=new HashMap<>();
  
  /**
   * Constructeur
   * @param _types Les natures des r�sultats stock�s.
   */
  public PivResultsDefault(ResultType[] _types) {
    setResults(_types);
  }
    
  /**
   * Indique la nature des r�sultats stock�s.
   * @param _types Les natures de r�sultats stock�s.
   */
  private void setResults(ResultType[] _types) {
    types.clear();
    types.addAll(Arrays.asList(_types));
    
    for (ResultType type : _types) {
      if (type.isVector()) {
        vals.put(type, new TDoubleArrayList());
      }
      else {
        vals.put(type, new TDoubleArrayList(new double[1]));
      }
    }
  }
  
  /**
   * @return La nature des r�sultats stock�s.
   */
  @Override
  public ResultType[] getResults() {
    return types.toArray(new ResultType[0]);
  }
  
  /**
   * Verifie que les r�sultats contient bien la nature de r�sultat donn�
   * @param _tpRes La nature de r�sultat.
   * @return True si cette nature est stock�e.
   */
  @Override
  public boolean hasResult(ResultType _tpRes) {
    return vals.keySet().contains(_tpRes);
  }
  
  /**
   * Ajoute un point, avec ses r�sultats.
   * @param _x La coordonn�e X du point
   * @param _y La coordonn�e Y du point
   * @param _vals Les valeurs de r�sultats dans l'ordre pour ce point.
   */
  public void addPoint(double _x, double _y, double[] _vals) {
    if (_vals.length!=types.size()) {
      FuLog.error("Program error : bad size _res table");
      return;
    }
    
    x.add(_x);
    y.add(_y);    
    
    for (int i=0; i<_vals.length; i++) {
      // Type vectoriel
      if (types.get(i).isVector()) {
        vals.get(types.get(i)).add(_vals[i]);
      }
      // Type scalaire
      else {
        vals.get(types.get(i)).set(0, _vals[i]);
      }
    }
  }

  /**
   * Supprime un point de r�sultat
   * @param _ind L'indice du point
   */
  public void removePoint(int _ind) {
    if (_ind < 0 || _ind >= getNbPoints())
      return;

    x.remove(_ind);
    y.remove(_ind);

    for (int i=0; i < types.size(); i++) {
      // Type vectoriel
      if (types.get(i).isVector()) {
        vals.get(types.get(i)).remove(_ind);
      }
    }
  }
  
  /**
   * Ajoute une nature de r�sultats. Les valeurs pour ce r�sultats sont par defaut egales � 0.
   * @param _tpRes La nouvelle nature.
   */
  public void addResult(ResultType _tpRes) {
    if (types.contains(_tpRes))
      return;
    
    types.add(_tpRes);
    if (_tpRes.isVector()) {
      vals.put(_tpRes, new TDoubleArrayList(new double[x.size()]));
    }
    else {
      vals.put(_tpRes, new TDoubleArrayList(new double[1]));
    }
  }
  
  /**
   * Supprime une nature de resultat 
   * @param _tpRes La nature a supprimer.
   */
  public void removeResult(ResultType _tpRes) {
    types.remove(_tpRes);
    vals.remove(_tpRes);
  }
  
  /**
   * @return Le nombre de points
   */
  @Override
  public int getNbPoints() {
    return x.size();
  }
  
  /**
   * @param _ind L'indice du point
   * @return La coordonn�e X du point d'indice donn�.
   */
  @Override
  public double getX(int _ind) {
    return x.get(_ind);
  }
  
  /**
   * @param _ind L'indice du point
   * @return La coordonn�e Y du point d'indice donn�.
   */
  @Override
  public double getY(int _ind) {
    return y.get(_ind);
  }
  
  /**
   * @param _ind L'indice du point
   * @param _tpRes Le type du resultat.
   * @return La valeur pour le resultat et le point d'indice donn�.
   */
  @Override
  public double getValue(int _ind, ResultType _tpRes) {
    if (_tpRes.isVector())
      return vals.get(_tpRes).get(_ind);
    else
      return vals.get(_tpRes).get(0);
  }
  
  @Override
  public double[] getValues(ResultType _tpRes) {
    return vals.get(_tpRes).toNativeArray();
  }

  @Override
  public void setValue(int _ind, ResultType _tpRes, double _val) {
    if (!hasResult(_tpRes))
      return;
    
    if (_tpRes.isVector()) {
      if (_ind >= vals.get(_tpRes).size())
        return;

      vals.get(_tpRes).set(_ind, _val);
    }
    else {
      vals.get(_tpRes).set(0, _val);
    }
  }
}
