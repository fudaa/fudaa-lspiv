package org.fudaa.fudaa.piv.metier;

import org.fudaa.ctulu.CtuluVariable;

/**
 * Une interface de stockage des resultats par nature. Les r�sultats peuvent �tre vectoriels, ou scalaires (1 seule valeur pour tous les points).
 * Les r�sultats sont �ditables.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public interface PivResultsI {
  
  /**
   * Un type de r�sultats.
   * @author Bertrand Marchand
   */
  public static enum ResultType implements CtuluVariable {
    VX(true),
    VY(true),
    NORME(true),
    CORREL(true),
    OMEGA(true),
    DIVERG(true),
    /** Mode de calcul des vitesses sur les transect : Extrapol�e si valeur 1, calcul�e si 0. */
    COMPUTE_MODE(true),
    /** D�bit total */
    FULL_DISCHARGE(false),
    /** D�bit mesur� */
    MEASURED_DISCHARGE(false),
    /** Coefficient calcul� */
    COMPUTED_VEL_COEFFICIENT(false),
    WETTED_AREA(false),
    MEAN_VELOCITY(false),
    WATER_ELEVATION(false);
    
    private boolean isvector_;
    
    /**
     * Constructeur
     * @param _isVector True : Le type est vectoriel
     */
    private ResultType(boolean _isVector) {
      isvector_=_isVector;
    }

    @Override
    public Object getCommonUnit() {
      return null;
    }

    @Override
    public String getID() {
      return this.toString();
    }

    @Override
    public String getName() {
      return this.toString();
    }

    @Override
    public String getLongName() {
      return this.toString();
    }
    
    public boolean isVector() {
      return isvector_;
    }
  }

  /**
   * Verifie que les r�sultats contient bien la nature de r�sultat donn�
   * @param _tpRes La nature de r�sultat.
   * @return True si cette nature est stock�e.
   */
  public abstract boolean hasResult(ResultType _tpRes);
  
  /**
   * @return La nature des r�sultats stock�s.
   */
  public ResultType[] getResults();

  /**
   * @return Le nombre de points
   */
  public abstract int getNbPoints();

  /**
   * @param _ind L'indice du point
   * @return La coordonn�e X du point d'indice donn�.
   */
  public abstract double getX(int _ind);

  /**
   * @param _ind L'indice du point
   * @return La coordonn�e Y du point d'indice donn�.
   */
  public abstract double getY(int _ind);

  /**
   * @param _ind L'indice du point
   * @param _tpRes Le type du resultat.
   * @return La valeur pour le resultat et le point d'indice donn�. Si le resultat est scalaire, la valeur retourn�e est toujours la m�me.
   */
  public abstract double getValue(int _ind, ResultType _tpRes);

  /**
   * Definit la valeur pour un point donn� pour une type de r�sultat.
   * @param _ind L'indice du point
   * @param _tpRes Le type du resultat.
   * @param _val La valeur pour le resultat et le point d'indice donn�. Si le resultat est scalaire, la valeur est remplac�e.
   */
  public abstract void setValue(int _ind, ResultType _tpRes, double _val);
  
  /**
   * @param _tpRes Le type de r�sultat.
   * @return Les valeurs pour tous les points dans l'ordre pour le type demand�. Si le resultat est scalaire, le tableau est de longeur 1.
   */
  public abstract double[] getValues(ResultType _tpRes);

}