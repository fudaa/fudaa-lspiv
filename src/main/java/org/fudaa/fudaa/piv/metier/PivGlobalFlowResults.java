package org.fudaa.fudaa.piv.metier;

import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;

/**
 * Une classe pour englober la totalit� des r�sultats de d�bit.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivGlobalFlowResults {
  PivResultsI[] res_;
  int cpt_;

  /**
   * Cr�ation d'un r�sultat global.
   * @param _res Les r�sultats individuels. Peut �tre <code>null</code>
   */
  public PivGlobalFlowResults(PivResultsI[] _res) {
    res_=_res;
  }
  
  /**
   * @return La coordonn�e X du point d'index donn�e.
   */
  public double getX(int _i) {
    PivResultsI res=getRes(_i);
    if (res!=null) return res.getX(_i-cpt_);
    return -1;
  }

  /**
   * @return La coordonn�e Y du point d'index donn�e.
   */
  public double getY(int _i) {
    PivResultsI res=getRes(_i);
    if (res!=null) return res.getY(_i-cpt_);
    return -1;
  }

  /**
   * @return La vitesse Vx du point d'index donn�e.
   */
  public double getVx(int _i) {
    PivResultsI res=getRes(_i);
    if (res!=null) return res.getValue(_i-cpt_,ResultType.VX);
    return -1;
  }

  /**
   * @return La vitesse Vy du point d'index donn�e.
   */
  public double getVy(int _i) {
    PivResultsI res=getRes(_i);
    if (res!=null) return res.getValue(_i-cpt_,ResultType.VY);
    return -1;
  }
  
  public double getComputeMode(int _i) {
    PivResultsI res=getRes(_i);
    if (res!=null) 
      return res.getValue(_i-cpt_,ResultType.COMPUTE_MODE);
    
    return -1;
  }
  
  /**
   * @return Le nombre de points de v�locit�.
   */
  public int getNombre() {
    cpt_=0;
    if (res_ != null) {
      for (PivResultsI res : res_) {
        cpt_+=res.getNbPoints();
      }
    }
    return cpt_;
  }
  
  private PivResultsI getRes(int _i) {
    cpt_=0;
    if (res_ != null) {
      for (PivResultsI res : res_) {
        if (_i < cpt_+res.getNbPoints())
          return res;
        cpt_+=res.getNbPoints();
      }
    }
    return null;
  }

  /**
   * @param _tpRes La nature de resultat
   * @return La valeur moyenne pour tous les resultats, ou null si la nature du resultat n'est pas scalaire ou pas de resultats.
   */
  public Double getAverageScalarResult(ResultType _tpRes) {
    if (_tpRes.isVector() || res_==null)
      return null;
    
    double ret=0;

    for (PivResultsI res : res_) {
      ret+=res.getValue(-1,_tpRes);
    }

    ret/=res_.length;
    return ret;
  }
}
