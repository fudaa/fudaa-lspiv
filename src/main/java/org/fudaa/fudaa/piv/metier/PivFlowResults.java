/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.piv.metier;

import java.util.Arrays;

/**
 * Les r�sultats d'un calcul de d�bit.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivFlowResults implements PivResultsI {
  /** Le debit */
  protected double discharge;
  /** La surface mouill�e */
  protected double wettedArea;
  /** La v�locit� moyenne. */
  protected double meanVelocity;
  /** Le niveau d'eau */
  protected double waterElevation;
  /** Le d�bit mesur�. Peut �tre NaN si la mesure n'est pas calcul�e (Fudaa-LSPIV < 1.5.0). */
  protected double measuredDischarge = Double.NaN;
  /** Le coefficient unique calcul�. Peut �tre NaN si la mesure n'est pas calcul�e (Fudaa-LSPIV < 1.7.2). */
  protected double computedVelCoefficient = Double.NaN;
  /** Les coordonn�es X des points */
  protected double[] x;
  /** Les coordonn�es Y des points */
  protected double[] y;
  /** Les vitesses suivant Vx */
  protected double[] vx;
  /** Les vitesses suivant Vy */
  protected double[] vy;
  /** Mode de calcul : Si 1, la vitesse est extrapol�e. 0 : Calcul�e */
  protected double[] computeMode;

  /**
   * @return Le debit
   */
  public double getDischarge() {
    return discharge;
  }

  /**
   * D�finit le d�bit
   * @param _discharge Le d�bit
   */
  public void setDischarge(double _discharge) {
    this.discharge=_discharge;
  }

  /**
   * @return La surface mouill�e
   */
  public double getWettedArea() {
    return wettedArea;
  }

  /**
   * D�finit la surface mouill�e
   * @param _wettedArea La surface mouill�e
   */
  public void setWettedArea(double _wettedArea) {
    this.wettedArea=_wettedArea;
  }

  /**
   * @return La v�locit� moyenne
   */
  public double getMeanVelocity() {
    return meanVelocity;
  }

  /**
   * D�finit la v�locit� moyenne
   * @param _meanVelocity La v�locit� moyenne
   */
  public void setMeanVelocity(double _meanVelocity) {
    this.meanVelocity=_meanVelocity;
  }

  /**
   * @return Le niveau d'eau.
   */
  public double getWaterElevation() {
    return waterElevation;
  }

  /**
   * Definit le niveau d'eau.
   * @param _waterElevation Le niveau d'eau.
   */
  public void setWaterElevation(double _waterElevation) {
    this.waterElevation=_waterElevation;
  }

  /**
   * @return La coordonn�e X du point d'index donn�e.
   */
  public double getX(int _i) {
    return x[_i];
  }

  /**
   * @return La coordonn�e Y du point d'index donn�e.
   */
  public double getY(int _i) {
    return y[_i];
  }

  /**
   * @return La vitesse Vx du point d'index donn�e.
   */
  public double getVx(int _i) {
    return vx[_i];
  }

  /**
   * @return La vitesse Vy du point d'index donn�e.
   */
  public double getVy(int _i) {
    return vy[_i];
  }
  
  /**
   * @return Le nombre de points de v�locit�.
   */
  public int getNombre() {
    return vy.length;
  }
  
  /**
   * Definit les coordonn�es X des points de r�sultats
   * @param _x Les coordonn�es X
   */
  public void setX(double[] _x) {
    x=_x;
  }
  
  /**
   * Definit les coordonn�es Y des points de r�sultats
   * @param _y Les coordonn�es Y
   */
  public void setY(double[] _y) {
    y=_y;
  }
  
  /**
   * Definit les vitesses X des points de r�sultats
   * @param _vx Les vitesses X
   */
  public void setVx(double[] _vx) {
    vx=_vx;
  }
  
  /**
   * Definit les vitesses Y des points de r�sultats
   * @param _vy Les vitesses Y
   */
  public void setVy(double[] _vy) {
    vy=_vy;
  }
  
  /**
   * Definit le caract�re extrapol� ou calcul� des vitesses.
   * @param _computeMode Si 1 : Extrapol�e, 0 : Calcul�e.
   */
  public void setComputeMode(double[] _computeMode) {
    computeMode = _computeMode;
  }

  @Override
  public boolean hasResult(ResultType _tpRes) {
    return Arrays.asList(getResults()).contains(_tpRes);
  }

  @Override
  public ResultType[] getResults() {
    return new ResultType[]{ResultType.VX, ResultType.VY, ResultType.COMPUTE_MODE, ResultType.FULL_DISCHARGE,
                            ResultType.WETTED_AREA, ResultType.MEAN_VELOCITY, ResultType.WATER_ELEVATION, 
                            ResultType.MEASURED_DISCHARGE, ResultType.COMPUTED_VEL_COEFFICIENT};
  }

  @Override
  public int getNbPoints() {
    return getNombre();
  }

  @Override
  public double getValue(int _ind, ResultType _tpRes) {
    if (ResultType.VX.equals(_tpRes))
      return getVx(_ind);
    else if (ResultType.VY.equals(_tpRes))
      return getVy(_ind);
    else if (ResultType.COMPUTE_MODE.equals(_tpRes))
      return computeMode[_ind];
    else if (ResultType.FULL_DISCHARGE.equals(_tpRes))
      return discharge;
    else if (ResultType.WETTED_AREA.equals(_tpRes))
      return wettedArea;
    else if (ResultType.MEAN_VELOCITY.equals(_tpRes))
      return meanVelocity;
    else if (ResultType.WATER_ELEVATION.equals(_tpRes))
      return waterElevation;
    else if (ResultType.MEASURED_DISCHARGE.equals(_tpRes))
      return measuredDischarge;
    else if (ResultType.COMPUTED_VEL_COEFFICIENT.equals(_tpRes))
      return computedVelCoefficient;
    else
      return -1;
  }

  @Override
  public double[] getValues(ResultType _tpRes) {
    if (ResultType.VX.equals(_tpRes))
      return vy;
    else if (ResultType.VY.equals(_tpRes))
      return vy;
    else if  (ResultType.COMPUTE_MODE.equals(_tpRes))
      return computeMode;
    else if (ResultType.FULL_DISCHARGE.equals(_tpRes))
      return new double[]{discharge};
    else if (ResultType.WETTED_AREA.equals(_tpRes))
      return new double[]{wettedArea};
    else if (ResultType.MEAN_VELOCITY.equals(_tpRes))
      return new double[]{meanVelocity};
    else if (ResultType.WATER_ELEVATION.equals(_tpRes))
      return new double[]{waterElevation};
    else if (ResultType.MEASURED_DISCHARGE.equals(_tpRes))
      return new double[]{measuredDischarge};
    else if (ResultType.COMPUTED_VEL_COEFFICIENT.equals(_tpRes))
      return new double[]{computedVelCoefficient};
    else
      return null;
  }

  @Override
  public void setValue(int _ind, ResultType _tpRes, double _val) {
    if (!hasResult(_tpRes))
      return;
    
    if (_tpRes.isVector() && _ind>=vx.length)
      return;
    
    if (ResultType.VX.equals(_tpRes))
      vx[_ind] = _val;
    else if (ResultType.VY.equals(_tpRes))
      vy[_ind] = _val;
    else if (ResultType.COMPUTE_MODE.equals(_tpRes))
      computeMode[_ind] = _val;
    else if (ResultType.FULL_DISCHARGE.equals(_tpRes))
      discharge = _val;
    else if (ResultType.WETTED_AREA.equals(_tpRes))
      wettedArea = _val;
    else if (ResultType.MEAN_VELOCITY.equals(_tpRes))
      meanVelocity = _val;
    else if (ResultType.WATER_ELEVATION.equals(_tpRes))
      waterElevation = _val;
    else if (ResultType.MEASURED_DISCHARGE.equals(_tpRes))
      measuredDischarge = _val;
    else if (ResultType.COMPUTED_VEL_COEFFICIENT.equals(_tpRes)) {
      computedVelCoefficient = _val;
    }
  }
}
