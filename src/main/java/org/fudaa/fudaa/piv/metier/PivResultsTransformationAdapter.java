package org.fudaa.fudaa.piv.metier;

import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * Une classe pour retourner des résultats avec transformation ou non des coordonnées.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivResultsTransformationAdapter implements PivResultsI {
  GrMorphisme mat_;
  PivResultsI res_;
  /** Cache localisation des points */
//  GrPoint[] locationPts_;
  /** Cache localisation des extremités de vecteurs */
//  GrPoint[] extPts_;

  public PivResultsTransformationAdapter(PivResultsI _res, GrMorphisme _mat) {
    mat_=_mat;
    res_=_res;
//    locationPts_=new GrPoint[_res.getNbPoints()];
//    extPts_=new GrPoint[_res.getNbPoints()];
  }
  
  private GrPoint getLocation(int _ind) {
    GrPoint pt;
//    if ((pt=locationPts_[_ind])==null) {
      pt=new GrPoint(res_.getX(_ind),res_.getY(_ind),0);
      pt.autoApplique(mat_);
//      locationPts_[_ind]=pt;
//    }
    return pt;
  }
  
  private GrPoint getExt(int _ind) {
    GrPoint pt;
//    if ((pt=extPts_[_ind])==null) {
      pt=new GrPoint(res_.getX(_ind)+res_.getValue(_ind,ResultType.VX),res_.getY(_ind)+res_.getValue(_ind,ResultType.VY),0);
      pt.autoApplique(mat_);
      GrPoint ptOrig = getLocation(_ind);
      pt.setCoordonnees(pt.x_-ptOrig.x_, pt.y_-ptOrig.y_, pt.z_-ptOrig.z_);
//      extPts_[_ind]=pt;
//    }
    return pt;
  }
  
  @Override
  public boolean hasResult(ResultType _tpRes) {
    return res_.hasResult(_tpRes);
  }

  @Override
  public ResultType[] getResults() {
    return res_.getResults();
  }

  @Override
  public int getNbPoints() {
    return res_.getNbPoints();
  }

  @Override
  public double getX(int _ind) {
    return getLocation(_ind).x_;
  }

  @Override
  public double getY(int _ind) {
    return getLocation(_ind).y_;
  }
  
  @Override
  public double getValue(int _ind, ResultType _tpRes) {
    if (_tpRes.equals(ResultType.VX)) {
      return getExt(_ind).x_;
    }
    else if (_tpRes.equals(ResultType.VY)) {
      return getExt(_ind).y_;
    }
    else {
      return res_.getValue(_ind, _tpRes);
    }
  }

  @Override
  public double[] getValues(ResultType _tpRes) {
    if (_tpRes.equals(ResultType.VX)) {
      double[] val=new double[getNbPoints()];
      for (int i=0; i<getNbPoints(); i++) {
        val[i]=getExt(i).x_;
      }
      return val;
    }
    else if (_tpRes.equals(ResultType.VY)) {
      double[] val=new double[getNbPoints()];
      for (int i=0; i<getNbPoints(); i++) {
        val[i]=getExt(i).y_;
      }
      return val;
    }
    else {
      return res_.getValues(_tpRes);
    }
  }

  /** Non implementé */
  @Override
  public void setValue(int _ind, ResultType _tpRes, double _val) {
  }
}
