package org.fudaa.fudaa.piv.metier;

import java.util.ArrayList;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;

/**
 * Une classe permettant de stocker les points d'un transect. Un transect est une
 * droite constitu�e de plusieurs points 3D avec des param�tres de calcul associ�s
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivTransect implements Cloneable {
  /** Les param�tres du transect */
  protected PivTransectParams params=new PivTransectParams();
  /** La droite du transect */
  protected GrPolyligne transect;
  /** Les coefficients de vitesse pour chaque point. Un coef � -1 signifie que le coeff n'est pas pris en compte pour le point */
  protected double[] ptsCoefs = new double[0];
  
  
  @Override
  public PivTransect clone() {
    try {
      PivTransect o=(PivTransect)super.clone();
      o.setParams(new PivTransectParams(getParams()));
      o.setStraight(getStraight().clone());
      
      if (getCoefs() != null)
        o.setCoefs(Arrays.copyOf(getCoefs(), getCoefs().length));
      
      return o;
    }
    // Ne doit pas se produire.
    catch (CloneNotSupportedException e) {
      throw new InternalError();
    }
  }

  /**
   * Retourne la droite du transect.
   * @return Le transect
   */
  public GrPolyligne getStraight() {
    return transect;
  }

  /**
   * Definit la droite du transect
   * @param _line Le transect
   */
  public void setStraight(GrPolyligne _line) {
    this.transect = _line;
    ptsCoefs = Arrays.copyOf(ptsCoefs, transect.nombre());
  }
  
  public void setParams(PivTransectParams _params) {
    this.params=_params;
  }
  
  public PivTransectParams getParams() {
    return this.params;
  }
  
  public void setCoefs(double[] _coefs) {
    ptsCoefs = Arrays.copyOf(_coefs, transect.nombre());
  }
  
  public double[] getCoefs() {
    return ptsCoefs;
  }
  
  /**
   * Ajoute un point au transecrt.
   * @param _ind L'index auquel le point est ins�r�. Si l'index est sup�rieur au nombre de points, le point est ajout� en fin de transect.
   * @param _pt Le point.
   * @param _coef Le coefficient. Si le tableau des coefs existe, le coef est ajout�. Si le coef est null, le coef est initialis� � 0. Si le tableau
   * des coefs est inexistant, la valeur n'est pas utilis�e.
   */
  public void addPoint(int _ind, GrPoint _pt, Double _coef) {
    if (_ind < 0 || _ind >= transect.sommets_.nombre()) {
      transect.sommets_.ajoute(_pt);
      
      if (ptsCoefs != null) {
        ptsCoefs = Arrays.copyOf(ptsCoefs, ptsCoefs.length + 1);
        ptsCoefs[ptsCoefs.length - 1] = _coef == null ? 0 : _coef;
      }
    }
    else {
      transect.sommets_.insere(_pt, _ind);
      
      if (ptsCoefs != null) {
        double[] tmpCoefs = new double[ptsCoefs.length + 1];
        System.arraycopy(ptsCoefs, 0, tmpCoefs, 0, _ind);
        System.arraycopy(ptsCoefs, _ind, tmpCoefs, _ind + 1, ptsCoefs.length - _ind);
        tmpCoefs[_ind] = _coef == null ? 0 : _coef;
        ptsCoefs = tmpCoefs;
      }
    }
  }
  
  /**
   * Remplace le point d'indice donn�.
   * @param _ind L'indice dans le transect.
   * @param _pt Le nouveau point.
   * @param _coef Le coef a replacer. Si null, le coef n'est pas remplac�.
   */
  public void replacePoint(int _ind, GrPoint _pt, Double _coef) {
    if (_ind < 0 || _ind >= transect.sommets_.nombre())
      return;
    
    transect.sommets_.remplace(_pt, _ind);
    
    if (ptsCoefs != null && _coef != null) {
      ptsCoefs[_ind] = _coef;
    }
  }
  
  public void removePoint(int _ind) {
    if (_ind < 0 || _ind >= transect.sommets_.nombre())
      return;
    
    transect.sommets_.enleve(_ind);
    
    if (ptsCoefs != null) {
      double[] tmpCoefs = new double[ptsCoefs.length - 1];
      System.arraycopy(ptsCoefs, 0, tmpCoefs, 0, _ind);
      System.arraycopy(ptsCoefs, _ind + 1, tmpCoefs, _ind, ptsCoefs.length - _ind - 1);
      ptsCoefs = tmpCoefs;
    }
  }
  
  public void clearPoints() {
    transect.sommets_.vide();
    if (ptsCoefs != null) {
      ptsCoefs = new double[0];
    }
  }
  
  /**
   * Inverse le transect.
   */
  public void invertGeometry() {
    transect.inverse();
    
    if (ptsCoefs != null) {
      CtuluLibArray.invert(ptsCoefs, 0);
    }
  }
  
  /**
   * Recr�e un transect depuis les r�sultats calcul�s et le transect d'origine. Lors du calcul,
   * les points sont r�align�s. La bathy et le nombre de points n'est pas modifi�e.
   * @param _res Les r�sultats.
   * @param _trans Le transect original, avec points non align�s.
   * @return Le transect recalcul�.
   */
  public static PivTransect buildFromAlignResults(PivFlowResults _res, PivTransect _trans) {
    PivTransect trans=new PivTransect();
    
    GrPolyligne pl=new GrPolyligne();
    for (int i=0; i<_res.getNombre(); i++) {
      pl.sommets_.ajoute(_res.getX(i), _res.getY(i), _trans.getStraight().sommet(i).z_);
    }
    trans.transect=pl;
    trans.params=_trans.params;
    if (_trans.ptsCoefs != null)
      trans.ptsCoefs = Arrays.copyOf(_trans.ptsCoefs, _trans.ptsCoefs.length);
    
    return trans;
  }
}
