package org.fudaa.fudaa.piv.metier;

import java.util.ArrayList;
import java.util.List;

import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;

/**
 * Une classe pour le stockage des param�tres de stabilisation
 * @author Bertrand Marchand (marchand@detacad.fr)
 */
public class PivStabilizationParameters {
  
  /**
   * Les densit�s de points.
   * Attention, l'index des valeurs d'enum correspond est utilis� dans les fichiers.
   * @author Bertrand Marchand (marchand@detacad.fr)
   *
   */
  public enum StabilizationPointsDensity {
    LOW,
    MEDIUM,
    HIGH;
    
    public static StabilizationPointsDensity ordinal(int _i) {
      if (_i == 0)
        return LOW;
      else if (_i == 1)
        return MEDIUM;
      else if (_i == 2)
        return HIGH;
      
      throw new IllegalArgumentException();
    }
  }
  
  /**
   * Les modeles de transformation
   * Attention, l'index des valeurs d'enum correspond est utilis� dans les fichiers.
   * @author Bertrand Marchand (marchand@detacad.fr)
   *
   */
  public enum StabilizationTransformationModel {
    PROJECTIVE,
    SIMILARITY;
    
    public static StabilizationTransformationModel ordinal(int _i) {
      if (_i == 0)
        return PROJECTIVE;
      else if (_i == 1)
        return SIMILARITY;
      
      throw new IllegalArgumentException();
    }
  }
  
  /** True : Le module de stabilisation est actif. */
  protected boolean isActive_ = false;
  /** true : Les zones sont des zones d'�coulement. false : Ce sont des zones fixes */
  protected boolean isFlowAreas_ = true;
  /** Les contours des zones */
  protected List<GrPolygone> outlines_ = new ArrayList<>();
  /** La densit� des points d'int�r�ts images*/
  protected StabilizationPointsDensity density_ = StabilizationPointsDensity.MEDIUM;
  /** Le modelete de tranformation */
  protected StabilizationTransformationModel model_ = StabilizationTransformationModel.SIMILARITY;
  
  public PivStabilizationParameters() {}
  
  public PivStabilizationParameters(PivStabilizationParameters _params) {
    if (_params == null) {
      return;
    }
    
    isActive_ = _params.isActive_;
    isFlowAreas_ = _params.isFlowAreas_;
    density_ = _params.density_;
    model_ = _params.model_;
    
    for (GrPolygone paramOutline : _params.outlines_) {
      GrPolygone outline = new GrPolygone();
      for (int i=0; i<paramOutline.nombre(); i++) {
        outline.sommets_.ajoute(new GrPoint(paramOutline.sommet(i)));
      }
      outlines_.add(outline);
    }
  }

  public boolean isFlowAreas() {
    return isFlowAreas_;
  }

  public void setFlowAreas(boolean isFlowAreas_) {
    this.isFlowAreas_ = isFlowAreas_;
  }

  public List<GrPolygone> getOutlines() {
    return outlines_;
  }

  public void setOutlines(List<GrPolygone> outlines) {
    this.outlines_ = outlines;
  }

  public StabilizationPointsDensity getDensity() {
    return density_;
  }

  public void setDensity(StabilizationPointsDensity density) {
    this.density_ = density;
  }

  public StabilizationTransformationModel getModel() {
    return model_;
  }

  public void setModel(StabilizationTransformationModel model) {
    this.model_ = model;
  }
  
  public boolean isActive() {
    return isActive_;
  }

  public void setActive(boolean isActive) {
    this.isActive_ = isActive;
  }

  
  @Override
  public boolean equals(Object _o) {
    if (!(_o instanceof PivStabilizationParameters)) return false;
    
    PivStabilizationParameters o=(PivStabilizationParameters)_o;
    
    if (o.isActive_ != isActive_ || o.density_ != density_ || o.model_ != model_ || o.isFlowAreas_ != isFlowAreas_ || o.outlines_.size() != outlines_.size())
      return false;
    
    for (int i=0; i<outlines_.size(); i++) {
      if (!outlines_.get(i).equals(o.outlines_.get(i)))
        return false;
    }
    
    return true;
  }
}
