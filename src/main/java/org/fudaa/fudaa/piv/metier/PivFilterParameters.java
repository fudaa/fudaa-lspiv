package org.fudaa.fudaa.piv.metier;

import org.fudaa.fudaa.piv.utils.PivUtils;

/**
 * Les param�tres de filtre pour le post calcul PIV.
 */
public class PivFilterParameters {
  protected boolean isCorrelationFilterEnabled;
  /** La correlation minimale */
  protected double minCorrelation;
  /** La correlation maximale */
  protected double maxCorrelation;
  protected boolean isVelocityFilterEnabled;
  /** Valeur min des normes de vitesse */
  protected double smin;
  /** Valeur max des normes de vitesse */
  protected double smax;
  /** Valeur min composante Vx */
  protected double vxmin;
  /** Valeur max composante Vx */
  protected double vxmax;
  /** Valeur min composante Vy */
  protected double vymin;
  /** Valeur max composante Vy */
  protected double vymax;
  protected boolean isMedianTestFilterEnabled;
  protected double epsilon;
  protected double r0min;
  protected boolean isMedianTestAdvancedFilterEnabled;
  protected double distmax;
  protected int nneighbor;
  protected boolean isCorrelationPeakFilterEnabled;
  protected double rcut;
  protected double rhomax;
  protected boolean isVelocityDistributionFilterEnabled;
  protected int nstdvel;
  protected boolean isVelocityDispersionFilterEnabled;
  protected double covmax;
  protected boolean isAngularDispersionFilterEnabled;
  protected double circvarmax;

  public PivFilterParameters() {
    resetAllFilters();
  }

  public PivFilterParameters(PivFilterParameters _o) {
    if (_o == null) {
      return;
    }

    isCorrelationFilterEnabled = _o.isCorrelationFilterEnabled;
    minCorrelation = _o.minCorrelation;
    maxCorrelation = _o.maxCorrelation;
    isVelocityFilterEnabled = _o.isVelocityFilterEnabled;
    smin = _o.smin;
    smax = _o.smax;
    vxmin = _o.vxmin;
    vxmax = _o.vxmax;
    vymin = _o.vymin;
    vymax = _o.vymax;
    isMedianTestFilterEnabled = _o.isMedianTestFilterEnabled;
    epsilon = _o.epsilon;
    r0min = _o.r0min;
    isMedianTestAdvancedFilterEnabled = _o.isMedianTestAdvancedFilterEnabled;
    distmax = _o.distmax;
    nneighbor = _o.nneighbor;
    isCorrelationPeakFilterEnabled = _o.isCorrelationPeakFilterEnabled;
    rcut = _o.rcut;
    rhomax = _o.rhomax;
    isVelocityDistributionFilterEnabled = _o.isVelocityDistributionFilterEnabled;
    nstdvel = _o.nstdvel;
    isVelocityDispersionFilterEnabled = _o.isVelocityDispersionFilterEnabled;
    covmax = _o.covmax;
    isAngularDispersionFilterEnabled = _o.isAngularDispersionFilterEnabled;
    circvarmax = _o.circvarmax;
  }

  /**
   * Intialise les filtres a leur valeur par defaut.
   */
  public void resetAllFilters() {
    resetVelocityFilter();
    resetCorrelationFilter();
    resetMedianTestFilter();
    resetCorrelationPeakFilter();
    resetVelocityDistributionFilter();
    resetVelocityDispersionFilter();
    resetAngularDispersionFilter();
  }

  public void resetVelocityFilter() {
    isVelocityFilterEnabled = true;
    smin = 0;
    smax = PivUtils.FORTRAN_DOUBLE_MAX;
    vxmin = PivUtils.FORTRAN_DOUBLE_MIN;
    vxmax = PivUtils.FORTRAN_DOUBLE_MAX;
    vymin = PivUtils.FORTRAN_DOUBLE_MIN;
    vymax = PivUtils.FORTRAN_DOUBLE_MAX;
  }

  public void resetCorrelationFilter() {
    isCorrelationFilterEnabled = true;
    minCorrelation = 0.4;
    maxCorrelation = 1;
  }

  public void resetMedianTestFilter() {
    isMedianTestFilterEnabled = true;
    epsilon = 0.1;
    r0min = 2.0;
    resetMedianTestAdvancedFilter();
  }
  
  public void resetMedianTestAdvancedFilter() {
    nneighbor = 8;
    distmax = 10.0;
  }

  public void resetCorrelationPeakFilter() {
    isCorrelationPeakFilterEnabled = true;
    rcut = 0.8;
    rhomax = 0.5;
  }

  public void resetVelocityDistributionFilter() {
    isVelocityDistributionFilterEnabled = true;
    nstdvel = 3;
  }

  public void resetVelocityDispersionFilter() {
    isVelocityDispersionFilterEnabled = false;
    covmax = 0.4;
  }

  public void resetAngularDispersionFilter() {
    isAngularDispersionFilterEnabled = true;
    circvarmax = 0.25;
  }

  public void setVelocityFilterEnabled(boolean isVelocityFilterEnabled) {
    this.isVelocityFilterEnabled = isVelocityFilterEnabled;
  }

  /**
   * @return True si les vitesses sont filtr�es
   */
  public boolean isVelocityFilterEnabled() {
    return isVelocityFilterEnabled;
  }

  public void setCorrelationFilterEnabled(boolean isCorrelationFilterEnabled) {
    this.isCorrelationFilterEnabled = isCorrelationFilterEnabled;
  }

  /**
   * @return True si la correlation est filtr�e
   */
  public boolean isCorrelationFilterEnabled() {
    return isCorrelationFilterEnabled;
  }

  public boolean isMedianTestFilterEnabled() {
    return isMedianTestFilterEnabled;
  }

  public void setMedianTestFilterEnabled(boolean isMedianTestEnabled) {
    this.isMedianTestFilterEnabled = isMedianTestEnabled;
  }

  public boolean isMedianTestAdvancedFilterEnabled() {
    return isMedianTestAdvancedFilterEnabled;
  }

  public void setMedianTestFilterAdvancedEnabled(boolean isMedianTestAdvancedFilterEnabled) {
    this.isMedianTestAdvancedFilterEnabled = isMedianTestAdvancedFilterEnabled;
  }

  public double getEpsilon() {
    return epsilon;
  }

  public void setEpsilon(double epsilon) {
    this.epsilon = epsilon;
  }

  public double getR0min() {
    return r0min;
  }

  public void setR0min(double r0min) {
    this.r0min = r0min;
  }

  public double getDistmax() {
    return distmax;
  }

  public void setDistmax(double distmax) {
    this.distmax = distmax;
  }

  public int getNneighbor() {
    return nneighbor;
  }

  public void setNneighbor(int nneighbor) {
    this.nneighbor = nneighbor;
  }

  public boolean isCorrelationPeakFilterEnabled() {
    return isCorrelationPeakFilterEnabled;
  }

  public void setCorrelationPeakFilterEnabled(boolean isCorrelationPeakEnabled) {
    this.isCorrelationPeakFilterEnabled = isCorrelationPeakEnabled;
  }

  public double getRcut() {
    return rcut;
  }

  public void setRcut(double rcut) {
    this.rcut = rcut;
  }

  public double getRhomax() {
    return rhomax;
  }

  public void setRhomax(double rhomax) {
    this.rhomax = rhomax;
  }

  public boolean isVelocityDistributionFilterEnabled() {
    return isVelocityDistributionFilterEnabled;
  }

  public void setVelocityDistributionFilterEnabled(boolean isVelocityDistributionEnabled) {
    this.isVelocityDistributionFilterEnabled = isVelocityDistributionEnabled;
  }

  public int getNstdvel() {
    return nstdvel;
  }

  public void setNstdvel(int nstdvel) {
    this.nstdvel = nstdvel;
  }

  public boolean isVelocityDispersionFilterEnabled() {
    return isVelocityDispersionFilterEnabled;
  }

  public void setVelocityDispersionFilterEnabled(boolean isVelocityDispersionEnabled) {
    this.isVelocityDispersionFilterEnabled = isVelocityDispersionEnabled;
  }

  public double getCovmax() {
    return covmax;
  }

  public void setCovmax(double covmax) {
    this.covmax = covmax;
  }

  public boolean isAngularDispersionFilterEnabled() {
    return isAngularDispersionFilterEnabled;
  }

  public void setAngularDispersionFilterEnabled(boolean isAngularDispersionEnabled) {
    this.isAngularDispersionFilterEnabled = isAngularDispersionEnabled;
  }

  public double getCircvarmax() {
    return circvarmax;
  }

  public void setCircvarmax(double circvarmax) {
    this.circvarmax = circvarmax;
  }

  /**
   * @return the minCorrelation
   */
  public double getMinCorrelation() {
    return minCorrelation;
  }

  /**
   * @param minCorrelation the minCorrelation to set
   */
  public void setMinCorrelation(double minCorrelation) {
    this.minCorrelation = minCorrelation;
  }

  /**
   * @return the maxCorrelation
   */
  public double getMaxCorrelation() {
    return maxCorrelation;
  }

  /**
   * @param maxCorrelation the maxCorrelation to set
   */
  public void setMaxCorrelation(double maxCorrelation) {
    this.maxCorrelation = maxCorrelation;
  }

  /**
   * @return the smin
   */
  public double getSmin() {
    return smin;
  }

  /**
   * @param smin the smin to set
   */
  public void setSmin(double smin) {
    this.smin = smin;
  }

  /**
   * @return the smax
   */
  public double getSmax() {
    return smax;
  }

  /**
   * @param smax the smax to set
   */
  public void setSmax(double smax) {
    this.smax = smax;
  }

  /**
   * @return the vmin
   */
  public double getVxmin() {
    return vxmin;
  }

  /**
   * @param vmin the vmin to set
   */
  public void setVxmin(double vmin) {
    this.vxmin = vmin;
  }

  /**
   * @return the vmax
   */
  public double getVxmax() {
    return vxmax;
  }

  /**
   * @param vmax the vmax to set
   */
  public void setVxmax(double vmax) {
    this.vxmax = vmax;
  }

  /**
   * @return the vmin
   */
  public double getVymin() {
    return vymin;
  }

  /**
   * @param vmin the vmin to set
   */
  public void setVymin(double vmin) {
    this.vymin = vmin;
  }

  /**
   * @return the vmax
   */
  public double getVymax() {
    return vymax;
  }

  /**
   * @param vmax the vmax to set
   */
  public void setVymax(double vmax) {
    this.vymax = vmax;
  }

  @Override
  public boolean equals(Object _o) {
    if (!(_o instanceof PivFilterParameters))
      return false;
    PivFilterParameters o = (PivFilterParameters) _o;

    return isCorrelationFilterEnabled == o.isCorrelationFilterEnabled && maxCorrelation == o.maxCorrelation && minCorrelation == o.minCorrelation
        && isVelocityFilterEnabled == o.isVelocityFilterEnabled && smax == o.smax && smin == o.smin && vxmax == o.vxmax && vxmin == o.vxmin && vymax == o.vymax && vymin == o.vymin
        && isMedianTestFilterEnabled == o.isMedianTestFilterEnabled && epsilon == o.epsilon && r0min == o.r0min
        && isMedianTestAdvancedFilterEnabled == o.isMedianTestAdvancedFilterEnabled && nneighbor == o.nneighbor && distmax == o.distmax
        && isCorrelationPeakFilterEnabled == o.isCorrelationPeakFilterEnabled && rcut == o.rcut && rhomax == o.rhomax
        && isVelocityDistributionFilterEnabled == o.isVelocityDistributionFilterEnabled && nstdvel == o.nstdvel
        && isVelocityDispersionFilterEnabled == o.isVelocityDispersionFilterEnabled && covmax == o.covmax
        && isAngularDispersionFilterEnabled == o.isAngularDispersionFilterEnabled && circvarmax == o.circvarmax;
  }

}