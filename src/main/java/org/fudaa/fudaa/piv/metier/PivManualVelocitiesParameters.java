package org.fudaa.fudaa.piv.metier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.fudaa.ebli.geometrie.GrPoint;

/**
 * Les parametres pour un calcul manuel des vitesses.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivManualVelocitiesParameters {
  /** La liste de couples de points de d�placement. Le premier point est l'origine, le deuxieme la destination */
  protected List<GrPoint[]> displacementImgPoints = new ArrayList<>();
  /** Les indexes des 2 images associ�es. */
  protected List<Integer[]> imgIndexes = new ArrayList<>();

  public PivManualVelocitiesParameters() {
  }

  /**
   * Cr�ation par copie
   * 
   * @param _params Les parametres a copier.
   */
  public PivManualVelocitiesParameters(PivManualVelocitiesParameters _params) {
    if (_params == null) {
      return;
    }

    for (GrPoint[] pts : _params.displacementImgPoints) {
      GrPoint[] ptsCopy = new GrPoint[pts.length];
      for (int i = 0; i < pts.length; i++) {
        ptsCopy[i] = new GrPoint(pts[i]);
      }
      displacementImgPoints.add(ptsCopy);
    }
  }

  public void setDisplacementImgPoints(List<GrPoint[]> _couples) {
    this.displacementImgPoints = _couples;
  }

  public List<GrPoint[]> getDisplacementImgPoints() {
    return displacementImgPoints;
  }

  public void setImagesIndexes(List<Integer[]> _imgIndexes) {
    imgIndexes = _imgIndexes;
  }

  public List<Integer[]> getImagesIndexes() {
    return imgIndexes;
  }

  public int getNbCouplePoints() {
    return displacementImgPoints.size();
  }
  
  /**
   * @return True : Si les deplacements sont d�finis.
   */
  public boolean isActive() {
    return displacementImgPoints.size() != 0;
  }
  
  /**
   * Change la periode d'�chantillonnage. Les indexes des images sont recalcul�s, � l'indice le plus proche.
   * @param oldPeriod L'ancienne periode.
   * @param newPeriod La nouvelle periode.
   */
  public void changeUnderSamplingPeriod(int oldPeriod, int newPeriod) {
    for (Integer[] imgInds : imgIndexes) {
      for (int i = 0; i < imgInds.length; i++) {
        imgInds[i] = (int) ((imgInds[i] * oldPeriod + 0.5) / newPeriod);
      }
    }
  }

  public boolean equals(Object _o) {
    if (!(_o instanceof PivManualVelocitiesParameters))
      return false;
    PivManualVelocitiesParameters o = (PivManualVelocitiesParameters) _o;

    if (imgIndexes.size() != o.imgIndexes.size())
      return false;
    
    for (int i=0; i<imgIndexes.size(); i++) {
      if (!Arrays.equals(imgIndexes.get(i), o.imgIndexes.get(i)))
        return false;
    }
    
    if (displacementImgPoints.size() != o.displacementImgPoints.size())
      return false;
    
    for (int i=0; i<displacementImgPoints.size(); i++) {
      if (!Arrays.equals(displacementImgPoints.get(i), o.displacementImgPoints.get(i)))
        return false;
    }

    return true;
  }
}
