/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivTransect;
import org.fudaa.fudaa.piv.metier.PivTransectParams;

/**
 * Un panneau de saisie des param�tres des transects s�lectionn�s pour le calcul des d�bits.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivTransectParamPanel extends CtuluDialogPanel {

  PivImplementation impl_;
  PivTransect[] savedTrans_;
  PivTransect[] trans_;
  int[] selIds_;
  private PivTransectParamSubPanel pnParams_;

  /**
   * Constructeur.
   */
  public PivTransectParamPanel(PivImplementation _impl) {
    impl_=_impl;
    customize();
    
    trans_=impl_.getCurrentProject().getTransects();
    // Sauvegarde des transects pr�cedents pour le cas d'annulation.
    savedTrans_=new PivTransect[trans_.length];
    for (int i=0; i<savedTrans_.length; i++)
      savedTrans_[i]=trans_[i].clone();
  }

  private void customize() {
    this.setLayout(new BorderLayout());
    pnParams_ = new PivTransectParamSubPanel(this, impl_);
    this.add(pnParams_, BorderLayout.CENTER);
  }
  
  /**
   * Change selection
   * @param _selIds Les transects s�lectionn�s.
   */
  public void setSelected(int... _selIds) {
    selIds_=_selIds;
    ArrayList<PivTransectParams> params = new ArrayList<PivTransectParams>();
    
    PivTransect[] transects = impl_.getCurrentProject().getTransects();
    for (int isel = 0; isel<_selIds.length; isel++) {
      params.add(transects[_selIds[isel]].getParams());
    }
    
    pnParams_.setFlowParams(params.toArray(new PivTransectParams[0]));
  }
  
  /**
   * Recup�re les param�tres saisis par l'utilisateur. Ces param�tres sont
   * retourn�s dans les params transects.
   */
  private void retrieveFlowParams(PivTransectParams[] _params) {
    PivTransectParams params = pnParams_.getFlowParams();
    
    for (int id : selIds_) {
      Double val;
      Integer ival;
      if ((val=params.getSurfaceCoef()) != null)
        _params[id].setSurfaceCoef(val);
      if ((val=params.getRadiusX()) != null)
        _params[id].setRadiusX(val);
      if ((val=params.getRadiusY()) != null)
        _params[id].setRadiusY(val);
      if ((val=params.getInterpolationStep()) != null)
        _params[id].setInterpolationStep(val);
      if ((ival=params.getMaxPoints()) != null)
        _params[id].setMaxPoints(ival);
    }
  }

  @Override
  public boolean isDataValid() {
    return pnParams_.isDataValid();
  }
  
  @Override
  public boolean apply() {
    PivProject prj = impl_.getCurrentProject();
    PivTransect[] trans=prj.getTransects();
    
    PivTransectParams[] originalParams = new PivTransectParams[trans.length];
    PivTransectParams[] params = new PivTransectParams[trans.length];
    for (int i = 0; i<params.length; i++) {
      originalParams[i] = trans[i].getParams();
      params[i] = new PivTransectParams(trans[i].getParams());
    }
    
    retrieveFlowParams(params);
    
    // Retour si pas de modifications
    if (Arrays.equals(originalParams, params)) {
      return true;
    }

    // Suppression des resultats si confirmation.
    if (prj.hasFlowResults() && !impl_.question(PivResource.getS("Suppression des r�sultats"), PivResource.getS(
        "Attention : des r�sultats existent et seront supprim�s si vous modifiez les param�tres.\nVoulez-vous continuer ?"))) {
      return false;
    }
    
    prj.setFlowResults(null);

    for (int i = 0; i < params.length; i++) {
      trans[i].setParams(params[i]);
    }

    for (int id : selIds_) {
      CtuluLog log=new CtuluLog();
      log.setDesc(PivResource.getS("Reconditionnement du transect"));
      PivExeLauncher.instance().computeTransectRecond(log, impl_.getCurrentProject(), trans[id], null);
      if (log.containsErrorOrSevereError()) {
        impl_.error(log.getResume());
        return true;
      }
    }
    
    // Pour forcer le rafraichissement d'�cran
    impl_.getCurrentProject().setTransects(trans);
    // Conserve la selection pour l'affichage des cercles.
    impl_.get2dFrame().getVisuPanel().getRealView().getTransectLayer().setSelection(selIds_);

    return true;
  }
  
  

  @Override
  public boolean cancel() {
    impl_.getCurrentProject().setTransects(savedTrans_);
    // Conserve la selection pour l'affichage des cercles.
    impl_.get2dFrame().getVisuPanel().getRealView().getTransectLayer().setSelection(selIds_);
    
    return super.cancel();
  }

  @Override
  public boolean ok() {
    return super.ok();
  }
}
