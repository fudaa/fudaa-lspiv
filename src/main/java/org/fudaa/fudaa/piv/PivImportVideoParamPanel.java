/*
 * @creation 8 sept. 06
 * @modification $Date: 2009-06-03 15:10:45 +0200 (mer., 03 juin 2009) $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.io.PivSamplingVideoParamReader;
import org.fudaa.fudaa.piv.metier.PivProject.SRC_IMAGE_TYPE;
import org.fudaa.fudaa.piv.metier.PivSamplingVideoParameters;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.bu.BuGridLayout;


/**
 * Un panneau pour l'affichage des param�tres dimport vid�o.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivImportVideoParamPanel extends CtuluDialogPanel {
  
  /**
   * Le panneau de previsialisation.
   * @author marchand
   */
  public class PreviewPanel extends JPanel {
    
    public class ImagePreviewPanel extends JPanel {
      BufferedImage img;
      
      public ImagePreviewPanel() {
        setOpaque(false);
      }
      
      public void setImage(BufferedImage _image) {
        img = _image;
        repaint();
      }

      @Override
      public void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (img != null) {
          Insets insets = getInsets();
          Rectangle r = g.getClipBounds();
          r.x += insets.left;
          r.y += insets.top;
          r.width -= (insets.left+insets.right);
          r.height -= (insets.bottom+insets.top);
              
          double pnRatio = r.getWidth() / r.getHeight();
          double imgRatio = (double)img.getWidth() / (double)img.getHeight();

          int scaledWidth;
          int scaledHeight;
          
          // L'image est coll�e dans le sens horizontal
          if (pnRatio > imgRatio) {
            scaledHeight = r.height;
            scaledWidth = (int) Math.round(scaledHeight * imgRatio);
          }
          // L'image est coll�e dans le sens vertical
          else {
            scaledWidth = r.width;
            scaledHeight = (int) Math.round(scaledWidth / imgRatio);
          }

          img.getScaledInstance(scaledWidth, scaledHeight, BufferedImage.SCALE_DEFAULT);
          g.drawImage(img, r.x + (r.width - scaledWidth) / 2, r.y + (r.height - scaledHeight) / 2, scaledWidth, scaledHeight, null);
        }
      }
    }

    private ImagePreviewPanel pnImagePreview_;
    private JSlider slPreview_;
    private JLabel lbPreviewInfo_;
    private File previewDir;
    
    public PreviewPanel() {
      lbPreviewInfo_ = new JLabel("");

      slPreview_ = new JSlider();
      slPreview_.setMinimum(0);
      slPreview_.setValue(0);
      slPreview_.addChangeListener(e -> {
        int ind = slPreview_.getValue();
        setImageIndex(ind);
      });

      setBorder(new TitledBorder(PivResource.getS("Pr�visualisation")));
      setLayout(new BorderLayout());

      pnImagePreview_ = new ImagePreviewPanel();
      pnImagePreview_.setPreferredSize(new Dimension(300, 300));

      add(pnImagePreview_, BorderLayout.CENTER);

      JPanel pnSliderPreview = new JPanel();
      pnSliderPreview.setLayout(new BorderLayout());
      pnSliderPreview.add(slPreview_, BorderLayout.CENTER);
      pnSliderPreview.add(lbPreviewInfo_, BorderLayout.SOUTH);

      add(pnSliderPreview, BorderLayout.SOUTH);
    }
    
    /**
     * Iniialise la pr�visualisation, avec l'image 1 s'il y en a une.
     */
    public void initPreview() {
      previewDir=getPreviewTempDirectory(); 

      if (previewDir.exists()) {
        // Le nombre d'images pourrait etre recup�r� de la vid�o, mais ca ne semble pas
        // correct, voire parfoi inexistant sur certains formats.
        nbVideoFrames_ = previewDir.list().length;
      }
      else {
        nbVideoFrames_ = 0;
      }

      slPreview_.setMaximum(nbVideoFrames_ - 1);
      slPreview_.setValue(0);
    }
    
    public int getNbVideoFrames() {
      return nbVideoFrames_;
    }
    
    /**
     * @param _imageIndex L'index de l'image, commencant � 0.
     */
    public void setImageIndex(int _imageIndex) {
      previewDir=getPreviewTempDirectory();     
      
      File imgFile = new File(previewDir, "image"+PivUtils.formatOn4Chars(_imageIndex + 1)+".png");
      if (imgFile.exists()) {
        try {
          BufferedImage img = ImageIO.read(imgFile);
          pnImagePreview_.setImage(img);
          lbPreviewInfo_.setText(PivResource.getS("Image : {0}/{1}, Temps : {2} s", _imageIndex + 1, nbVideoFrames_, CtuluLib.getDecimalFormat().format(imageIndex2Time(_imageIndex))));
          slPreview_.setValue(_imageIndex);
        }
        catch (IOException e) {
        }
      }
      else {
        pnImagePreview_.setImage(null);
        lbPreviewInfo_.setText("");
        slPreview_.setValue(0);
      }
    }
    
    public int getImageIndex() {
      return slPreview_.getValue();
    }
  }
  
  private PivImplementation impl_;
  /** Le file chooser de la vid�o. */
  private CtuluFileChooserPanel fcVideo_;
  private CtuluDialog diProgress_;
  private JTextField tfInitialTimeStep_;
  JTextField tfInitialNbImgSec_;
  private JTextField tfInitialXSize_;
  private JTextField tfInitialYSize_;
  private PreviewPanel pnPreview_;
  /** Le panneau des parametres de sampling */
  private PivImportVideoSamplingSubPanel pnSampling_;
  private boolean isVideoFileLoaded_ = false;
  public int widthVideo_ = 0;
  public int heightVideo_ = 0;
  /** Le nombre d'images de la vid�o (approximatif avant le chargement de la preview), puis exact apr�s le chargement */
  private int nbVideoFrames_;
  private JButton btImportParams_;
  /** Le file chooser pour le fichier des parametres */
  private CtuluFileChooser fcVideoParams_;

  public PivImportVideoParamPanel(PivImplementation _impl) {
    impl_=_impl;
    
    // Wrap, sinon, la fenetre est trop large.
    String text = PivUtils.wrapMessage("<html>"+
        PivResource.getS("Les images import�es remplaceront les images existantes. Pour modifier le type des images (calcul/fond), utilisez le menu Gestion des images sources apr�s l'import.<p><p>")+
        PivResource.getS("<u>Intervalle de temps</u><br>L'intervalle de temps entre images successives doit �tre suffisamment long pour que les traceurs de l'�coulement se d�placent d'au moins 10-15 pixels, mais suffisamment court pour que les motifs restent semblables d'une image � la suivante (et donc pour que les niveaux de corr�lation soient bons).")+
        "</html>", 132);
    setHelpText(text);
    
    setBorder(BorderFactory.createEmptyBorder(5, 5, 15, 5));
    setLayout(new BorderLayout(10, 10));
    
    JPanel pnVideo = buildVideoPanel();
    pnSampling_ = new PivImportVideoSamplingSubPanel(this, impl_);
    
    JPanel pnNorth = new JPanel();
    pnNorth.setLayout(new BorderLayout());
    pnNorth.add(pnVideo, BorderLayout.CENTER);
    pnNorth.add(pnSampling_, BorderLayout.EAST);
    
    JPanel pnParamsFile = new JPanel();
    pnParamsFile.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
    pnParamsFile.add(new JLabel(PivResource.getS("Les param�tres vid�o peuvent �tre charg�s depuis un fichier")));
    btImportParams_ = new JButton();
    btImportParams_.setText(PivResource.getS("Charger..."));
    btImportParams_.addActionListener((evt) -> selectAndLoadVideoParamsFile());
    
    pnParamsFile.add(btImportParams_);
    add(pnParamsFile, BorderLayout.SOUTH);
    add(pnNorth, BorderLayout.CENTER);

    setSamplingVideoParameters(_impl.getCurrentProject().getSamplingParameters());
  }
  
  /**
   * Construction de tous les composants du panneau vid�o.
   */
  protected JPanel buildVideoPanel() {
    fcVideo_=new CtuluFileChooserPanel(PivResource.getS("Fichier vid�o")) {
      
      // Apr�s la lecture du fichier, on lit le frame rate.
      protected void fileChooserAccepted(final CtuluFileChooser _fc){
        loadVideoFile(_fc.getSelectedFile());
      }
    };
    
    fcVideo_.setFileSelectMode(JFileChooser.FILES_ONLY);
    fcVideo_.setWriteMode(false);
    JPanel pnDirExport=CtuluFileChooserPanel.buildPanel(fcVideo_, PivResource.getS("Fichier vid�o:"));
    
    tfInitialNbImgSec_ = new JTextField();
    tfInitialNbImgSec_.setColumns(10);
    tfInitialNbImgSec_.setEditable(false);
    tfInitialTimeStep_ = new JTextField();
    tfInitialTimeStep_.setColumns(10);
    tfInitialTimeStep_.setEditable(false);
    tfInitialXSize_ = new JTextField();
    tfInitialXSize_.setColumns(10);
    tfInitialXSize_.setEditable(false);
    tfInitialYSize_ = new JTextField();
    tfInitialYSize_.setColumns(10);
    tfInitialYSize_.setEditable(false);
    
    JPanel pnVideo = new JPanel();
    pnVideo.setLayout(new BorderLayout(5,5));
    pnVideo.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(PivResource.getS("Vid�o")), BorderFactory.createEmptyBorder(5, 3, 5, 3)));
    
    pnVideo.add(pnDirExport, BorderLayout.NORTH);

    pnPreview_ = new PreviewPanel();
    pnVideo.add(pnPreview_, BorderLayout.CENTER);
    
    JPanel pnVideoProps = new JPanel();
    pnVideoProps.setLayout(new BuGridLayout(2, 5, 5, true, false));
    pnVideoProps.add(new JLabel(PivResource.getS("Nombre d'images par seconde:")));
    pnVideoProps.add(tfInitialNbImgSec_);
    pnVideoProps.add(new JLabel(PivResource.getS("Intervalle de temps")+" (s):"));
    pnVideoProps.add(tfInitialTimeStep_);
        
    JPanel pnDimensions = new JPanel();
    pnDimensions.setLayout(new BuGridLayout(4, 5, 5, true, false));
    pnDimensions.add(new JLabel(PivResource.getS("X:")));
    pnDimensions.add(tfInitialXSize_);
    pnDimensions.add(new JLabel(PivResource.getS("Y:")));
    pnDimensions.add(tfInitialYSize_);
    
    pnVideoProps.add(new JLabel(PivResource.getS("Dimensions:")));
    pnVideoProps.add(pnDimensions);
    
    pnVideo.add(pnVideoProps, BorderLayout.SOUTH);
    
    return pnVideo;
  }
  
  public PreviewPanel getPreviewPanel() {
    return pnPreview_;
  }
    
  /**
   * Selectionne et charge les param�tres vid�o
   */
  private void selectAndLoadVideoParamsFile() {

    // Selection du fichier
    if (fcVideoParams_ == null) {
      fcVideoParams_=new CtuluFileChooser(true);
      fcVideoParams_.setAcceptAllFileFilterUsed(true);
      fcVideoParams_.setFileFilter(PivUtils.FILE_FLT_USER_EXTRACT);
      // Pas terrible, mais evite de redonner le nom manuellement.
      fcVideoParams_.setSelectedFile(new File(PivUtils.FILE_FLT_USER_EXTRACT.getFirstExt()));
      fcVideoParams_.setMultiSelectionEnabled(false);
      fcVideoParams_.setDialogTitle(PivResource.getS("S�lection du fichier des param�tres vid�o"));
    }
    
    if (fcVideoParams_.showOpenDialog(impl_.getFrame()) == JFileChooser.CANCEL_OPTION) {
      return;
    }
    
    File paramsFile=fcVideoParams_.getSelectedFile();

    CtuluIOResult<PivSamplingVideoParameters> ret = new PivSamplingVideoParamReader().read(paramsFile, null);
    if (ret.getAnalyze().containsErrorOrSevereError()) {
      impl_.error(ret.getAnalyze().getResume());
      return;
    }

    PivSamplingVideoParameters pars=ret.getSource();
    
    fcVideo_.setFile(pars.getVideoFile());
    loadVideoFile(pars.getVideoFile());
    setSamplingVideoParameters(pars);
  }
  
  /**
   * Charge la vid�o, ses propri�t�s et sa pr�visualisation.
   * @param _file Le fichier vid�o.
   */
  protected void loadVideoFile(File _file) {
    PivTaskAbstract r = new PivTaskAbstract(PivResource.getS("Chargement de la pr�visualisation de la vid�o")) {

      public boolean act(PivTaskObserverI _observer) {
        try {
          isVideoFileLoaded_ = false;
          clearPreviewImages();
          
          pnPreview_.initPreview();
          pnSampling_.setSamplingPanelEnabled(false);
          
          CtuluLog ana = new CtuluLog();
          ana.setDesc(getName());

          Properties props = PivExeLauncher.instance().getVideoProperties(ana, impl_.getCurrentProject(), _file, _observer);
          if (ana.containsErrorOrSevereError()) {
            impl_.error(ana.getResume());
            return false;
          }

          // Evaluation du rate frame par Javascript (pas d'equivalent Java).
          String expression = props.getProperty("avg_frame_rate");
          // Chaine sous la forme x/y
          double value;
          if (expression.indexOf("/") != -1) {
            String[] vals = expression.split("/");
            double x = Double.parseDouble(vals[0]);
            double y = Double.parseDouble(vals[1]);
            value = x/y;
          }
          else {
            value = Double.parseDouble(expression);
          }
          
//          ScriptEngineManager manager = new ScriptEngineManager();
//          ScriptEngine engine = manager.getEngineByName("javascript");
//
//          double value = Double.parseDouble(engine.eval(expression).toString());
          // On arrondit si la valeur est supp�rieure � 1. Sinon, on reste en decimales
          if (value > 1) {
            value = (int) (value + 0.5);
          }

          widthVideo_ = Integer.parseInt(props.getProperty("width"));
          heightVideo_ = Integer.parseInt(props.getProperty("height"));
          
          // Rotation possible en degr�s
          try {
            double rotation = Double.parseDouble(props.getProperty("rotation"));
            rotation += 360;
            rotation %= 180;
            if (Math.abs(rotation - 90) < 1.e6) {
              int tmpval = widthVideo_;
              widthVideo_ = heightVideo_;
              heightVideo_ = tmpval;
            }
          }
          // Pas de rotation
          catch (NullPointerException | NumberFormatException _exc) {}

          int previewWidth = 300;
          int previewHeight = (previewWidth * heightVideo_) / widthVideo_;
          
          try {
            double duration = Double.parseDouble(props.getProperty("video-duration"));
            nbVideoFrames_ = (int) (value * duration);
          }
          catch (NumberFormatException _exc) {
            nbVideoFrames_ = -1;
          }
          
          tfInitialNbImgSec_.setText(CtuluLib.getDecimalFormat().format(value));
          tfInitialTimeStep_.setText(CtuluLib.getDecimalFormat(5).format(1. / value));
          tfInitialXSize_.setText(""+widthVideo_);
          tfInitialYSize_.setText(""+heightVideo_);
          
          // Preview d'echantillonnage

          // Les images sont cr��es dans un repertoire temporaire, supprim� a la fermeture
          // de la fenetre.
          PivExeLauncher.instance().computeSampling(ana, impl_.getCurrentProject(), getVideoFile(), getPreviewTempDirectory(),
              1, nbVideoFrames_, null, null, new Dimension(previewWidth, previewHeight), _observer);
          if (ana.containsErrorOrSevereError()) {
            impl_.error(ana.getResume());
            return false;
          }

          pnPreview_.initPreview();
          
          pnSampling_.setBeginTime(imageIndex2Time(0));
          pnSampling_.setEndTime(imageIndex2Time(pnPreview_.getNbVideoFrames() - 1));
          pnSampling_.setVideoSize(new Dimension(widthVideo_, heightVideo_));
          pnSampling_.setSamplingPanelEnabled(true);
          
          isVideoFileLoaded_ = true;
          return true;
        }
        catch (/*ScriptException | */NumberFormatException exc) {
          impl_.error(PivResource.getS("Impossible de lire le nombre d'images par secondes depuis le fichier"));
          return false;
        }
      }
    };
    
    PivProgressionPanel pnProgress_=new PivProgressionPanel(r);
    diProgress_=pnProgress_.createDialog(impl_.getFrame());
    diProgress_.setOption(CtuluDialog.ZERO_OPTION);
    diProgress_.setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
    diProgress_.setTitle(r.getName());

    r.start();
    diProgress_.afficheDialogModal();
    
    // Dans le Thread Swing, sinon la mise a jour du text li� ne se fait pas.
    pnSampling_.setPeriodImgs(1);
  }

  @Override
  public boolean isDataValid() {
    File fv=fcVideo_.getFile();
    if (fv == null || !fv.exists() || !isVideoFileLoaded_) {
      setErrorText(PivResource.getS("Un fichier vid�o doit �tre s�lectionn�, �tre valide et charg�"));
      return false;
    }
    
    if (!pnSampling_.isDataValid()) {
      return false;
    }
    
    setErrorText("");
    return true;
  }
  
  protected File getVideoFile() {
    return fcVideo_.getFile();
  }
  
  /**
   * @return Le nombre d'images par secondes pour �chantillonner.
   */
  public double getFrameRate() {
    return Double.parseDouble(tfInitialNbImgSec_.getText())/pnSampling_.getPeriodImgs();
  }
  
  /**
   * @return Le nombre d'images par secondes de la vid�o.
   */
  public double getInitialFrameRate() {
    return Double.parseDouble(tfInitialNbImgSec_.getText());
  }
  
  /**
   * @param _time Le temps.
   * @return L'indice de l'image la plus proche du temps donn�.
   */
  public int time2ImageIndex(double _time) {
    return (int)Math.round(_time * getInitialFrameRate());    
  }
  
  /**
   * @param _ind L'indice de l'image dans la vid�o
   * @return Le temps correspondant � cet indice.
   */
  public double imageIndex2Time(int _ind) {
    return _ind / getInitialFrameRate();
  }

  /**
   * @return La periode de conservation des images
   */
  protected int getPeriodImgs() {
    return pnSampling_.getPeriodImgs();
  }
  
  protected Double getBeginTime() {
    return pnSampling_.getBeginTime();
  }

  protected Double getEndTime() {
    return pnSampling_.getEndTime();
  }
  
  protected Dimension getVideoSize() {
    return pnSampling_.getVideoSize();
  }
  
  public PivSamplingVideoParameters getSamplingParameters() {
    PivSamplingVideoParameters params = new PivSamplingVideoParameters();
    params.setVideoFile(getVideoFile());
    params.setSamplingSize(getVideoSize());
    params.setSamplingPeriod(getPeriodImgs());
    params.setSamplingBeginTime(getBeginTime());
    params.setSamplingEndTime(getEndTime());
    
    return params;
  }
  
  public void setSamplingVideoParameters(PivSamplingVideoParameters _params) {
    if (_params == null)
      return;
    
    fcVideo_.setFile(_params.getVideoFile());
    pnSampling_.setSamplingVideoParameters(_params);
  }

  public void clearPreviewImages() {
    File tmpOutDir = getPreviewTempDirectory();
    CtuluLibFile.deleteDir(tmpOutDir);
    tmpOutDir.mkdirs();
  }
  
  protected File getPreviewTempDirectory() {
    return new File(impl_.getCurrentProject().getTempDirectory(), "img_preview");
  }
  
  @Override
  public boolean cancel() {
    clearPreviewImages();
    return super.cancel();
  }

  @Override
  public boolean ok() {
     if (!impl_.getCurrentProject().hasSrcImages(SRC_IMAGE_TYPE.ALL) || 
         impl_.question(PivResource.getS("Images existantes"), PivResource.getS("Des images existent, elles seront remplac�es.\nSouhaitez vous continuer ?"))) {
       clearPreviewImages();
       return true;
     }
     
     return false;
  }
  
}