package org.fudaa.fudaa.piv;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteractionTargetI;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.commun.EbliSelectedChangeListener;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.piv.action.PivEnterGeometryAction;
import org.fudaa.fudaa.piv.action.PivImportStabilizationAreaAction;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters.StabilizationPointsDensity;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters.StabilizationTransformationModel;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.fu.FuLog;

/**
 * Un panneau de saisie des param�tres pour la transformation des images.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivStabilizationParamPanel extends CtuluDialogPanel {
  
  /**
   * Le modele pour la table des zones.
   * @author marchand
   */
  public class AreasTableModel extends AbstractTableModel {
    String[] colNames = new String[] { PivResource.getS("N�"), PivResource.getS("Nombre de points") };
    
    @Override
    public int getRowCount() {
      return outlines_.size();
    }

    @Override
    public int getColumnCount() {
      return 2;
    }
    
    @Override
    public String getColumnName(int _col) {
      return colNames[_col];
    }

    @Override
    public Class<?> getColumnClass(int _col) {
      if (_col == 0)
        return String.class;
      else
        return Integer.class;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (columnIndex == 0)
        return PivResource.getS("Zone") + " " + (rowIndex + 1);
      else
        return outlines_.get(rowIndex).nombre();
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return false;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }
    
    public void addArea(GrPolygone _pl) {
      outlines_.add(_pl);
      
      fireTableDataChanged();
      fireParametersChanged();
    }
    
    public void removeAreas(int... rowIndex) {
      for (int i = rowIndex.length - 1; i >= 0; i--) {
        outlines_.remove(rowIndex[i]);
      }
      
      fireTableDataChanged();
      fireParametersChanged();
    }
    
    public void setAreas(List<GrPolygone> _outlines) {
      outlines_ = _outlines;
      
      fireTableDataChanged();
      fireParametersChanged();
    }
  }
  
  /**
   * Un editeur qui r�agit � la saisie d'une forme pour la grille.
   *
   * @author Bertrand Marchand (marchand@deltacad.fr)
   * @version $Id$
   */
  class EditionController implements ZCalqueEditionInteractionTargetI {

    /**
     * Declenche le changement d'�tat du bouton fin.
     */
    public void atomicChanged() {
      GrPolyligne pl = (GrPolyligne)actAddArea_.getCalqueInteraction().getFormeEnCours().getFormeEnCours();
      ((PivSimplifiedEditionPalette)actAddArea_.getPaletteContent()).getButton(PivSimplifiedEditionPalette.BUTTON_END).setEnabled(pl != null);
    }

    public boolean addNewPolygone(GrPolygone _pg, ZEditionAttributesDataI _data) {
      if (_pg.nombre()<3) {
        impl_.error(PivResource.getS("La zone doit comporter au moins 3 sommets."));
        return false;
      }

      FuLog.debug("Nombre de points "+_pg.nombre());
      mdlAreas_.addArea(_pg);
      
      actAddArea_.hideWindow();
      return true;
    }

    /** 
     * Remet a jour la position du point centre.
     */
    public boolean addNewPoint(GrPoint _point, ZEditionAttributesDataI _data) {
//      actEnterDistance_.changeAll();
      return true;
    }

    /** Non utilis� */
    public boolean addNewRectangle(GrPoint _pt, GrPoint _pt2, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewPolyligne(GrPolyligne _pt, ZEditionAttributesDataI _data) {
      FuLog.debug("Nombre de points "+_pt.nombre());
      GrPolygone pg = new GrPolygone();
      for (int i=0; i<_pt.nombre(); i++) {
        pg.sommets_.ajoute(_pt.sommet(i));
      }
      mdlAreas_.addArea(pg);
      
      actAddArea_.hideWindow();
      return true;
    }

    /** Non utilis� */
    public boolean addNewMultiPoint(GrPolyligne _pt, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public void setMessage(String _s) {
    }

    /** Non utilis� */
    public void unsetMessage() {
    }

    /** Non utilis� */
    public void pointMove(double _x, double _y) {
    }
  }
  
  PivImplementation impl_;
  PivEnterGeometryAction actAddArea_;
  PivImportStabilizationAreaAction actImportArea_;
  // Les param�tres, restitu�s si le bouton cancel est press�.
  PivStabilizationParameters params_;
  // Les polygones stock�s temporairement.
  List<GrPolygone> outlines_ = new ArrayList<>();
  AreasTableModel mdlAreas_ = new AreasTableModel();
  
  boolean eventEnabled_ = true;

  /**
   * Constructeur.
   */
  public PivStabilizationParamPanel(PivImplementation _impl) {
    impl_=_impl;
    initComponents();
    customize();

    params_=impl_.getCurrentProject().getStabilizationParameters();
    
    PivStabilizationParameters params=new PivStabilizationParameters(params_);
    setParams(params);
  }

	private void customize() {
    // Wrap, sinon, la fenetre est trop large.
    String text = PivUtils.wrapMessage("<html><div style='font-weight:normal'>" +
        "<b>" + PivResource.getS("Densit� des points d'int�r�t pour l'alignement") + " :</b>" +
        "<br><ul>" +
        "<li>" + PivResource.getS("Choisissez Faible si les images montrent des berges avec un fort contraste et de nombreux �l�ments reconnaissables, comme des bords ou des angles") + "</li>" +
        "<li>" + PivResource.getS("Choisissez Forte si les images montrent peu d'�l�ments reconnaissables, comme la surface lisse d'une dalle de b�ton") + "</li>" +
        "</ul>" +
        "<b>" + PivResource.getS("Mod�le de stabilisation des images") + " :</b>" +
        "<br><ul>" +
        "<li>" + PivResource.getS("Faibles mouvements de cam�ra (&lt;10 pixels) : pr�f�rez le mod�le par Similitude  / Plus robuste") + "</li>" +
        "<li>" + PivResource.getS("Forts mouvements de cam�ra (&gt;10 pixels) : pr�f�rez le mod�le par Perspective / Plus sensible") + "</li>" +
        "</ul>" +
        PivResource.getS("Pour que la stabilisation donne de bons r�sultats, chaque berge de la rivi�re doit occuper au moins un quart de l'image. L'�coulement doit donc occuper environ 50% de l'image. Il est d�conseill� d'utiliser une densit� de points d'int�r�t <b>Faible</b> avec le mod�le par <b>Perspective</b>, celui-ci devenant tr�s incertain s'il y a peu de points d'int�r�t") +
        "</div></html>", 132);
    
    setHelpText(text);
	  
	  actAddArea_ = new PivEnterGeometryAction(impl_.get2dFrame().getVisuPanel(), PivResource.getS("Saisie interactive..."), PivResource.getS("D�finit la zone par saisie interactive"), true);
	  actAddArea_.setGeometryForm(DeForme.POLYGONE);
    actAddArea_.setEditionController(new EditionController());
	  btAddArea.setAction(actAddArea_);
    actAddArea_.addPropertyChangeListener(new EbliSelectedChangeListener(btAddArea));
    actImportArea_ = new PivImportStabilizationAreaAction(impl_, this);
    btImportArea.setAction(actImportArea_);
    
    rbFixedAreas.setText(PivResource.getS("Zones fixes"));
    rbFlowAreas.setText(PivResource.getS("Zones d'�coulement"));
//		btDefineFlowArea.setText(PivResource.getS("Saisie interactive..."));
		pnDensity.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Densit� des points d'int�r�t pour l'alignement")));
		rbStrongDensity.setText(PivResource.getS("<html>Forte<span style='font-weight:normal'> : si peu de points d'int�r�t sont visibles sur les berges</span></html>"));
		rbMiddleDensity.setText(PivResource.getS("Interm�diaire"));
    rbLowDensity.setText(PivResource.getS("<html>Faible<span style='font-weight:normal'> : si beaucoup de points d'int�r�t sont visibles sur les berges</span></html>"));
    pnTransfModel.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Mod�le de stabilisation des images")));
    rbSimilarity.setText(PivResource.getS("<html>Similitude<span style='font-weight:normal'> : pour de faibles mouvements de cam�ra (&lt;10 pixels)</span></html>"));
    rbProjective.setText(PivResource.getS("<html>Projective<span style='font-weight:normal'> : pour de forts mouvements de cam�ra (&gt;10 pixels)</span></html>"));
    cbActiveStabilization.setText(PivResource.getS("Activer le module de stabilisation"));
    cbActiveStabilization.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        activeStabilization(cbActiveStabilization.isSelected());
      }
    });

    btDeleteArea.setEnabled(false);
    btDeleteArea.setText(PivResource.getS("Supprimer"));
    btDeleteArea.setToolTipText(PivResource.getS("Supprime les zones s�lectionn�es"));
    btDeleteArea.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        mdlAreas_.removeAreas(tbAreas.getSelectedRows());
      }
    });
    
    tbAreas.setModel(mdlAreas_);
    tbAreas.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        btDeleteArea.setEnabled(tbAreas.getSelectedRows().length > 0);
      }
    });
    
    activeStabilization(false);
  }

	private void activeStabilization(boolean _b) {
    cbActiveStabilization.setSelected(_b);
    
	  btAddArea.setEnabled(_b);
	  btDeleteArea.setEnabled(_b && tbAreas.getSelectedRowCount() > 0);
    btImportArea.setEnabled(_b);
    rbLowDensity.setEnabled(_b);
    rbMiddleDensity.setEnabled(_b);
    rbStrongDensity.setEnabled(_b);
    rbSimilarity.setEnabled(_b);
    rbProjective.setEnabled(_b);
    rbFlowAreas.setEnabled(_b);
    rbFixedAreas.setEnabled(_b);
    tbAreas.setEnabled(_b);
    pnDensity.setEnabled(_b);
    pnTransfModel.setEnabled(_b);
	}
	
  /**
   * Rempli le panneau depuis les donn�es du projet.
   * @param _params L'objet m�tier pour l'orthorectification.
   */
  public void setParams(PivStabilizationParameters _params) {
    if (_params == null) {
      _params = new PivStabilizationParameters();
    }
    
    activeStabilization(_params != null && _params.isActive());
    
    if (_params.getDensity() == StabilizationPointsDensity.LOW)
      rbLowDensity.setSelected(true);
    else if (_params.getDensity() == StabilizationPointsDensity.MEDIUM)
      rbMiddleDensity.setSelected(true);
    else if (_params.getDensity() == StabilizationPointsDensity.HIGH)
      rbStrongDensity.setSelected(true);
    
    if (_params.getModel() == StabilizationTransformationModel.SIMILARITY)
      rbSimilarity.setSelected(true);
    else if (_params.getModel() == StabilizationTransformationModel.PROJECTIVE)
      rbProjective.setSelected(true);

    rbFlowAreas.setSelected(_params.isFlowAreas());
    
    outlines_ = _params.getOutlines();
    mdlAreas_.fireTableChanged(null);
  }
  
  public void setAreas(List<GrPolygone> _outlines) {
    mdlAreas_.setAreas(_outlines);
  }
  
  public void setFlowAreas(boolean _b) {
    if (_b)
      rbFlowAreas.setSelected(true);
    else
      rbFixedAreas.setSelected(true);
  }

  /**
   * Met a jour les parametres d'ortho rectification donn�s.
   * @param _params Les parametres d'ortho rectification.
   */
  public void retrieveParams(PivStabilizationParameters _params) {
    _params.setActive(cbActiveStabilization.isSelected());
    
    if (rbLowDensity.isSelected())
      _params.setDensity(StabilizationPointsDensity.LOW);
    else if (rbMiddleDensity.isSelected())
      _params.setDensity(StabilizationPointsDensity.MEDIUM);
    else if (rbStrongDensity.isSelected())
      _params.setDensity(StabilizationPointsDensity.HIGH);
    
    if (rbSimilarity.isSelected())
      _params.setModel(StabilizationTransformationModel.SIMILARITY);
    else if (rbProjective.isSelected())
      _params.setModel(StabilizationTransformationModel.PROJECTIVE);
    
    _params.setFlowAreas(rbFlowAreas.isSelected());
    
    // On copie le tableau, pour �tre sur que ce n'est pas celui de d�part.
    List<GrPolygone> outlines = new ArrayList<>(outlines_);
    _params.setOutlines(outlines);
  }

  @Override
  public boolean isDataValid() {
    if (cbActiveStabilization.isSelected() && outlines_.size() == 0) {
      setErrorText(PivResource.getS("Vous devez saisir au moins une zone"));
      return false;
    }
    
    setErrorText("");
    return true;
  }

  /**
   * Notifie que les param�tres ont chang� (les param�tres peuvent �tre d�grad�s, c'est a dire pas completement remplis)
   */
  public void fireParametersChanged() {
    if (!eventEnabled_)
      return;
    
    PivStabilizationParameters oldParams = impl_.getCurrentProject().getStabilizationParameters();
    PivStabilizationParameters params = new PivStabilizationParameters();
    retrieveParams(params);
    impl_.getCurrentProject().setStabilizationParameters(params);
  }

  /**
   * Surcharg� pour pouvoir pr�visualiser l'image transform�e sans sortir du 
   * dialogue.
   * @return 
   */
  @Override
  public boolean apply() {
    PivStabilizationParameters params=new PivStabilizationParameters();
    retrieveParams(params);
    
    impl_.getCurrentProject().setStabilizationParameters(params);
    
    // Pour faire disparaitre le panneau de saisie de la zone d'ecoulement.
    if (btAddArea.isSelected())
      btAddArea.doClick();
    
    return true;
  }

  @Override
  public boolean cancel() {
    // Les param�tres originaux sont restitu�s.
    impl_.getCurrentProject().setStabilizationParameters(params_);
    
    // Pour faire disparaitre le panneau de saisie de la zone d'ecoulement.
    if (btAddArea.isSelected())
      btAddArea.doClick();
    
    return true;
  }

  /** This method is called from within the constructor to
   * initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is
   * always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgDensity = new javax.swing.ButtonGroup();
        bgTransfModel = new javax.swing.ButtonGroup();
        bgAreaType = new javax.swing.ButtonGroup();
        pnDensity = new javax.swing.JPanel();
        rbStrongDensity = new javax.swing.JRadioButton();
        rbMiddleDensity = new javax.swing.JRadioButton();
        rbLowDensity = new javax.swing.JRadioButton();
        pnTransfModel = new javax.swing.JPanel();
        rbSimilarity = new javax.swing.JRadioButton();
        rbProjective = new javax.swing.JRadioButton();
        cbActiveStabilization = new javax.swing.JCheckBox();
        sepStab = new javax.swing.JSeparator();
        pnAreas = new javax.swing.JPanel();
        rbFixedAreas = new javax.swing.JRadioButton();
        rbFlowAreas = new javax.swing.JRadioButton();
        btAddArea = new javax.swing.JToggleButton();
        btImportArea = new javax.swing.JButton();
        btDeleteArea = new javax.swing.JButton();
        spAreas = new javax.swing.JScrollPane();
        tbAreas = new javax.swing.JTable();

        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        pnDensity.setBorder(javax.swing.BorderFactory.createTitledBorder("Densit� des points d'int�r�ts images pour l'alignement"));

        bgDensity.add(rbStrongDensity);
        rbStrongDensity.setText("Forte");
        rbStrongDensity.setToolTipText("");

        bgDensity.add(rbMiddleDensity);
        rbMiddleDensity.setSelected(true);
        rbMiddleDensity.setText("Moyenne");

        bgDensity.add(rbLowDensity);
        rbLowDensity.setText("Faible");

        javax.swing.GroupLayout pnDensityLayout = new javax.swing.GroupLayout(pnDensity);
        pnDensity.setLayout(pnDensityLayout);
        pnDensityLayout.setHorizontalGroup(
            pnDensityLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnDensityLayout.createSequentialGroup()
                .addGroup(pnDensityLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(rbLowDensity, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(rbMiddleDensity, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(rbStrongDensity, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnDensityLayout.setVerticalGroup(
            pnDensityLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnDensityLayout.createSequentialGroup()
                .addComponent(rbStrongDensity)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbMiddleDensity)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbLowDensity))
        );

        pnTransfModel.setBorder(javax.swing.BorderFactory.createTitledBorder("Mod�le de transformation d'images"));

        bgTransfModel.add(rbSimilarity);
        rbSimilarity.setSelected(true);
        rbSimilarity.setText("Similarit� (Rotation + Translation + Zoom)");

        bgTransfModel.add(rbProjective);
        rbProjective.setText("Projective (Projections)");

        javax.swing.GroupLayout pnTransfModelLayout = new javax.swing.GroupLayout(pnTransfModel);
        pnTransfModel.setLayout(pnTransfModelLayout);
        pnTransfModelLayout.setHorizontalGroup(
            pnTransfModelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnTransfModelLayout.createSequentialGroup()
                .addGroup(pnTransfModelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(rbSimilarity, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE)
                    .addComponent(rbProjective, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnTransfModelLayout.setVerticalGroup(
            pnTransfModelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnTransfModelLayout.createSequentialGroup()
                .addComponent(rbSimilarity)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbProjective))
        );

        cbActiveStabilization.setText("Activer le module de stabilisation");

        pnAreas.setBorder(javax.swing.BorderFactory.createTitledBorder("Saisie des zones"));

        bgAreaType.add(rbFixedAreas);
        rbFixedAreas.setSelected(true);
        rbFixedAreas.setText("Zones fixes");
        rbFixedAreas.setToolTipText("");

        bgAreaType.add(rbFlowAreas);
        rbFlowAreas.setText("Zones d'�coulement");

        btAddArea.setText("Saisie interactive...");

        btImportArea.setText("Import...");

        btDeleteArea.setText("Supprimer");

        tbAreas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"1"},
                {"2"},
                {"3"},
                {"4"}
            },
            new String [] {
                "N�"
            }
        ));
        spAreas.setViewportView(tbAreas);

        javax.swing.GroupLayout pnAreasLayout = new javax.swing.GroupLayout(pnAreas);
        pnAreas.setLayout(pnAreasLayout);
        pnAreasLayout.setHorizontalGroup(
            pnAreasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnAreasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnAreasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnAreasLayout.createSequentialGroup()
                        .addComponent(spAreas, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnAreasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btAddArea, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btDeleteArea, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btImportArea, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(pnAreasLayout.createSequentialGroup()
                        .addComponent(rbFixedAreas)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rbFlowAreas)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnAreasLayout.setVerticalGroup(
            pnAreasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnAreasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnAreasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rbFixedAreas)
                    .addComponent(rbFlowAreas))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnAreasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnAreasLayout.createSequentialGroup()
                        .addComponent(btAddArea)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btDeleteArea)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btImportArea)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(spAreas, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnDensity, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnAreas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sepStab)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cbActiveStabilization)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(pnTransfModel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cbActiveStabilization)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sepStab, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnAreas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnDensity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnTransfModel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgAreaType;
    private javax.swing.ButtonGroup bgDensity;
    private javax.swing.ButtonGroup bgTransfModel;
    private javax.swing.JToggleButton btAddArea;
    private javax.swing.JButton btDeleteArea;
    private javax.swing.JButton btImportArea;
    private javax.swing.JCheckBox cbActiveStabilization;
    private javax.swing.JPanel pnAreas;
    private javax.swing.JPanel pnDensity;
    private javax.swing.JPanel pnTransfModel;
    private javax.swing.JRadioButton rbFixedAreas;
    private javax.swing.JRadioButton rbFlowAreas;
    private javax.swing.JRadioButton rbLowDensity;
    private javax.swing.JRadioButton rbMiddleDensity;
    private javax.swing.JRadioButton rbProjective;
    private javax.swing.JRadioButton rbSimilarity;
    private javax.swing.JRadioButton rbStrongDensity;
    private javax.swing.JSeparator sepStab;
    private javax.swing.JScrollPane spAreas;
    private javax.swing.JTable tbAreas;
    // End of variables declaration//GEN-END:variables
}
