/*
 * @creation     18 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.jdesktop.swingx.VerticalLayout;

import com.memoire.bu.BuVerticalLayout;

/**
 * Un panneau pour exporter les images transform�es pgm ou autres vers un r�pertoire choisi.
 * 
 * @author marchand@deltacad.fr
 * @version $Id$
 */
public class PivExportImageFilesPanel extends CtuluDialogPanel {

  private CtuluUI ui_;
  /** Le repertoire d'export. */
  private CtuluFileChooserPanel fcDirExport_;
  /** Le mod�le pour la liste des images */
  private CtuluListEditorModel mdFiles_;
  /** La liste des images */
  private CtuluListEditorPanel pnFiles_;
  /** toutes les images */
  private JRadioButton rbAllFiles_;
  /** Une selection des images */
  private JRadioButton rbSelectedImages_;
  /** Format d'export */
  private JComboBox cbFormat_;

  /**
   * Constructeur.
   * @param _ui Le parent pour la boite de dialogue.
   * @param _originalFormatSupported 
   */
  public PivExportImageFilesPanel(CtuluUI _ui) {
    ui_=_ui;
    
    setLayout(new BorderLayout(5, 5));
    setHelpText(PivResource.getS("S�lectionnez les images que vous souhaitez exporter, puis le format d'export."));
    
    fcDirExport_=new CtuluFileChooserPanel(PivResource.getS("R�pertoire destination"));
    fcDirExport_.setFileSelectMode(JFileChooser.DIRECTORIES_ONLY);
    fcDirExport_.setWriteMode(false);
    JPanel pnDirExport=CtuluFileChooserPanel.buildPanel(fcDirExport_, PivResource.getS("R�pertoire destination:"));
    
    // Liste des images
    mdFiles_=new CtuluListEditorModel(true) {
      @Override
      public boolean isCellEditable(int _rowIndex, int _columnIndex) {
        return false;
      }
      @Override
      public Object createNewObject() {
        return null;
      }
    };
    
    pnFiles_=new CtuluListEditorPanel(mdFiles_, false, false, false, false, false) {
      @Override
      public void actionAdd() {
      }
    };
    
    pnFiles_.setValueListCellRenderer(new CtuluCellTextRenderer() {
      @Override
      public void setValue(Object _file) {
        File f=(File)_file;
        
        if (f.getName().startsWith(PivProject.BACKGROUND_IMAGE_NAME_PREFIX)) {
          setText(f.getName().substring(PivProject.BACKGROUND_IMAGE_NAME_PREFIX.length()) + " (" + PivResource.getS("Fond") + ")");
        }
        else {
          setText(f.getName());
        }
      }
    });
    pnFiles_.setPreferredSize(new Dimension(pnFiles_.getPreferredSize().width,200));

    
    rbAllFiles_=new JRadioButton(PivResource.getS("Toutes les images"));
    rbAllFiles_.addItemListener(new ItemListener() {
      
      @Override
      public void itemStateChanged(ItemEvent e) {
        pnFiles_.setAllEnabled(!rbAllFiles_.isSelected());
      }
    });
    rbSelectedImages_=new JRadioButton(PivResource.getS("Les images s�lectionn�es"));
   
    ButtonGroup bg=new ButtonGroup();
    bg.add(rbAllFiles_);
    bg.add(rbSelectedImages_);
    
    rbAllFiles_.setSelected(true);

    cbFormat_=new JComboBox();
    // 2 formats suppl�mentaires
    cbFormat_.addItem(PivResource.getS("<original>"));
    cbFormat_.addItem("pgm");
    List<String> exts=CtuluImageExport.FORMAT_LIST;
    for (String ext : exts) {
      cbFormat_.addItem(ext);
    }
    
    JPanel pnFormat=new JPanel();
    pnFormat.setLayout(new BorderLayout(3,3));
    pnFormat.add(new JLabel(PivResource.getS("Format d'export:")),BorderLayout.WEST);
    pnFormat.add(cbFormat_,BorderLayout.CENTER);
    
    JPanel pnChoice = new JPanel();
    pnChoice.setLayout(new VerticalLayout(5));
    pnChoice.add(rbAllFiles_);
    pnChoice.add(rbSelectedImages_);
    
    JPanel pnOther = new JPanel();
    pnOther.setLayout(new VerticalLayout(5));
    pnOther.add(pnFormat);
    pnOther.add(pnDirExport);
    
    add(pnChoice, BorderLayout.NORTH);
    add(pnFiles_, BorderLayout.CENTER);
    add(pnOther, BorderLayout.SOUTH);
  }
  
  @Override
  public void setValue(Object _files) {
    if (!(_files instanceof File[]))
      throw new IllegalArgumentException("bad type parameter");
    setFiles((File[])_files);
  }
  
  @Override
  public File[] getValue() {
    return getFiles();
  }
  
  /**
   * D�finit l'utilitaire � modifier.
   * @param _tool L'utilitaire
   */
  public void setFiles(File[] _files) {
    if (_files==null) return;
    
    mdFiles_.setData(_files);
  }
  
  /**
   * @return Les fichiers image.
   */
  public File[] getFiles() {
    File[] files=new File[mdFiles_.getRowCount()];
    mdFiles_.getValues(files);
    
    return files;
  }
  
  /**
   * @return Les fichiers selectionn�s de la liste, ou tous les fichiers si le bouton radio
   * "toutes images" est activ�.
   */
  public File[] getSelectedFiles() {
    if (rbAllFiles_.isSelected()) {
      return getFiles();
    }
    else {
      int[] selind=pnFiles_.getTable().getSelectedRows();
      File[] files=new File[selind.length];
      for (int i=0; i<selind.length; i++) {
        files[i]=(File)mdFiles_.getValueAt(selind[i]);
      }
      return files;
    }
  }
  
  /**
   * @return Le repertoire destination
   */
  public File getDestinationDir() {
    return fcDirExport_.getFile();
  }
  
  /**
   * @return Le format d'export. ORIGIN si le format d'origine est autoris�.
   */
  public String getFormat() {
    if (cbFormat_.getSelectedIndex()==0) {
      return "ORIGIN";
    }
    return (String)cbFormat_.getSelectedItem();
  }
  
  @Override
  public boolean isDataValid() {
    if (getSelectedFiles().length==0) {
      setErrorText(PivResource.getS("Pas d'image � transf�rer ou aucune image s�lectionn�e"));
      return false;
    }
    File dir=fcDirExport_.getFile();
    if (dir==null || !dir.exists() || !dir.canWrite()) {
      setErrorText(PivResource.getS("Le r�pertoire de destination n'est pas d�fini ou est inaccessible"));
      return false;
    }
    return true;
  }

  @Override
  public boolean apply() {
    setErrorText("");
    return super.apply();
  }

  @Override
  public boolean cancel() {
    setErrorText("");
    return super.cancel();
  }

  @Override
  public boolean ok() {
    setErrorText("");
    return super.ok();
  }
}
