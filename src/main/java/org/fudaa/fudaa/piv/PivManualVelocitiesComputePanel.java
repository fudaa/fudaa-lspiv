package org.fudaa.fudaa.piv;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.Locale;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.action.ActionsInstaller;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteraction;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteractionTargetI;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.commun.EbliSelectedChangeListener;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.piv.action.PivEnterGeometryAction;
import org.fudaa.fudaa.piv.action.PivNextImageAction;
import org.fudaa.fudaa.piv.action.PivPreviousImageAction;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.metier.PivManualVelocitiesParameters;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivStatisticParameters;

import com.memoire.fu.FuLog;

/**
 * Un panneau de saisie des couples pour les vitesses manuelles.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivManualVelocitiesComputePanel extends CtuluDialogPanel {
  
  final int COL_IND = 0;
  final int COL_J1 = 1;
  final int COL_I1 = 2;
  final int COL_J2 = 3;
  final int COL_I2 = 4;
  final int COL_IMG1 = 5;
  final int COL_IMG2 = 6;
  
  class CoupleRow {
    /** Peut �tre null si modifi� par l'utilisateur. */
    Double i1;
    /** Peut �tre null si modifi� par l'utilisateur. */
    Double j1;
    /** Peut �tre null si modifi� par l'utilisateur. */
    Double i2;
    /** Peut �tre null si modifi� par l'utilisateur. */
    Double j2;
    /** Peut �tre null quand pas saisi. */
    Integer img1;
    /** Peut �tre null quand pas saisi. */
    Integer img2;

    /**
     * @return True si tous les champs de coordonn�es image sont remplis
     */
    public boolean isImgPointsFilled() {
      return i1 != null && i2 != null && j1 != null && j2 != null;
    }

    /**
     * @return True si les champs images sont remplis
     */
    public boolean isImagesFilled() {
      return img1 != null && img2 != null;
    }
  }

  /**
   * Pour visualiser les Double de mani�re condens�e. Les autres types sont redu avec le renderer standard.
   * 
   * @author Bertrand Marchand
   */
  class CoupleTableCellRenderer extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
        int row, int column) {

      Object sval = value;
      if (value instanceof Double) {
        sval = String.format(Locale.US, "%.4G", value);
      }

      if (column == COL_IMG1 || column == COL_IMG2) {
        sval = CtuluLibFile.getSansExtension(transfImgs_[(int)value].getName()).replace("_transf","");
      }

      return super.getTableCellRendererComponent(table, sval, isSelected, hasFocus, row, column);
    }
  }

  class CoupleTableCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {

    // La checkbox n'est affich�e que s'il y a un double click dessus.
    @Override
    public boolean isCellEditable(EventObject e) {
      if (super.isCellEditable(e)) {
        if (e instanceof MouseEvent) {
          MouseEvent me = (MouseEvent) e;
          return me.getClickCount() >= 2;
        }
        if (e instanceof KeyEvent) {
          KeyEvent ke = (KeyEvent) e;
          return ke.getKeyCode() == KeyEvent.VK_F2;
        }
      }
      return false;
    }

    int imgIndex;

    @Override
    public Object getCellEditorValue() {
      return imgIndex;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      if (value instanceof Integer) {
        imgIndex = (int) value;
      }

      JComboBox<String> cbImg = new JComboBox<String>();

      for (int i = 0; i < transfImgs_.length; i++) {
        String sval = CtuluLibFile.getSansExtension(transfImgs_[i].getName()).replace("_transf","");
        cbImg.addItem(sval);
      }

      cbImg.setSelectedIndex(imgIndex);
      cbImg.addActionListener(this);

//      if (isSelected) {
//          cbImg.setBackground(table.getSelectionBackground());
//      } else {
//          cbImg.setBackground(table.getSelectionForeground());
//      }

      return cbImg;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
      JComboBox<String> cbImg = (JComboBox<String>) event.getSource();
      imgIndex = cbImg.getSelectedIndex();

      stopCellEditing();
    }

  }

  /**
   * Un modele pour les couples
   * 
   * @author Bertrand Marchand (marchand@detacad.fr)
   */
  class CoupleTableModel extends AbstractTableModel {

    List<CoupleRow> rows = new ArrayList<>();

    public CoupleTableModel() {
    }

    @Override
    public int getRowCount() {
      return rows.size();
    }

    @Override
    public int getColumnCount() {
      return 7;
    }

    @Override
    public Object getValueAt(int row, int column) {
      if (column == COL_IND) {
        return row + 1;
      }
      else if (column == COL_I1) {
        return rows.get(row).i1;
      }
      else if (column == COL_J1) {
        return rows.get(row).j1;
      }
      else if (column == COL_I2) {
        return rows.get(row).i2;
      }
      else if (column == COL_J2) {
        return rows.get(row).j2;
      }
      else if (column == COL_IMG1) {
        return rows.get(row).img1;
      }
      else {
        return rows.get(row).img2;
      }
    }

    @Override
    public String getColumnName(int column) {
      if (column == COL_IND) {
        return PivResource.getS("N�");
      }
      else if (column == COL_I1) {
        return PivResource.getS("I1");
      }
      else if (column == COL_J1) {
        return PivResource.getS("J1");
      }
      else if (column == COL_I2) {
        return PivResource.getS("I2");
      }
      else if (column == COL_J2) {
        return PivResource.getS("J2");
      }
      else if (column == COL_IMG1) {
        return PivResource.getS("Image 1");
      }
      else {
        return PivResource.getS("Image 2");
      }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
      return column!= COL_IND;
    }

    private Double getDoubleValue(Object aValue) {
      if (!aValue.toString().trim().isEmpty()) {
        // Double attendu
        try {
          return Double.parseDouble(aValue.toString().trim());
        }
        catch (NumberFormatException _exc) {
        }
      }

      return null;
    }

    private Integer getIntegerValue(Object aValue) {
      if (!aValue.toString().trim().isEmpty()) {
        // Double attendu
        try {
          return Integer.parseInt(aValue.toString().trim());
        }
        catch (NumberFormatException _exc) {
        }
      }

      return null;
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
      if (column == COL_I1) {
        rows.get(row).i1 = getDoubleValue(aValue);
      }
      else if (column == COL_J1) {
        rows.get(row).j1 = getDoubleValue(aValue);
      }
      else if (column == COL_I2) {
        rows.get(row).i2 = getDoubleValue(aValue);
      }
      else if (column == COL_J2) {
        rows.get(row).j2 = getDoubleValue(aValue);
      }
      else if (column == COL_IMG1) {
        rows.get(row).img1 = getIntegerValue(aValue);
      }
      else if (column == COL_IMG2) {
        rows.get(row).img2 = getIntegerValue(aValue);
      }

      fireTableDataChanged();

      fireParametersChanged();
    }

    public void addCouple(CoupleRow _cpl) {
      rows.add(_cpl);
      fireTableDataChanged();

      fireParametersChanged();
    }

    public void removeAllCouples() {
      rows.clear();
      fireTableDataChanged();

      fireParametersChanged();
    }

    public void removeCouple(int... rowIndex) {
      for (int i = rowIndex.length - 1; i >= 0; i--) {
        rows.remove(rowIndex[i]);
      }
      fireTableDataChanged();

      fireParametersChanged();
    }
  }

  /**
   * Un editeur qui r�agit � la saisie d'une forme pour la grille.
   *
   * @author Bertrand Marchand (marchand@deltacad.fr)
   * @version $Id$
   */
  class EditionController implements ZCalqueEditionInteractionTargetI {
    int img1Index = 0;
    int img2Index = 1;

    /** Non utilis� */
    public void atomicChanged() {

      // On notifie la palette que la forme a chang�, pour activer ou non le
      // bouton fin d'edition.
      GrPolyligne pl = (GrPolyligne) cqEdition_.getFormeEnCours().getFormeEnCours();
      if (pl == null)
        return;

      if (pl.sommets_.nombre() == 1) {
        img1Index = impl_.get2dFrame().getVisuPanel().getTransfView().getSelectedImageIndex();
      }
      if (pl.sommets_.nombre() == 2) {
        cqEdition_.endEdition();

        CoupleRow couple = new CoupleRow();
        couple.j1 = pl.sommet(0).x_;
        couple.i1 = pl.sommet(0).y_;
        couple.j2 = pl.sommet(1).x_;
        couple.i2 = pl.sommet(1).y_;

        // Et on recupere le n� de l'image affich�e
        img2Index = impl_.get2dFrame().getVisuPanel().getTransfView().getSelectedImageIndex();
        couple.img1 = img1Index;
        couple.img2 = img2Index;

        mdlPoints.addCouple(couple);

        actEnterDistance_.hideWindow();
      }
    }

    /** Non utilis� */
    public boolean addNewPolygone(GrPolygone _pg, ZEditionAttributesDataI _data) {
      return true;
    }

    /**
     * Remet a jour la position du point centre.
     */
    public boolean addNewPoint(GrPoint _point, ZEditionAttributesDataI _data) {
//      actEnterDistance_.changeAll();
      return true;
    }

    /** Non utilis� */
    public boolean addNewRectangle(GrPoint _pt, GrPoint _pt2, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewPolyligne(GrPolyligne _pt, ZEditionAttributesDataI _data) {
      FuLog.debug("Nombre de points " + _pt.nombre());
      return true;
    }

    /** Non utilis� */
    public boolean addNewMultiPoint(GrPolyligne _pt, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public void setMessage(String _s) {
    }

    /** Non utilis� */
    public void unsetMessage() {
    }

    /** Non utilis� */
    public void pointMove(double _x, double _y) {
    }
  }

  private PivImplementation impl_;
  private PivVisuPanel pnLayers_;
  private ZCalqueEditionInteractionTargetI ctrl_ = new EditionController();
  private PivEnterGeometryAction actEnterDistance_;
  private ZCalqueEditionInteraction cqEdition_;
  private CtuluTable tbCouple = new CtuluTable();
  private CoupleTableModel mdlPoints;
  /** Les parametres precedents */
  private PivManualVelocitiesParameters oldParams_;
//  private PivScalingParamPanel pnParams_;
  private File[] transfImgs_;

  /**
   * Constructeur.
   */
  public PivManualVelocitiesComputePanel(PivImplementation _impl) {
    impl_ = _impl;
    pnLayers_ = _impl.get2dFrame().getVisuPanel();
    transfImgs_ = _impl.getCurrentProject().getTransfImageFiles();

    initComponents();
    customize();

    cqEdition_ = pnLayers_.getEditionLayer();

    oldParams_ = impl_.getCurrentProject().getManualVelocitiesParameters();
    setParams(oldParams_);
  }

//  @Override
//  public boolean isDataValid() {
//    PivManualVelocitiesParameters params=new PivManualVelocitiesParameters();
//    retrieveParams(params);

  // On doit avoir au moins 1 couple de points, sinon pas de calcul.
//    if (params.getImagesIndexes().size() == 0) {
//      setErrorText(PivResource.getS("Au moins 1 couple de points doit �tre saisi"));
//      return false;
//    }
//    
//    return true;
//  }

  @Override
  public boolean apply() {
    PivProject prj = impl_.getCurrentProject();
    PivStatisticParameters statParams = prj.getStatisticParameters();

    PivManualVelocitiesParameters params = new PivManualVelocitiesParameters();
    retrieveParams(params);

    if (params.equals(oldParams_))
      return true;

    // Si des r�sultats moyens existent, et que le mode combinatoire est activ�, message pour confirmation
    if (prj.hasAverageResults() && (statParams.isManualFieldUsed())) {
      if (!impl_.question(PivResource.getS("Suppression des r�sultats"), PivResource.getS("Attention : des r�sultats existent et seront supprim�s si vous modifiez les param�tres.\nVoulez-vous continuer ?"))) {
        return false;
      }
    }

    prj.setManualVelocitiesParameters(params);

    // On supprime les r�sultats
    if (params.getNbCouplePoints() == 0) {
      // Suppression des resultats sur disque.
      prj.removeManualResultsOnFS();
      prj.setManualResults(null);
    }

    // Calcul des r�sultats
    else {
      CtuluLog ana = new CtuluLog();
      ana.setDesc(PivResource.getS("Calcul manuel des vitesses"));

      PivExeLauncher.instance().computeManualVelocities(ana, prj, null);
      if (ana.containsErrorOrSevereError()) {
        impl_.error(ana.getResume());
        return true;
      }
    }

    impl_.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);
    impl_.get2dFrame().getVisuPanel().setManualVelocitiesLayerVisible(true);
    impl_.get2dFrame().getVisuPanel().restaurer();

    return true;
  }

  @Override
  public boolean cancel() {
    boolean ret = super.cancel();
    if (ret)
      impl_.getCurrentProject().setManualVelocitiesParameters(oldParams_);

    return ret;
  }

  private void customize() {
    
    PivNextImageAction  nextAct = new PivNextImageAction(impl_);
    String nextKeyText = ActionsInstaller.getAcceleratorText(nextAct.getKey());
    getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(nextAct.getKey(), "nextAct");
    getActionMap().put("nextAct", nextAct);
    
    PivPreviousImageAction  previousAct = new PivPreviousImageAction(impl_);
    String previousKeyText = ActionsInstaller.getAcceleratorText(previousAct.getKey());
    getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(previousAct.getKey(), "previousAct");
    getActionMap().put("previousAct", previousAct);

    btDelete.setText(PivResource.getS("Supprimer"));
    btDelete.setToolTipText(PivResource.getS("Supprime les couples s�lectionn�s"));
    btDelete.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        mdlPoints.removeCouple(tbCouple.getSelectedRows());
      }
    });

    pnCouples.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Saisie des couples de points")));
    lbInfo.setText(PivResource.getS("<html><i>Saisissez le premier point sur l'image de d�but, le deuxi�me sur l'image de fin.<br>Utilisez {0} et {1} pour faire reculer/avancer les images.</i></html>", previousKeyText, nextKeyText));

    actEnterDistance_ = new PivEnterGeometryAction(pnLayers_, PivResource.getS("Saisir un couple"),
        PivResource.getS("Ajoute un couple par saisie de 2 points"), false);
    actEnterDistance_.setEditionController(ctrl_);
    btAdd.setAction(actEnterDistance_);
    actEnterDistance_.addPropertyChangeListener(new EbliSelectedChangeListener(btAdd));
//    actEnterDistance_.getCalqueInteraction().setIconModel(new TraceIconModel(TraceIcon.CARRE_PLEIN, 2, Color.black));
//    actEnterDistance_.getCalqueInteraction().setLineModel(new TraceLigneModel(TraceLigne.POINTILLE, 2.0f, Color.red));  
//    actEnterDistance_.getCalqueInteraction().setTypeTrait(TraceLigne.POINTILLE);

    spCouple.setViewportView(tbCouple);

    mdlPoints = new CoupleTableModel();
    mdlPoints.addTableModelListener(new TableModelListener() {
      @Override
      public void tableChanged(TableModelEvent e) {

//        Double average=mdlPoints.getAverageResolution();
//        if (average==null) {
//        }
//        else {
//          String val = String.format(Locale.US, "%.4G", average);
//        }
      }
    });

    tbCouple.setModel(mdlPoints);
    // Toutes les colonnes sont concern�es.
    tbCouple.setDefaultRenderer(Object.class, new CoupleTableCellRenderer());
    tbCouple.getColumnModel().getColumn(COL_IMG1).setCellEditor(new CoupleTableCellEditor());
    tbCouple.getColumnModel().getColumn(COL_IMG2).setCellEditor(new CoupleTableCellEditor());
    tbCouple.getColumnModel().getColumn(COL_IMG1).setMinWidth(100);
    tbCouple.getColumnModel().getColumn(COL_IMG2).setMinWidth(100);
    // #5754 : Blocage de fenetre si l'edition n'est pas termin�e et qu'on essai de supprimer la ligne s�lectionn�e.
    tbCouple.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

    tbCouple.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        btDelete.setEnabled(tbCouple.getSelectedRows().length > 0);
      }
    });

  }

  /**
   * Notifie que les param�tres ont chang� (les param�tres peuvent �tre d�grad�s, c'est a dire pas completement remplis)
   */
  public void fireParametersChanged() {
//    if (!eventEnabled_)
//      return;

    PivManualVelocitiesParameters params = new PivManualVelocitiesParameters();
    retrieveParams(params);
    impl_.getCurrentProject().setManualVelocitiesParameters(params);
  }

  public void setParams(PivManualVelocitiesParameters _params) {
    if (_params == null)
      return;

    List<GrPoint[]> limgPts = _params.getDisplacementImgPoints();
    List<Integer[]> limgIndexes = _params.getImagesIndexes();

    if (limgPts.size() == 0) {
    }
    else {

      mdlPoints.removeAllCouples();

      for (int i = 0; i < limgPts.size(); i++) {
        GrPoint[] imgPts = limgPts.get(i);
        Integer[] imgIndexes = limgIndexes.get(i);

        CoupleRow cpl = new CoupleRow();
        cpl.j1 = imgPts[0].x_;
        cpl.i1 = imgPts[0].y_;
        cpl.j2 = imgPts[1].x_;
        cpl.i2 = imgPts[1].y_;
        cpl.img1 = imgIndexes[0];

        // Fix BM 09/03/2023 : L'index affich� est limit� par le nombre d'images transform�es
        int nbImgTransf = impl_.getCurrentProject().getTransfImageFiles().length;
        cpl.img2 = Math.min(nbImgTransf - 1, imgIndexes[1]);

        mdlPoints.addCouple(cpl);
      }
    }

  }

  /**
   * Met a jour les parametres depuis l'interface. On suppose que la validit� des donn�es de la fenetre ont �t� test�es avant.
   * 
   * ATTENTION : Il existe un mode d�grad�, ou les donn�es ne sont pas necessairement toutes valid�es (remplies), pour l'affichage interactif des
   * couples de points pendant la saisie.
   * 
   * @param _params Les parametres.
   */
  public void retrieveParams(PivManualVelocitiesParameters _params) {
    List<GrPoint[]> limgPts = new ArrayList<>();
    List<Integer[]> limgIndexes = new ArrayList<>();

    // Donn�e par couples de points + images debut/fin
    for (CoupleRow row : mdlPoints.rows) {
      if (row.isImgPointsFilled()) {
        GrPoint[] imgPts = new GrPoint[2];
        imgPts[0] = new GrPoint(row.j1, row.i1, 0);
        imgPts[1] = new GrPoint(row.j2, row.i2, 0);
        limgPts.add(imgPts);

        Integer[] imgIndexes = new Integer[2];
        imgIndexes[0] = row.img1;
        imgIndexes[1] = row.img2;

        limgIndexes.add(imgIndexes);
      }
    }

    _params.setDisplacementImgPoints(limgPts);
    _params.setImagesIndexes(limgIndexes);
  }

  public boolean isDataValid() {
    
    for (int i=0; i<mdlPoints.rows.size(); i++) {
      CoupleRow row = mdlPoints.rows.get(i);
      if (!row.isImgPointsFilled()) {
        setErrorText(PivResource.getS("Les couples de points doivent �tre renseign�s"));
        return false;
      }
      
      // Controle que les images 1 et 2 sont pas les m�mes
      if (row.img1 == row.img2) {
        setErrorText(PivResource.getS("Couple " + (i+1) + " : Les images ne peuvent pas �tre identiques"));
        return false;
      }
    }
    
    return true;
  }
//  
//  private void setErrorText(String _error) {
//    if (_error.isEmpty()) {
//      pnParams_.setErrorText("");
//    }
//    else {
//      pnParams_.setErrorText(PivResource.getS("R�solution")+" : "+_error);
//    }
//  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgResol = new javax.swing.ButtonGroup();
        pnCouples = new javax.swing.JPanel();
        spCouple = new javax.swing.JScrollPane();
        btDelete = new javax.swing.JButton();
        btAdd = new javax.swing.JToggleButton();
        lbInfo = new javax.swing.JLabel();

        pnCouples.setBorder(javax.swing.BorderFactory.createTitledBorder("Saisie des couples de points"));

        btDelete.setText("Supprimer");

        btAdd.setText("Ajouter un couple");

        lbInfo.setText("<html>Saisissez le premier point sur l'image de d�but, le deuxi�me sur l'image de fin<br>Vous  pouvez utiliser {0} pour faire avancer les images<br>ou {1} pour faire reculer les images </html>");

        javax.swing.GroupLayout pnCouplesLayout = new javax.swing.GroupLayout(pnCouples);
        pnCouples.setLayout(pnCouplesLayout);
        pnCouplesLayout.setHorizontalGroup(
            pnCouplesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCouplesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnCouplesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnCouplesLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btAdd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btDelete))
                    .addComponent(spCouple, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnCouplesLayout.createSequentialGroup()
                        .addComponent(lbInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnCouplesLayout.setVerticalGroup(
            pnCouplesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCouplesLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(lbInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(spCouple, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnCouplesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btDelete)
                    .addComponent(btAdd))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnCouples, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnCouples, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

  private void cbCombineActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cbCombineActionPerformed
    // TODO add your handling code here:
  }// GEN-LAST:event_cbCombineActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgResol;
    private javax.swing.JToggleButton btAdd;
    private javax.swing.JButton btDelete;
    private javax.swing.JLabel lbInfo;
    private javax.swing.JPanel pnCouples;
    private javax.swing.JScrollPane spCouple;
    // End of variables declaration//GEN-END:variables
}
