package org.fudaa.fudaa.piv.layer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;
import org.fudaa.fudaa.piv.metier.PivProject;

/**
 * Un modele pour les points d'orthorectification, soit dans l'espace d'image
 * source, soit dans l'espace r�el.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivOrthoPointsModel extends ZModelePointEditable implements PivEditableModel {
  /** L'attribut utilis� pour la saisie de X r�el. L'unit� devrait �tre dans CommonUnit, mais l'affichage ne se fait pas quand c'est le cas. */
  public static final GISAttributeDouble ATT_IND_XR=new GISAttributeDouble(PivResource.getS("X r�el")+" (m)",true) {

    @Override
    public String getID() {
      return "ATT_REAL_X";
    }
  };
  /** L'attribut utilis� pour la saisie de Y r�el. L'unit� devrait �tre dans CommonUnit, mais l'affichage ne se fait pas quand c'est le cas. */
  public static final GISAttributeDouble ATT_IND_YR=new GISAttributeDouble(PivResource.getS("Y r�el")+" (m)",true) {

    @Override
    public String getID() {
      return "ATT_REAL_Y";
    }
  };
  /** L'attribut utilis� pour la saisie de X origine. */
  public static final GISAttributeDouble ATT_IND_XI=new GISAttributeDouble(PivResource.getS("X img"),true) {

    @Override
    public String getID() {
      return "ATT_IMAGE_X";
    }
  };
  /** L'attribut utilis� pour la saisie de Y origine. */
  public static final GISAttributeDouble ATT_IND_YI=new GISAttributeDouble(PivResource.getS("Y img"),true) {

    @Override
    public String getID() {
      return "ATT_IMAGE_Y";
    }
  };

  /** Le projet. */
  PivProject prj_;
  /** Le type de mod�le */
  int type_=PivVisuPanel.MODE_ORIGINAL_VIEW;
  /** Pour empecher les evenements cycliques */
  protected boolean eventEnabled_=true;

  /**
   * Construction du mod�le.
   * @param _type Le mod�le est d�fini soit dans l'espace d'image source
   * {@linkplain PivVisuPanel#MODE_ORIGINAL_VIEW}, soit dans l'espace r�el
   * {@linkplain PivVisuPanel#MODE_REAL_VIEW}.
   */
  public PivOrthoPointsModel(int _type) {
    super(new GISZoneCollectionPoint());
    
    GISAttribute[] attrs = null;
    type_=_type;
    if (type_ == PivVisuPanel.MODE_ORIGINAL_VIEW) {
      attrs = new GISAttribute[]{ATT_IND_XR, ATT_IND_YR, PivVisuPanel.ATT_IND_ZR, PivVisuPanel.ATT_LABEL};
    }
    else {
      attrs = new GISAttribute[]{PivVisuPanel.ATT_IND_ZR, ATT_IND_XI, ATT_IND_YI, PivVisuPanel.ATT_LABEL};
      getGeomData().setAttributeIsZ(PivVisuPanel.ATT_IND_ZR);
    }
    getGeomData().setAttributes(attrs, null);
  }

  @Override
  public boolean removePoint(int[] _idx, CtuluCommandContainer _cmd) {
    boolean b=super.removePoint(_idx, _cmd);
    if (!b) return b;
    
    PivOrthoPoint[] orthos=prj_.getOrthoPoints();
    List<PivOrthoPoint> lorthos=new ArrayList<PivOrthoPoint>(Arrays.asList(orthos));
    for (int i=_idx.length-1; i>=0; i--) {
      lorthos.remove(_idx[i]);
    }
    orthos=lorthos.toArray(new PivOrthoPoint[lorthos.size()]);
    prj_.setOrthoPoints(orthos);
    
    return true;
  }



  /**
   * Redefinit le projet, et remet a jour le modele
   * @param _prj Le projet associ� au mod�le.
   */
  public void setProjet(PivProject _prj) {
    prj_=_prj;
    update();
  }

  @Override
  public boolean setPoint(int _idx, double _newX, double _newY, CtuluCommandContainer _cmd) {
    boolean b=super.setPoint(_idx, _newX, _newY, _cmd);
    if (!b) return b;
    geometryChanged(_idx);
    return true;
  }

  /**
   * Met a jour le mod�le en fonction des points d'orthorectification et du rep�re.
   */
  public void update() {
    if (!eventEnabled_) return;
    eventEnabled_=false;

    try {
      GISZoneCollectionPoint zp=(GISZoneCollectionPoint) getGeomData();
      zp.removeAll(null);

      if (prj_.getOrthoPoints() != null) {
        GrMorphisme toReal=prj_.getTransformationParameters().getToReal();
        
        PivOrthoPoint[] pts=prj_.getOrthoPoints();
        
        for (int ipt=0; ipt < pts.length; ipt++) {

          // En espace image
          if (type_ == PivVisuPanel.MODE_ORIGINAL_VIEW) {
            
            GrPoint ptReal=pts[ipt].getRealPoint().applique(toReal);
            GISPoint pt=new GISPoint(pts[ipt].getImgPoint().x_, pts[ipt].getImgPoint().y_, 0);
            zp.add(pt, null);

            // Les valeurs d'attributs
            zp.getModel(ATT_IND_XR).setObject(ipt, ptReal.x_, null);
            zp.getModel(ATT_IND_YR).setObject(ipt, ptReal.y_, null);
            zp.getModel(PivVisuPanel.ATT_IND_ZR).setObject(ipt, ptReal.z_, null);
            zp.getModel(PivVisuPanel.ATT_LABEL).setObject(ipt, "P" + (ipt + 1), null);
          }

        // En espace r�el
        else {

            GrPoint ptReal=pts[ipt].getRealPoint().applique(toReal);
            GISPoint pt=new GISPoint(ptReal.x_, ptReal.y_, 0);
            zp.add(pt, null);

            // Les valeurs d'attributs
            zp.getModel(ATT_IND_XI).setObject(ipt, pts[ipt].getImgPoint().x_, null);
            zp.getModel(ATT_IND_YI).setObject(ipt, pts[ipt].getImgPoint().y_, null);
            zp.getModel(PivVisuPanel.ATT_IND_ZR).setObject(ipt, ptReal.z_, null);
            zp.getModel(PivVisuPanel.ATT_LABEL).setObject(ipt, "P" + (ipt + 1), null);
          }
        }
      }
    }
    finally {
      eventEnabled_=true;
    }
  }

  /** 
   * Appel� en cas de modification de g�om�tries => On remet a jour le projet pour la selection.
   * @param _sel <code>null</code> : Toutes les g�om�tries doivent �tre remises � jour.
   */
  @Override
  public void geometryChanged(int... _sel) {
    if (!eventEnabled_) return;
    eventEnabled_=false;

    try {
      PivOrthoPoint[] pts=prj_.getOrthoPoints();
      GISZoneCollectionPoint zp=(GISZoneCollectionPoint) getGeomData();

      if (_sel == null) {
        _sel=new int[zp.getNbGeometries()];
        for (int i=0; i < _sel.length; i++)
          _sel[i]=i;
      }

      if (zp.getNbGeometries() != 0) {
        GrMorphisme toData=prj_.getTransformationParameters().getToData();
        
        for (int i=0; i < _sel.length; i++) {

          // En espace image
          if (type_ == PivVisuPanel.MODE_ORIGINAL_VIEW) {
            
            int iattXR=zp.getIndiceOf(ATT_IND_XR);
            int iattYR=zp.getIndiceOf(ATT_IND_YR);
            int iattZR=zp.getIndiceOf(PivVisuPanel.ATT_IND_ZR);

            GrPoint imgPt=new GrPoint(zp.getX(_sel[i]), zp.getY(_sel[i]), 0);
            GrPoint realPt=new GrPoint((Double) zp.getValue(iattXR, _sel[i]), (Double) zp.getValue(iattYR, _sel[i]), (Double) zp.getValue(iattZR, _sel[i]));
            realPt.autoApplique(toData);
            pts[_sel[i]]=new PivOrthoPoint(realPt, imgPt);
          }

          // En espace r�el
          else {

            int iattXI=zp.getIndiceOf(ATT_IND_XI);
            int iattYI=zp.getIndiceOf(ATT_IND_YI);
            int iattZR=zp.getIndiceOf(PivVisuPanel.ATT_IND_ZR);

            GrPoint imgPt=new GrPoint((Double) zp.getValue(iattXI, _sel[i]), (Double) zp.getValue(iattYI, _sel[i]), 0);
            GrPoint realPt=new GrPoint(zp.getX(_sel[i]), zp.getY(_sel[i]), (Double) zp.getValue(iattZR, _sel[i]));
            realPt.autoApplique(toData);
            pts[_sel[i]]=new PivOrthoPoint(realPt, imgPt);
          }

        }
      }

      prj_.setOrthoPoints(pts);
    }
    finally {
      eventEnabled_=true;
    }
  }
}
