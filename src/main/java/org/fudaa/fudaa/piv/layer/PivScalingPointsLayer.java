package org.fudaa.fudaa.piv.layer;

import java.awt.Color;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.palette.BPalettePlageDiscret;
import org.fudaa.ebli.palette.BPlageDiscret;
import org.fudaa.ebli.trace.BPlageInterface;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.piv.PivResource;

/**
 * Le calque pour tracer les couples de points pour la mise a l'echelle. Ce calque s'appuie sur une palette discrete 
 * pour afficher les couples resolution ou transformation suivant 2 couleurs diff�rentes.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivIASALayer.java 6586 2011-10-27 13:15:36Z bmarchan $
 */
public class PivScalingPointsLayer extends ZCalqueLigneBrisee {
  /** L'attribut utilis� pour mapper la palette de couleurs. */
  protected GISAttributeInterface att_;

  /**
   * Construction du calque.
   */
  public PivScalingPointsLayer() {
    super();

    // Definition de la palette (utilis�e pour les couleurs de trait et la l�gende)
    BPalettePlageDiscret pal=new BPalettePlageDiscret(new Integer[0]);
    BPlageDiscret[] plage=new BPlageDiscret[2];
    plage[0]=new BPlageDiscret(0);
    plage[0].setCouleur(Color.green.darker());
    plage[0].setLegende(PivResource.getS("R�solution"));

    plage[1]=new BPlageDiscret(1);
    plage[1].setCouleur(Color.blue);
    plage[1].setLegende(PivResource.getS("Transformation"));
    pal.setPlages(plage);
    setPaletteCouleurPlages(pal);

    att_=PivScalingPointsModel.ATT_IND_TYPE;
  }

  /**
   * Initialise le contexte de trac�. Se base sur la palette de couleurs
   * @param _ligne Le contexte de trac�.
   * @param _idxPoly La g�om�trie pour laquelle initialiser le contexte de trac�.
   */
  @Override
  protected void initTrace(TraceLigneModel _ligne, int _idxPoly) {
    final int idx = modele_.isGeometryFermee(_idxPoly) ? 0 : 1;
    _ligne.updateData(getLineModel(idx));

    // Si la palette de couleur est d�finie, la couleur pour le trac� est
    // d�termin�e suivant l'attribut donn� comme attribut de couleur.
    if (att_!=null && paletteCouleur_!=null) {
      Integer val=(Integer)modele_.getGeomData().getModel(att_).getObjectValueAt(_idxPoly);
      BPlageInterface plage=((BPalettePlageDiscret)paletteCouleur_).getPlageFor(val);
      _ligne.setCouleur(plage.getCouleur());
    }

    if (isAttenue()) {
      _ligne.setCouleur(EbliLib.getAlphaColor(attenueCouleur(_ligne.getCouleur()), alpha_));
    } else if (EbliLib.isAlphaChanged(alpha_)) {
      _ligne.setCouleur(EbliLib.getAlphaColor(_ligne.getCouleur(), alpha_));
    }
  }

  /**
   * Le calque utilise-t-il une palette discrete.
   * @return Fix� � true pour ce calque.
   */
  @Override
  public boolean isDiscrete() {
    return true;
  }

  @Override
  public boolean isPaletteModifiable() {
    return false;
  }
}
