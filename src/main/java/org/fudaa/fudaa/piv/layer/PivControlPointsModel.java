package org.fudaa.fudaa.piv.layer;

import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISMultiPoint;
import org.fudaa.ctulu.gis.GISZoneCollectionMultiPoint;
import org.fudaa.ebli.calque.edition.ZModeleMultiPointEditable;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.locationtech.jts.geom.CoordinateSequence;

/**
 * Un modele pour les points d'orthorectification recalcul�s, dans l'espace r�el.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivControlPointsModel extends ZModeleMultiPointEditable {

  /** Le projet. */
  PivProject prj_;

  /**
   * Construction du mod�le.
   */
  public PivControlPointsModel() {
    super(new GISZoneCollectionMultiPoint());
  }

  /**
   * Redefinit le projet, et remet a jour le modele
   * @param _prj Le projet courant.
   */
  public void setProjet(PivProject _prj) {
    prj_=_prj;
    update();
  }

  /**
   * Remise a jour du mod�le depuis le projet.
   */
  public void update() {
    GISZoneCollectionMultiPoint zp = (GISZoneCollectionMultiPoint) getGeomData();
    zp.removeAll(null);

    GrMorphisme toReal=prj_.getTransformationParameters().getToReal();
    
    if (prj_.getOrthoPoints() != null) {
      PivOrthoPoint[] pts = prj_.getOrthoPoints();
      if (pts.length == 0 || pts[0].getComputeRealPoint() == null) {
        return;
      }

      CoordinateSequence seq = GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(pts.length, 2);
      for (int i = 0; i < pts.length; i++) {
        GrPoint ptReal=pts[i].getComputeRealPoint().applique(toReal);
        seq.setOrdinate(i, 0, ptReal.x_);
        seq.setOrdinate(i, 1, ptReal.y_);
      }

      GISMultiPoint mp = (GISMultiPoint) GISGeometryFactory.INSTANCE.createMultiPoint(seq);
      zp.addGeometry(mp, null, null);
    }
  }
}
