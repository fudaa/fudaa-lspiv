/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.piv.layer;

/**
 * Un modele de calque editable au sens PIV. Ce type de modele r�agit � la commande
 * d'�dition.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public interface PivEditableModel {

  /**
   * Appel� en cas d'�dition de la g�om�trie => On remet a jour le projet
   * @param _sel Les identifiants des objets modifi�s dans l'ordre du mod�le.
   */
  void geometryChanged(int... _sel);
}
