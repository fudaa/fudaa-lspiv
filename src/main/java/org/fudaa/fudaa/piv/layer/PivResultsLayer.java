package org.fudaa.fudaa.piv.layer;

import java.awt.Graphics2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesConfigure;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesTraceConfigure;
import org.fudaa.ebli.calque.ZModelePolygone;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;
import org.fudaa.fudaa.meshviewer.layer.MvIsoLayerQuickPainter;
import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;

/**
 * Un calque pour afficher des r�sultats sous forme d'isocouleurs/isolignes. Ce calque
 * affiche �galement les elements du maillage supportant ces trac�s de surfaces.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 *
 */
public class PivResultsLayer extends MvElementLayer implements PropertyChangeListener {
  /** La classe d�l�gu�e pour le trac� des isocouleurs. */
  MvIsoLayerQuickPainter isoPainter;
  /** Les isolignes sont-elles trac�es */
  boolean btraceIsoLine_=false;
  /** Les isosurfaces sont-elles trac�es */
  boolean btraceIsoSurface_=true;
  /** Le modele de ligne pour les isolignes */
  TraceLigneModel isolineModel_=new TraceLigneModel();
  
  @Override
  protected BConfigurableInterface getAffichageConf() {
    final BConfigurableInterface[] sect = new BConfigurableInterface[2 + getNbSet()];
    sect[0] = new ZCalqueAffichageDonneesConfigure(this);
    sect[1] = new PivResultatsLayerConfigure(this);
    for (int i = 2; i < sect.length; i++) {
      sect[i] = new ZCalqueAffichageDonneesTraceConfigure(this, i - 1);
    }
    return new BConfigurableComposite(sect, EbliLib.getS("Affichage"));
  }

  @Override
  public void paintDonnees(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel) {
    if (isoPainter!=null && (btraceIsoLine_ || btraceIsoSurface_)) {
      isoPainter.setTraceIsoLine(isTraceIsoLine());
      isoPainter.setPaletteCouleur(getPaletteCouleur());
      isoPainter.setIsAntaliasing(isAntialiasing());
      isoPainter.setAlpha(getAlpha());
      isoPainter.setLigneModel(getLineModel(1));

      isoPainter.paintDonnees(getWidth(), getHeight(), _g, _versEcran, _versReel, _clipReel);
    }
    super.paintDonnees(_g, _versEcran, _versReel, _clipReel);
  }

  @Override
  public void setModele(ZModelePolygone _modele) {
    if (_modele!=null && !(_modele instanceof PivResultsModel))
      throw new IllegalArgumentException("Bad type argument");
    
    if (modele_!=null) {
      modele().removePropertyChangeListener(this);
    }
    super.setModele(_modele);
    if (modele_!=null) {
      modele().addPropertyChangeListener(this);
    }
    adjustPalette();
    
    if (_modele==null) {
      isoPainter=null;
    }
    else {
      if (isoPainter==null) {
        isoPainter = new MvIsoLayerQuickPainter(modele());
      }
      else {
        isoPainter.setModel(modele());
      }
    }
  }

  @Override
  public PivResultsModel modele() {
    return (PivResultsModel)super.modele();
  }
  
  /**
   * Definit que les isolignes doivent etre trac�es
   * @param _b True : Affichage en mode isolignes
   */
  public void setTraceIsoLine(boolean _b) {
    if (btraceIsoLine_ != _b) {
      btraceIsoLine_ = _b;
      firePropertyChange(PivResultatsLayerConfigure.ISO_LINE_PAINTED, !btraceIsoLine_, btraceIsoLine_);
      repaint();
    }
  }
  
  /**
   *  @return True : L'affichage est en mode isolignes
   */
  public boolean isTraceIsoLine() {
    return btraceIsoLine_;
  }
  
  /**
   * Definit que les isosurfaces doivent etre trac�es
   * @param _b True : Affichage en mode isosurfaces
   */
  public void setTraceIsoSurface(boolean _b) {
    if (btraceIsoSurface_ != _b) {
      btraceIsoSurface_ = _b;
      firePropertyChange(PivResultatsLayerConfigure.ISO_SURFACE_PAINTED, !btraceIsoSurface_, btraceIsoSurface_);
      repaint();
    }
  }

  /**
   * Retourne un modele diff�rent pour les elements et pour les isolignes.
   * @param _idx 1 : Modele pour les isolignes.
   */
  @Override
  public TraceLigneModel getLineModel(final int _idx) {
    if (_idx==1) return isolineModel_;
    return ligneModel_;
  }

  @Override
  public int getNbSet() {
    return 2;
  }
  
  /**
   *  @return True : L'affichage est en mode isosurfaces
   */
  public boolean isTraceIsoSurface() {
    return btraceIsoSurface_;
  }

  @Override
  public boolean isPaletteModifiable() {
    return true;
  }

  /**
   * True : Les bornes de la palette peuvent �tre d�finies de mani�re automatique pour tous
   * les temps.
   */
  @Override
  public boolean isDonneesBoiteAvailable() {
    return true;
  }

  /**
   * True : Les bornes de la palette peuvent �tre d�finies de mani�re automatique pour le temps
   * temps courant.
   */
  @Override
  public boolean isDonneesBoiteTimeAvailable() {
    return true;
  }
  
  /**
   * Definition des bornes de la palette pour tous les pas de temps.
   */
  @Override
  public boolean getRange(final CtuluRange _b) {
    PivResultsModel mdl=modele();
    if (mdl==null) return false;
    
    CtuluVariable var=mdl.getCurrentVar();
    
    // on initialise le min/max;
    _b.setToNill();
    
    // Pour la correlation, on initialise entre 0 et 1.
    if (var==ResultType.CORREL) {
      _b.expandTo(0);
      _b.expandTo(1);
    }
    else {
      for (int tidx = mdl.getNbTime() - 1; tidx >= 0; tidx--) {
        for (int i = mdl.getNbPoint() - 1; i >= 0; i--) {
          _b.expandTo(mdl.getData(var, tidx, i));
        }
      }
    }
    
    return true;
  }
  
  /**
   * Definition des bornes de la palette pour le temps courant.
   */
  @Override
  public boolean getTimeRange(final CtuluRange _b) {
    PivResultsModel mdl=modele();
    if (mdl==null) return false;
    
    CtuluVariable var=mdl.getCurrentVar();
    
    // on initialise le min/max;
    _b.setToNill();
    
    if (var==ResultType.CORREL) {
      _b.expandTo(0);
      _b.expandTo(1);
    }
    else {
      for (int i=mdl.getNbPoint() - 1; i >= 0; i--) {
        _b.expandTo(mdl.getData(var, mdl.getCurrentTimeIdx(), i));
      }
    }
    
    return true;
  }

  /**
   * Methode appel�e quand les propri�t�s du mod�le changent.
   */
  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (evt.getPropertyName().equals("var")) {
      adjustPalette();
    }
  }
  
  /**
   * Ajuste la palette en fonction des bornes min/max des valeurs pour tous les
   * pas de temps.
   */
  private void adjustPalette() {
    BPalettePlage palette=(BPalettePlage)getPaletteCouleur();
    if (palette==null) {
      palette = (BPalettePlage)this.createPaletteCouleur();
      palette.setReduit(true);
      palette.initPlages(30, 0, 1);
    }
    else {
      CtuluRange range=new CtuluRange();
      this.getRange(range);
      // Il se peut que le passage par OMEGA ou DIVERG ait forc� � 1 le nombre de plages => On le remet � 30.
      palette.initPlages(palette.getNbPlages()<=1 ? 30:palette.getNbPlages(), range.min_, range.max_);
    }
    
    palette.setSousTitre((modele()==null || modele().getCurrentVar()==null) ? null:modele().getCurrentVar().getName());
    this.setPaletteCouleurPlages(palette);
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    final EbliUIProperties res = super.saveUIProperties();
    
    res.put(PivResultatsLayerConfigure.ISO_LINE_PAINTED, isTraceIsoLine());
    res.put(PivResultatsLayerConfigure.ISO_SURFACE_PAINTED, isTraceIsoSurface());
    if (modele()!=null && modele().getCurrentVar()!=null)
      res.put("iso.variable", modele().getCurrentVar().toString());
    
    return res;
  }

  @Override
  public void initFrom(final EbliUIProperties _p) {
    if (_p != null) {
      if (_p.isDefined(PivResultatsLayerConfigure.ISO_LINE_PAINTED)) {
        setTraceIsoLine(_p.getBoolean(PivResultatsLayerConfigure.ISO_LINE_PAINTED));
      }
      if (_p.isDefined(PivResultatsLayerConfigure.ISO_SURFACE_PAINTED)) {
        setTraceIsoSurface(_p.getBoolean(PivResultatsLayerConfigure.ISO_SURFACE_PAINTED));
      }
      if (_p.isDefined("iso.variable")) {
        if (modele()!=null) {
          modele().setCurrentVar(ResultType.valueOf(_p.getString("iso.variable")));
        }
      }
      super.initFrom(_p);
    }
  }

}
