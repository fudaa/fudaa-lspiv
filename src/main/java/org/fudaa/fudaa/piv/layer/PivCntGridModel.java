package org.fudaa.fudaa.piv.layer;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeModelObjectArray;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.metier.PivCntGrid;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.locationtech.jts.geom.CoordinateSequence;

/**
 * Un modele pour le contour de grille
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivCntGridModel extends ZModeleLigneBriseeEditable implements PivEditableModel {
  // L'attribut utilis� pour l'affichage des lignes suivant une palette de couleurs.
//  public static final GISAttributeInteger ATT_IND_COL=new GISAttributeInteger("color",false);
  // Le projet.
  PivProject prj_;
  /** Pour empecher les evenements cycliques */
  protected boolean eventEnabled_=true;

  /**
   * Constructeur.
   */
  public PivCntGridModel() {
    super(new GISZoneCollectionLigneBrisee());
    
    GISAttribute[] attrs = new GISAttribute[]{PivVisuPanel.ATT_LABEL};
    getGeomData().setAttributes(attrs, null);
  }

  @Override
  public boolean isDataValid(CoordinateSequence _seq, ZEditionAttributesDataI _data, CtuluAnalyze _ana) {
    if (_seq.size()!=5) {
      _ana.addFatalError(PivResource.getS("Le contour de grille doit comporter 4 sommets."));
      return false;
    }
    return true;
  }

  /**
   * Redefinit le projet, et remet a jour le modele.
   * @param _prj Le projet courant.
   */
  public void setProjet(PivProject _prj) {
    prj_=_prj;
    update();
  }
  
  @Override
  public boolean moveGlobal(CtuluListSelectionInterface _selection, double _dx, double _dy, double _dz, CtuluCommandContainer _cmd) {
    boolean b=super.moveGlobal(_selection, _dx, _dy, _dz, _cmd);
    geometryChanged(null);
    return b;
  }

  @Override
  public boolean rotateGlobal(CtuluListSelectionInterface _selection, double _angRad, double _xreel0, double _yreel0, CtuluCommandContainer _cmd) {
    boolean b=super.rotateGlobal(_selection, _angRad, _xreel0, _yreel0, _cmd);
    geometryChanged(null);
    return b;
  }

  /**
   * Remise a jour du mod�le depuis le projet.
   */
  public void update() {
    if (!eventEnabled_) return;
    eventEnabled_=false;

    try {
      GISZoneCollectionLigneBrisee zl=(GISZoneCollectionLigneBrisee) getGeomData();
      zl.removeAll(null);

      if (prj_.getComputeCntGrid() != null) {
        GrPolygone pl=prj_.getComputeCntGrid().getContour();
        this.addGeometry(pl, null, null, null);

        // Les labels
        GISAttributeModelObjectArray lbData=(GISAttributeModelObjectArray) PivVisuPanel.ATT_LABEL.createDataForGeom(null, pl.nombre() + 1);
        for (int i=0; i < lbData.getSize(); i++) {
          lbData.set(i, "P" + (i + 1));
        }
        zl.getModel(PivVisuPanel.ATT_LABEL).setObject(0, lbData, null);
      }
    }
    finally {
      eventEnabled_=true;
    }
  }

  /** 
   * Appel� en cas de modification de g�om�tries => On remet a jour le projet pour la selection.
   * @param _sel <code>null</code> : Toutes les g�om�tries doivent �tre remises � jour.
   */
  @Override
  public void geometryChanged(int... _sel) {
    if (!eventEnabled_) return;
    eventEnabled_=false;

    try {
      GISZoneCollectionLigneBrisee zp=(GISZoneCollectionLigneBrisee) getGeomData();

      CoordinateSequence seq=zp.getCoordinateSequence(0);

      int nbPts=seq.size();
      if (isGeometryFermee(0)) {
        nbPts--;
      }

      GrPolygone pl=new GrPolygone();
      for (int i=0; i < nbPts; i++) {
        pl.sommets_.ajoute(seq.getX(i), seq.getY(i), 0);
      }
      PivCntGrid cnt=prj_.getComputeCntGrid();
      cnt.setContour(pl);
      prj_.setComputeCntGrid(cnt);
    }
    finally {
      eventEnabled_=true;
    }
  }
}
