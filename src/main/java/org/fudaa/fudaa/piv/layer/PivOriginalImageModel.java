package org.fudaa.fudaa.piv.layer;

import java.awt.geom.Point2D;
import java.io.File;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.fudaa.ctulu.CtuluImageContainer;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.fudaa.piv.metier.PivProject;

/**
 * Un modele pour l'image affich�e en espace image (image source ou stabilis�e).
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivOriginalImageModel extends PivAbstractImageModel {
  // Le projet.
  PivProject prj_;
  /** Si true, affichage des images stabilis�es. */
  boolean showStabilizedImages_ = false;

  /**
   * Construction du mod�le.
   */
  public PivOriginalImageModel() {
    super();
  }

  /**
   * Redefinit le projet, et remet a jour le modele
   * @param _prj Le projet associ� au mod�le.
   */
  public void setProjet(PivProject _prj) {
    prj_=_prj;
    update();
  }

  @Override
  public PivProject getProject() {
    return prj_;
  }

  public void showStabilizedImages(boolean _b) {
    showStabilizedImages_ = _b;
    update();
  }
  
  /**
   * Met a jour le mod�le en fonction de la premi�re image reconditionn�e.
   */
  public void update() {
    setSelectedImage(0);
  }

  /**
   * Modifie l'image affich�e suivant son index dans le projet.
   * @param _idx L'index de l'image dans le projet.
   */
  @Override
  public void setSelectedImage(int _idx) {

    File[] srcFiles;
    if (showStabilizedImages_)
      srcFiles = prj_.getStabilizedImageFiles();
    else
      srcFiles=prj_.getReadySrcImageFiles();
    
    // Si l'indice d'image est bien dans les images disponibles
    if (_idx!=-1 && srcFiles.length>_idx) {

      File img=prj_.getCacheImageFile(srcFiles[_idx]);
      ImageInputStream iis=prj_.getSrcOrStabCacheImageInputStream(img);

      // Repris de CtuluLibImage.getImageReader(), mais en utilisant l'extension (pgm) au lieu
      // du nom de reader (pnm)
      String extension = CtuluLibFile.getExtension(img.getName());
      final Iterator<ImageReader> it = ImageIO.getImageReadersBySuffix(extension);
      ImageReader imgReader_ = null;
      if (it != null && it.hasNext()) {
        imgReader_ = it.next();
      }
      if (imgReader_==null) return;

      imgReader_.setInput(iis);

      CtuluImageContainer imgCont=new CtuluImageContainer(imgReader_,img,false);

      Point2D.Double[] imgPts=new Point2D.Double[]{
        new Point2D.Double(0,0),
        new Point2D.Double(imgCont.getImageWidth(),0),
        new Point2D.Double(0,imgCont.getImageHeight())
      };
      Point2D.Double[] realPts=new Point2D.Double[]{
        new Point2D.Double(0,imgCont.getImageHeight()),
        new Point2D.Double(imgCont.getImageWidth(),imgCont.getImageHeight()),
        new Point2D.Double(0,0)
      };

      setImage(imgCont);
      setProj(imgPts, realPts);
    }
    // Aucune image dans le projet, le mod�le fonctionnera sans image.
    else {
      setImage(null);
    }
  }
}
