/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fudaa.fudaa.piv.layer;

import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeInteger;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;
import org.fudaa.fudaa.piv.metier.PivProject;

/**
 * Un modele pour les couples de points  bas� sur le modele de ligne bris�es.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivIASAModel.java 6709 2011-11-24 16:17:00Z bmarchan $
 */
public class PivScalingPointsModel extends ZModeleLigneBriseeEditable {
  /** L'attribut utilis� pour l'affichage des lignes suivant une palette de couleurs. */
  public static final GISAttributeInteger ATT_IND_TYPE=new GISAttributeInteger("type",false);
  // Le projet.
  PivProject prj_;
  
  class AttributeDataAdapter implements ZEditionAttributesDataI {
    GrPolyligne trans_;
    int type_;

    public AttributeDataAdapter(GrPolyligne _trans, int _type) {
      trans_=_trans;
      type_ = _type;
    }
    
    @Override
    public int getNbVertex() {
      return trans_.nombre();
    }

    @Override
    public int getNbValues() {
      return 1;
    }

    @Override
    public GISAttributeInterface getAttribute(int _i) {
      return ATT_IND_TYPE;
    }

    @Override
    public Object getValue(GISAttributeInterface _attr, int _idxVertex) {
      if (_attr==ATT_IND_TYPE) {
        return type_;
      }
      return null;
    }

    @Override
    public void setValue(GISAttributeInterface _attr, int _idxVertex, Object _val) {
    }
  }

  /**
   * Construction du mod�le.
   */
  public PivScalingPointsModel() {
    super(new GISZoneCollectionLigneBrisee());
    GISAttribute[] attrs = new GISAttribute[]{ATT_IND_TYPE};
    getGeomData().setAttributes(attrs, null);
  }

  /**
   * Redefinit le projet, et remet a jour le modele.
   * @param _prj Le projet courant.
   */
  public void setProjet(PivProject _prj) {
    prj_=_prj;
    update();
  }

  /**
   * Remise a jour du mod�le depuis le projet.
   */
  public void update() {
    GISZoneCollectionLigneBrisee zl=(GISZoneCollectionLigneBrisee)getGeomData();
    zl.removeAll(null);

    PivOrthoParameters params=prj_.getOrthoParameters();
    if (params==null) return;

    // Les couples pour la resolution
    if (params.getScalingResolutionImgPoints() != null) {
      for (GrPoint[] couple : params.getScalingResolutionImgPoints()) {
        GrPolyligne pl = new GrPolyligne();
        pl.sommets_.ajoute(couple[0]);
        pl.sommets_.ajoute(couple[1]);
        
        
        this.addGeometry(pl, null, null, new AttributeDataAdapter(pl, 0));
      }
    }
    
    // Les couples pour la transformation
    if (params.getScalingTranformationImgPoints() != null && params.getScalingTranformationImgPoints().length == 2) {
      GrPoint[] couple = params.getScalingTranformationImgPoints();
      GrPolyligne pl = new GrPolyligne();
      pl.sommets_.ajoute(couple[0]);
      pl.sommets_.ajoute(couple[1]);
        
      this.addGeometry(pl, null, null, new AttributeDataAdapter(pl, 1));
    }
  }
}
