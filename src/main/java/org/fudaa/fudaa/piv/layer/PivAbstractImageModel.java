/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fudaa.fudaa.piv.layer;

import org.fudaa.ebli.calque.ZModeleStatiqueImageRaster;
import org.fudaa.fudaa.piv.metier.PivProject;

/**
 * Un modele pour un calque image.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public abstract class PivAbstractImageModel extends ZModeleStatiqueImageRaster {

  /**
   * @return Le projet associ� au mod�le, <tt>null</tt> si aucun projet associ�.
   */
  public abstract PivProject getProject();

  /**
   * S�lectionne l'image du projet a afficher.
   * @param _idx L'index de l'image dans la liste des images, -1 si aucune image � afficher.
   */
  public abstract void setSelectedImage(int _idx);

}
