package org.fudaa.fudaa.piv.layer;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalqueFleche;
import org.fudaa.ebli.calque.ZModeleFleche;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivResultsI;
import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;
import org.fudaa.fudaa.piv.metier.PivResultsTransformationAdapter;

import com.memoire.bu.BuTable;

/**
 * Un modele pour le trac� des vecteurs vitesses.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivVelResultsModel implements ZModeleFleche {
  /** L'index du r�sultat instantan� s�lectionn�. */
  int iselres_=0;
  /** Le point, toujours le m�me, pour retourner les coordonn�es 3D d'un r�sultat. */
  GrPoint ptCache_=new GrPoint();
  /** Les resultats dans le rep�re courant */
  PivResultsI[] results_;
  /** Tous les r�sultats */
  PivResultsI[] resData;
  /** Le projet */
  PivProject prj_;
  /** Le system de coordonn�es */
  private boolean isSystemInitial_=false;
  /** Le segment, toujours le meme, pour retourner les valeurs de norme */
  GrSegment segCache_ = new GrSegment(new GrPoint(), new GrPoint());

  /**
   * Le constructeur.
   */
  public PivVelResultsModel(PivProject _prj, PivResultsI... _res) {
    resData=_res;
    prj_=_prj;
    setSystemViewInitial(false);
  }
  
  public void setResults(PivResultsI[] _res) {
    resData=_res;
    
    if (!isSystemInitial_) {
      results_=resData;
    }
    else {
      results_=adaptResultsToReal(resData);
    }
  }
  
  /**
   * Definit si le system de coordonn�es est initial ou calcul.
   * @param _b True : Initial
   */
  public void setSystemViewInitial(boolean _b) {
    if (!_b) {
      results_=resData;
    }
    else {
      results_=adaptResultsToReal(resData);
    }
    isSystemInitial_=_b;
  }
  
  /**
   * @return Les resultats avec les coordonn�es adapt�es dans le repere courant.
   */
  protected PivResultsI[] adaptResultsToReal(PivResultsI... _res) {
    if (_res==null)
      return null;

    PivResultsI[] ret=new PivResultsI[_res.length];
    for (int i=0; i<ret.length; i++)
      ret[i]=new PivResultsTransformationAdapter(_res[i], prj_.getTransformationParameters().getToInitial());
    return ret;
  }
  
  /**
   * @return Le nombre de temps du mod�le
   */
  public int getNbTime() {
    return results_==null ? 0 : results_.length;
  }
  
  /**
   * D�finit le r�sultat instantan� selectionn�.
   * @param _ind L'index. Peut �tre -1 si aucun r�sultat s�lectionn�.
   */
  public void setSelectedResult(int _ind) {
    iselres_=_ind;
  }

  /** Non implement� */
  @Override
  public boolean interpolate(GrSegment _seg, double _x, double _y) {
    return false;
  }

  /**
   * Initialise le segment depuis les points d'indice _i et _i+1, pour le temps courant.
   * @param _s Le segement.
   * @param _i L'indice du premier point.
   * @param _force Inutilis�.
   * @return Fix� � true.
   */
  @Override
  public boolean segment(GrSegment _s, int _i, boolean _force) {
    return segment(_s, iselres_, _i);
  }

  /**
   * Retourne la norme de la vitesse au point d'indice _ipt, pour le temps donn�.
   * @param _tidx L'indice du temps
   * @param _ipt L'indice du point.
   * @return La norme.
   */
  public boolean segment(GrSegment _s, int _tidx, int _ipt) {
    _s.o_.x_=getX(_ipt);
    _s.o_.y_=getY(_ipt);
    _s.e_.x_=getX(_ipt)+getVx(_tidx, _ipt);
    _s.e_.y_=getY(_ipt)+getVy(_tidx, _ipt);

    return true;
  }
  
  /**
   * Retourne la norme de la vitesse au point d'indice _i, pour le temps courant.
   * @param _i L'indice du point.
   * @return La norme.
   */
  @Override
  public double getNorme(int _i) {
    // On passe par le segment, car le calcul de plages est aussi fait avec le segment, ceci evite des probl�mes d'arrondi.
    return getNorme(iselres_, _i);
  }

  /**
   * Retourne la norme de la vitesse au point d'indice _ipt, pour le temps donn�.
   * @param _tidx L'indice du temps
   * @param _ipt L'indice du point.
   * @return La norme.
   */
  public double getNorme(int _tidx, int _ipt) {
    // On passe par le segment, car le calcul de plages est aussi fait avec le segment, ceci evite des probl�mes d'arrondi.
    segment(segCache_, _tidx, _ipt);
    return segCache_.longueurXY();
  }

  /**
   * Retourne la vitesse suivant X au point d'indice _i pour le
   * temps courant.
   * @param _i L'indice du point.
   * @return La vitesse.
   */
  @Override
  public double getVx(int _i) {
    if (iselres_>=results_.length || results_[iselres_]==null) return 0;
    return results_[iselres_].getValue(_i,ResultType.VX);
  }

  /**
   * Retourne la vitesse suivant X au point d'indice _i pour le
   * temps donn�.
   * @param _tidx L'indice du temps
   * @param _ipt L'indice du point.
   * @return La vitesse.
   */
  public double getVx(int _tidx, int _ipt) {
    if (_tidx>=results_.length || results_[_tidx]==null) return 0;
    return results_[_tidx].getValue(_ipt,ResultType.VX);
  }

  /**
   * Retourne la vitesse suivant Y au point d'indice _i pour le
   * temps courant.
   * @param _i L'indice du point.
   * @return La vitesse.
   */
  @Override
  public double getVy(int _i) {
    if (iselres_>=results_.length || results_[iselres_]==null) return 0;
    return results_[iselres_].getValue(_i,ResultType.VY);
  }

  /**
   * Retourne la vitesse suivant Y au point d'indice _i pour le
   * temps donn�.
   * @param _tidx L'indice du temps
   * @param _ipt L'indice du point.
   * @return La vitesse.
   */
  public double getVy(int _tidx, int _ipt) {
    if (_tidx>=results_.length || results_[_tidx]==null) return 0;
    return results_[_tidx].getValue(_ipt,ResultType.VY);
  }

  /**
   * Retourne la coordonn�e X au point d'indice _i pour le temps courant
   * @param _i L'indice du point.
   * @return La coordonn�e.
   */
  @Override
  public double getX(int _i) {
    if (iselres_>=results_.length || results_[iselres_]==null) return 0;
    return results_[iselres_].getX(_i);
  }

  /**
   * Retourne la coordonn�e Y au point d'indice _i pour le temps courant
   * @param _i L'indice du point.
   * @return La coordonn�e.
   */
  @Override
  public double getY(int _i) {
    if (iselres_>=results_.length || results_[iselres_]==null) return 0;
    return results_[iselres_].getY(_i);
  }

  /** Non implement� */
  @Override
  public double getZ1(int _i) {
    return 0;
  }

  /** Non implement� */
  @Override
  public double getZ2(int _i) {
    return 0;
  }

  @Override
  public BuTable createValuesTable(ZCalqueAffichageDonneesInterface _layer) {
    return new CtuluTable(new ZCalqueFleche.ValueTableModel(this));
  }

  @Override
  public void fillWithInfo(InfoData _d, ZCalqueAffichageDonneesInterface _layer) {
    final ZCalqueFleche.StringInfo info = new ZCalqueFleche.StringInfo();
    info.titleIfOne_ = PivResource.getS("Vit. instantan�es: point n�");
    info.title_ = PivResource.getS("Vit. instantan�es");
    ZCalqueFleche.fillWithInfo(_d, _layer.getLayerSelection(), this, info);
  }

  /** Non implement� */
  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  /**
   * Retourne la boite englobante des points du mod�le.
   * @return La boite.
   */
  @Override
  public GrBoite getDomaine() {
    if (results_==null || results_.length == 0 || results_[0].getNbPoints() == 0) return null;

    GrBoite bt=new GrBoite();
    for (int i=0; i<results_[0].getNbPoints(); i++) {
      bt.ajuste(results_[0].getX(i),results_[0].getY(i),0);
    }
    return bt;
  }

  /**
   * Retourne le nombre de points du mod�le.
   * @return Le nombre de points.
   */
  @Override
  public int getNombre() {
    if (results_==null) return 0;
    return results_[0].getNbPoints();
  }

  /** Non implement� */
  @Override
  public Object getObject(int _ind) {
    return null;
  }

  @Override
  public GrPoint getVertexForObject(int _ind, int _idVertex) {
    if (results_==null) return null;
    if (_ind>getNombre()) return null;
    
    ptCache_.x_=results_[0].getX(_ind);
    ptCache_.y_=results_[0].getY(_ind);
    ptCache_.z_=0;
    return ptCache_;
  }

  @Override
  public void prepare() {
  }
  
  /**
   * Supprime la velocit� pour les points selectionn�s, sans supprimer le point.
   * @param _inds
   * @param _cmd
   * @return
   */
  public boolean removeVelocity(int[] _inds, CtuluCommandContainer _cmd) {
    if (resData==null) return false;
    
    for (int ind : _inds) {
      resData[iselres_].setValue(ind, ResultType.VX, 0.);
      resData[iselres_].setValue(ind, ResultType.VY, 0.);
    }
    
    return true;
  }
}
