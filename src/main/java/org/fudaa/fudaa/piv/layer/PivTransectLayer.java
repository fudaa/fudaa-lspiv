package org.fudaa.fudaa.piv.layer;

import java.awt.Color;
import java.awt.Graphics2D;

import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.utils.PivUtils;

/**
 * Un calque d'affichage des transects. Il affiche les cercles aux points interpol�s pour verifier les rayons de
 * recherche.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivTransectLayer extends ZCalqueLigneBriseeEditable {
  /** True : Les cercles sont affich�s sur les points d'interpolation. */
  protected boolean showCircle_ = true;
  /** Le point coin bas/gauche du cercle */
  GrPoint pt1_=new GrPoint();
  /** Le point haut/droite du cercle */
  GrPoint pt2_=new GrPoint();

  public PivTransectLayer() {
    super(new PivTransectModel(),null);
    
    setAttributForLabels(PivVisuPanel.ATT_LABEL);
    // Legerement transparent.
    setLabelsBackgroundColor(PivUtils.TRANPARENT_WHITE_COLOR);
    setLabelsPrefix("T");
    setLabelsVisible(true);
  }

  /**
   * La m�thode ajoute le trac� des cercles.
   */
  @Override
  public void paintDonnees(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel) {
    super.paintDonnees(_g, _versEcran, _versReel, _clipReel);
        
    // Trac� des cercles sur les points interpol�s des transects s�lectionn�s.
    if (showCircle_ && getSelectedIndex()!=null) {
      GrBoite bPoly=new GrBoite();
      final GrBoite clip = _clipReel;
      PivTransectModel mdl=(PivTransectModel)modeleDonnees();
      final int idxAttRx=mdl.getGeomData().getIndiceOf(PivVisuPanel.ATT_RX);
      final int idxAttRy=mdl.getGeomData().getIndiceOf(PivVisuPanel.ATT_RY);
      final TraceLigne tl=PivUtils.TMP_LINE_MODEL.buildCopy();
      
      for (int igeom : getSelectedIndex()) {
        double xradius=(Double)modele_.getGeomData().getValue(idxAttRx, igeom);
        double yradius=(Double)modele_.getGeomData().getValue(idxAttRy, igeom);
        final int nbPoints = modele_.getNbPointForGeometry(igeom);
        
        if (nbPoints <= 0) {
          continue;
        }
        // La g�ometrie n'est pas visible
        if (!modele_.isGeometryVisible(igeom)) {
          continue;
        }

        modele_.getDomaineForGeometry(igeom, bPoly);
        // Si la boite du polygone n'est pas dans la boite d'affichage on passe
        if (bPoly.intersectionXY(clip) == null) {
          continue;
        }
        for (int j=0; j<nbPoints; j++) {
          modele_.point(pt1_, igeom, j);
          pt2_.initialiseAvec(pt1_);
          pt1_.x_-=xradius;
          pt1_.y_-=yradius;
          pt1_.autoApplique(_versEcran);
          pt2_.x_+=xradius;
          pt2_.y_+=yradius;
          pt2_.autoApplique(_versEcran);
          
          tl.dessineEllipse2D(_g, pt1_.x_, pt2_.y_, Math.abs(pt2_.x_-pt1_.x_),(int)Math.abs(pt2_.y_-pt1_.y_));
        }
      }
    }
  }
  
  @Override
  public PivTransectModel modeleDonnees() {
    return (PivTransectModel)super.modeleDonnees();
  }

  /**
   * Affiche ou non les cercles de recherche
   * @param _b True : Les cercles seront affich�s
   */
  public void setCirclesVisible(boolean _b) {
    showCircle_=_b;
    repaint();
  }
  
  /**
   * 
   * @return True si les cercles sont affich�s
   */
  public boolean isCirclesVisible() {
    return showCircle_;
  }
}
