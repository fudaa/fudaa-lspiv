package org.fudaa.fudaa.piv.layer;

import java.util.ArrayList;
import java.util.List;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeModelObjectArray;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters;
import org.locationtech.jts.geom.CoordinateSequence;

/**
 * Un modele pour les contours des zones d'�coulement ou fixes.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivStabAreasModel extends ZModeleLigneBriseeEditable implements PivEditableModel {

  // Le projet.
  PivProject prj_;
  /** Pour empecher les evenements cycliques */
  protected boolean eventEnabled_=true;

  /**
   * Constructeur.
   */
  public PivStabAreasModel() {
    super(new GISZoneCollectionLigneBrisee());
    
    GISAttribute[] attrs = new GISAttribute[]{PivVisuPanel.ATT_LABEL};
    getGeomData().setAttributes(attrs, null);
  }

  @Override
  public boolean isDataValid(CoordinateSequence _seq, ZEditionAttributesDataI _data, CtuluAnalyze _ana) {
    if (_seq.size()<3) {
      _ana.addFatalError(PivResource.getS("La zone doit comporter au moins 3 sommets."));
      return false;
    }
    return true;
  }

  /**
   * Redefinit le projet, et remet a jour le modele.
   * @param _prj Le projet courant.
   */
  public void setProjet(PivProject _prj) {
    prj_=_prj;
    update();
  }
  
  @Override
  public boolean moveGlobal(CtuluListSelectionInterface _selection, double _dx, double _dy, double _dz, CtuluCommandContainer _cmd) {
    boolean b=super.moveGlobal(_selection, _dx, _dy, _dz, _cmd);
    geometryChanged(null);
    return b;
  }

  @Override
  public boolean rotateGlobal(CtuluListSelectionInterface _selection, double _angRad, double _xreel0, double _yreel0, CtuluCommandContainer _cmd) {
    boolean b=super.rotateGlobal(_selection, _angRad, _xreel0, _yreel0, _cmd);
    geometryChanged(null);
    return b;
  }

  /**
   * Remise a jour du mod�le depuis le projet.
   */
  public void update() {
    if (!eventEnabled_) return;
    eventEnabled_=false;

    try {
      GISZoneCollectionLigneBrisee zl=(GISZoneCollectionLigneBrisee) getGeomData();
      zl.removeAll(null);

      if (prj_.isStabilizationActive() && prj_.getStabilizationParameters().getOutlines() != null) {
        List<GrPolygone> outlines = prj_.getStabilizationParameters().getOutlines();
        for (int icnt = 0; icnt < outlines.size(); icnt++) {
          GrPolygone pl = outlines.get(icnt);
          this.addGeometry(pl, null, null, null);

          // Les labels
          GISAttributeModelObjectArray lbData = (GISAttributeModelObjectArray) PivVisuPanel.ATT_LABEL.createDataForGeom(null, pl.nombre() + 1);
          for (int ipt = 0; ipt < lbData.getSize(); ipt++) {
            lbData.set(ipt, "P" + (ipt + 1));
          }
          zl.getModel(PivVisuPanel.ATT_LABEL).setObject(icnt, lbData, null);
        }
      }
    }
    finally {
      eventEnabled_=true;
    }
  }

  /** 
   * Appel� en cas de modification de g�om�tries => On remet a jour le projet pour la selection.
   * @param _sel <code>null</code> : Toutes les g�om�tries doivent �tre remises � jour.
   */
  @Override
  public void geometryChanged(int... _sel) {
    if (!eventEnabled_) return;
    eventEnabled_=false;

    try {
      GISZoneCollectionLigneBrisee zp=(GISZoneCollectionLigneBrisee) getGeomData();

      //  Ens dde tableau des index null, on prend toutes les g�ometries.
      if (_sel == null) {
        _sel = new int[zp.getNbGeometries()];
        for (int i=0; i<_sel.length;  i++) {
          _sel[i] = i;
        }
      }

      PivStabilizationParameters pars=new PivStabilizationParameters(prj_.getStabilizationParameters());
      List<GrPolygone> outlines = pars.getOutlines();
      for (int icnt = 0; icnt < _sel.length; icnt++) {
        CoordinateSequence seq = zp.getCoordinateSequence(icnt);

        int nbPts = seq.size();
        if (isGeometryFermee(icnt)) {
          nbPts--;
        }

        GrPolygone pl = outlines.get(icnt);
        pl.sommets_.vide();
        for (int i = 0; i < nbPts; i++) {
          pl.sommets_.ajoute(seq.getX(i), seq.getY(i), 0);
        }
      }

      prj_.setStabilizationParameters(pars);
    }
    finally {
      eventEnabled_=true;
    }
  }
}
