package org.fudaa.fudaa.piv.layer;

import java.awt.Color;

import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ebli.calque.ZCalqueFleche;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPlage;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.piv.PivResource;

import com.memoire.fu.FuLog;

/**
 * Un modele pour le trac� des d�bits sous forme de vecteurs vitesses.
 * @author Bertrand Marchand (marchand@detacad.fr)
 */
public class PivFlowResultsLayer extends ZCalqueFleche {

  /**
   * Construction du calque.
   */
  public PivFlowResultsLayer() {
    super();
    
    definePalette();
  }

  @Override
  public void initFrom(final EbliUIProperties _p) {
    if (_p != null) {
      super.initFrom(_p);
    }
    
    if (paletteCouleur_ == null || paletteCouleur_.getNbPlages() != 2) {
      definePalette();
    }
  }
  
  /**
   * Definition de la palette (utilis�e pour les couleurs de trait et la l�gende)
   */
  protected void definePalette() {
    BPalettePlage pal=new BPalettePlage();
    BPlage[] plages=new BPlage[2];
    plages[0]=new BPlage();
    plages[0].setMin(-0.5);
    plages[0].setMax(0.5);
    plages[0].setCouleur(Color.ORANGE);
    plages[0].setLegende(PivResource.getS("Interpol�es"));

    plages[1]=new BPlage();
    plages[1].setMin(0.5);
    plages[1].setMax(1.5);
    plages[1].setCouleur(new Color(200,120,0));
    plages[1].setLegende(PivResource.getS("Extrapol�es"));
    pal.setPlages(plages);
    setPaletteCouleurPlages(pal);
  }

  @Override
  public boolean isPaletteModifiable() {
    return false;
  }
  

//  @Override
//  public boolean getRange(final CtuluRange _b) {
//    _b.setMin(0);
//    _b.setMax(1);
//    return true;
//  }

  /**
   * On recupere la couleur des vecteurs suivant le mode de calcul du vecteur.
   */
  @Override
  protected boolean updateTraceLigne(TraceLigneModel _ligne, TraceIconModel _icone, GrSegment _s, int _idx) {
    Color color=getPaletteColorFor(modeleDonnees().getComputeMode(_idx));
    if (color != null) {
      _ligne.setCouleur(color);
      _ligne.setTypeTrait(ligneModel_.getTypeTrait());
    }
    // Trait invisible
    else {
      _ligne.setCouleur(ligneModel_.getCouleur());
      _ligne.setTypeTrait(TraceLigne.INVISIBLE);
    }
    return true;
  }

  @Override
  public PivFlowResultsModel modeleDonnees() {
    return (PivFlowResultsModel)modele_;
  }
  
  public void setModele(PivFlowResultsModel _s) {
    if (_s==null || _s instanceof PivFlowResultsModel)
      setFlecheModele(_s);
    else {
      FuLog.error(new Throwable("PivFlowResultsModel is expected"));
    }
  }
  
  @Override
  protected String getFlecheUnit() {
    return "m/s";
  }
  
}
