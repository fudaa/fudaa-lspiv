package org.fudaa.fudaa.piv.layer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.metier.PivFlowResults;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;
import org.fudaa.fudaa.piv.metier.PivTransect;
import org.fudaa.fudaa.piv.metier.PivTransectParams;
import org.locationtech.jts.geom.CoordinateSequence;

/**
 * Un modele pour la visualisation d'un transect
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivTransectModel extends ZModeleLigneBriseeEditable implements PivEditableModel {
  // L'attribut utilis� pour l'affichage des lignes suivant une palette de couleurs.
//  public static final GISAttributeInteger ATT_IND_COL=new GISAttributeInteger("color",false);
  /** Le projet. */
  PivProject prj_;
  /** Les attributs de la collection */
  private GISAttribute[] attrs_ = new GISAttribute[] { 
      PivVisuPanel.ATT_IND_ZR,
      PivVisuPanel.ATT_INTER,
      PivVisuPanel.ATT_RX, 
      PivVisuPanel.ATT_RY, 
      PivVisuPanel.ATT_COEF_VIT,
      PivVisuPanel.ATT_P_COEF_VIT,
      PivVisuPanel.ATT_MAX_POINT_NUMBER,
      PivVisuPanel.ATT_LABEL 
      };
  /** Pour empecher les evenements cycliques */
  protected boolean eventEnabled_=true;
  
  class AttributeDataAdapter implements ZEditionAttributesDataI {
    PivTransect trans_;

    public AttributeDataAdapter(PivTransect _trans) {
      trans_=_trans;
    }
    
    @Override
    public int getNbVertex() {
      return trans_.getStraight().nombre();
    }

    @Override
    public int getNbValues() {
      return attrs_.length;
    }

    @Override
    public GISAttributeInterface getAttribute(int _i) {
      return attrs_[_i];
    }

    @Override
    public Object getValue(GISAttributeInterface _attr, int _idxVertex) {
      if (_attr==PivVisuPanel.ATT_IND_ZR) {
        return trans_.getStraight().sommet(_idxVertex).z_;
      }
      else if (_attr==PivVisuPanel.ATT_INTER) {
        return trans_.getParams().getInterpolationStep();
      }
      else if (_attr==PivVisuPanel.ATT_RX) {
        return trans_.getParams().getRadiusX();
      }
      else if (_attr==PivVisuPanel.ATT_RY) {
        return trans_.getParams().getRadiusY();
      }
      else if (_attr==PivVisuPanel.ATT_COEF_VIT) {
        return trans_.getParams().getSurfaceCoef();
      }
      else if (_attr==PivVisuPanel.ATT_MAX_POINT_NUMBER) {
        return trans_.getParams().getMaxPoints();
      }
      else if (_attr==PivVisuPanel.ATT_P_COEF_VIT) {
        return trans_.getCoefs() == null ? 0 : trans_.getCoefs()[_idxVertex];
      }
      else if (_attr==PivVisuPanel.ATT_LABEL) {
        // Rive gauche
        if (_idxVertex==0) {
          return PivResource.getS("RG");
        }
        // Rive droite
        else if (_idxVertex==getNbVertex()-1) {
          return PivResource.getS("RD");
        }
        // Les autres points : Aucune visualisation
        else {
          return "";
        }
      }
      return null;
    }

    @Override
    public void setValue(GISAttributeInterface _attr, int _idxVertex, Object _val) {
      if (_attr==PivVisuPanel.ATT_IND_ZR) {
        trans_.getStraight().sommet(_idxVertex).z_=(Double)_val;
      }
      else if (_attr==PivVisuPanel.ATT_INTER) {
        trans_.getParams().setInterpolationStep((Double)_val);
      }
      else if (_attr==PivVisuPanel.ATT_RX) {
        trans_.getParams().setRadiusX((Double)_val);
      }
      else if (_attr==PivVisuPanel.ATT_RY) {
        trans_.getParams().setRadiusY((Double)_val);
      }
      else if (_attr==PivVisuPanel.ATT_COEF_VIT) {
        trans_.getParams().setSurfaceCoef((Double)_val);
      }
      else if (_attr==PivVisuPanel.ATT_MAX_POINT_NUMBER) {
        trans_.getParams().setMaxPoints((Integer)_val);
      }
      else if (_attr==PivVisuPanel.ATT_P_COEF_VIT) {
        trans_.getCoefs()[_idxVertex] = (Double)_val;
      }
      // Les labels ne sont pas modifi�s.
    }
    
  }

  /**
   * Constructeur.
   */
  public PivTransectModel() {
    super(new GISZoneCollectionLigneBrisee());

    getGeomData().setAttributeIsZ(PivVisuPanel.ATT_IND_ZR);
    getGeomData().setAttributes(attrs_, null);
  }

  /**
   * Redefinit le projet, et remet a jour le modele.
   * @param _prj Le projet courant.
   */
  public void setProjet(PivProject _prj) {
    prj_=_prj;
    update();
  }
  
  /**
   * Remise a jour du mod�le depuis le projet.
   */
  public void update() {
    if (!eventEnabled_) return;
    eventEnabled_=false;
    
    try {
      GISZoneCollectionLigneBrisee zl=(GISZoneCollectionLigneBrisee) getGeomData();
      zl.removeAll(null);
      
      GrMorphisme toReal=prj_.getTransformationParameters().getToReal();

      PivTransect[] trans=prj_.getTransects();
      if (trans != null) {
        for (int i=0; i < trans.length; i++) {
          GrPolyligne pl=trans[i].getStraight().applique(toReal);
            
          this.addGeometry(pl, null, null, new AttributeDataAdapter(trans[i]));
          // Initialisation du Z.
          zl.initZAttribute(i);
        }
      }
    }
    finally {
      eventEnabled_=true;
    }
  }

  @Override
  public boolean removeLigneBrisee(int[] _idx, CtuluCommandContainer _cmd) {
    // On est oblig� de passer par une surcharge de cette m�thode pour la suppression d'objet,
    // l'�coute par un GISZoneListener de la collection ne permet pas de savoir les indices des
    // objets qui ont �t� supprim�s.
    
    boolean b=super.removeLigneBrisee(_idx, _cmd);
    if (!b) return b;
    
    PivTransect[] trans=prj_.getTransects();
    List<PivTransect> ltrans=new ArrayList<PivTransect>(Arrays.asList(trans));
    for (int i=_idx.length-1; i>=0; i--) {
      ltrans.remove(_idx[i]);
    }
    trans=ltrans.toArray(new PivTransect[ltrans.size()]);
    prj_.setTransects(trans);
    
    return true;
  }
  
  @Override
  public boolean moveGlobal(CtuluListSelectionInterface _selection, double _dx, double _dy, double _dz, CtuluCommandContainer _cmd) {
    boolean b=super.moveGlobal(_selection, _dx, _dy, _dz, _cmd);
    geometryChanged(null);
    return b;
  }

  @Override
  public boolean rotateGlobal(CtuluListSelectionInterface _selection, double _angRad, double _xreel0, double _yreel0, CtuluCommandContainer _cmd) {
    boolean b=super.rotateGlobal(_selection, _angRad, _xreel0, _yreel0, _cmd);
    geometryChanged(null);
    return b;
  }

  /** 
   * Appel� en cas de modification de g�om�tries => On remet a jour le projet pour la selection.
   * @param _sel <code>null</code> : Toutes les g�om�tries doivent �tre remises � jour.
   */
  @Override
  public void geometryChanged(int... _sel) {
    if (!eventEnabled_) return;
    eventEnabled_=false;

    try {
      PivTransect[] trans=prj_.getTransects();
      GISZoneCollectionLigneBrisee zp=(GISZoneCollectionLigneBrisee) getGeomData();

      if (_sel == null) {
        _sel=new int[zp.getNbGeometries()];
        for (int i=0; i < _sel.length; i++)
          _sel[i]=i;
      }

      if (zp.getNbGeometries() != 0) {
        GrMorphisme toData=prj_.getTransformationParameters().getToData();
        
        for (int i=0; i < _sel.length; i++) {
          zp.initZCoordinate(_sel[i]);

          CoordinateSequence seq=zp.getCoordinateSequence(_sel[i]);
          int nbPts=seq.size();

          GrPolyligne pl=new GrPolyligne();
          for (int j = 0; j < nbPts; j++) {
            pl.sommets_.ajoute(seq.getOrdinate(j, 0), seq.getOrdinate(j, 1), seq.getOrdinate(j, 2));
          }
          pl.autoApplique(toData);
          
          double[] ptsCoeffs = new double[pl.nombre()];
          for (int j = 0; j < nbPts; j++) {
            ptsCoeffs[j] = ((CtuluCollectionDouble)zp.getModel(zp.getIndiceOf(PivVisuPanel.ATT_P_COEF_VIT)).getObjectValueAt(_sel[i])).getValue(j);
          }

          PivTransectParams params=new PivTransectParams();
          params.setInterpolationStep(zp.getDoubleValue(zp.getIndiceOf(PivVisuPanel.ATT_INTER), _sel[i]));
          params.setRadiusX(zp.getDoubleValue(zp.getIndiceOf(PivVisuPanel.ATT_RX), _sel[i]));
          params.setRadiusY(zp.getDoubleValue(zp.getIndiceOf(PivVisuPanel.ATT_RY), _sel[i]));
          params.setSurfaceCoef(zp.getDoubleValue(zp.getIndiceOf(PivVisuPanel.ATT_COEF_VIT), _sel[i]));
          params.setMaxPoints((Integer)zp.getValue(zp.getIndiceOf(PivVisuPanel.ATT_MAX_POINT_NUMBER), _sel[i]));

          trans[_sel[i]].setStraight(pl);
          trans[_sel[i]].setParams(params);
          trans[_sel[i]].setCoefs(ptsCoeffs);
        }
      }

      prj_.setTransects(trans);
    }
    finally {
      eventEnabled_=true;
    }
  }
  
  @Override
  public void fillWithInfo(InfoData _d, ZCalqueAffichageDonneesInterface _layer) {
//    final ZCalqueFleche.StringInfo info = new ZCalqueFleche.StringInfo();
//    info.titleIfOne_ = PivResource.getS("R�sultats: point n�");
//    info.title_ = PivResource.getS("R�sultats");
    super.fillWithInfo(_d, _layer);
    
    // Infos de r�sultats de calcul
    
    if (!_layer.isSelectionEmpty()) {
      int[] selIds=_layer.getLayerSelection().getSelectedIndex();
      
      if (selIds.length==1) {
        _d.setTitle(PivResource.getS("Transect n� {0}", selIds[0]+1));
      }

      // Indique que aucun resultat n'a �t� defini.
      boolean isSet = false;
      Double waterElevation = null;
      Double discharge = null;
      Double wettedArea = null;
      Double meanVelocity = null;
      Double measuredDischarge = null;
      
      if (prj_.getFlowResults() != null) {
        for (int idx : selIds) {
          if (idx < prj_.getFlowResults().length) {
            PivFlowResults res = prj_.getFlowResults()[idx];
            
            // Les resultats n'ont jamais �t� donn�s.
            if (!isSet) {
              isSet = true;
              waterElevation = res.getDischarge();
              discharge = res.getDischarge();
              wettedArea = res.getWettedArea();
              meanVelocity = res.getMeanVelocity();
              measuredDischarge = res.getValue(-1, ResultType.MEASURED_DISCHARGE);
            }
            
            if (waterElevation != res.getWaterElevation())
              waterElevation = null;
            if (discharge != res.getDischarge())
              discharge = null;
            if (wettedArea != res.getWettedArea())
              wettedArea = null;
            if (meanVelocity != res.getMeanVelocity())
              meanVelocity = null;
            if (measuredDischarge != res.getValue(-1, ResultType.MEASURED_DISCHARGE))
              measuredDischarge = null;
          }
          else {
            isSet = true;
            waterElevation = null;
            discharge = null;
            wettedArea = null;
            meanVelocity = null;
            measuredDischarge = null;
          }
        }
      }

      if (waterElevation != null)
        _d.put(PivResource.getS("Niveau d'eau (m)"), "" + waterElevation);
      if (discharge != null)
        _d.put(PivResource.getS("D�bit total (m�/s)"), "" + discharge);
      if (wettedArea != null)
        _d.put(PivResource.getS("Aire mouill�e (m�)"), "" + wettedArea);
      if (meanVelocity != null)
        _d.put(PivResource.getS("Vitesse moyenne sur la section (m/s)"), "" + meanVelocity);
      if (measuredDischarge != null)
        _d.put(PivResource.getS("D�bit mesur� (m�/s)"), "" + measuredDischarge);
    }
    
//    ZCalqueFleche.fillWithInfo(_d, _layer.getLayerSelection(), this, info);
  }
}
