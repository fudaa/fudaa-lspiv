package org.fudaa.fudaa.piv.layer;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.gis.comparator.CoordinateComparator;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.fudaa.meshviewer.layer.MvIsoModelInterface;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivResultsI;
import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;
import org.fudaa.fudaa.piv.metier.PivResultsTransformationAdapter;
import org.locationtech.jts.geom.Coordinate;
import org.poly2tri.Poly2Tri;
import org.poly2tri.triangulation.TriangulationPoint;
import org.poly2tri.triangulation.delaunay.DelaunayTriangle;
import org.poly2tri.triangulation.point.TPoint;
import org.poly2tri.triangulation.sets.PointSet;

import com.memoire.fu.FuLog;

/**
 * Un modele pour le trac� des r�sultats sous forme d'isocouleurs/isolignes. Les elements sont
 * trac�s �galement.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivResultsModel extends MvElementModelDefault implements EfGridData, MvIsoModelInterface {
  /** La variable courante pour laquelle les valeurs sont retourn�es */
  private ResultType var;
  /** Le temps courant pour lequel les valeurs sont retourn�es */
  private int tIdx=0;
  /** Tous les resultats dans le repere courant pour tous les temps */
  private PivResultsI[] results;
  /** Tous les resultats pour tous les temps */
  private PivResultsI[] resData;
  /** Le projet associ� */
  private PivProject prj_;
  /** Pour emettre un evenement de propri�t� */
  private PropertyChangeSupport propDelegate=new PropertyChangeSupport(this);
  /** La progression */
  ProgressionInterface prog_;  
  /** Le system de coordonn�es */
  private boolean isSystemInitial_=false;
  
  /**
   * Construction avec les resultats.
   * @param _res Les resultats.
   */
  public PivResultsModel(PivProject _prj, ProgressionInterface _prog, PivResultsI... _res) {
    super(null);
    prj_=_prj;
    prog_=_prog;
    setResults(_res);
  }
  
  public void setResults(PivResultsI[] _res) {
    PivResultsI[] oldRes=resData;
    resData=_res;
    
    setSystemViewInitial(isSystemInitial_);
    
    if (results != null && results.length != 0 && var == null) {
      if (results[0].hasResult(ResultType.NORME))
        var=ResultType.NORME;
      else
        var=results[0].getResults()[0];
    }
    
    propDelegate.firePropertyChange("results", oldRes, resData);
  }
  
  /**
   * Definit si le system de coordonn�es est initial ou calcul.
   * @param _b True : Initial
   */
  public void setSystemViewInitial(boolean _b) {
    if (!_b) {
      results=resData;
    }
    else {
      results=adaptResultsToReal(resData);
    }
    
    EfGrid grid;
    // On cr�e un maillage vide pour eviter les traceback si aucun resultat n'existe, ou si le nombre de points est insuffisant pour
    // cr�er un maillage.
    if (results==null || results.length == 0 || results[0].getNbPoints() < 3) {
      grid = new EfGrid(new EfNode[0],new EfElement[0]);
    }
    else {
      grid = buildFromResults(results[0], prog_);
    }
    setGrid(grid);
    
    propDelegate.firePropertyChange("systemView", isSystemInitial_, isSystemInitial_=_b);
  }
  
  /**
   * @return Les resultats avec les coordonn�es adapt�es dans le repere courant.
   */
  protected PivResultsI[] adaptResultsToReal(PivResultsI... _res) {
    if (_res==null)
      return null;
    
    PivResultsI[] ret=new PivResultsI[_res.length];
    for (int i=0; i<ret.length; i++)
      ret[i]=new PivResultsTransformationAdapter(_res[i], prj_.getTransformationParameters().getToInitial());
    return ret;
  }
  
  /**
   * Ajout d'un calque listener pour les propri�t�s du mod�le (hors datas).
   * @param _listener Le listener
   */
  public void addPropertyChangeListener(PropertyChangeListener _listener) {
    propDelegate.addPropertyChangeListener(_listener);
  }
  
  /**
   * Suppression d'un calque listener pour les propri�t�s du mod�le (hors datas).
   * @param _listener Le listener
   */
  public void removePropertyChangeListener(PropertyChangeListener _listener) {
    propDelegate.removePropertyChangeListener(_listener);
  }

  /**
   * Construction d'une grille de maillage depuis les r�sultats.
   * @param _res Les r�sultats
   * @param _prog La tache de progression.
   * @return La grille de maillage cr��e
   */
  private EfGrid buildFromResults(PivResultsI _res, ProgressionInterface _prog) {
    // Si le nombre est inf�rieur � 3, on ne peut pas trianguler.
    if (_res.getNbPoints() <3)
      return null;
    
    List<TriangulationPoint> pointSet = new ArrayList<TriangulationPoint>();
    for (int i = 0; i < _res.getNbPoints(); i++) {
      pointSet.add(new TPoint(_res.getX(i), _res.getY(i)));
    }
    PointSet pt = new PointSet(pointSet);
//    updater.majProgessionStateOnly(PivResource.getS("Triangulation"));
    try {
      Poly2Tri.triangulate(pt);
    } catch (Exception e) {
      Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "message {0}", e);
    } catch (StackOverflowError e) {
      Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "message {0}", e);
    }
    
    
    return createGrid(pt.getTriangles(), _prog);
  }

  /**
   * Construction d'une grille de maillage depuis une triangulation Delaunay.
   * @param _triangles La triangulation Delaunay
   * @param _prog La tache de progression
   * @return La grille cr��
   */
  private EfGrid createGrid(List<DelaunayTriangle> _triangles, ProgressionInterface _prog) {
    if (_triangles.isEmpty()) {
      return null;
    }
    // Remplissage de la table de correspondance des coordonn�es -> indices 
    TreeMap<Coordinate, Integer> map = new TreeMap<Coordinate, Integer>(new CoordinateComparator());
    for (int i=0; i<results[0].getNbPoints(); i++) {
      Coordinate c=new Coordinate(results[0].getX(i),results[0].getY(i));
      map.put(c, i);
    }
    
    EfElement[] elts = new EfElement[_triangles.size()];
    ProgressionUpdater updater = new ProgressionUpdater(_prog);
    updater.majProgessionStateOnly(PivResource.getS("Construction des �l�ments"));
    updater.setValue(10, _triangles.size());
    for (int idxElt = 0; idxElt < _triangles.size(); idxElt++) {
      final TriangulationPoint[] points = _triangles.get(idxElt).points;
      int[] elt = new int[points.length];
      for (int idxPt = 0; idxPt < points.length; idxPt++) {
        TriangulationPoint triangulationPoint = points[idxPt];
        Coordinate c = new Coordinate(triangulationPoint.getX(), triangulationPoint.getY());
        Integer ptIdx = map.get(c);
        if (ptIdx == null) {
          FuLog.error(PivResource.getS("Correspondance impossible des indices de noeuds lors de la creation du maillage"));
        }
        elt[idxPt] = ptIdx;
      }
      elts[idxElt] = new EfElement(elt);
      updater.majAvancement();
    }
    updater.majProgessionStateOnly(PivResource.getS("Construction des noeuds"));
    updater.setValue(10, map.size());
    EfNode[] nodes = new EfNode[map.size()];
    for (Map.Entry<Coordinate, Integer> entry : map.entrySet()) {
      Integer idxNode = entry.getValue();
      nodes[idxNode] = new EfNode(entry.getKey());
      updater.majAvancement();
    }
    final EfGrid efGrid = new EfGrid(nodes, elts);
    efGrid.computeBord(_prog, new CtuluAnalyze());
    return efGrid;
  }

  @Override
  public boolean isElementVar(CtuluVariable _idxVar) {
    return false;
  }

  @Override
  public boolean isDefined(CtuluVariable _var) {
    if (results==null) return false;
    return results[0].hasResult((ResultType)_var);
  }

  @Override
  public EfData getData(CtuluVariable _o, int _timeIdx) throws IOException {
    if (results==null) return null;
    
    EfData data=new EfDataNode(results[_timeIdx].getValues((ResultType)_o));
    return data;
  }

  @Override
  public double getData(CtuluVariable _var, int _timeIdx, int _idxObjet) {
    if (results==null) return 0.;
    
    double r=results[_timeIdx].getValue(_idxObjet,(ResultType)_var);
    return r;
  }

  @Override
  public double[] fillWithData(int _idxElement, double[] _l) {
    final EfElement el = getGrid().getElement(_idxElement);
    final int n = el.getPtNb();
    double[] r = _l;
    if ((r == null) || (r.length != n)) {
      r = new double[n];
    }
    for (int i = 0; i < n; i++) {
      r[i] = getData(var, tIdx, el.getPtIndex(i));
    }
    return r;
  }

  /**
   * @return La variable courante
   */
  public CtuluVariable getCurrentVar() {
    return var;
  }

  /**
   * Definit la variable courante du modele (les resultats retourn�s sont
   * pour cette variable)
   * @param _var La variable
   */
  public void setCurrentVar(ResultType _var) {
    if (var==_var) return;
    
    ResultType old=var;
    var = _var;
    propDelegate.firePropertyChange("var", old, var);
  }

  /**
   * @return L'indice de temps courant
   */
  public int getCurrentTimeIdx() {
    return tIdx;
  }

  /**
   * Definit l'indice de temps courant du modele (les resultats retourn�s sont
   * pour cet indice)
   * @param _tIdx L'indice de temps
   */
  public void setCurrentTimeIdx(int _tIdx) {
    if (tIdx==_tIdx) return;
    
    int old=tIdx;
    tIdx = _tIdx;
    propDelegate.firePropertyChange("tIdx", old, tIdx);
  }
  
  /**
   * @return Les variables r�sultats.
   */
  public CtuluVariable[] getVariables() {
    if (results==null || results.length==0) return new CtuluVariable[0];
    
    return results[0].getResults();
  }
  
  /**
   * @return Le nombre de temps du mod�le
   */
  public int getNbTime() {
    return results==null ? 0 : results.length;
  }

  @Override
  public double getDatatFor(int idxElt, int idxPtOnElt) {
    return getData(var, tIdx, getGrid().getElement(idxElt).getPtIndex(idxPtOnElt));
  }
}
