/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fudaa.fudaa.piv.layer;

import org.fudaa.ebli.calque.ZCalqueImageRaster;

/**
 * Un calque image, avec possibilité de remplacement de l'image en cours.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivImageRasterLayer extends ZCalqueImageRaster {

  @Override
  public PivAbstractImageModel getModelImage() {
    return (PivAbstractImageModel)super.getModelImage();
  }



}
