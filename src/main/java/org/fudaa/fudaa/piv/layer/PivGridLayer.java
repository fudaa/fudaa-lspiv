package org.fudaa.fudaa.piv.layer;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;

/**
 * Le layer pour les points de grille.
 */
public class PivGridLayer extends ZCalquePointEditable {

  PivVisuPanel pnCalques_;
  
  public PivGridLayer(PivVisuPanel pnCalques) {
    super();
    
    pnCalques_ = pnCalques;
  }

  
  /**
   * On controle qu'il y a des resultats, et on demande a l'utilisateur s'il souhaite continuer.
   */
  @Override
  public boolean removeSelectedObjects(CtuluCommandContainer _cmd, CtuluUI _ui) {
    if (pnCalques_.getProject().hasInstantRawResults()
        && !pnCalques_.getCtuluUI().question(PivResource.getS("Suppression des r�sultats"), PivResource.getS("Attention : des r�sultats existent et seront supprim�s si vous modifiez la grille.\nVoulez-vous continuer ?"))) {
      return false;
    }
    
    pnCalques_.getProject().setInstantRawResults(null);
    
    return super.removeSelectedObjects(_cmd, _ui);
  }
}
