package org.fudaa.fudaa.piv.layer;

import java.awt.geom.Point2D;
import java.io.File;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.fudaa.ctulu.CtuluImageContainer;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.piv.metier.PivProject;

/**
 * Un modele pour l'image dans l'espace r�el.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivRealImageModel extends PivAbstractImageModel {
  // Le projet.
  PivProject prj_;

  /**
   * Constructeur
   */
  public PivRealImageModel() {
    super();
  }

  /**
   * Redefinit le projet, et remet a jour le modele
   * @param _prj Le projet associ� au mod�le.
   */
  public void setProjet(PivProject _prj) {
    prj_=_prj;
    update();
  }

  @Override
  public PivProject getProject() {
    return prj_;
  }

  /**
   * Met a jour le mod�le en fonction de la premi�re image transform�e.
   */
  public void update() {
    setSelectedImage(0);
  }

  /**
   * Modifie l'image affich�e suivant son index dans le projet.
   * @param _idx L'index de l'image dans le projet.
   */
  @Override
  public void setSelectedImage(int _idx) {

    File[] imgFiles=prj_.getTransfImageFiles();
    
    GrMorphisme toReal=prj_.getTransformationParameters().getToReal();
    
    // Si l'indice d'image est bien dans les images disponibles
    if (_idx!=-1 && imgFiles.length>_idx && prj_.getOrthoParameters()!=null) {
      File img=prj_.getCacheImageFile(imgFiles[_idx]);
      ImageInputStream iis=prj_.getRealCacheImageInputStream(img);

      // Repris de CtuluLibImage.getImageReader(), mais en utilisant l'extension (pgm) au lieu
      // du nom de reader (pnm)
      String extension = CtuluLibFile.getExtension(img.getName());
      final Iterator<ImageReader> it = ImageIO.getImageReadersBySuffix(extension);
      ImageReader imgReader = null;
      if (it != null && it.hasNext()) {
        imgReader = it.next();
      }
      if (imgReader==null) return;

      imgReader.setInput(iis);

      CtuluImageContainer imgCont=new CtuluImageContainer(imgReader,img,false);

      Point2D.Double[] imgPts=new Point2D.Double[]{
        new Point2D.Double(0,0),
        new Point2D.Double(imgCont.getImageWidth(),0),
        new Point2D.Double(0,imgCont.getImageHeight())
      };
      GrPoint[] transfPts=new GrPoint[]{
        new GrPoint(prj_.getOrthoParameters().getXmin(),prj_.getOrthoParameters().getYmin(),0).applique(toReal),
        new GrPoint(prj_.getOrthoParameters().getXmax(),prj_.getOrthoParameters().getYmin(),0).applique(toReal),
        new GrPoint(prj_.getOrthoParameters().getXmin(),prj_.getOrthoParameters().getYmax(),0).applique(toReal)
      };
      Point2D.Double[] realPts=new Point2D.Double[transfPts.length];
      for (int i=0; i<realPts.length; i++)
        realPts[i]=new Point2D.Double(transfPts[i].x_, transfPts[i].y_);

      setImage(imgCont);
      setProj(imgPts, realPts);
    }
    // Aucune image dans le projet, le mod�le fonctionnera sans image.
    else {
      setImage(null);
    }
  }
}
