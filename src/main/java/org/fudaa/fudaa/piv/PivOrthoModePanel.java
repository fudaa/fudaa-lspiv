package org.fudaa.fudaa.piv;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.border.EtchedBorder;

import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivProject.OrthoMode;

import com.memoire.bu.BuVerticalLayout;

/**
 * Un panneau de saisie du mode d'orthorectification.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivTransectParamPanel.java 9500 2017-01-09 17:22:31Z bmarchan $
 */
public class PivOrthoModePanel extends CtuluDialogPanel {
  PivImplementation impl_;
  JRadioButton rbScaling_;
  JRadioButton rbOrtho_;

  /**
   * Constructeur.
   */
  public PivOrthoModePanel(PivImplementation _impl) {
    impl_=_impl;
    customize();
  }

  private void customize() {
    rbScaling_ = new JRadioButton(PivResource.getS("Mise � l'�chelle"));
    rbOrtho_ = new JRadioButton(PivResource.getS("Orthorectification compl�te"));
    rbOrtho_.setSelected(true);
    ButtonGroup bg=new ButtonGroup();
    bg.add(rbScaling_);
    bg.add(rbOrtho_);
    
    setLayout(new BuVerticalLayout(2));
    setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
    add(rbScaling_);
    add(rbOrtho_);
  }
  
  public void setModeFullOrtho(boolean _b) {
    if (_b) {
      rbOrtho_.setSelected(true);
    }
    else {
      rbScaling_.setSelected(true);
    }
    
  }
  
  public boolean isModeFullOrtho() {
    return rbOrtho_.isSelected();
  }

  @Override
  public boolean apply() {
    if (impl_.getCurrentProject().getOrthoMode() != PivProject.OrthoMode.SCALING ^ isModeFullOrtho()) {
       if (impl_.getCurrentProject().hasFlowResults() && !impl_.question(PivResource.getS("Suppression des r�sultats"), 
           PivResource.getS("Attention : des r�sultats existent et seront supprim�s si vous modifiez les param�tres.\nVoulez-vous continuer ?"))) {
         return false;
       }
       impl_.getCurrentProject().setOrthoMode(isModeFullOrtho() ? OrthoMode.ORTHO_2D:OrthoMode.SCALING);
    }

    return super.apply();
  }
}
