/*
 * @creation 8 sept. 06
 * @modification $Date: 2009-06-03 15:10:45 +0200 (mer., 03 juin 2009) $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv;

import org.fudaa.fudaa.piv.io.PivExeLauncher;
import com.jidesoft.swing.FolderChooser;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JTextField;
import org.fudaa.ctulu.gui.CtuluDialogPanel;


/**
 * Un panneau de saisie du repertoire des exes.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivExePanel extends CtuluDialogPanel {
  
  private JTextField tfExePath;
  private JButton btExePath;

  public PivExePanel() {

//    setLayout(new BuVerticalLayout(5,true,true));
//    add(new JLabel(PivResource.getS("R�pertoire contenant les executables")));
//    JPanel pnExeDir=new JPanel();
    setLayout(new BorderLayout(3,3));
    tfExePath=new JTextField();
    tfExePath.setPreferredSize(new Dimension(350,tfExePath.getPreferredSize().height));
    btExePath=new JButton();
    btExePath.setText("...");
    btExePath.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        FolderChooser fc=new FolderChooser(tfExePath.getText());
        if (fc.showOpenDialog(PivExePanel.this)==FolderChooser.APPROVE_OPTION) {
          tfExePath.setText(fc.getSelectedFolder().getPath());
          setErrorText("");
        }
      }
    });
    add(tfExePath,BorderLayout.CENTER);
    add(btExePath,BorderLayout.EAST);
//    add(pnExeDir);

    tfExePath.setText(PivExeLauncher.instance().getExePath().getPath());

    isDataValid();
//    setErrorText(PivResource.getS("Le r�pertoire des ex�cutables n'a pas encore �t� pr�cis� ou n'est pas valide."));
  }

  @Override
  public boolean isDataValid() {
    if (tfExePath.getText().trim().equals("")) {
      setErrorText(PivResource.getS("Vous devez pr�ciser un nom de r�pertoire"));
      return false;
    }
    File exePath=new File(tfExePath.getText());
    if (!exePath.isDirectory()) {
      setErrorText(PivResource.getS("Le chemin donn� n'est pas un r�pertoire ou n'existe pas"));
      return false;
    }
    String[] missingExe=PivExeLauncher.instance().getMissingExeOrCmdIn(exePath);
    if (missingExe.length!=0) {
      
      String mes=missingExe[0];
      for (int i=1; i<missingExe.length; i++) {
        mes+=", "+missingExe[i];
      }
      setErrorText(PivResource.getS("Les executables suivants manquent dans le r�pertoire donn�:\n")+mes);
      return false;
    }

    return true;
  }

  /**
   * Fermeture du dialogue, et mise a jour du path.
   */
  private void close() {
    PivExeLauncher.instance().setExePath(new File(tfExePath.getText().trim()));
    PivPreferences.PIV.putStringProperty(PivPreferences.PIV_EXE_PATH,PivExeLauncher.instance().getExePath().getPath());
    PivPreferences.PIV.writeIniFile();
  }

  /**
   * Appel� quand le bouton "ok" est activ�.
   */
  @Override
  public boolean ok() {
    close();
    return super.ok();
  }
}