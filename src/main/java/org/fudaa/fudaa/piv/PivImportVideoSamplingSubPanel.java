/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.piv;

import java.awt.Cursor;
import java.awt.Dimension;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.fudaa.piv.metier.PivSamplingVideoParameters;
import org.fudaa.fudaa.piv.metier.PivTransectParams;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuInsets;
import com.memoire.bu.BuLib;

/**
 * Un panneau de saisie des param�tres des transects s�lectionn�s pour le calcul des d�bits.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivImportVideoSamplingSubPanel extends JPanel {

  PivImplementation impl_;
  PivImportVideoParamPanel pnMain_;
  PivTransectParams[] transParams_;
  private PivLinkNumericalTextFields<Integer,Integer> lnkSize_;

  /**
   * Constructeur.
   */
  public PivImportVideoSamplingSubPanel(PivImportVideoParamPanel _pnMain, PivImplementation _impl) {
    pnMain_ = _pnMain;
    impl_=_impl;
    initComponents();
    customize();
  }

  private void customize() {
    setBorder(BorderFactory.createTitledBorder(PivResource.getS("Echantillonnage")));
    
    btInitBeginning_.setToolTipText(PivResource.getS("Initialiser depuis la pr�visualisation"));
    btInitBeginning_.setText(PivResource.getS("Initialiser"));
    btInitBeginning_.addActionListener(e -> {
      int ind = pnMain_.getPreviewPanel().getImageIndex();
      setBeginTime(pnMain_.imageIndex2Time(ind));
    });
    
    btInitEnding_.setToolTipText(PivResource.getS("Initialiser depuis la pr�visualisation"));
    btInitEnding_.setText(PivResource.getS("Initialiser"));
    btInitEnding_.addActionListener(e -> {
      int ind = pnMain_.getPreviewPanel().getImageIndex();
      setEndTime(pnMain_.imageIndex2Time(ind));
    });

    // BM : Repris de BuTransparentToggleButton, on ne peut pas creer un BuTransparentToggleButton dans le designer de NetBeans.
    btLockSize_.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    btLockSize_.setIcon(CtuluResource.CTULU.getIcon("non-lie.png"));
    btLockSize_.setMargin(BuInsets.INSETS0000);
    btLockSize_.setBorder(BuBorders.EMPTY0000);
    btLockSize_.setRequestFocusEnabled(false);
    btLockSize_.setOpaque(false);
    btLockSize_.setContentAreaFilled(false);
    btLockSize_.setHorizontalAlignment(SwingConstants.LEFT);
    btLockSize_.setRolloverEnabled(true);
    btLockSize_.setToolTipText(PivResource.getS("Lie/d�lie les dimensions suivant X et Y"));
    BuLib.setSelectedIcons(btLockSize_,CtuluResource.CTULU.getIcon("lie.png"),true);
    
    btLockSize_.setEnabled(true);
    btLockSize_.setSelected(true);
    btLockSize_.addActionListener((evt) -> {
      lnkSize_.setActive(btLockSize_.isSelected());
    });
    
    lbXSize_.setText(PivResource.getS("Dimension X:"));
    lbYSize_.setText(PivResource.getS("Dimension Y:"));
    lbNbKeptImgs_.setText(PivResource.getS("Conserver 1 image sur:"));
    lbTimeStep_.setText(PivResource.getS("Intervalle de temps")+" (s):");
    lbBeginning_.setText(PivResource.getS("Instant de d�but")+" (s):");
    lbEnding_.setText(PivResource.getS("Instant de fin")+" (s):");
    
    
    // Pour lier les 2 champs num�riques.
    new PivLinkNumericalTextFields<Double,Integer>(tfTimeStep_, tfNbKeptImgs_) {

      @Override
      public Double calculateFromTF2() {
        double val = Double.parseDouble(pnMain_.tfInitialNbImgSec_.getText());
        if (val==0)
          return 0.;
        
        double valtf2 = Integer.parseInt(tf2_.getText());
        if (valtf2 == 0)
          return 0.;
        
        return Integer.parseInt(tf2_.getText())/val;
      }
      
      @Override
      protected DecimalFormat getTF1DecimalFormat() {
        return CtuluLib.getDecimalFormat(5);
      }

      @Override
      public Integer calculateFromTF1() {
        double val = Double.parseDouble(pnMain_.tfInitialNbImgSec_.getText());
        if (val==0)
          return 0;
        
        double valtf1 = Double.parseDouble(tf1_.getText());
        if (valtf1 == 0)
          return 0;
        
        return Math.max(1,(int)(0.5+val*valtf1));
      }
    };
    
    lnkSize_ = new PivLinkNumericalTextFields<Integer,Integer>(tfXSize_, tfYSize_) {

      @Override
      public Integer calculateFromTF2() {
        int val = Integer.parseInt(tfYSize_.getText());
//        if (val==0)
//          return 0.;
//        
//        double valtf2 = Integer.parseInt(tf2_.getText());
//        if (valtf2 == 0)
//          return 0.;
        
        return (int)( val * ((double)pnMain_.widthVideo_ / (double)pnMain_.heightVideo_));
      }

      @Override
      public Integer calculateFromTF1() {
        int val = Integer.parseInt(tfXSize_.getText());
//      if (val==0)
//        return 0.;
//      
//      double valtf2 = Integer.parseInt(tf2_.getText());
//      if (valtf2 == 0)
//        return 0.;
      
      return (int)( val * ((double)pnMain_.heightVideo_ / (double)pnMain_.widthVideo_));
      }
    };
    
    this.setSamplingPanelEnabled(false);
  }
  
  /**
   * @param _b True : Active le panneau d'�chantillonage. False : Desactive.
   */
  protected void setSamplingPanelEnabled(boolean _b) {
    tfNbKeptImgs_.setEnabled(_b);
    tfBeginning_.setEnabled(_b);
    tfEnding_.setEnabled(_b);
    tfTimeStep_.setEnabled(_b);
    btInitBeginning_.setEnabled(_b);
    btInitEnding_.setEnabled(_b);
    btLockSize_.setEnabled(_b);
    tfXSize_.setEnabled(_b);
    tfYSize_.setEnabled(_b);
  }
  
  public void setSamplingVideoParameters(PivSamplingVideoParameters _params) {
    if (_params == null)
      return;
    
    lnkSize_.setActive(false);
    tfBeginning_.setText(CtuluLib.getDecimalFormat().format(_params.getSamplingBeginTime()));
    tfEnding_.setText(CtuluLib.getDecimalFormat().format(_params.getSamplingEndTime()));
    tfYSize_.setText(""+_params.getSamplingSize().height);
    tfXSize_.setText(""+_params.getSamplingSize().width);
    tfNbKeptImgs_.setText(""+_params.getSamplingPeriod());
  }

  /**
   * @return La periode de conservation des images
   */
  public int getPeriodImgs() {
    return Integer.parseInt(tfNbKeptImgs_.getText());
  }

  public void setPeriodImgs(int period) {
    tfNbKeptImgs_.setText("" + period);
  }
  
  public Double getBeginTime() {
    return Double.parseDouble(tfBeginning_.getText());
  }
  
  public void setBeginTime(Double t) {
    tfBeginning_.setText(CtuluLib.getDecimalFormat().format(t));
  }

  public Double getEndTime() {
    return Double.parseDouble(tfEnding_.getText());
  }

  public void setEndTime(Double t) {
    tfEnding_.setText(CtuluLib.getDecimalFormat().format(t));
  }

  public Dimension getVideoSize() {
    return new Dimension(Integer.parseInt(tfXSize_.getText()), Integer.parseInt(tfYSize_.getText()));
  }

  public void setVideoSize(Dimension size) {
    tfXSize_.setText("" + size.width);
    tfYSize_.setText("" + size.height);
    lnkSize_.setActive(btLockSize_.isSelected());
  }

  public boolean isDataValid() {
    if (PivUtils.isStrictPositiveInteger(pnMain_, tfNbKeptImgs_.getText(),PivResource.getS("Conserver 1 image sur"))==null) {
      return false;
    }
    if (PivUtils.isPositiveReal(pnMain_, tfBeginning_.getText(),PivResource.getS("Instant de d�but"))==null) {
      return false;
    }
    if (PivUtils.isPositiveReal(pnMain_, tfEnding_.getText(),PivResource.getS("Instant de fin"))==null) {
      return false;
    }
    if (Double.parseDouble(tfEnding_.getText())<=Double.parseDouble(tfBeginning_.getText())) {
      pnMain_.setErrorText(PivResource.getS("L'instant de fin doit �tre sup�rieur � l'instant de d�but"));
      return false;
    }
    if (PivUtils.isStrictPositiveInteger(pnMain_, tfXSize_.getText(),PivResource.getS("Dimension X"))==null) {
      return false;
    }
    if (PivUtils.isStrictPositiveInteger(pnMain_, tfYSize_.getText(),PivResource.getS("Dimension Y"))==null) {
      return false;
    }
    
    pnMain_.setErrorText("");
    return true;
  }
  
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbXSize_ = new javax.swing.JLabel();
        lbYSize_ = new javax.swing.JLabel();
        lbNbKeptImgs_ = new javax.swing.JLabel();
        tfXSize_ = new javax.swing.JTextField();
        tfYSize_ = new javax.swing.JTextField();
        btLockSize_ = new javax.swing.JToggleButton();
        tfNbKeptImgs_ = new javax.swing.JTextField();
        tfTimeStep_ = new javax.swing.JTextField();
        tfBeginning_ = new javax.swing.JTextField();
        lbTimeStep_ = new javax.swing.JLabel();
        lbBeginning_ = new javax.swing.JLabel();
        tfEnding_ = new javax.swing.JTextField();
        lbEnding_ = new javax.swing.JLabel();
        btInitBeginning_ = new javax.swing.JButton();
        btInitEnding_ = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createTitledBorder("Echantillonnage"));

        lbXSize_.setText("Dimension X:");
        lbXSize_.setName(""); // NOI18N

        lbYSize_.setText("Dimension Y:");

        lbNbKeptImgs_.setText("Conserver 1  image sur:");

        lbTimeStep_.setText("Intervalle de temps (s):");

        lbBeginning_.setText("Instant de d�but (s):");

        lbEnding_.setText("Instant de fin (s):");

        btInitBeginning_.setText("Initialiser");

        btInitEnding_.setText("Initialiser");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbXSize_)
                    .addComponent(lbYSize_)
                    .addComponent(lbNbKeptImgs_)
                    .addComponent(lbTimeStep_)
                    .addComponent(lbBeginning_)
                    .addComponent(lbEnding_))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tfEnding_)
                    .addComponent(tfBeginning_)
                    .addComponent(tfTimeStep_)
                    .addComponent(tfYSize_)
                    .addComponent(tfXSize_, javax.swing.GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE)
                    .addComponent(tfNbKeptImgs_))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btLockSize_, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btInitBeginning_, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btInitEnding_, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfXSize_, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbXSize_))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfYSize_, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbYSize_))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfNbKeptImgs_, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbNbKeptImgs_, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btLockSize_, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfTimeStep_, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbTimeStep_))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfBeginning_, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbBeginning_)
                    .addComponent(btInitBeginning_))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfEnding_, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbEnding_)
                    .addComponent(btInitEnding_))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btInitBeginning_;
    private javax.swing.JButton btInitEnding_;
    private javax.swing.JToggleButton btLockSize_;
    private javax.swing.JLabel lbBeginning_;
    private javax.swing.JLabel lbEnding_;
    private javax.swing.JLabel lbNbKeptImgs_;
    private javax.swing.JLabel lbTimeStep_;
    private javax.swing.JLabel lbXSize_;
    private javax.swing.JLabel lbYSize_;
    private javax.swing.JTextField tfBeginning_;
    private javax.swing.JTextField tfEnding_;
    private javax.swing.JTextField tfNbKeptImgs_;
    private javax.swing.JTextField tfTimeStep_;
    private javax.swing.JTextField tfXSize_;
    private javax.swing.JTextField tfYSize_;
    // End of variables declaration//GEN-END:variables
}
