/**
 * @creation     2002-09-25
 * @modification $Date: 2006/09/19 15:10:20 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.piv;

import com.memoire.bu.BuResource;
import com.memoire.fu.FuLib;

import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * Une classe de ressources pour Fudaa-Piv, en particulier la traduction de
 * langage.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public final class PivResource extends FudaaResource {

  /**
   * Singleton, utilis� pour la r�cup�ration des icones propres � Fudaa-Piv.
   */
  public final static PivResource PIV = new PivResource(FudaaResource.FUDAA);

  private PivResource(final BuResource _b) {
    super(_b);
  }

  /**
   * Traduit et retourne la chaine traduite, avec ou sans valeurs � ins�rer.
   *
   * @param _s La chaine � traduire.
   * @param _vals Les valeurs, de n'importe quelle type.
   * @return La chaine traduite.
   */
  public static String getS(String _s, Object ... _vals) {
    String r = PIV.getString(_s);
    if (r == null) {
      return r;
    }

    for (int i=0; i<_vals.length; i++) {
      r = FuLib.replace(r, "{"+i+"}", _vals[i].toString());
    }
    return r;
  }
}