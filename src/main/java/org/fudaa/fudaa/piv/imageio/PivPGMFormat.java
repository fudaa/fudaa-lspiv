package org.fudaa.fudaa.piv.imageio;

/**
 * Les formats possibles pour les PGM.
 * @author marchand
 *
 */
public enum PivPGMFormat {
  P2,
  P5
}
