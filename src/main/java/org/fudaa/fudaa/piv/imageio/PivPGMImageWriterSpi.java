package org.fudaa.fudaa.piv.imageio;

import java.util.Locale;

import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriter;
import javax.imageio.spi.ImageWriterSpi;

public class PivPGMImageWriterSpi extends ImageWriterSpi {
  
  static final String vendorName = "DeltaCAD";
  static final String version = "1.0";
  static final String writerClassName = "org.fudaa.fudaa.piv.imageio.PivPGMImageWriter";
  static final String[] names = { "pgm" };
  static final String[] suffixes = { "pgm" };
  static final String[] MIMETypes = { "image/x-portable-graymap" };
  static final String[] readerSpiNames = { "org.fudaa.fudaa.piv.imageio.PivPGMImageReaderSpi" };
    
  static final boolean supportsStandardStreamMetadataFormat = false;
  static final String nativeStreamMetadataFormatName = null;
  static final String nativeStreamMetadataFormatClassName = null;
  static final String[] extraStreamMetadataFormatNames = null;
  static final String[] extraStreamMetadataFormatClassNames = null;
  static final boolean supportsStandardImageMetadataFormat = false;
  static final String nativeImageMetadataFormatName = null;
  static final String nativeImageMetadataFormatClassName = null;
  static final String[] extraImageMetadataFormatNames = null;
  static final String[] extraImageMetadataFormatClassNames = null;
    
  /**
   * Cette classe n'est pas assez avanc�e pour etre utilis�e.
   * @author Bertrand Marchand
   * @deprecated Use {@link PivPGMWriter} instead
   */
  public PivPGMImageWriterSpi() {
    super(
        vendorName,
        version,
        names,
        suffixes,
        MIMETypes,
        writerClassName,
        STANDARD_OUTPUT_TYPE, // Write to ImageOutputStreams
        readerSpiNames,
        supportsStandardStreamMetadataFormat,
        nativeStreamMetadataFormatName,
        nativeStreamMetadataFormatClassName,
        extraStreamMetadataFormatNames,
        extraStreamMetadataFormatClassNames,
        supportsStandardImageMetadataFormat,
        nativeImageMetadataFormatName,
        nativeImageMetadataFormatClassName,
        extraImageMetadataFormatNames,
        extraImageMetadataFormatClassNames);
  }

  public boolean canEncodeImage(ImageTypeSpecifier imageType) {
    int bands = imageType.getNumBands();
    return bands == 1 || bands == 3;
  }
    
  public String getDescription(Locale locale) {
    // Localize as appropriate
    return "Grey Ascii PGM";
  }

  public ImageWriter createWriterInstance(Object extension) {
      return new PivPGMImageWriter(this);
  }

}
