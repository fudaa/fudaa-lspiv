package org.fudaa.fudaa.piv.imageio;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.memoire.fu.FuLog;

/**
 * Une classe d'ecriture d'une image PGM. Cette classe ne respecte pas le standard IIO, mais
 * est beaucoup simple et rapide.
 * 
 * @author Bertrand Marchand
 * @since 1.3
 * @version $Id$
 */
public class PivPGMWriter {
  
  private PivPGMFormat format_ = "P5".equalsIgnoreCase(System.getProperty("piv.pgm.format")) ? PivPGMFormat.P5:PivPGMFormat.P2;
  
  /**
   * Definit le format de sortie du fichier.
   * @param _fmt LE format.
   */
  public void setFormat(PivPGMFormat _fmt) {
    format_ = _fmt;
  }
  
  /**
   * Lit le buffer en entr�e sur un fichier PGM. Le buffer est converti en niveaux de gris.
   * @param _srcFile Le fichier image.
   * @param _img L'image bufferis�e.
   * @throws IOException Si le fichier ne peut �tre ecrit.
   */
  public void write(File _srcFile, BufferedImage _img) throws IOException {
    FuLog.trace("Ecriture du fichier "+_srcFile.getName());
    
    int width=_img.getWidth();
    int height=_img.getHeight();
    
    // On passe le buffer en niveaux de gris obligatoirement.
    BufferedImage bgrey=new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
    bgrey.getGraphics().drawImage(_img, 0, 0, null);
    
    PrintWriter writer=new PrintWriter(new FileWriter(_srcFile));
    
    if (format_ == PivPGMFormat.P2) {
      writeP2Format(writer, bgrey, width, height);
    }
    else {
      writeP5Format(writer, bgrey, width, height);
    }
    
    writer.close();
  }
  
  /**
   * Ecriture en format P2 (ascii)
   * @param writer
   * @param bgrey
   * @param width
   * @param height
   */
  public void writeP2Format(PrintWriter writer, BufferedImage bgrey, int width, int height) {
    
    writer.println("P2");
    writer.println(width+" "+height);
    writer.println("255");

    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        int val = bgrey.getRaster().getSample(j, i, 0);
        writer.print(val + " ");
      }
      writer.println();
    }
  }
  
  /**
   * Ecriture en format P5 (binaire)
   * @param writer
   * @param bgrey
   * @param width
   * @param height
   */
  public void writeP5Format(PrintWriter writer, BufferedImage bgrey, int width, int height) {
    
    writer.println("P5");
    writer.println(width+" "+height);
    writer.println("255");

    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        int val = bgrey.getRaster().getSample(j, i, 0);
        writer.write(val);
      }
    }
    
  }
}
