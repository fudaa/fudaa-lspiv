package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.text.NumberFormat;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;

/**
 *
 * Un panneau pour la v�rification par affichage des coordonn�es r�elles donn�es
 * et recalcul�es des points d'orthorectification
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivOrthoVerifyGRPPanel extends CtuluDialogPanel {
  PivOrthoPoint[] pts_;
  PivImplementation impl_;
  boolean bImgVisible_;
  private GrMorphisme toReal_;

  /**
   * Une classe pour le modele de la table affichant les valeurs r�elles et
   * calcul�es.
   */
  private class GRPTableModel implements TableModel {

    public int getRowCount() {
      return pts_.length;
    }

    public int getColumnCount() {
      return 6;
    }

    public String getColumnName(int columnIndex) {
      switch (columnIndex) {
        default:
        case 0:
          return PivResource.getS("N�");
        case 1:
          return PivResource.getS("X r�el");
        case 2:
          return PivResource.getS("Y r�el");
        case 3:
          return PivResource.getS("X recalcul�");
        case 4:
          return PivResource.getS("Y recalcul�");
        case 5:
          return PivResource.getS("Ecart");
      }
    }

    public Class<?> getColumnClass(int columnIndex) {
      return columnIndex==0 ? Integer.class:Double.class;
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return true;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
      switch (columnIndex) {
        default:
        case 0:
          return rowIndex+1;
        case 1:
          return pts_[rowIndex].getRealPoint().applique(toReal_).x_;
        case 2:
          return pts_[rowIndex].getRealPoint().applique(toReal_).y_;
        case 3:
          return pts_[rowIndex].getComputeRealPoint().applique(toReal_).x_;
        case 4:
          return pts_[rowIndex].getComputeRealPoint().applique(toReal_).y_;
        case 5:
          return pts_[rowIndex].getError();
      }
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {}
    public void addTableModelListener(TableModelListener l) {}
    public void removeTableModelListener(TableModelListener l) {}
  }

  /**
   * Une classe pour une repr�sentation des doubles.
   */
  private class GRPCellRenderer extends DefaultTableCellRenderer {
    NumberFormat fmt = CtuluLib.getDecimalFormat(3);

    public GRPCellRenderer() {
      this.setHorizontalAlignment(SwingConstants.RIGHT);
      fmt.setMaximumFractionDigits(3);
      fmt.setMinimumFractionDigits(3);
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
      
      if (value instanceof Double) {
        this.setText(fmt.format((Double)value));
      }
      return this;
    }
  }

  /**
   * Le constructeur du panneau.
   * @param _impl L'implementation de l'application.
   */
  public PivOrthoVerifyGRPPanel(PivImplementation _impl) {
    impl_=_impl;
    pts_=_impl.getCurrentProject().getOrthoPoints();
    toReal_=_impl.getCurrentProject().getTransformationParameters().getToReal();
    
    // On efface le calque image, l'image qu'elle contient n'est pas forcement pertinente.
    bImgVisible_=impl_.get2dFrame().getVisuPanel().isImageLayerVisible();
    impl_.get2dFrame().getVisuPanel().setImageLayerVisible(false);

    customize();
  }

  private void customize() {
    setLayout(new BorderLayout(5,5));
    JTable tbResults=new JTable();
    tbResults.setModel(new GRPTableModel());
    tbResults.setDefaultRenderer(Double.class,new GRPCellRenderer());
    JScrollPane spResults=new JScrollPane();
    spResults.getViewport().add(tbResults);
    spResults.setPreferredSize(new Dimension(500,200));
    add(spResults, BorderLayout.CENTER);
  }

  /**
   * Surcharg� pour rendre invisible la calque des points de controle � la
   * fermeture du dialogue.
   * @return 
   */
  @Override
  public boolean apply() {
    impl_.get2dFrame().getVisuPanel().setControlPointsLayerVisible(false);
    impl_.get2dFrame().getVisuPanel().setImageLayerVisible(bImgVisible_);
    return true;
  }

  /**
   * Surcharg� pour rendre invisible la calque des points de controle � la
   * fermeture du dialogue.
   */
  @Override
  public boolean cancel() {
    impl_.get2dFrame().getVisuPanel().setControlPointsLayerVisible(false);
    impl_.get2dFrame().getVisuPanel().setImageLayerVisible(bImgVisible_);
    return super.cancel();
  }
}
