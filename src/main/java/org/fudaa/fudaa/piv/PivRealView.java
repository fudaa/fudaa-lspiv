/**
 * 
 */
package org.fudaa.fudaa.piv;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ebli.animation.EbliAnimationAction;
import org.fudaa.ebli.animation.EbliAnimationAdapterInterface;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueAffichage;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZCalqueMultiPointEditable;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.commun.EbliActionAbstract;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;
import org.fudaa.ebli.commun.EbliFormatter;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.palette.BPalettePlageDiscret;
import org.fudaa.ebli.palette.BPlageDiscret;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.piv.action.PivCreateParticleLinesAction;
import org.fudaa.fudaa.piv.action.PivCreateTransectAbsZAction;
import org.fudaa.fudaa.piv.action.PivInvertTransectAction;
import org.fudaa.fudaa.piv.action.PivNewTransectAction;
import org.fudaa.fudaa.piv.action.PivRealViewAction;
import org.fudaa.fudaa.piv.action.PivTransectParamAction;
import org.fudaa.fudaa.piv.layer.PivControlPointsModel;
import org.fudaa.fudaa.piv.layer.PivFlowResultsLayer;
import org.fudaa.fudaa.piv.layer.PivFlowResultsModel;
import org.fudaa.fudaa.piv.layer.PivImageRasterLayer;
import org.fudaa.fudaa.piv.layer.PivOrthoPointsModel;
import org.fudaa.fudaa.piv.layer.PivRealImageModel;
import org.fudaa.fudaa.piv.layer.PivResultsLayer;
import org.fudaa.fudaa.piv.layer.PivResultsModel;
import org.fudaa.fudaa.piv.layer.PivTransectLayer;
import org.fudaa.fudaa.piv.layer.PivVelResultsLayer;
import org.fudaa.fudaa.piv.layer.PivVelResultsModel;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivProjectStateListener;
import org.fudaa.fudaa.piv.metier.PivResultsI;
import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;
import org.fudaa.fudaa.piv.particles.PivParticlesLayer;
import org.fudaa.fudaa.piv.particles.PivResultSource;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.bu.BuTransparentToggleButton;

import si.uom.SI;

/**
 * Une vue en espace r�el.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivRealView implements PivViewI, PivProjectStateListener {
  /** Le nom de la vue en espace image r�el */
  public static final String TITLE=PivResource.getS("Espace r�el");
  /** La d�finition de coordonn�e pour X */
  private static final EbliCoordinateDefinition DEF_COOR_X=new EbliCoordinateDefinition("X", new EbliFormatter(SI.METRE));
  /** La d�finition de coordonn�e pour Y */
  private static final EbliCoordinateDefinition DEF_COOR_Y=new EbliCoordinateDefinition("Y", new EbliFormatter(SI.METRE));
  
  private PivNewTransectAction actCreateXYZTransect_;
  private PivCreateTransectAbsZAction actCreateAbsZTransect_;
  private PivTransectParamAction actParamsTransect_;
  private PivInvertTransectAction actInvertTransect_;
  private PivCreateParticleLinesAction actParticleLines_;
  private EbliActionAbstract actRealView_;
  private PivVisuPanel pnLayers_;
  private JComboBox<CtuluVariable> cbVar_;
  private JComboBox<String> cbImg_;
  private BuTransparentToggleButton btLock_;
  private JComboBox<String> cbVel_;
  private EbliAnimationAction actAnim_;
  private AbstractButton btAnim_;
  private AbstractButton btStreamLines_;
  private PivProject prj_;
  private List<BCalque> layers_=new ArrayList<BCalque>();
  private boolean enableEvents2_=true;
  private boolean enableEvents_=true;
  
  /** L'affichage des GRP */
  ZCalquePointEditable cqRealOrthoPoints_;
  /** L'affichage des GRP de controle */
  ZCalqueMultiPointEditable cqRealControlPoints_;
  /** L'affichage de l'image en coordon�es r�elles */
  PivImageRasterLayer cqRealImg_;
  /** L'affichage d'un transect */
  PivTransectLayer cqTransect_;
  /** L'affichage des vitesses moyenn�es */
  PivVelResultsLayer cqAveVelResults_;
  /** L'affichage des vitesses instantan�es */
  PivVelResultsLayer cqRawVelResults_;
  /** L'affichage des vitesses filtr�es instantan�es */
  PivVelResultsLayer cqFltVelResults_;
  /** L'affichage des r�sultats de d�bit */
  PivFlowResultsLayer cqFlowResults;
  /** L'affichage des r�sultats instantan�s filtr�s */
  PivResultsLayer cqFltIsoResults_;
  /** L'affichage des r�sultats instantan�s */
  PivResultsLayer cqRawIsoResults_;
  /** L'affichage des r�sultats moyenn�s */
  PivResultsLayer cqAveIsoResults_;
  /** L'affichage des r�sultats manuels */
  PivVelResultsLayer cqManualVelResults_;
  /** L'affichage des lignes de courant/trajectoires moyenn�es */
  PivParticlesLayer cqAveParticles_;
  /** L'affichage des lignes de courant/trajectoires brutes */
  PivParticlesLayer cqRawParticles_;
  /** L'affichage des lignes de courant/trajectoires filtr�es */
  PivParticlesLayer cqFltParticles_;

  PivOrthoPointsModel mdlRealOrthoPoints;
  PivRealImageModel mdlRealImage;
  PivControlPointsModel mdlRealControlPoints;
  PivVelResultsModel mdlVelResults;
  PivVelResultsModel mdlManualResults;
//  PivTransectModel mdlTransect;
  PivFlowResultsModel mdlFlowResults;
  PivVelResultsModel mdlInstantVelResults;
  PivVelResultsModel mdlInstantVelFltResults;
  PivResultsModel mdlInstantResults;
  PivResultsModel mdlInstantFltResults;
  PivResultsModel mdlAverageResults;
  PivResultsModel mdlParticleAverageResults_;
  PivResultsModel mdlParticleRawResults_;
  PivResultsModel mdlParticleFilteredResults_;

  /**
   * Une classe de d�finition de la s�quence d'animation.
   * @author Bertrand Marchand (marchand@deltacad.fr)
   */
  class SequenceAnimationAdapter implements EbliAnimationAdapterInterface {

    @Override
    public int getNbTimeStep() {
      return prj_.getTransfImageFiles().length;
    }

    @Override
    public String getTimeStep(int _idx) {
        return prj_.getTransfImageFiles()[_idx].getName();
    }

    @Override
    public double getTimeStepValueSec(int _idx) {
        return _idx;
    }

    @Override
    public void setTimeStep(int _idx) {
      cbImg_.setSelectedIndex(_idx);
    }

    @Override
    public String getTitle() {
      return cqRawVelResults_.getTitle();
    }
  }
  
  /**
   * Constructeur.
   * @param _pn Le panneau de calques.
   */
  public PivRealView(PivVisuPanel _pn) {
    pnLayers_=_pn;
    buildLayers();
    buildTools();
    
    pnLayers_.getArbreCalqueModel().addTreeSelectionListener(new TreeSelectionListener() {
      @Override
      public void valueChanged(TreeSelectionEvent e) {
        majToolsState();
      }
    });
  }
  
  /**
   * D�finit le projet.
   * @param _prj Le projet.
   */
  public void setProject(PivProject _prj) {
    if (prj_==_prj) return;
    
    if (prj_!=null)
      prj_.removeListener(this);
    prj_=_prj;
    if (prj_!=null)
      prj_.addListener(this);
    
    buildLayers();
    majLayers();
    majTools();
  }
  
  /**
   * Construction des calques.
   */
  private void buildLayers() {
    layers_.clear();
    
    // Layer des ortho points
    cqRealOrthoPoints_=new ZCalquePointEditable(null,null);
    cqRealOrthoPoints_.setTitle(PivResource.getS("Points r�f�rence"));
    cqRealOrthoPoints_.setLongTitle(PivResource.getS("Points r�f�rence"));
    cqRealOrthoPoints_.setIconModel(0, new TraceIconModel(TraceIcon.PLUS, 4, Color.RED));
    cqRealOrthoPoints_.setName("cqRealOrthoPoints");
    cqRealOrthoPoints_.setDestructible(false);
    cqRealOrthoPoints_.setLabelsVisible(true);
    cqRealOrthoPoints_.setAttributForLabels(PivVisuPanel.ATT_LABEL);
    // Legerement transparent.
    cqRealOrthoPoints_.setLabelsBackgroundColor(PivUtils.TRANPARENT_WHITE_COLOR);
    cqRealOrthoPoints_.setEditor(pnLayers_.getEditor());

    // Layer des ortho points recalcul�s (ajout� a la demande)
    cqRealControlPoints_=new ZCalqueMultiPointEditable(null,null);
    cqRealControlPoints_.setTitle(PivResource.getS("Points de contr�le"));
    cqRealControlPoints_.setLongTitle(PivResource.getS("Points de contr�le"));
    cqRealControlPoints_.setIconModel(0, new TraceIconModel(TraceIcon.CERCLE, 4, Color.BLUE));
    cqRealControlPoints_.setName("cqRealControlPoints");
    cqRealControlPoints_.setDestructible(false);

    BPalettePlageDiscret pal=new BPalettePlageDiscret(new Integer[0]);
    BPlageDiscret[] plage=new BPlageDiscret[2];
    plage[0]=new BPlageDiscret(0);
    plage[0].setCouleur(Color.RED);
    plage[0].setLegende(PivResource.getS("Pts r�els"));
    plage[1]=new BPlageDiscret(1);
    plage[1].setCouleur(Color.BLUE);
    plage[1].setLegende(PivResource.getS("Pts calcul�s"));
    pal.setPlages(plage);
    cqRealControlPoints_.setPaletteCouleurPlages(pal);

    // Layer de l'image en espace reel
    cqRealImg_ = new PivImageRasterLayer();
    cqRealImg_.setTitle(PivResource.getS("Image transform�e"));
    cqRealImg_.setLongTitle(PivResource.getS("Image transform�e"));
    cqRealImg_.setName("cqRealImg");

    // Layer des vitesses moyennes.
    cqAveVelResults_ = new PivVelResultsLayer();
    cqAveVelResults_.setEditable(false);
    cqAveVelResults_.setTitle(PivResource.getS("Vitesses M"));
    cqAveVelResults_.setLongTitle(PivResource.getS("Vitesses de surface moyenn�es"));
    cqAveVelResults_.setTitleModifiable(true);
    cqAveVelResults_.setName("cqRealVel");
    // Donne la couleur des fl�ches si pas de palette de couleurs.
    cqAveVelResults_.setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 2, Color.RED));
//    // On force la taille de vecteur � 2.5 m/s
//    cqAveVelResults.getScaleData().setLegendFixRealNorm(2.5);
//    cqAveVelResults.getScaleData().setLegendUseFixRealNorm(true);
    cqAveVelResults_.setVisible(false);
    cqAveVelResults_.addPropertyChangeListener(new PropertyChangeListener() {
      public void propertyChange(PropertyChangeEvent evt) {
        if ("visible".equals(evt.getPropertyName())) {
          pnLayers_.getShowVelocitiesAction().setSelected((Boolean)evt.getNewValue());
        }
      }
    });

    // Layer des vitesses instantan�es brutes.
    cqRawVelResults_ = new PivVelResultsLayer();
    cqRawVelResults_.setEditable(false);
    cqRawVelResults_.setTitle(PivResource.getS("Vitesses B"));
    cqRawVelResults_.setLongTitle(PivResource.getS("Vitesses de surface instantan�es brutes"));
    cqRawVelResults_.setName("cqInstantVel");
    // Donne la couleur des fl�ches si pas de palette de couleurs.
    cqRawVelResults_.setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 2, Color.ORANGE));
    cqRawVelResults_.setTitleModifiable(true);
//    // On force la taille de vecteur � 2.5 m/s
//    cqRawVelResults_.getScaleData().setLegendFixRealNorm(2.5);
//    cqRawVelResults_.getScaleData().setLegendUseFixRealNorm(true);
    cqRawVelResults_.setVisible(false);

    // Layer des vitesses instantan�es filtr�es.
    cqFltVelResults_ = new PivVelResultsLayer();
    // Editable, pour supprimer des vitesses.
    cqFltVelResults_.setEditable(true);
    cqFltVelResults_.setTitle(PivResource.getS("Vitesses F"));
    cqFltVelResults_.setLongTitle(PivResource.getS("Vitesses de surface instantan�es filtr�es"));
    cqFltVelResults_.setName("cqInstantVelFlt");
    // Donne la couleur des fl�ches si pas de palette de couleurs.
    cqFltVelResults_.setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 2, Color.GREEN.darker()));
    cqFltVelResults_.setTitleModifiable(true);
//    // On force la taille de vecteur � 2.5 m/s
//    cqFltVelResults_.getScaleData().setLegendFixRealNorm(2.5);
//    cqFltVelResults_.getScaleData().setLegendUseFixRealNorm(true);
    cqFltVelResults_.setVisible(false);

    // Layer des vitesses instantan�es manuelles.
    cqManualVelResults_ = new PivVelResultsLayer();
    // Editable, pour supprimer des vitesses.
    cqManualVelResults_.setTitle(PivResource.getS("Vitesses Manuelles"));
    cqManualVelResults_.setLongTitle(PivResource.getS("Vitesses de surface instantan�es manuelles"));
    cqManualVelResults_.setName("cqManualVel");
    // Donne la couleur des fl�ches si pas de palette de couleurs.
    cqManualVelResults_.setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 2, Color.BLUE.darker()));
    cqManualVelResults_.setTitleModifiable(true);
    cqManualVelResults_.setVisible(false);
    cqManualVelResults_.setIconModel(0, new TraceIconModel(TraceIcon.DISQUE, 4, Color.BLACK));
    
    // Layer des d�bits.
    cqFlowResults = new PivFlowResultsLayer();
    cqFlowResults.setTitle(PivResource.getS("Vitesses moyennes"));
    cqFlowResults.setLongTitle(PivResource.getS("Vitesses moyenn�es sur la profondeur"));
    cqFlowResults.setTitleModifiable(true);
    cqFlowResults.setName("cqRealFlow");
    // Donne la couleur des fl�ches si pas de palette de couleurs.
    cqFlowResults.setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 2, Color.ORANGE));
//    // On force la taille de vecteur � 2.5 m/s
//    cqFlowResults.getScaleData().setLegendFixRealNorm(2.5);
//    cqFlowResults.getScaleData().setLegendUseFixRealNorm(true);
    
    // Layer des transect
    cqTransect_= new PivTransectLayer();
    cqTransect_.setTitle(PivResource.getS("Transects"));
    cqTransect_.setLongTitle(PivResource.getS("Les transects"));
    cqTransect_.setName("cqTransect");
    cqTransect_.setFormeEnable(new int[]{DeForme.LIGNE_BRISEE});
    cqTransect_.setLineModel(1, new TraceLigneModel(TraceLigne.POINTILLE, 2, Color.ORANGE));
    cqTransect_.setIconModel(1, new TraceIconModel(TraceIcon.LOSANGE_PLEIN, 3, Color.ORANGE));
    cqTransect_.setTitleModifiable(true);
    cqTransect_.setDestructible(false);
    cqTransect_.setEditor(pnLayers_.getEditor());
//    cqTransect_.setActions(new EbliActionInterface[]{getInvertTransectAction(), null});
    getCreateTransectXYZAction().setTransectLayer(cqTransect_);
    getCreateTransectAbsZAction().setTransectLayer(cqTransect_);

    // Layer des iso couleurs instantan�es filtr�es
    cqFltIsoResults_ = new PivResultsLayer();
    cqFltIsoResults_.setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 1, Color.LIGHT_GRAY));
    cqFltIsoResults_.setTitle(PivResource.getS("Isosurfaces F"));
    cqFltIsoResults_.setLongTitle(PivResource.getS("Isosurfaces r�sultats instantan�s filtr�s"));
    cqFltIsoResults_.setName("cqInstantFltResults");
    cqFltIsoResults_.setTitleModifiable(true);
    cqFltIsoResults_.setDestructible(false);
    cqFltIsoResults_.setVisible(false);

    // Layer des iso couleurs instantan�es
    cqRawIsoResults_ = new PivResultsLayer();
    cqRawIsoResults_.setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 1, Color.LIGHT_GRAY));
    cqRawIsoResults_.setTitle(PivResource.getS("Isosurfaces B"));
    cqRawIsoResults_.setLongTitle(PivResource.getS("Isosurfaces r�sultats instantan�s bruts"));
    cqRawIsoResults_.setName("cqInstantResults");
    cqRawIsoResults_.setTitleModifiable(true);
    cqRawIsoResults_.setDestructible(false);
    cqRawIsoResults_.setVisible(false);

    // Layer des iso couleurs instantan�es
    cqAveIsoResults_ = new PivResultsLayer();
    cqAveIsoResults_.setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 1, Color.LIGHT_GRAY));
    cqAveIsoResults_.setTitle(PivResource.getS("Isosurfaces M"));
    cqAveIsoResults_.setLongTitle(PivResource.getS("Isosurfaces r�sultats moyenn�s"));
    cqAveIsoResults_.setName("cqAveResults");
    cqAveIsoResults_.setTitleModifiable(true);
    cqAveIsoResults_.setDestructible(false);
    cqAveIsoResults_.setVisible(false);
    
    // Trac� de particules (moyenn�s)
    cqAveParticles_ = new PivParticlesLayer(pnLayers_, new PivResultSource(null, PivResource.getS("R�sultats moyenn�s")), null);
    cqAveParticles_.setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 1, Color.LIGHT_GRAY));
    cqAveParticles_.setTitle(PivResource.getS("Particules M"));
    cqAveParticles_.setLongTitle(PivResource.getS("Trac�s de particules sur r�sultats moyenn�s"));
    cqAveParticles_.setName("cqAveParticles");
    cqAveParticles_.setTitleModifiable(true);
    cqAveParticles_.setDestructible(false);
    cqAveParticles_.setVisible(false);
    
    // Trac� de particules (filtr�es)
    cqFltParticles_ = new PivParticlesLayer(pnLayers_, new PivResultSource(null, PivResource.getS("R�sultats filtr�s")), null);
    cqFltParticles_.setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 1, Color.LIGHT_GRAY));
    cqFltParticles_.setTitle(PivResource.getS("Particules F"));
    cqFltParticles_.setLongTitle(PivResource.getS("Trac�s de particules sur r�sultats filtr�s"));
    cqFltParticles_.setName("cqFltParticles");
    cqFltParticles_.setTitleModifiable(true);
    cqFltParticles_.setDestructible(false);
    cqFltParticles_.setVisible(false);
    
    // Trac� de particules (brutes)
    cqRawParticles_ = new PivParticlesLayer(pnLayers_, new PivResultSource(null, PivResource.getS("R�sultats bruts")), null);
    cqRawParticles_.setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 1, Color.LIGHT_GRAY));
    cqRawParticles_.setTitle(PivResource.getS("Particules B"));
    cqRawParticles_.setLongTitle(PivResource.getS("Trac�s de particules sur r�sultats bruts"));
    cqRawParticles_.setName("cqRawParticles");
    cqRawParticles_.setTitleModifiable(true);
    cqRawParticles_.setDestructible(false);
    cqRawParticles_.setVisible(false);
    
    BGroupeCalque gc;
    layers_.add(cqRealOrthoPoints_);
    gc = new BGroupeCalque("gcDischarge", PivResource.getS("D�bit"));
    gc.add(cqTransect_);
    gc.add(cqFlowResults);
    layers_.add(gc);
    layers_.add(cqManualVelResults_);
    gc = new BGroupeCalque("gcAveResults", PivResource.getS("R�sultats moyens (M)"));
    gc.add(cqAveVelResults_);
    gc.add(cqAveParticles_);
    gc.add(cqAveIsoResults_);
    layers_.add(gc);
    gc = new BGroupeCalque("gcFltResults", PivResource.getS("R�sultats filtr�s (F)"));
    gc.add(cqFltVelResults_);
    gc.add(cqFltParticles_);
    gc.add(cqFltIsoResults_);
    layers_.add(gc);
    gc = new BGroupeCalque("gcRawResults", PivResource.getS("R�sultats bruts (B)"));
    gc.add(cqRawVelResults_);
    gc.add(cqRawParticles_);
    gc.add(cqRawIsoResults_);
    layers_.add(gc);
    layers_.add(cqRealImg_);
  }
  
  /**
   * Mise a jour des calques quand il y a chagement de projet.
   */
  protected void majLayers() {
    mdlRealOrthoPoints=new PivOrthoPointsModel(PivVisuPanel.MODE_REAL_VIEW);
    mdlRealOrthoPoints.setProjet(prj_);
    cqRealOrthoPoints_.setModele(mdlRealOrthoPoints);

    mdlRealControlPoints=new PivControlPointsModel();
    mdlRealControlPoints.setProjet(prj_);
    cqRealControlPoints_.modele(mdlRealControlPoints);

    mdlRealImage=new PivRealImageModel();
    mdlRealImage.setProjet(prj_);
    cqRealImg_.setModele(mdlRealImage);

    PivResultsI[] aveResultats = null;
    if (prj_.getAverageResults() != null)
      aveResultats = new PivResultsI[] { prj_.getAverageResults() };

    mdlVelResults = new PivVelResultsModel(prj_, aveResultats) {
      public boolean removeVelocity(int[] _inds, CtuluCommandContainer _cmd) {
        boolean b = super.removeVelocity(_inds, _cmd);
        prj_.setAverageResultsChanged();
        return b;
      }
    };
    cqAveVelResults_.setModele(mdlVelResults);

    mdlAverageResults = new PivResultsModel(prj_, null, aveResultats);
    cqAveIsoResults_.setModele(mdlAverageResults);

    // Les resultats restent dans le repere de calcul. Seules les lignes calcul�es
    // le sont dans le rep�re courant.
    mdlParticleAverageResults_ = new PivResultsModel(prj_, null, aveResultats);
    cqAveParticles_.getSource().setResults(mdlParticleAverageResults_);
    cqAveParticles_.changeRepere();

    mdlInstantVelResults = new PivVelResultsModel(prj_, prj_.getInstantRawResults()) {
      public boolean removeVelocity(int[] _inds, CtuluCommandContainer _cmd) {
        boolean b = super.removeVelocity(_inds, _cmd);
        prj_.setInstantRawResultsChanged();
        return b;
      }
    };
    cqRawVelResults_.setModele(mdlInstantVelResults);

    mdlInstantResults = new PivResultsModel(prj_, null, prj_.getInstantRawResults());
    cqRawIsoResults_.setModele(mdlInstantResults);

    // Les resultats restent dans le repere de calcul. Seules les lignes calcul�es
    // le sont dans le rep�re courant.
    mdlParticleRawResults_ = new PivResultsModel(prj_, null, prj_.getInstantRawResults());
    cqRawParticles_.getSource().setResults(mdlParticleRawResults_);
    cqRawParticles_.changeRepere();

    mdlInstantVelFltResults = new PivVelResultsModel(prj_, prj_.getInstantFilteredResults()) {
      public boolean removeVelocity(int[] _inds, CtuluCommandContainer _cmd) {
        boolean b = super.removeVelocity(_inds, _cmd);
        prj_.setInstantFilteredResultsChanged();
        return b;
      }
    };
    cqFltVelResults_.setModele(mdlInstantVelFltResults);
    
    PivResultsI[] manualResultats = null;
    if (prj_.getManualResults() != null)
      manualResultats = new PivResultsI[] { prj_.getManualResults() };
    mdlManualResults = new PivVelResultsModel(prj_, manualResultats);
    cqManualVelResults_.setModele(mdlManualResults);

    mdlInstantFltResults = new PivResultsModel(prj_, null, prj_.getInstantFilteredResults());
    cqFltIsoResults_.setModele(mdlInstantFltResults);

    mdlParticleFilteredResults_ = new PivResultsModel(prj_, null, prj_.getInstantFilteredResults());
    cqFltParticles_.getSource().setResults(mdlParticleFilteredResults_);
    cqFltParticles_.changeRepere();

    mdlFlowResults=new PivFlowResultsModel();
    mdlFlowResults.setProjet(prj_);
    cqFlowResults.setModele(mdlFlowResults);
    
    
    
    // Evite de perdre la selection en cours.
    cqTransect_.modeleDonnees().setProjet(prj_);
  }
  
  @Override
  public BCalque[] getLayers() {
    return layers_.toArray(new BCalque[0]);
  }
  
  /**
   * Affiche ou efface la totalit� des fichiers r�sultats.
   * @param _b True : Affiche les resultats. False : Les efface.
   */
  protected void setResultLayersVisible(boolean _b) {
    BCalqueAffichage[] cqRes = new BCalqueAffichage[] {
        cqAveIsoResults_,
        cqAveParticles_,
        cqAveVelResults_,
        cqFlowResults,
        cqFltIsoResults_,
        cqFltParticles_,
        cqFltVelResults_,
        cqRawIsoResults_,
        cqRawParticles_,
        cqRawVelResults_,
        cqManualVelResults_
    };
    
    for (BCalqueAffichage cq : cqRes) {
      cq.setVisible(_b);
    }
  }
  
  /**
   * Construction des outils sp�cifiques � cette vue.
   */
  private void buildTools() {
    // Le bouton d'animation
    actAnim_=new EbliAnimationAction(pnLayers_.createAnimSource());
    btAnim_=actAnim_.buildToolButton(EbliComponentFactory.INSTANCE);
    btAnim_.setEnabled(true);

    actParticleLines_=new PivCreateParticleLinesAction(pnLayers_, (FudaaCommonImplementation)pnLayers_.getCtuluUI(), 
        new PivParticlesLayer[]{cqAveParticles_,cqFltParticles_,cqRawParticles_});
    btStreamLines_=actParticleLines_.buildToolButton(EbliComponentFactory.INSTANCE);
    
    // Le bouton des param�tres du rep�re.
//    actTransfMatrix_=new PivChangeTransformationAction();
//    btTransfMatrix_=actTransfMatrix_.buildToolButton(EbliComponentFactory.INSTANCE);
    
    // La liste des variables possibles
    cbVar_=new JComboBox<>();
    cbVar_.addItemListener(new ItemListener() {
      
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange()==ItemEvent.SELECTED) {
          setSelectedVar((ResultType)cbVar_.getSelectedItem());
        }
      }
    });
    cbVar_.setPreferredSize(new Dimension(180, cbVar_.getPreferredSize().height));
    cbVar_.setMaximumSize(cbVar_.getPreferredSize());
    cbVar_.setToolTipText(PivResource.getS("Affiche les r�sultats pour la variable s�lectionn�e"));
    cbVar_.setEnabled(false);
    
    // La liste d�roulante des images
    cbImg_ = new JComboBox<>();
    cbImg_.addItemListener(new ItemListener() {
      
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange()==ItemEvent.SELECTED) {
          setSelectedImage(cbImg_.getSelectedIndex());
          
          if (!enableEvents_) return;
          enableEvents_=false;
          
          if (btLock_.isSelected())
            cbVel_.setSelectedIndex(Math.min(cbImg_.getSelectedIndex(),cbVel_.getItemCount()-1));
          
          enableEvents_=true;
        }
      }
    });
    cbImg_.setPreferredSize(new Dimension(180, cbImg_.getPreferredSize().height));
    cbImg_.setMaximumSize(cbImg_.getPreferredSize());
    cbImg_.setToolTipText(PivResource.getS("Affiche l'image s�lectionn�e"));
    cbImg_.setEnabled(false);
    
    // Le bouton de lock entre images et vitesses instantann�es.
    btLock_ = new BuTransparentToggleButton(PivResource.PIV.getIcon("non-lie-horizontal"), PivResource.PIV.getIcon("lie-horizontal"));
    btLock_.setBorder(BorderFactory.createEmptyBorder(5, 3, 5, 3));
    btLock_.setToolTipText(PivResource.getS("Lie/d�lie l'image et le r�sultat instantan�"));
    btLock_.setEnabled(false);
    btLock_.setSelected(true);
    
    // La liste d�roulante des r�sultats.
    cbVel_ = new JComboBox<>();
    cbVel_.setPreferredSize(new Dimension(180, cbVel_.getPreferredSize().height));
    cbVel_.setMaximumSize(cbVel_.getPreferredSize());
    cbVel_.setToolTipText(PivResource.getS("Affiche le r�sultat instantan� s�lectionn�"));
    cbVel_.setEnabled(false);
    cbVel_.addItemListener(new ItemListener() {
      
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange()==ItemEvent.SELECTED) {
          setSelectedResult(cbVel_.getSelectedIndex());
          
          if (!enableEvents_) return;
          enableEvents_=false;
          
          if (btLock_.isSelected())
            cbImg_.setSelectedIndex(Math.min(cbVel_.getSelectedIndex(),cbImg_.getItemCount()-1));
          
          enableEvents_=true;
        }
      }
    });
  }
  
  private void setSelectedResult(int _ind) {
    if (cqRawVelResults_!=null && cqRawVelResults_.modeleDonnees()!=null) {
      cqRawVelResults_.modeleDonnees().setSelectedResult(_ind);
      // Pour que le calque soit r�affich�.
      cqRawVelResults_.repaint();
    }
    if (cqFltVelResults_!=null && cqFltVelResults_.modeleDonnees()!=null) {
      cqFltVelResults_.modeleDonnees().setSelectedResult(_ind);
      // Pour que le calque soit r�affich�.
      cqFltVelResults_.repaint();
    }
    if (cqRawIsoResults_!=null && cqRawIsoResults_.modeleDonnees()!=null) {
      cqRawIsoResults_.modele().setCurrentTimeIdx(_ind);
      cqRawIsoResults_.repaint();
    }
    if (cqFltIsoResults_!=null && cqFltIsoResults_.modeleDonnees()!=null) {
      cqFltIsoResults_.modele().setCurrentTimeIdx(_ind);
      cqFltIsoResults_.repaint();
    }
  }
  
  private void setSelectedImage(int _ind) {
    if (cqRealImg_!=null && cqRealImg_.getModelImage()!=null) {
      cqRealImg_.getModelImage().setSelectedImage(_ind);
      // Pour que le calque soit r�affich�.
      cqRealImg_.repaint();
    }
  }
  
  /**
   * Definit la variable courante selectionn�e
   * @param _var La variable
   */
  private void setSelectedVar(ResultType _var) {
    if (!enableEvents2_) return;
    
    BCalque cq=pnLayers_.getScene().getCalqueActif();
    
    if (cq==cqRawIsoResults_) {
      if (cqRawIsoResults_.modele()!=null) {
        cqRawIsoResults_.modele().setCurrentVar(_var);
        cqRawIsoResults_.repaint();
      }
    }
    else if (cq==cqFltIsoResults_) {
      if (cqFltIsoResults_.modele()!=null) {
        cqFltIsoResults_.modele().setCurrentVar(_var);
        cqFltIsoResults_.repaint();
      }
    }
    else if (cq==cqAveIsoResults_) {
      if (cqAveIsoResults_.modele()!=null) {
        cqAveIsoResults_.modele().setCurrentVar(_var);
        cqAveIsoResults_.repaint();
      }
    }
  }
   
  /**
   * Mise a jour les outils.
   */
  private void majTools() {
    // Force la mise a jour du composant d'animation
    actAnim_.setAnimAdapterInterface(new SequenceAnimationAdapter());
    // Les calques peuvent avoir chang�.
    actParticleLines_.setLayers(new PivParticlesLayer[]{cqAveParticles_, cqFltParticles_, cqRawParticles_});
    
    Object itImgSel=cbImg_.getSelectedItem();
    cbImg_.removeAllItems();
    cbImg_.setEnabled(false);
    
    Object itVelSel=cbVel_.getSelectedItem();
    cbVel_.removeAllItems();
    cbVel_.setEnabled(false);
    
    btLock_.setEnabled(false);
    
    if (prj_==null) return;
    
//    actTransfMatrix_.getPanel().setProject(prj_);
//    
//    if (prj_.getTransformationParameters().isCurrentSystemOriginal()) {
//      cbRep_.setSelectedIndex(1);
//    }
    
    // La liste d�roulante des images
    File[] imgs=prj_.getTransfImageFiles();
    if (imgs != null) {
      for (int i = 0; i < imgs.length; i++) {
        cbImg_.addItem(imgs[i].getName());
      }
      if (itImgSel != null)
        cbImg_.setSelectedItem(itImgSel);

      cbImg_.setEnabled(imgs.length > 0);
    }
    
    // La liste des r�sultats instantan�s/filtr�s
    PivResultsI[] instantRes=prj_.getInstantRawResults();
    if (instantRes != null) {
      for (int i = 0; i < instantRes.length; i++) {
        cbVel_.addItem(PivResource.getS("R�sultat : {0}",i+1));
      }
      if (itVelSel!=null)
        cbVel_.setSelectedItem(itVelSel);
      
      cbVel_.setEnabled(instantRes.length>0);
    }
    
    btLock_.setEnabled(imgs!=null && instantRes!=null && imgs.length==instantRes.length+1);
    
    majToolsState();
  }
  
  /**
   * Met a jour l'�tat des boutons en fonction du calque actif.
   */
  private void majToolsState() {
    BCalque cq=pnLayers_.getScene().getCalqueActif();
    
    try {
      enableEvents2_=false;
      
      // On place syst�matiquement le resultat NORME en premier.
      
      if (cq==cqAveIsoResults_ && cqAveIsoResults_.modele()!=null && cqAveIsoResults_.modele().getVariables().length!=0) {
        cbVar_.setEnabled(true);
        cbVar_.removeAllItems();
        for (CtuluVariable var : cqAveIsoResults_.modele().getVariables()) {
          if (var.equals(PivResultsI.ResultType.NORME)) {
            cbVar_.insertItemAt(var, 0);
          }
          else {
            cbVar_.addItem(var);
          }
        }
        cbVar_.setSelectedItem(cqAveIsoResults_.modele().getCurrentVar());
      }
      else if (cq==cqRawIsoResults_ && cqRawIsoResults_.modele()!=null && cqRawIsoResults_.modele().getVariables().length!=0) {
        cbVar_.setEnabled(true);
        cbVar_.removeAllItems();
        for (CtuluVariable var : cqRawIsoResults_.modele().getVariables()) {
          if (var.equals(PivResultsI.ResultType.NORME)) {
            cbVar_.insertItemAt(var, 0);
          }
          else {
            cbVar_.addItem(var);
          }
        }
        cbVar_.setSelectedItem(cqRawIsoResults_.modele().getCurrentVar());
      }
      else if (cq==cqFltIsoResults_ && cqFltIsoResults_.modele()!=null && cqFltIsoResults_.modele().getVariables().length!=0) {
        cbVar_.setEnabled(true);
        cbVar_.removeAllItems();
        for (CtuluVariable var : cqFltIsoResults_.modele().getVariables()) {
          if (var.equals(PivResultsI.ResultType.NORME)) {
            cbVar_.insertItemAt(var, 0);
          }
          else {
            cbVar_.addItem(var);
          }
        }
        cbVar_.setSelectedItem(cqFltIsoResults_.modele().getCurrentVar());
      }
      else {
        cbVar_.setEnabled(false);
      }
    }
    finally {
      enableEvents2_=true;
    }
  }

  @Override
  public void projectStateChanged(PivProject _prj, String _prop) {
    if ("transfParams".equals(_prop)) {
      mdlRealImage.update();
      mdlRealOrthoPoints.update();
      cqTransect_.modeleDonnees().update();
      mdlFlowResults.update();
      mdlAverageResults.setSystemViewInitial(prj_.getTransformationParameters().isCurrentSystemInitial());
      mdlVelResults.setSystemViewInitial(prj_.getTransformationParameters().isCurrentSystemInitial());
      mdlInstantResults.setSystemViewInitial(prj_.getTransformationParameters().isCurrentSystemInitial());
      mdlInstantVelResults.setSystemViewInitial(prj_.getTransformationParameters().isCurrentSystemInitial());
      mdlInstantFltResults.setSystemViewInitial(prj_.getTransformationParameters().isCurrentSystemInitial());
      mdlInstantVelFltResults.setSystemViewInitial(prj_.getTransformationParameters().isCurrentSystemInitial());
      mdlManualResults.setSystemViewInitial(prj_.getTransformationParameters().isCurrentSystemInitial());
      
      cqAveParticles_.changeRepere();
      cqFltParticles_.changeRepere();
      cqRawParticles_.changeRepere();
    }
    else if ("transfImages".equals(_prop)) {
      majTools();
      mdlRealImage.update();
    }
    else if("orthoPoints".equals(_prop)) {
      mdlRealOrthoPoints.update();
    }
    else if ("transect".equals(_prop)) {
      cqTransect_.modeleDonnees().update();
    }
    else if("flowResults".equals(_prop)) {
      mdlFlowResults.update();
    }
    else if("averageResults".equals(_prop)) {
      PivResultsI[] aveResultats=null;
      if (prj_.getAverageResults()!=null)
        aveResultats=new PivResultsI[]{prj_.getAverageResults()};
      
      mdlAverageResults.setResults(aveResultats);
      mdlParticleAverageResults_.setResults(aveResultats);
      mdlVelResults.setResults(aveResultats);
//      }
//      else {
//        mdlAverageResults=null;
//        mdlVelResults=null;
//      }
      cqAveIsoResults_.setModele(mdlAverageResults);
      cqAveVelResults_.setModele(mdlVelResults);
      cqAveParticles_.getSource().setResults(mdlParticleAverageResults_);
      
      majTools();
    }
    else if ("instantResults".equals(_prop)) {
        mdlParticleRawResults_.setResults(prj_.getInstantRawResults());
        mdlInstantResults.setResults(prj_.getInstantRawResults());
        mdlInstantVelResults.setResults(prj_.getInstantRawResults());
//      }
//      else {
//        mdlInstantResults=null;
//        mdlInstantVelResults=null;
//      }
      cqRawIsoResults_.setModele(mdlInstantResults);
      cqRawVelResults_.setModele(mdlInstantVelResults);
      cqRawParticles_.getSource().setResults(mdlParticleRawResults_);
      
      majTools();
    }
    else if ("instantFilteredResults".equals(_prop)) {
//      if (prj_.getInstantFilteredResults()!=null) {
        mdlParticleFilteredResults_.setResults(prj_.getInstantFilteredResults());
        mdlInstantFltResults.setResults(prj_.getInstantFilteredResults());
        mdlInstantVelFltResults.setResults(prj_.getInstantFilteredResults());
//      }
//      else {
//        mdlInstantFltResults=null;
//        mdlInstantVelFltResults=null;
//      }
      cqFltIsoResults_.setModele(mdlInstantFltResults);
      cqFltVelResults_.setModele(mdlInstantVelFltResults);
      cqFltParticles_.getSource().setResults(mdlParticleFilteredResults_);
      
      majTools();
    }
    else if("manualResults".equals(_prop)) {
      PivResultsI[] manualResultats=null;
      if (prj_.getManualResults()!=null)
        manualResultats=new PivResultsI[]{prj_.getManualResults()};
      
      mdlManualResults.setResults(manualResultats);
      cqManualVelResults_.setModele(mdlManualResults);
      
      majTools();
    }
    
    pnLayers_.getVueCalque().repaint();
  }

  @Override
  public String getTitle() {
    return TITLE;
  }

  @Override
  public JComponent[] getSpecificTools() {
    return new JComponent[]{btStreamLines_,btAnim_,cbVar_,cbImg_,btLock_,cbVel_};
  }

  @Override
  public EbliCoordinateDefinition[] getCoordinateDefinitions() {
    return new EbliCoordinateDefinition[]{DEF_COOR_X,DEF_COOR_Y};
  }

  @Override
  public void restoreLayerProperties(Map<String, EbliUIProperties> _props) {
    for (BCalque cq : layers_) {
      restoreLayerProperties(_props, cq);
    }
    // Special : Ce calque n'est pas dans la liste des calques.
    cqRealControlPoints_.initFrom(_props.get(cqRealControlPoints_.getName()));
  }

  @Override
  public Map<String, EbliUIProperties> saveLayerProperties() {
    HashMap<String, EbliUIProperties> props=new HashMap<String, EbliUIProperties>();
    for (BCalque cq : layers_) {
      saveLayerProperties(props, cq);
    }
    
    // Special : Ce calque n'est pas dans la liste des calques.
    props.put(cqRealControlPoints_.getName(),cqRealControlPoints_.saveUIProperties());
    return props;
  }
  
  /**
   * Restore les propri�t�s graphiques recursivement.
   * @param _props Les propri�t�s
   * @param _cq Le calque a restaurer
   */
  private void restoreLayerProperties(Map<String, EbliUIProperties> _props, BCalque _cq) {
    if (_cq instanceof BGroupeCalque) {
      for (BCalque cq : ((BGroupeCalque)_cq).getCalques()) {
        restoreLayerProperties(_props, cq);
      }
    }
    else {
      _cq.initFrom(_props.get(_cq.getName()));
    }
  }
  
  /**
   * Sauve les propri�t�s graphiques recursivement.
   * @param _props Les propri�t�s
   * @param _cq Le calque a sauver
   */
  private void saveLayerProperties(Map<String, EbliUIProperties> _props, BCalque _cq) {
    if (_cq instanceof BGroupeCalque) {
      for (BCalque cq : ((BGroupeCalque)_cq).getCalques()) {
        saveLayerProperties(_props, cq);
      }
    }
    else {
      _props.put(_cq.getName(),_cq.saveUIProperties());
    }
  }
  
  /**
   * Rend visible le calque de controle des points d'orthorectification.
   * @param _b True : Le calque est visible.
   */
  public void setControlPointsLayerVisible(boolean _b) {
    if (_b) {
      mdlRealControlPoints.update();
      pnLayers_.addCalque(cqRealControlPoints_,true);
      cqRealControlPoints_.setLegende(pnLayers_.getCqLegend());
      cqRealControlPoints_.setVisible(true);
    }
    else {
      pnLayers_.removeCalque(cqRealControlPoints_);
      cqRealControlPoints_.setLegende(null);
      pnLayers_.getCqLegend().enleve(cqRealControlPoints_);
    }
  }

  /**
   * Rend visible le calque des vitesses.
   * @param _b True : Le calque est visible.
   */
  public void setAveVelocitiesLayerVisible(boolean _b) {
    if (_b) {
      setResultLayersVisible(false);
    }

    cqAveVelResults_.setVisible(_b);
  }

  /**
   * Rend visible le calque des vitesses brutes.
   * @param _b True : Le calque est visible.
   */
  public void setRawVelocitiesLayerVisible(boolean _b) {
    if (_b) {
      setResultLayersVisible(false);
    }
    
    cqRawVelResults_.setVisible(_b);
  }

  /**
   * Rend visible le calque des vitesses filtr�es.
   * @param _b True : Le calque est visible.
   */
  public void setFilteredVelocitiesLayerVisible(boolean _b) {
    if (_b) {
      setResultLayersVisible(false);
    }

    cqFltVelResults_.setVisible(_b);
  }

  /**
   * Rend visible le calque des vitesses manuelles.
   * @param _b True : Le calque est visible.
   */
  public void setManualVelocitiesLayerVisible(boolean _b) {
    if (_b) {
      setResultLayersVisible(false);
    }
    
    cqManualVelResults_.setVisible(_b);
  }

  /**
   * Rend visible le calque des d�bits.
   * @param _b True : Le calque est visible.
   */
  public void setFlowLayerVisible(boolean _b) {
    boolean isAveVisible = cqAveVelResults_.isVisible();
    
    if (_b) {
      setResultLayersVisible(false);
    }

    // Lors du calcul de d�bit on visualise aussi les viteses moyennes si elles sont d�ja affich�es.
    cqAveVelResults_.setVisible(isAveVisible);
    cqFlowResults.setVisible(_b);
  }

  /**
   * Rend visible le calque image.
   * @param _b True : Le calque est visible.
   */
  public void setImageLayerVisible(boolean _b) {
    cqRealImg_.setVisible(_b);
  }

  /**
   * Retourne la visibilit� du calque image.
   * @return True : Le calque est visible.
   */
  public boolean isImageLayerVisible() {
    return cqRealImg_.isVisible();
  }
  
  public PivTransectLayer getTransectLayer() {
    return cqTransect_;
  }

  public PivVelResultsLayer getVelRawResultsLayer() {
    return cqRawVelResults_;
  }

  public PivVelResultsLayer getVelFilteredResultsLayer() {
    return cqFltVelResults_;
  }

  public PivVelResultsLayer getVelAverageResultsLayer() {
    return cqAveVelResults_;
  }
  
  @Override
  public EbliActionAbstract getActivationAction() {
    if (actRealView_==null) {
      actRealView_=new PivRealViewAction((PivImplementation)pnLayers_.getCtuluUI());
    }
    return actRealView_;
  }

  /**
   * Retourne l'action pour la saisie d'un transect.
   * @return L'action.
   */
  public PivNewTransectAction getCreateTransectXYZAction() {
    if (actCreateXYZTransect_==null) {
      actCreateXYZTransect_=new PivNewTransectAction(pnLayers_);
    }
    return actCreateXYZTransect_;
  }

  /**
   * Retourne l'action pour la saisie d'un transect.
   * @return L'action.
   */
  public PivCreateTransectAbsZAction getCreateTransectAbsZAction() {
    if (actCreateAbsZTransect_==null) {
      actCreateAbsZTransect_=new PivCreateTransectAbsZAction(pnLayers_);
    }
    return actCreateAbsZTransect_;
  }

  /**
   * Retourne l'action pour la saisie des parametres d'un transect.
   * @return L'action.
   */
  public PivTransectParamAction getParamsTransectAction() {
    if (actParamsTransect_==null) {
      actParamsTransect_=new PivTransectParamAction((PivImplementation)pnLayers_.getCtuluUI());
      pnLayers_.getScene().addSelectionListener(actParamsTransect_);
    }
    return actParamsTransect_;
  }

  /**
   * Retourne l'action pour l'inversion d'un transect.
   * @return L'action.
   */
  public PivInvertTransectAction getInvertTransectAction() {
    if (actInvertTransect_==null) {
      actInvertTransect_=new PivInvertTransectAction((PivImplementation)pnLayers_.getCtuluUI());
      pnLayers_.getScene().addSelectionListener(actInvertTransect_);
    }
    return actInvertTransect_;
  }

  /**
   * Retourne l'action pour la saisie des parametres d'un transect.
   * @return L'action.
   */
  public PivCreateParticleLinesAction getTraceParticlesAction() {
    return actParticleLines_;
  }

  @Override
  public void nextImage() {
    int nextInd = cbImg_.getSelectedIndex() + 1;
    if (nextInd < cbImg_.getItemCount()) {
      cbImg_.setSelectedIndex(nextInd);
    }
  }

  @Override
  public void previousImage() {
    int previousInd = cbImg_.getSelectedIndex() -1;
    if (previousInd >= 0) {
      cbImg_.setSelectedIndex(previousInd);
    }
  }
}
