/*
 * @creation 8 sept. 06
 * @modification $Date: 2009-06-03 15:10:45 +0200 (mer., 03 juin 2009) $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;

import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.commun.FudaaBrowserControl;
import org.fudaa.fudaa.commun.impl.FudaaStartupExitPreferencesPanel;
import org.jdesktop.swingx.VerticalLayout;


/**
 * Un panneau pour l'affichage de la version a telecharger.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivFlowInfoPanel.java 9491 2016-12-20 14:46:54Z bmarchan $
 */
public class PivUpdatePanel extends CtuluDialogPanel {
  
  private JCheckBox cbHidden_;

  public PivUpdatePanel(String _version, final URL _url, boolean _showHideButton) {
    setLayout(new VerticalLayout(10));
    setBorder(BorderFactory.createEmptyBorder(5, 5, 15, 5));

    final JLabel lbLink=new JLabel("<html><a href=\"{" + _url.toString() + "}\">" + PivResource.getS("T�l�chargez la version {0} de Fudaa-LSPIV", _version) + "</a></html>");
    lbLink.addMouseListener(new MouseAdapter() {
      public void mouseEntered(MouseEvent me) {
        lbLink.setCursor(new Cursor(Cursor.HAND_CURSOR));
      }

      public void mouseExited(MouseEvent me) {
        lbLink.setCursor(Cursor.getDefaultCursor());
      }

      public void mouseClicked(MouseEvent me) {
        try {
          FudaaBrowserControl.displayURL(_url);
        }
        catch (Exception e) {
          System.out.println(e);
        }
      }
    });
    

    add(new JLabel(PivResource.getS("<html>Une mise � jour de Fudaa-LSPIV est disponible.<br>Cliquez sur le lien ci-dessous pour la t�l�charger.</html>")));
    add(lbLink);

    if (_showHideButton) {
      cbHidden_=new JCheckBox(PivResource.getS("Ne plus afficher ce dialogue"));
      add(cbHidden_);
    }
  }

  @Override
  public boolean isDataValid() {
    return true;
  }

  @Override
  public boolean apply() {
    if (cbHidden_ != null)
      PivPreferences.PIV.putBooleanProperty(FudaaStartupExitPreferencesPanel.PREF_CHECK_NEW_VERSION, !cbHidden_.isSelected());

    return super.apply();
  }
  
  
}