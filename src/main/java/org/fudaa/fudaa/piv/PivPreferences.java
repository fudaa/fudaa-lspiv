/*
 * @file         PrertPreferences.java
 * @creation     2002-08-30
 * @modification $Date: 2008-02-14 17:03:33 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.piv;

import com.memoire.bu.BuPreferences;
import org.fudaa.fudaa.commun.FudaaPreferencesAbstract;

/**
 * Les pr�f�rences sp�cifiques � piv, toutes stock�es dans piv.ini.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public final class PivPreferences extends FudaaPreferencesAbstract {
  
  /** Le path des exes. */
  public static final String PIV_EXE_PATH="piv.exe.path";
  /** La regeneration automatique des cache */
  public static final String PIV_AUTO_CACHE="piv.auto.cache";
  /** Le path de la doc */
  public static final String PIV_DOC_PATH="piv.doc.path";
  /** Le path pour les templates Excel pour la g�n�ration d'un rapport. */
  public static final String PIV_TEMPLATES_PATH="piv.templates.path";
  
  /**
   * Singleton.
   */
  public final static BuPreferences PIV=BU;

  private PivPreferences() {
    super();
  }

}
