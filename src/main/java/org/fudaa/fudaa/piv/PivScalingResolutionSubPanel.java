package org.fudaa.fudaa.piv;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteraction;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteractionTargetI;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.commun.EbliSelectedChangeListener;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.piv.action.PivEnterGeometryAction;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.fu.FuLog;

/**
 * Un panneau de saisie de la r�solution pour la mise � l'�chelle.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivScalingResolutionSubPanel extends JPanel {
  
  class CoupleRow {
    /** Peut �tre null si modifi� par l'utilisateur. */
    Integer i1;
    /** Peut �tre null si modifi� par l'utilisateur. */
    Integer j1;
    /** Peut �tre null si modifi� par l'utilisateur. */
    Integer i2;
    /** Peut �tre null si modifi� par l'utilisateur. */
    Integer j2;
    /** Peut �tre null quand pas saisi. */
    Double x1;
    /** Peut �tre null quand pas saisi. */
    Double y1;
    /** Peut �tre null quand pas saisi. */
    Double x2;
    /** Peut �tre null quand pas saisi. */
    Double y2;
    /** Peut �tre null quand pas saisi. */
    Double z;
    /** Peut �tre null quand pas saisi. */
    Double distance;
    /** Peut �tre null si la distance ou les coordonn�es n'ont pas �t� donn�s */
    Double resolution;
    
    /**
     * @return True si tous les champs de coordonn�es image sont remplis
     */
    public boolean isImgPointsFilled() {
      return i1 != null && i2 != null && j1 != null && j2 != null;
    }
    
    /**
     * @return True si tous les champs de coordonn�es reelles sont remplis
     */
    public boolean isRealPointsFilled() {
      return x1 != null && x2 != null && y1 != null && y2 != null;
    }
    
    /**
     * @return True si le champs de distance est rempli
     */
    public boolean isDistanceFilled() {
      return distance!=null;
    }
  }
  
  /**
   * Pour visualiser les Double de mani�re condens�e. Les autres types sont redu avec le renderer standard.
   * @author Bertrand Marchand
   */
  class CoupleTableCellRenderer extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
        int row, int column) {
      
      Object sval = value;
      if (value instanceof Double) {
        sval = String.format(Locale.US, "%.4G", value);
      }
      
      return super.getTableCellRendererComponent(table, sval, isSelected, hasFocus, row, column);
    }
  }
  
  
  /**
   * Un modele pour les couples / (distance ou coordonn�es)
   * @author Bertrand Marchand (marchand@detacad.fr)
   */
  class CoupleTableModel extends AbstractTableModel {
    
    int COL_I1;
    int COL_J1;
    int COL_I2;
    int COL_J2;
    int COL_X1;
    int COL_Y1;
    int COL_X2;
    int COL_Y2;
    int COL_Z;
    int COL_DISTANCE;
    int COL_RES;
    
    List<CoupleRow> rows = new ArrayList<>();
    boolean modeDistance_=true;
    boolean modeDrone_=false;
    int nbCols = 0;
    
    public CoupleTableModel() {
      setModeDistance(true);
      setModeDrone(false);
    }
    
    @Override
    public int getRowCount() {
      return rows.size();
    }

    @Override
    public int getColumnCount() {
      return nbCols;
    }

    @Override
    public Object getValueAt(int row, int column) {
      if (column == COL_I1) {
        return rows.get(row).i1;
      }
      else if (column == COL_J1) {
        return rows.get(row).j1;
      }
      else if (column == COL_I2) {
        return rows.get(row).i2;
      }
      else if (column == COL_J2) {
        return rows.get(row).j2;
      }
      else if (column == COL_X1) {
        return rows.get(row).x1;
      }
      else if (column == COL_Y1) {
        return rows.get(row).y1;
      }
      else if (column == COL_X2) {
        return rows.get(row).x2;
      }
      else if (column == COL_Y2) {
        return rows.get(row).y2;
      }
      else if (column == COL_DISTANCE) {
        return rows.get(row).distance;
      }
      else if (column == COL_Z) {
        return rows.get(row).z;
      }
      else {
        return rows.get(row).resolution;
      }
    }

    @Override
    public String getColumnName(int column) {
      if (column == COL_I1) {
        return PivResource.getS("I1");
      }
      else if (column == COL_J1) {
        return PivResource.getS("J1");
      }
      else if (column == COL_I2) {
        return PivResource.getS("I2");
      }
      else if (column == COL_J2) {
        return PivResource.getS("J2");
      }
      else if (column == COL_X1) {
        return PivResource.getS("X1") + " (m)";
      }
      else if (column == COL_Y1) {
        return PivResource.getS("Y1") + " (m)";
      }
      else if (column == COL_X2) {
        return PivResource.getS("X2") + " (m)";
      }
      else if (column == COL_Y2) {
        return PivResource.getS("Y2") + " (m)";
      }
      else if (column == COL_DISTANCE) {
        return PivResource.getS("Distance") + " (m)";
      }
      else if (column == COL_Z) {
        return PivResource.getS("Altitude") + " (m)";
      }
      else {
        return PivResource.getS("R�solution");
      }
    }
    
    @Override
    public boolean isCellEditable(int row, int column) {
//      if (modeDistance_)
//        return column<=4;
//      else
//        return column<=7;
      return column <= getColumnCount() - 2;
    }

    private Double getDoubleValue(Object aValue) {
      if (!aValue.toString().trim().isEmpty()) {
        // Double attendu
        try {
          return Double.parseDouble(aValue.toString().trim());
        }
        catch (NumberFormatException _exc) {
        }
      }
      
      return null;
    }

    private Integer getIntegerValue(Object aValue) {
      if (!aValue.toString().trim().isEmpty()) {
        // Double attendu
        try {
          return Integer.parseInt(aValue.toString().trim());
        }
        catch (NumberFormatException _exc) {
        }
      }
      
      return null;
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
      if (column == COL_I1) {
        rows.get(row).i1=getIntegerValue(aValue);
      }
      else if (column == COL_J1) {
        rows.get(row).j1=getIntegerValue(aValue);
      }
      else if (column == COL_I2) {
        rows.get(row).i2=getIntegerValue(aValue);
      }
      else if (column == COL_J2) {
        rows.get(row).j2=getIntegerValue(aValue);
      }
      else if (column == COL_X1) {
        rows.get(row).x1=getDoubleValue(aValue);
      }
      else if (column == COL_Y1) {
        rows.get(row).y1=getDoubleValue(aValue);
      }
      else if (column == COL_X2) {
        rows.get(row).x2=getDoubleValue(aValue);
      }
      else if (column == COL_Y2) {
        rows.get(row).y2=getDoubleValue(aValue);
      }
      else if (column == COL_DISTANCE) {
        rows.get(row).distance=getDoubleValue(aValue);
      }
      else if (column == COL_Z) {
        rows.get(row).z=getDoubleValue(aValue);
      }
      
      computeResolution(row);

      fireTableDataChanged();
      
      pnParams_.fireParametersChanged();
    }
    
    /**
     * Initialise la r�solution en fonction des donn�es de la table.
     * @param row La ligne pour laquelle initialiser la r�solution
     */
    public void computeResolution(int row) {
      rows.get(row).resolution = null;

      if (rows.get(row).i1 == null || rows.get(row).j1 == null || rows.get(row).i2 == null || rows.get(row).j2 == null)
        return;

      if (modeDrone_ && rows.get(row).z == null)
        return;

      // Calcul de la distance du segment.
      double realDist = -1;
      if (modeDistance_) {
        if (rows.get(row).distance == null)
          return;

        realDist = rows.get(row).distance;
      }
      else {
        if (rows.get(row).x1 == null || rows.get(row).y1 == null || rows.get(row).x2 == null || rows.get(row).y2 == null)
          return;

        double dxr = rows.get(row).x2 - rows.get(row).x1;
        double dyr = rows.get(row).y2 - rows.get(row).y1;
        realDist = Math.sqrt(dxr * dxr + dyr * dyr);
      }

      // Rectification de la distance si on est en mode drone
      if (modeDrone_) {
        Double zDrone = getZDrone();
        Double waterLevel = pnParams_.getLimitsPanel().getWaterLevel();
        if (zDrone == null || rows.get(row).z == null || waterLevel == null)
          return;

        // On n'autorise pas les r�solutions n�gatives ou infinies.
        if (zDrone - rows.get(row).z <= 0)
          return;
        
        realDist = realDist * (zDrone - waterLevel) / (zDrone - rows.get(row).z);
        if (realDist < 0 )
          return;
      }

      double dxi = rows.get(row).i2 - rows.get(row).i1;
      double dyi = rows.get(row).j2 - rows.get(row).j1;
      rows.get(row).resolution = realDist / Math.max(1, Math.sqrt(dxi * dxi + dyi * dyi));
    }
      
    public void addCouple(CoupleRow _cpl) {
      rows.add(_cpl);
      fireTableDataChanged();
      
      pnParams_.fireParametersChanged();
    }
    
    public void removeAllCouples() {
      rows.clear();
      fireTableDataChanged();
      
      pnParams_.fireParametersChanged();
    }
    
    public void removeCouple(int... rowIndex) {
      for (int i=rowIndex.length-1; i>=0; i--) {
        rows.remove(rowIndex[i]);
      }
      fireTableDataChanged();
      
      pnParams_.fireParametersChanged();
    }
    
    public void setDroneElevationChanged() {
      for (int i=0; i<rows.size(); i++) {
        computeResolution(i);
      }
      fireTableDataChanged();
    }
    
    public void setModeDrone(boolean _b) {
      modeDrone_ = _b;
      
      adjustColumns();
    }
    
    /**
     * Definit que le mod�le est en mode distance ou en mode coordonn�es r�elles.
     * @param _b True : En mode distance.
     */
    public void setModeDistance(boolean _b) {
      modeDistance_=_b;
      
      adjustColumns();
    }
    
    public void adjustColumns() {
      nbCols = 0;
      
      COL_I1 = -1;
      COL_J1 = -1;
      COL_X1 = -1;
      COL_Y1 = -1;
      COL_I2 = -1;
      COL_J2 = -1;
      COL_X2 = -1;
      COL_Y2 = -1;
      COL_DISTANCE = -1;
      COL_Z = -1;
      COL_RES = -1;
      
      if (modeDistance_) {
        COL_I1 = nbCols++;
        COL_J1 = nbCols++;
        COL_I2 = nbCols++;
        COL_J2 = nbCols++;
        COL_DISTANCE = nbCols++;
        if (modeDrone_)
          COL_Z = nbCols++;
        COL_RES = nbCols++;
      }
      else {
        COL_I1 = nbCols++;
        COL_J1 = nbCols++;
        COL_X1 = nbCols++;
        COL_Y1 = nbCols++;
        COL_I2 = nbCols++;
        COL_J2 = nbCols++;
        COL_X2 = nbCols++;
        COL_Y2 = nbCols++;
        if (modeDrone_)
          COL_Z = nbCols++;
        COL_RES = nbCols++;
      }
      
      for (int i=0; i<rows.size(); i++) {
        computeResolution(i);
      }
      fireTableStructureChanged();
      
      pnParams_.fireParametersChanged();
    }
        
    /**
     * @return True : Si les points sont donn�s en mode distance.
     */
    public boolean isModeDistance() {
      return modeDistance_;
    }
    
    /**
     * @return True : Si l'altitude du drone est pris en compte.
     */
    public boolean isModeDrone() {
      return modeDrone_;
    }
    
    /**
     * @return La resolution moyenne, ou null si aucune r�solution n'est d�finie dans le tableau.
     */
    public Double getAverageResolution() {
      int nb=0;
      double somme=0;
      for (CoupleRow cpl : rows) {
        if (cpl.resolution!=null) {
          somme+=cpl.resolution;
          nb++;
        }
      }
      
      if (nb>0) {
        return somme/nb;
      }
      else {
        return null;
      }
    }
  }
  
  
  /**
   * Un editeur qui r�agit � la saisie d'une forme pour la grille.
   *
   * @author Bertrand Marchand (marchand@deltacad.fr)
   * @version $Id$
   */
  class EditionController implements ZCalqueEditionInteractionTargetI {

    /** Non utilis� */
    public void atomicChanged() {
      // On notifie la palette que la forme a chang�, pour activer ou non le
      // bouton fin d'edition.
      GrPolyligne pl=(GrPolyligne) cqEdition_.getFormeEnCours().getFormeEnCours();
      if (pl.sommets_.nombre() == 2) {
        cqEdition_.endEdition();

        CoupleRow couple=new CoupleRow();
        couple.i1=(int)pl.sommet(0).x_;
        couple.j1=(int)pl.sommet(0).y_;
        couple.i2=(int)pl.sommet(1).x_;
        couple.j2=(int)pl.sommet(1).y_;
        mdlPoints.addCouple(couple);
        
        actEnterDistance_.hideWindow();
      }
    }

    /** Non utilis� */
    public boolean addNewPolygone(GrPolygone _pg, ZEditionAttributesDataI _data) {
      return true;
    }

    /** 
     * Remet a jour la position du point centre.
     */
    public boolean addNewPoint(GrPoint _point, ZEditionAttributesDataI _data) {
//      actEnterDistance_.changeAll();
      return true;
    }

    /** Non utilis� */
    public boolean addNewRectangle(GrPoint _pt, GrPoint _pt2, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewPolyligne(GrPolyligne _pt, ZEditionAttributesDataI _data) {
      FuLog.debug("Nombre de points "+_pt.nombre());
      return true;
    }

    /** Non utilis� */
    public boolean addNewMultiPoint(GrPolyligne _pt, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public void setMessage(String _s) {
    }

    /** Non utilis� */
    public void unsetMessage() {
    }

    /** Non utilis� */
    public void pointMove(double _x, double _y) {
    }
  }

  private PivVisuPanel pnLayers_;
  private ZCalqueEditionInteractionTargetI ctrl_=new EditionController();
  private PivEnterGeometryAction actEnterDistance_;
  private ZCalqueEditionInteraction cqEdition_;
  private CtuluTable tbCouple=new CtuluTable();
  private CoupleTableModel mdlPoints;
  private PivScalingParamPanel pnParams_;
  
  /**
   * Constructeur.
   */
  public PivScalingResolutionSubPanel(PivVisuPanel _pn, PivScalingParamPanel _pnParams) {
    pnLayers_=_pn;
    pnParams_=_pnParams;
    
    initComponents();
    customize();
    
    cqEdition_=_pn.getEditionLayer();
  }
  
  private void customize() {
    
    rbDirectResol.setText(PivResource.getS("Saisie directe de la r�solution")+" (m/pix):");
    rbDistanceResol.setText(PivResource.getS("Saisie par couples de points /"));
    btDelete.setText(PivResource.getS("Supprimer"));
    btDelete.setToolTipText(PivResource.getS("Supprime les couples s�lectionn�s"));
    cbDrone.setText(PivResource.getS("Images prises par drone"));
    lbZDrone.setText(PivResource.getS("Altitude du drone") + " (m)");
    
    btDelete.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        mdlPoints.removeCouple(tbCouple.getSelectedRows());
      }
    });
    
    lbComputedResol.setText(PivResource.getS("R�solution moyenne calcul�e")+" (m/pix):");
    
    actEnterDistance_=new PivEnterGeometryAction(pnLayers_, PivResource.getS("Saisir un couple"), PivResource.getS("Ajoute un couple par saisie de 2 points"), false);
    actEnterDistance_.setEditionController(ctrl_);
    btAdd.setAction(actEnterDistance_);
    actEnterDistance_.addPropertyChangeListener(new EbliSelectedChangeListener(btAdd));
    
    spCouple.setViewportView(tbCouple);
    
    coComputedResol.removeAllItems();
    coComputedResol.addItem(PivResource.getS("Distance"));
    coComputedResol.addItem(PivResource.getS("Coordonn�es"));
    coComputedResol.addItemListener(new ItemListener() {
      
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange()!=ItemEvent.SELECTED)
          return;
        
        mdlPoints.setModeDistance(coComputedResol.getSelectedIndex()==0);
      }
    });
    
    rbDirectResol.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        rbResolChanged();
      }
    });
    rbDistanceResol.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        rbResolChanged();
      }
    });
    
    rbDirectResol.setSelected(true);
    
    cbDrone.addItemListener((evt) -> {
      tfZDrone.setEnabled(cbDrone.isSelected());
      mdlPoints.setModeDrone(cbDrone.isSelected());
    });
    
    
    tfZDrone.addCaretListener(new CaretListener() {
      @Override
      public void caretUpdate(CaretEvent e) {
        mdlPoints.setDroneElevationChanged();
      }
    });

    
    mdlPoints= new CoupleTableModel();
    mdlPoints.addTableModelListener(new TableModelListener() {
      @Override
      public void tableChanged(TableModelEvent e) {
        
        Double average=mdlPoints.getAverageResolution();
        if (average==null) {
          tfComputedResol.setText("");
        }
        else {
          String val = String.format(Locale.US, "%.4G", average);
          tfComputedResol.setText(val);
        }
      }
    });
    
    tbCouple.setModel(mdlPoints);
    // Toutes les colonnes sont concern�es.
    tbCouple.setDefaultRenderer(Object.class, new CoupleTableCellRenderer());
    // #5754 : Blocage de fenetre si l'edition n'est pas termin�e et qu'on essai de supprimer la ligne s�lectionn�e.
    tbCouple.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    tbCouple.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        btDelete.setEnabled(tbCouple.getSelectedRows().length>0);
      }
    });
    
    rbDirectResol.doClick();
  }
  
  protected void rbResolChanged() {
    tfDirectResol.setEnabled(rbDirectResol.isSelected());
    tbCouple.setEnabled(rbDistanceResol.isSelected());
    coComputedResol.setEnabled(rbDistanceResol.isSelected());
    btAdd.setEnabled((rbDistanceResol.isSelected()));
    btDelete.setEnabled(rbDistanceResol.isSelected() && tbCouple.getSelectedRows().length>0);
    cbDrone.setEnabled(rbDistanceResol.isSelected());
    tfZDrone.setEnabled(rbDistanceResol.isSelected() && cbDrone.isSelected());
  }

  /**
   * Met a jour les resolutions en fonction de param�tres chang�s dans les autres sous panneaux quand le panneau est rendu visible.
   */
  public void updatePanel() {
    mdlPoints.adjustColumns();
  }
  
  public void setParams(PivOrthoParameters _params) {
    if (_params==null)
      return;
    
    List<GrPoint[]> limgPts = _params.getScalingResolutionImgPoints();
    List<GrPoint[]> lrealPts = _params.getScalingResolutionRealPoints();
    List<Double> ldistances = _params.getScalingResolutionDistances();
    List<Double> lzSegments = _params.getScalingZSegments();
    
    if (limgPts.size()==0) {
      tfDirectResol.setText("" + _params.getResolution());
      rbDirectResol.doClick();
    }
    else {
      boolean modeDistance=ldistances.size() != 0;
      boolean modeDrone = _params.isScalingDroneMode();
      
      if (modeDrone) {
        tfZDrone.setText("" + _params.getScalingZDrone());
      }
      
      mdlPoints.removeAllCouples();
      
      for (int i=0; i<limgPts.size(); i++) {
        GrPoint[] imgPts = limgPts.get(i);
        
        CoupleRow cpl = new CoupleRow();
        cpl.i1 = (int)imgPts[0].x_;
        cpl.j1 = (int)imgPts[0].y_;
        cpl.i2 = (int)imgPts[1].x_;
        cpl.j2 = (int)imgPts[1].y_;
        
        if (modeDistance) {
          double dist = ldistances.get(i);
          cpl.distance = dist;
        }
        else {
          GrPoint[] realPoints = lrealPts.get(i);
          cpl.x1=realPoints[0].x_;
          cpl.y1=realPoints[0].y_;
          cpl.x2=realPoints[1].x_;
          cpl.y2=realPoints[1].y_;
        }
        
        if (modeDrone) {
          cpl.z = lzSegments.get(i);
        }
        mdlPoints.addCouple(cpl);
      }
      
      rbDistanceResol.doClick();
      mdlPoints.setModeDistance(modeDistance);
      mdlPoints.setModeDrone(modeDrone);
      
      cbDrone.setSelected(modeDrone);
      coComputedResol.setSelectedIndex(modeDistance ? 0:1);
    }
  }
  
  /**
   * Met a jour les parametres depuis l'interface. On suppose que la validit� des donn�es de la fenetre ont �t� test�es avant.
   * 
   * ATTENTION : Il existe un mode d�grad�, ou les donn�es ne sont pas necessairement toutes valid�es (remplies), pour l'affichage
   * interactif des couples de points pendant la saisie.
   *  
   * @param _params Les parametres.
   */
  public void retrieveParams(PivOrthoParameters _params) {
    List<GrPoint[]> limgPts = new ArrayList<>();
    List<GrPoint[]> lrealPts = new ArrayList<>();
    List<Double> ldistances = new ArrayList<>();
    List<Double> lzSegments = new ArrayList<>();
    
    // Resolution directement donn�e.
    if (rbDirectResol.isSelected()) {
      double resol = 0;
      try {
        resol=Double.parseDouble(tfDirectResol.getText());
      }
      catch (NumberFormatException _exc) {
        _params.setResolution(resol);
      }
    }
    
    // Donn�e par couples de points + distances
    else if (mdlPoints.isModeDistance()) {
      for (CoupleRow row : mdlPoints.rows) {
        if (row.isImgPointsFilled()) {
          GrPoint[] imgPts=new GrPoint[2];
          imgPts[0]=new GrPoint(row.i1, row.j1, 0);
          imgPts[1]=new GrPoint(row.i2, row.j2, 0);
          limgPts.add(imgPts);

          ldistances.add(row.isDistanceFilled() ? row.distance : 0);
        }
      }
    }
    
    // donn�e par couples de points + coordonn�es r�elles
    else {
      for (CoupleRow row : mdlPoints.rows) {
        if (row.isImgPointsFilled()) {
          GrPoint[] imgPts=new GrPoint[2];
          imgPts[0]=new GrPoint(row.i1, row.j1, 0);
          imgPts[1]=new GrPoint(row.i2, row.j2, 0);
          limgPts.add(imgPts);

          GrPoint[] realPts=new GrPoint[2];
          realPts[0]=new GrPoint(row.x1 == null ? 0:row.x1, row.y1 == null ? 0:row.y1, 0);
          realPts[1]=new GrPoint(row.x2 == null ? 0:row.x2, row.y2 == null ? 0:row.y2, 0);
          lrealPts.add(realPts);
        }
      }
    }
    
    
    // En cas d'imageries drones
    Double zDrone = null;
    if (cbDrone.isSelected()) {
      zDrone = getZDrone();
      
      for (CoupleRow row : mdlPoints.rows) {
        lzSegments.add(row.z);
      }
    }
    
    _params.setScalingResolutionDistances(ldistances);
    _params.setScalingResolutionImgPoints(limgPts);
    _params.setScalingResolutionRealPoints(lrealPts);
    _params.setResolution(getResolution());
    _params.setScalingZDrone(zDrone);
    _params.setScalingZSegments(lzSegments);
  }
  
  /**
   * @return La resolution. Les champs sont suppos�s avoir �t� control�s avant l'appel � cette m�thode. 
   */
  public Double getResolution() {
    Double resol=null;
    try {
      if (rbDirectResol.isSelected()) {
        resol=Double.parseDouble(tfDirectResol.getText());
      }
      else {
        resol=Double.parseDouble(tfComputedResol.getText());
      }
    }
    catch (NumberFormatException exc) {
    }

    return resol;
  }
  
  protected Double getZDrone() {
    Double zDrone = null;
    if (cbDrone.isSelected()) {
      try {
        zDrone=Double.parseDouble(tfZDrone.getText());
      }
      catch (NumberFormatException _exc) {
      }
    }
    
    return zDrone;
  }

  public boolean isDataValid(boolean _withMes) {
    if (rbDirectResol.isSelected()) {
      boolean bok=PivUtils.isStrictPositiveReal(_withMes ? pnParams_:null,tfDirectResol.getText(),PivResource.getS("R�solution")+" : "+PivResource.getS("R�solution saisie"))!=null;
      if (!bok) return false;
    }
    else {
      if (cbDrone.isSelected() && PivUtils.isStrictPositiveReal(_withMes ? pnParams_:null, tfZDrone.getText(), PivResource.getS("R�solution")+" : " + PivResource.getS("Altitude du drone"))==null) {
        return false;
      }
      
      boolean bok = mdlPoints.rows.size() > 0;
      for (CoupleRow cpl : mdlPoints.rows) {
        if (cpl.resolution==null) {
          bok = false;
          break;
        }
      }
      
      if (!bok) {
        if (_withMes)
          setErrorText(PivResource.getS("Les couples de points doivent �tre renseign�s"));
        else
          setErrorText("");
        return false;
      }
    }
    
    pnParams_.setErrorText("");
    return true;
  }
  
  private void setErrorText(String _error) {
    if (_error.isEmpty()) {
      pnParams_.setErrorText("");
    }
    else {
      pnParams_.setErrorText(PivResource.getS("R�solution")+" : "+_error);
    }
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgResol = new javax.swing.ButtonGroup();
        pnCouples = new javax.swing.JPanel();
        rbDistanceResol = new javax.swing.JRadioButton();
        spCouple = new javax.swing.JScrollPane();
        btDelete = new javax.swing.JButton();
        tfComputedResol = new javax.swing.JTextField();
        lbComputedResol = new javax.swing.JLabel();
        btAdd = new javax.swing.JToggleButton();
        coComputedResol = new javax.swing.JComboBox<>();
        cbDrone = new javax.swing.JCheckBox();
        lbZDrone = new javax.swing.JLabel();
        tfZDrone = new javax.swing.JTextField();
        pnDirect = new javax.swing.JPanel();
        rbDirectResol = new javax.swing.JRadioButton();
        tfDirectResol = new javax.swing.JTextField();

        pnCouples.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        bgResol.add(rbDistanceResol);
        rbDistanceResol.setText("Saisie par couple de points /");

        btDelete.setText("Supprimer");

        tfComputedResol.setEnabled(false);

        lbComputedResol.setText("R�solution moyenne calcul�e");

        btAdd.setText("Ajouter un couple");

        cbDrone.setText("Images prises par drone");

        lbZDrone.setText("Altitude du drone (m)");

        javax.swing.GroupLayout pnCouplesLayout = new javax.swing.GroupLayout(pnCouples);
        pnCouples.setLayout(pnCouplesLayout);
        pnCouplesLayout.setHorizontalGroup(
            pnCouplesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCouplesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnCouplesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnCouplesLayout.createSequentialGroup()
                        .addGap(0, 424, Short.MAX_VALUE)
                        .addComponent(btAdd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btDelete))
                    .addGroup(pnCouplesLayout.createSequentialGroup()
                        .addComponent(lbComputedResol)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfComputedResol))
                    .addComponent(spCouple, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnCouplesLayout.createSequentialGroup()
                        .addGroup(pnCouplesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rbDistanceResol)
                            .addComponent(cbDrone))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnCouplesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnCouplesLayout.createSequentialGroup()
                                .addComponent(lbZDrone)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfZDrone))
                            .addComponent(coComputedResol, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        pnCouplesLayout.setVerticalGroup(
            pnCouplesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCouplesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnCouplesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rbDistanceResol, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(coComputedResol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnCouplesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbDrone)
                    .addComponent(lbZDrone)
                    .addComponent(tfZDrone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(spCouple, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnCouplesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btDelete)
                    .addComponent(btAdd))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnCouplesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfComputedResol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbComputedResol))
                .addGap(9, 9, 9))
        );

        pnDirect.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        bgResol.add(rbDirectResol);
        rbDirectResol.setText("Saisie directe de la r�solution");

        javax.swing.GroupLayout pnDirectLayout = new javax.swing.GroupLayout(pnDirect);
        pnDirect.setLayout(pnDirectLayout);
        pnDirectLayout.setHorizontalGroup(
            pnDirectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnDirectLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(rbDirectResol)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tfDirectResol)
                .addContainerGap())
        );
        pnDirectLayout.setVerticalGroup(
            pnDirectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnDirectLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnDirectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rbDirectResol)
                    .addComponent(tfDirectResol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnDirect, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnCouples, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnDirect, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnCouples, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgResol;
    private javax.swing.JToggleButton btAdd;
    private javax.swing.JButton btDelete;
    private javax.swing.JCheckBox cbDrone;
    private javax.swing.JComboBox<String> coComputedResol;
    private javax.swing.JLabel lbComputedResol;
    private javax.swing.JLabel lbZDrone;
    private javax.swing.JPanel pnCouples;
    private javax.swing.JPanel pnDirect;
    private javax.swing.JRadioButton rbDirectResol;
    private javax.swing.JRadioButton rbDistanceResol;
    private javax.swing.JScrollPane spCouple;
    private javax.swing.JTextField tfComputedResol;
    private javax.swing.JTextField tfDirectResol;
    private javax.swing.JTextField tfZDrone;
    // End of variables declaration//GEN-END:variables
}
