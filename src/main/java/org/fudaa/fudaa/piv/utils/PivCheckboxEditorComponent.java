package org.fudaa.fudaa.piv.utils;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.fudaa.ctulu.editor.CtuluValueEditorI;

import com.memoire.bu.BuTableCellRenderer;

/**
 * Un composant editeur contenant un textfield et un checkbox pour indiquer qu'on donne une valeur ou pas. Il autorise de
 * ne pas mettre de valeur.
 * 
 * @param <T> Le type de la valeur.
 * @author Bertrand Marchand
 */
public class PivCheckboxEditorComponent<T> extends JPanel {

  public class PivCheckboxTableCellEditor extends AbstractCellEditor implements TableCellEditor {
    
    @Override
    public Object getCellEditorValue() {
      return PivCheckboxEditorComponent.this.getValue();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row,
        int column) {
      PivCheckboxEditorComponent.this.setValue((T)value);
      return PivCheckboxEditorComponent.this;

    }

    // L'editeur est activ� uniquement suite a un double click.
    @Override
    public boolean isCellEditable(EventObject e) {
      if (e instanceof MouseEvent) {
        return ((MouseEvent) e).getClickCount() == 2;
      }
      else {
        return true;
      }
    }
  }

  public class PivCheckboxTableCellRenderer extends BuTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
        int row, int column) {

      Object val = (value == null || value.equals(PivCheckboxEditorComponent.this.unsetValue)) ? "" : value;
      return super.getTableCellRendererComponent(table, val, isSelected, hasFocus, row, column);
    }
  }

  public T unsetValue;
  /**  La valeur par defaut (affich� dans le textfield) */
  public T defVal_;

  /** Permet d'indiquer que le coefficient est unique (sinon ponctuel). */
  JCheckBox cb;
  /** Contient la valeur, double. */
  JTextField tf;
  /** Le composant encapsul�, pour un tableau. */
  PivCheckboxTableCellEditor tableCellEditor = new PivCheckboxTableCellEditor();
  /** Le renderer du composant, pour un tableau. */
  PivCheckboxTableCellRenderer tableCellRenderer = new PivCheckboxTableCellRenderer();
  /** L'�diteur coeff ponctuel associ� (pour desactivation si la coefficient est unique) */
  CtuluValueEditorI linkedEditor;
  String checkedTooltip;
  String uncheckedTooltip;

  public PivCheckboxEditorComponent() {
    setLayout(new BorderLayout());
    cb = new JCheckBox();
    cb.setSelected(true);
    cb.addItemListener((e) -> {
      PivCheckboxEditorComponent<T> cp = PivCheckboxEditorComponent.this;
      cp.firePropertyChange("value", null, cp.getValue());
      
      if (cb.isSelected()) {
        cb.setToolTipText(checkedTooltip);
      }
      else {
        cb.setToolTipText(uncheckedTooltip);
      }
      tf.setEnabled(cb.isSelected());

      // Pas propre, mais fonctionnel
      if (linkedEditor != null)
        linkedEditor.setEditable(!cb.isSelected());
    });

    tf = new JTextField();
    tf.getDocument().addDocumentListener(new DocumentListener() {
      
      @Override
      public void removeUpdate(DocumentEvent e) {
        PivCheckboxEditorComponent<T> cp = PivCheckboxEditorComponent.this;
        cp.firePropertyChange("value", null, cp.getValue());
      }
      
      @Override
      public void insertUpdate(DocumentEvent e) {
        PivCheckboxEditorComponent<T> cp = PivCheckboxEditorComponent.this;
        cp.firePropertyChange("value", null, cp.getValue());
      }
      
      @Override
      public void changedUpdate(DocumentEvent e) {
      }
    });

    add(cb, BorderLayout.WEST);
    add(tf, BorderLayout.CENTER);
  }

  public void setLinkedEditor(CtuluValueEditorI _linkedEditor) {
    linkedEditor = _linkedEditor;
    
    if (linkedEditor != null)
      linkedEditor.setEditable(!cb.isSelected());
  }

  /**
   * Defiinit les tooltips sur le chackbox.
   * @param _checkedTooltip Tooltip affich� quand le checkbox est s�lectionn�.
   * @param _uncheckedTooltip  Tooltip affich� quand le checkbox est d�s�lectionn�.
   */
  public void setCheckboxTooltips(String _checkedTooltip, String _uncheckedTooltip) {
    checkedTooltip = _checkedTooltip;
    uncheckedTooltip = _uncheckedTooltip;
    
    cb.setToolTipText(cb.isSelected() ? checkedTooltip:uncheckedTooltip);
  }
  
  public boolean isValueSet() {
    return getValue() != null;
  }
  
  public boolean isValueEmpty() {
    return tf.getText().trim().isEmpty();
  }

  /**
   * Defint la valeur unset.
   * @param _val
   */
  public void setUnsetValue(T _val) {
    unsetValue  = _val;
    tf.setText(""  + defVal_);
  }

  /**
   * Defint la valeur par defaut au d�but de l'affichage du composant.
   * @param _def
   */
  public void setDefaultValue(T _def) {
    defVal_  = _def;
    tf.setText(""  + defVal_);
  }
  
  /**
   * Definit la valeur pour le composant. 
   * @param _d La valeur. Si null, indique une valeur multiple.
   */
  public void setValue(T _d) {
    if (_d == null) {
      cb.setSelected(true);
      tf.setText("");
    }
    
    else if (_d.equals(unsetValue)) {
      cb.setSelected(false);
      // La valeur par defaut reste.
      tf.setText("" + defVal_);
    }
    else {
      cb.setSelected(true);
      tf.setText("" + (_d == null ? "" : _d.toString()));
    }
  }
  
  /**
   * @return La valeur, ou null, si la valeur n'est pas correcte.
   */
  public Object getValue() {
    if (!cb.isSelected()) {
      return unsetValue;
    }

    String s = tf.getText().trim();
    if (unsetValue instanceof Double) {
      try {
        return Double.parseDouble(s);
      }
      catch (Exception _exc) {
        return null;
      }
    }

    else if (unsetValue instanceof Integer) {
      try {
        return Integer.parseInt(s);
      }
      catch (Exception _exc) {
        return null;
      }
    }
    
    else {
      return s;
    }
  }

  public TableCellEditor createTableCellEditor() {
    return tableCellEditor;
  }

  public TableCellRenderer createTableCellRenderer() {
    return tableCellRenderer;
  }
}
