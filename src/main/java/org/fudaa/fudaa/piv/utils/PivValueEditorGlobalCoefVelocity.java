package org.fudaa.fudaa.piv.utils;

import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.editor.CtuluValueEditorI;

/**
 * Un editeur de coefficient unique, avec bouton pour indiquer que le coefficient sera donn� ponctuellement ou non.
 * 
 * @author Bertrand Marchand
 *
 */
public class PivValueEditorGlobalCoefVelocity extends CtuluValueEditorDouble {
  
  public final static double UNSET_VALUE = -1;
  
  Double defVal_;
  Double unsetVal_ = UNSET_VALUE;
  String checkedTooltip;
  String uncheckedTooltip;
  boolean isUnset;

  CtuluValueEditorI linkedEditor_;
  
  // Le composant n'est pas recr�� � chaque fois, pour conserver le linkedEditor.
  PivCheckboxEditorComponent<Double> comp_;
  
  private PivCheckboxEditorComponent<Double> buildComponent() {
    if (comp_ == null) {
    comp_ = new PivCheckboxEditorComponent<>();
    comp_.setUnsetValue(unsetVal_);
    comp_.setDefaultValue(defVal_);
    comp_.setCheckboxTooltips(checkedTooltip, uncheckedTooltip);
    comp_.setLinkedEditor(linkedEditor_);
    if (isUnset)
      comp_.setValue(unsetVal_);
    }
    
    return comp_;
  }
  
  /**
   * La valeur affich�e dans le textfield, lorsque le chackbox est activ�.
   * @param _val
   */
  public void setDefaultValue(Double _val) {
    defVal_ = _val;
  }

  public void setLinkedEditor(CtuluValueEditorI _linkedEditor) {
    linkedEditor_ = _linkedEditor;
  }

  @Override
  public TableCellRenderer createTableRenderer() {
    return buildComponent().createTableCellRenderer();
  }

  public void setValue(final double _v, final Component _comp) {
    PivCheckboxEditorComponent<Double> comp = (PivCheckboxEditorComponent<Double>) _comp;
    comp.setValue(_v);
  }

  @Override
  public void setValue(final Object _s, final Component _comp) {
    setValue((double)_s, _comp);
  }

  @Override
  public Object getValue(Component _comp) {
    PivCheckboxEditorComponent<Double> comp = (PivCheckboxEditorComponent<Double>) _comp;
    return comp.getValue();
  }

  @Override
  public boolean isValueValidFromComponent(Component _comp) {
    return getValue(_comp) != null;
  }

  @Override
  public boolean isEmpty(Component _c) {
    return ((PivCheckboxEditorComponent<Double>) _c).isValueSet();
  }

  @Override
  public String getStringValue(final Component _comp) {
    PivCheckboxEditorComponent<Double> c = (PivCheckboxEditorComponent<Double>)_comp;
    if (c.getValue() == null) {
      return "";
    }
    else {
      return c.getValue().toString();
    }
  }
  
  @Override
  public boolean isValid(final Object _o) {
    try {
      Double.parseDouble(_o.toString());
      return true;
    }
    catch (Exception _exc) {
      return false;
    }
  }

  @Override
  public TableCellEditor createTableEditorComponent() {
    return buildComponent().createTableCellEditor();
  }

  @Override
  public JComponent createEditorComponent() {
    return buildComponent();
  }

  @Override
  public TableCellEditor createCommonTableEditorComponent() {
    return buildComponent().createTableCellEditor();
  }

  @Override
  public JComponent createCommonEditorComponent() {
    return buildComponent();
  }
}
