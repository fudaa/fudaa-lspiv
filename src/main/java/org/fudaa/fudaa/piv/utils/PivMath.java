package org.fudaa.fudaa.piv.utils;

import java.util.Arrays;

/**
 * Des fonctions statistiques.
 * 
 * @author marchand@deltacad.fr
 */
public class PivMath {

  /**
   * @param _vals Les valeurs de la serie
   * @return Retourne le min d'une serie. Si la serie est de longueur 0, retourne
   *         Double.MAX_VALUE
   */
  public static double min(double[] _vals) {
    double min = Double.MAX_VALUE;
    for (double val : _vals) {
      min = Math.min(val, min);
    }

    return min;
  }

  /**
   * @param _vals Les valeurs de la serie
   * @return Retourne le max d'une serie. Si la serie est de longueur 0, retourne
   *         Double.MIN_VALUE
   */
  public static double max(double[] _vals) {
    double max = Double.MIN_VALUE;
    for (double val : _vals) {
      max = Math.max(val, max);
    }

    return max;
  }

  /**
   * @param _vals Les valeurs de la serie
   * @return Retourne la moyenne d'une serie. Si la serie est de longueur 0,
   *         retourne Double.NaN
   */
  public static double average(double[] _vals) {
    if (_vals.length == 0)
      return Double.NaN;

    double ave = 0;
    for (double val : _vals) {
      ave += val;
    }

    return ave / _vals.length;
  }

  /**
   * @param _vals Les valeurs de la serie
   * @return Retourne la m�diane d'une serie. Si la serie est de longueur 0,
   *         retourne Double.NaN
   */
  public static double median(double[] _vals) {
    if (_vals.length == 0)
      return Double.NaN;

    // Pour ne pas modifier le tableau d'origine.
    _vals = Arrays.copyOf(_vals, _vals.length);
    Arrays.sort(_vals);

    if (_vals.length % 2 == 0) {
      return (_vals[_vals.length / 2] + _vals[_vals.length / 2 + 1]) / 2.;
    }
    else {
      return _vals[_vals.length / 2 + 1];
    }
  }

  /**
   * @param _vals Les valeurs de la serie
   * @return Retourne l'�cart type d'une serie. Si la serie est de longueur 0,
   *         retourne Double.NaN
   */
  public static double standardDeviation(double[] _vals) {
    if (_vals.length == 0)
      return Double.NaN;

    // Calcul de la moyenne
    double moy = average(_vals);

    double s = 0;
    for (double val : _vals) {
      s += (val - moy) * (val - moy);
    }

    return Math.sqrt(s / _vals.length);
  }
}
