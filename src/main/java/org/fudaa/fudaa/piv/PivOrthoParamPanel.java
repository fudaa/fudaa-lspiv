package org.fudaa.fudaa.piv;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;
import org.fudaa.fudaa.piv.metier.PivProject.OrthoMode;

/**
 * Un panneau de saisie des param�tres pour la transformation des images.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivOrthoParamPanel extends CtuluDialogPanel {
  PivOrthoPoint[] pts_;
  PivImplementation impl_;

  /**
   * Constructeur.
   */
  public PivOrthoParamPanel(PivImplementation _impl) {
    impl_=_impl;
    initComponents();
    customize();

    PivOrthoParameters params=impl_.getCurrentProject().getOrthoParameters();
    if (params==null)
      params=new PivOrthoParameters();
    setOrthoParams(params);
    setOrthoPoints(impl_.getCurrentProject().getOrthoPoints());
  }

  private void customize() {
    lbXmin.setText(PivResource.getS("Xmin: "));
    lbYmin.setText(PivResource.getS("Ymin: "));
    lbXmax.setText(PivResource.getS("Xmax: "));
    lbYmax.setText(PivResource.getS("Ymax: "));
    lbResolution.setText(PivResource.getS("R�solution (m/pix): "));
    lbNivEau.setText(PivResource.getS("Niveau d'eau (m): "));
    pnPositions.setBorder(javax.swing.BorderFactory.createTitledBorder(PivResource.getS("Position des coins (m)")));
    btPositionSaisie.setText(PivResource.getS("Saisie interactive..."));
    btPositionSaisie.setVisible(false);
    btCalculer.setText(PivResource.getS("Calculer des valeurs par d�faut"));
    btCalculer.setEnabled(false);
    btCalculer.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setDefaultValues();
      }
    });
  }

  /**
   * Rempli le panneau depuis les donn�es du projet.
   * @param _params L'objet m�tier pour l'orthorectification.
   */
  public void setOrthoParams(PivOrthoParameters _params) {
    tfXmin.setText("" + _params.getXmin());
    tfXmax.setText("" + _params.getXmax());
    tfYmin.setText("" + _params.getYmin());
    tfYmax.setText("" + _params.getYmax());
    tfResolution.setText("" + _params.getResolution());
    tfNivEau.setText("" + (_params.getWaterElevation()==null ? "":_params.getWaterElevation()));
    btPositionSaisie.setEnabled(false);
  }

  /**
   * Les points de r�f�rence, pour le calcul des valeurs par defaut.
   * @param _pts Les points
   */
  public void setOrthoPoints(PivOrthoPoint[] _pts) {
    pts_=_pts;
    btCalculer.setEnabled(pts_!=null);
  }

  /**
   * Met a jour les parametres d'ortho rectification donn�s.
   * @param _params Les parametres d'ortho rectification.
   */
  public void retrieveOthoParams(PivOrthoParameters _params) {
    _params.setXmin(Double.parseDouble(tfXmin.getText().trim()));
    _params.setYmin(Double.parseDouble(tfYmin.getText().trim()));
    _params.setXmax(Double.parseDouble(tfXmax.getText().trim()));
    _params.setYmax(Double.parseDouble(tfYmax.getText().trim()));
    _params.setResolution(Double.parseDouble(tfResolution.getText().trim()));
    _params.setWaterElevation(Double.parseDouble(tfNivEau.getText().trim()));
  }

  @Override
  public boolean isDataValid() {
    boolean bok=false;
    double xmin=0;
    double xmax=0;
    double ymin=0;
    double ymax=0;

    try {
      bok=false;
      xmin=Double.parseDouble(tfXmin.getText().trim());
      bok=true;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      setErrorText(PivResource.getS("Xmin doit �tre un r�el"));
      return false;
    }

    try {
      bok=false;
      xmax=Double.parseDouble(tfXmax.getText().trim());
      bok=true;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      setErrorText(PivResource.getS("Xmax doit �tre un r�el"));
      return false;
    }

    if (xmin>=xmax) {
      setErrorText(PivResource.getS("Xmax doit �tre strictement sup�rieur � Xmin"));
      return false;
    }

    try {
      bok=false;
      ymin=Double.parseDouble(tfYmin.getText().trim());
      bok=true;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      setErrorText(PivResource.getS("Ymin doit �tre un r�el"));
      return false;
    }

    try {
      bok=false;
      ymax=Double.parseDouble(tfYmax.getText().trim());
      bok=true;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      setErrorText(PivResource.getS("Ymax doit �tre un r�el"));
      return false;
    }

    if (ymin>=ymax) {
      setErrorText(PivResource.getS("Ymax doit �tre strictement sup�rieur � Ymin"));
      return false;
    }

    try {
      bok=false;
      bok=Double.parseDouble(tfResolution.getText().trim())>0;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      setErrorText(PivResource.getS("La r�solution doit �tre un r�el strictement positif"));
      return false;
    }

    try {
      bok=false;
      Double.parseDouble(tfNivEau.getText().trim());
      bok=true;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      setErrorText(PivResource.getS("Le niveau d'eau doit �tre un r�el"));
      return false;
    }

    return true;
  }

  /**
   * Surcharg� pour pouvoir pr�visualiser l'image transform�e sans sortir du 
   * dialogue.
   * @return 
   */
  @Override
  public boolean apply() {
    PivOrthoParameters params=new PivOrthoParameters();
    retrieveOthoParams(params);

    if (params.equals(impl_.getCurrentProject().getOrthoParameters()))
      return true;
    
    // Sp�cifique orthorectification, quand tous les points ont la m�me cote.
    Double z=PivOrthoPoint.getIdenticalZ(impl_.getCurrentProject().getOrthoPoints());
    if (impl_.getCurrentProject().getOrthoMode() != OrthoMode.SCALING && z!=null && !z.equals(params.getWaterElevation())) {
      impl_.message(PivResource.getS("La cote du plan d'eau va �tre modifi�e pour correspondre aux cotes Z des GRP"));
      params.setWaterElevation(z);
      setOrthoParams(params);
    }
    
    impl_.getCurrentProject().setOrthoParameters(params);

    CtuluLog ana=new CtuluLog();
    ana.setDesc(PivResource.getS("Pr�visualisation des images transform�es"));
//    PivExeLauncher.instance().computeOrthoCoefs(ana, impl_.getCurrentProject(),null);
//    if (ana.containsErrorOrSevereError()) {
//      impl_.error(ana.getResume());
//      return;
//    }

    PivExeLauncher.instance().computeTransfImg(ana, impl_.getCurrentProject(),true,null);
    if (ana.containsErrorOrSevereError()) {
      impl_.error(ana.getResume());
      return true;
    }

    impl_.get2dFrame().getVisuPanel().restaurer();
    
    return true;
  }
  /**
   * Affecte les valeurs par defaut.
   */
  private void setDefaultValues() {
    // Les dimension max
    if (pts_==null) return;
    
    double xmin;
    double xmax;
    double ymin;
    double ymax;
    double resolution;
    
//    boolean newAlgo = true;
//    if (newAlgo) {
    CtuluLog ana = new CtuluLog();
    ana.setDesc(getName());

    PivOrthoParameters params = PivExeLauncher.instance().getTransfDefaultParameters(ana, impl_.getCurrentProject(),
        null);
    if (ana.containsErrorOrSevereError()) {
      impl_.error(ana.getResume());
      return;
    }
    xmin = params.getXmin();
    xmax = params.getXmax();
    ymin = params.getYmin();
    ymax = params.getYmax();
    resolution = params.getResolution();
//    }
//
//    else {
//      xmin = Double.POSITIVE_INFINITY;
//      xmax = Double.NEGATIVE_INFINITY;
//      ymin = Double.POSITIVE_INFINITY;
//      ymax = Double.NEGATIVE_INFINITY;
//
//      for (int i = 0; i < pts_.length; i++) {
//        xmin = Math.min(xmin, pts_[i].getRealPoint().x_);
//        xmax = Math.max(xmax, pts_[i].getRealPoint().x_);
//        ymin = Math.min(ymin, pts_[i].getRealPoint().y_);
//        ymax = Math.max(ymax, pts_[i].getRealPoint().y_);
//      }
//      resolution = ((int) (Math.max(xmax - xmin, ymax - ymin) * 10.)) / 5000.;
//    }

    tfXmin.setText("" + xmin);
    tfXmax.setText("" + xmax);
    tfYmin.setText("" + ymin);
    tfYmax.setText("" + ymax);
    tfResolution.setText("" + resolution);
  }

  /** This method is called from within the constructor to
   * initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is
   * always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    lbResolution = new javax.swing.JLabel();
    lbNivEau = new javax.swing.JLabel();
    tfNivEau = new javax.swing.JTextField();
    tfResolution = new javax.swing.JTextField();
    pnPositions = new javax.swing.JPanel();
    lbXmin = new javax.swing.JLabel();
    tfXmin = new javax.swing.JTextField();
    lbXmax = new javax.swing.JLabel();
    tfXmax = new javax.swing.JTextField();
    lbYmax = new javax.swing.JLabel();
    tfYmax = new javax.swing.JTextField();
    btPositionSaisie = new javax.swing.JButton();
    lbYmin = new javax.swing.JLabel();
    tfYmin = new javax.swing.JTextField();
    btCalculer = new javax.swing.JButton();

    setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

    lbResolution.setText("R�solution (m/pix):");

    lbNivEau.setText("Niveau d'eau (m):");

    pnPositions.setBorder(javax.swing.BorderFactory.createTitledBorder("Position des coins (m)"));

    lbXmin.setText("Xmin:");

    tfXmin.setPreferredSize(new Dimension(50,tfXmin.getPreferredSize().height));

    lbXmax.setText("Xmax:");

    lbYmax.setText("Ymax:");

    btPositionSaisie.setText("Saisie interactive...");

    lbYmin.setText("Ymin:");

    javax.swing.GroupLayout pnPositionsLayout = new javax.swing.GroupLayout(pnPositions);
    pnPositions.setLayout(pnPositionsLayout);
    pnPositionsLayout.setHorizontalGroup(
      pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(pnPositionsLayout.createSequentialGroup()
        .addGroup(pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(pnPositionsLayout.createSequentialGroup()
            .addGroup(pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(lbXmin)
              .addComponent(lbYmin))
            .addGap(8, 8, 8)
            .addGroup(pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(tfXmin, javax.swing.GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE)
              .addComponent(tfYmin, javax.swing.GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
              .addComponent(lbYmax)
              .addComponent(lbXmax))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(tfXmax, javax.swing.GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
              .addComponent(tfYmax, javax.swing.GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)))
          .addComponent(btPositionSaisie))
        .addContainerGap())
    );
    pnPositionsLayout.setVerticalGroup(
      pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(pnPositionsLayout.createSequentialGroup()
        .addGroup(pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(lbXmin)
          .addComponent(tfXmin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(lbXmax)
          .addComponent(tfXmax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(pnPositionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(lbYmax)
          .addComponent(tfYmax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(lbYmin)
          .addComponent(tfYmin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addComponent(btPositionSaisie))
    );

    btCalculer.setText("Calculer des valeurs par d�faut");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(lbNivEau)
          .addComponent(lbResolution))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
          .addComponent(tfNivEau)
          .addComponent(tfResolution, javax.swing.GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE))
        .addContainerGap(73, Short.MAX_VALUE))
      .addComponent(pnPositions, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
        .addContainerGap(65, Short.MAX_VALUE)
        .addComponent(btCalculer)
        .addContainerGap())
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addComponent(pnPositions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(18, 18, 18)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(layout.createSequentialGroup()
            .addComponent(lbResolution)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(lbNivEau))
          .addGroup(layout.createSequentialGroup()
            .addComponent(tfResolution, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(tfNivEau, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
        .addComponent(btCalculer))
    );
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btCalculer;
  private javax.swing.JButton btPositionSaisie;
  private javax.swing.JLabel lbNivEau;
  private javax.swing.JLabel lbResolution;
  private javax.swing.JLabel lbXmax;
  private javax.swing.JLabel lbXmin;
  private javax.swing.JLabel lbYmax;
  private javax.swing.JLabel lbYmin;
  private javax.swing.JPanel pnPositions;
  private javax.swing.JTextField tfNivEau;
  private javax.swing.JTextField tfResolution;
  private javax.swing.JTextField tfXmax;
  private javax.swing.JTextField tfXmin;
  private javax.swing.JTextField tfYmax;
  private javax.swing.JTextField tfYmin;
  // End of variables declaration//GEN-END:variables
}
