/*
 * @creation 8 sept. 06
 * @modification $Date: 2009-06-03 15:10:45 +0200 (mer., 03 juin 2009) $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.piv.PivTaskAbstract.TaskStatus;

import com.memoire.fu.FuLib;


/**
 * Un panneau pour visualiser la progression d'une tache, avec interruption
 * possible.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivProgressionPanel extends CtuluDialogPanel implements PivTaskObserverI {
  PivTaskAbstract task_;
  JProgressBar progressBar_;
  JLabel lbTimeRemain = new JLabel();
  String stepTitle_ = "";
  boolean isStopRequested_;
  long previousCurrentTime;
  int previousProg = 0;
  
  // Le temps estim� par pourcentage d'avancement.
  long timeByPercent = -1;
  // Pour le calcul de pourcentage.
  int nbProgs = 0;
  long remainTime;
  
  /**
   * Le constructeur, avec bouton stop actif.
   * @param _task La tache pour laquelle visualiser la progression au travers
   * du panneau.
   */
  public PivProgressionPanel(PivTaskAbstract _task) {
    this(_task,true);
  }
  
  /**
   * Le constructeur.
   * @param _task La tache pour laquelle visualiser la progression au travers
   * du panneau.
   * @param _showStop  True : Le bouton d'interruption est visible.
   */
  public PivProgressionPanel(PivTaskAbstract _task, boolean _showStop) {
    setLayout(new BorderLayout(5,5));

    
//    taskView_ = new BuTaskView();
//    taskView_.setPreferredSize(new Dimension(350, 30));
    progressBar_ = new JProgressBar(0,100);
    progressBar_.setStringPainted(true);
    progressBar_.setPreferredSize(new Dimension(350, progressBar_.getPreferredSize().height));
    JButton btStop=new JButton(PivResource.getS("Stop"));
    btStop.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        cancel();
      }
    });
    JPanel pnButtons=new JPanel();
    pnButtons.setLayout(new BorderLayout(5,5));
    if (_showStop) {
      pnButtons.add(btStop,BorderLayout.EAST);
    }

    lbTimeRemain.setText(PivResource.getS("Temps restant = {0}", "Calcul..."));
    lbTimeRemain.setPreferredSize(new Dimension(lbTimeRemain.getPreferredSize().width, lbTimeRemain.getPreferredSize().height));
    add(lbTimeRemain, BorderLayout.NORTH);
    add(progressBar_,BorderLayout.CENTER);
    add(pnButtons,BorderLayout.SOUTH);

    setTask(_task);
  }

  /**
   * Definit la tache associ�e.
   * @param _task La tache.
   */
  public void setTask(PivTaskAbstract _task) {
    task_=_task;
    task_.setObserver(this);
  }

  /**
   * Surcharg� pour ne pas fermer la fen�tre et afficher un message d'attente
   * d'interruption de la tache.
   */
  @Override
  public boolean cancel() {
    this.setErrorText(PivResource.getS("Interruption en cours. Merci de patienter..."));
    isStopRequested_ = true;
    return false;
  }

  @Override
  public void appendDetail(String _s) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void appendDetailln(String _s) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void setProgression(int _v) {

    if (previousProg < _v) {
      long currentTime = System.currentTimeMillis();
      // Pas encore estim�
      long newTimeByPercent = (currentTime - previousCurrentTime) / (_v - previousProg);
      if (timeByPercent == -1) {
        timeByPercent = newTimeByPercent;
      }
      else {
        timeByPercent = (timeByPercent * nbProgs + newTimeByPercent) / (nbProgs + 1);
      }
      nbProgs++;

      previousCurrentTime = currentTime;

      remainTime = (100 - _v) * timeByPercent;
    }
    
    previousProg = _v;
    updateProgressBar(_v);
  }
  
  @Override
  public int getProgression() {
    return previousProg;
  }
  
  /**
   * Mise a jour de la progression, le pourcentage et le temps restant.
   * @param _v Si le pourcentage est < 0, le temps restant est ind�termin�.
   */
  private void updateProgressBar(int _v) {
    
    // Progression indetermin�e
    if (_v < 0) {
      progressBar_.setIndeterminate(true);
      progressBar_.setString("- " + (stepTitle_ == null ? "" : stepTitle_));
      String sduration = PivResource.getS("Ind�termin�");
      lbTimeRemain.setText(PivResource.getS("Temps restant = {0}", sduration));
    }
    
    // Progression determin�e
    else {
      progressBar_.setIndeterminate(false);
      progressBar_.setValue(_v);

      if (stepTitle_ == null || stepTitle_ == "")
        progressBar_.setString("");
      else
        progressBar_.setString("- " + _v + "% : " + stepTitle_);

      if (remainTime == -1) {
        lbTimeRemain.setText("");
      }
      else {
        // Fluctuant au d�part, on attend pour afficher le temps restant.
        if (nbProgs < 4)
          lbTimeRemain.setText(PivResource.getS("Calcul du temps restant..."));
        else
          lbTimeRemain.setText(PivResource.getS("Temps restant = {0}", FuLib.duration(remainTime)));
      }
    }
  }

  @Override
  public void setDesc(String _s) {
    stepTitle_ = _s;
    setProgression(progressBar_.getValue());
  }

  @Override
  public void reset() {
    previousCurrentTime = System.currentTimeMillis();
    timeByPercent = -1;
    nbProgs = 0;
    stepTitle_ = "";
    remainTime = -1;
    
    updateProgressBar(0);
//    setDesc("");
  }

  @Override
  public void completed() {
    Window d = (Window)SwingUtilities.getAncestorOfClass(Window.class, this);
    d.dispose();
  }

  @Override
  public boolean isStopRequested() {
    return  isStopRequested_;
  }

  /**
   * La methode n'est pas utilis�e.
   */
  @Override
  public void setStatus(TaskStatus _state) {
  }
}