/**
 * 
 */
package org.fudaa.fudaa.piv;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JToggleButton;

import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueAffichage;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.commun.EbliActionAbstract;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;
import org.fudaa.ebli.commun.EbliFormatter;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.trace.TraceSurface;
import org.fudaa.ebli.trace.TraceSurfaceModel;
import org.fudaa.fudaa.piv.action.PivOriginalViewAction;
import org.fudaa.fudaa.piv.action.PivOrthoGRPAction;
import org.fudaa.fudaa.piv.layer.PivStabAreasModel;
import org.fudaa.fudaa.piv.layer.PivImageRasterLayer;
import org.fudaa.fudaa.piv.layer.PivOriginalImageModel;
import org.fudaa.fudaa.piv.layer.PivOrthoPointsModel;
import org.fudaa.fudaa.piv.layer.PivScalingPointsLayer;
import org.fudaa.fudaa.piv.layer.PivScalingPointsModel;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivProjectStateListener;
import org.fudaa.fudaa.piv.utils.PivUtils;

/**
 * Une vue en espace d'images reconditionn�es. 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivOriginalView implements PivViewI, PivProjectStateListener {
  /** Le nom de la vue en espace image source */
  public static final String TITLE=PivResource.getS("Espace image source");
  
  /** La d�finition de coordonn�e pour X */
  private static final EbliCoordinateDefinition DEF_COOR_I=new EbliCoordinateDefinition("I", new EbliFormatter(PivTransfView.PIXEL));
  /** La d�finition de coordonn�e pour Y */
  private static final EbliCoordinateDefinition DEF_COOR_J=new EbliCoordinateDefinition("J", new EbliFormatter(PivTransfView.PIXEL));
  
  private PivOrthoGRPAction actOrthoGRP_;
  private EbliActionAbstract actOriginalView_;
  private PivVisuPanel pnLayers_;
  private JComboBox<String> cbImg_;
  private JToggleButton btShowStabImages_;
  private PivProject prj_;
  private List<BCalqueAffichage> layers_=new ArrayList<BCalqueAffichage>();
  
  /** L'affichage des GRP */
  ZCalquePointEditable cqOrthoPoints_;
  /** L'affichage des images source */
  PivImageRasterLayer cqOriginalImg_;
  /** L'affichage des couples de points pour l'ortho scaling */
  PivScalingPointsLayer cqScalingPoints_;
  /** L'affichage des zones de stabilisation */
  ZCalqueLigneBriseeEditable cqStabAreas_;
  
  PivOriginalImageModel mdlOriginalImage;
  PivOrthoPointsModel mdlOrthoPoints;
  PivScalingPointsModel mdlScalingPoints;
  PivStabAreasModel mdlStabAreas;

  private boolean enableEvents_=true;
  
  /**
   * Constructeur.
   * @param _pn Le panneau de calques.
   */
  public PivOriginalView(PivVisuPanel _pn) {
    pnLayers_=_pn;
    buildLayers();
    buildTools();
  }
  
  /**
   * D�finit le projet.
   * @param _prj Le projet.
   */
  public void setProject(PivProject _prj) {
    if (prj_==_prj) return;
    
    if (prj_!=null)
      prj_.removeListener(this);
    prj_=_prj;
    if (prj_!=null)
      prj_.addListener(this);
    
    buildLayers();
    majLayers();
    majTools();
  }
  
  /**
   * Construction des calques pour l'espace image source.
   */
  private void buildLayers() {
    layers_.clear();
    
    // Layer des ortho points
    cqOrthoPoints_=new ZCalquePointEditable(null, null);
    cqOrthoPoints_.setTitle(PivResource.getS("Points r�f�rence"));
    cqOrthoPoints_.setIconModel(0, new TraceIconModel(TraceIcon.PLUS, 4, Color.RED));
    cqOrthoPoints_.setName("cqOrthoPoints");
    cqOrthoPoints_.setDestructible(false);
    cqOrthoPoints_.setAttributForLabels(PivVisuPanel.ATT_LABEL);
    // Legerement transparent.
    cqOrthoPoints_.setLabelsBackgroundColor(PivUtils.TRANPARENT_WHITE_COLOR);
    cqOrthoPoints_.setLabelsVisible(true);
    cqOrthoPoints_.setEditor(pnLayers_.getEditor());
    getOrthoGRPAction().setCalqueOrthoPoints(cqOrthoPoints_);

    // Layer des couples de points pour l'ortho scaling
    cqScalingPoints_ = new PivScalingPointsLayer();
    cqScalingPoints_.setTitle(PivResource.getS("Couples de points"));
    cqScalingPoints_.setLineModel(0, new TraceLigneModel(TraceLigne.TIRETE, 1.5f, Color.BLACK));
    cqScalingPoints_.setLineModel(1, new TraceLigneModel(TraceLigne.TIRETE, 1.5f, Color.BLACK));
    cqScalingPoints_.setName("cqScalingPoints");
    cqScalingPoints_.setDestructible(false);
    cqScalingPoints_.setVisible(false);
    
    cqStabAreas_ = new ZCalqueLigneBriseeEditable();
    cqStabAreas_.setFormeEnable(new int[]{DeForme.POLYGONE});
    cqStabAreas_.setTitle(PivResource.getS("Zones de stabilisation"));
    cqStabAreas_.setLineModel(0, new TraceLigneModel(TraceLigne.POINTILLE, 1, Color.RED));
    cqStabAreas_.setSurfaceModel(0, new TraceSurfaceModel(TraceSurface.UNIFORME, new Color(1.f,0.f,0.f,0.5f), new Color(1.f,0.f,0.f,0.5f)));
    cqStabAreas_.setIconModel(0, new TraceIconModel(TraceIcon.PLUS, 1, Color.RED));
    cqStabAreas_.setName("cqStabArea");
    cqStabAreas_.setDestructible(false);
//    cqStabAreas_.setAttributForLabels(PivVisuPanel.ATT_LABEL);
    // On ne peut utiliser Z pour le traduire, ce sera traduit en Z (et non A comme attendu).
    cqStabAreas_.setLabelsPrefix(PivResource.getS("Zone").substring(0,1));
    cqStabAreas_.setLabelsVisible(true);
    // Labels legerement transparents.
    cqStabAreas_.setLabelsBackgroundColor(PivUtils.TRANPARENT_WHITE_COLOR);
    cqStabAreas_.setEditor(pnLayers_.getEditor());
    
    // Layer de l'image d'origine
    cqOriginalImg_ = new PivImageRasterLayer();
    cqOriginalImg_.setTitle(PivResource.getS("Image source"));
    cqOriginalImg_.setName("cqImg");
    
    layers_.add(cqOrthoPoints_);
    layers_.add(cqScalingPoints_);
    layers_.add(cqStabAreas_);
    layers_.add(cqOriginalImg_);
  }
  
  /**
   * Mise a jour des calques depuis le projet.
   */
  private void majLayers() {
    mdlOrthoPoints=new PivOrthoPointsModel(PivVisuPanel.MODE_ORIGINAL_VIEW);
    mdlOrthoPoints.setProjet(prj_);
    cqOrthoPoints_.setModele(mdlOrthoPoints);

    mdlOriginalImage=new PivOriginalImageModel();
    mdlOriginalImage.setProjet(prj_);
    mdlOriginalImage.showStabilizedImages(btShowStabImages_.isSelected());
    cqOriginalImg_.setModele(mdlOriginalImage);
    
    mdlScalingPoints=new PivScalingPointsModel();
    mdlScalingPoints.setProjet(prj_);
    cqScalingPoints_.modele(mdlScalingPoints);
    
    mdlStabAreas = new PivStabAreasModel();
    mdlStabAreas.setProjet(prj_);
    cqStabAreas_.modele(mdlStabAreas);
  }
  
  @Override
  public BCalqueAffichage[] getLayers() {
    return layers_.toArray(new BCalqueAffichage[0]);
  }

  /**
   * Rend visible le calque.
   * @param _b True : Le calque est visible.
   */
  public void setScalingPointsLayerVisible(boolean _b) {
    cqScalingPoints_.setVisible(_b);
  }

  /**
   * Rend visible le calque.
   * @param _b True : Le calque est visible.
   */
  public void setOrthoPointsLayerVisible(boolean _b) {
    cqOrthoPoints_.setVisible(_b);
  }
  
  /**
   * Construction des outils sp�cifiques � cette vue.
   */
  private void buildTools() {
    // La liste d�roulante des images
    cbImg_ = new JComboBox<>();
    cbImg_.addItemListener(new ItemListener() {
      
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange()==ItemEvent.SELECTED) {
          if (!enableEvents_) return;
          
          if (cqOriginalImg_!=null && cqOriginalImg_.getModelImage()!=null) {
            cqOriginalImg_.getModelImage().setSelectedImage(cbImg_.getSelectedIndex());
            // Pour que le calque soit r�affich�.
            cqOriginalImg_.repaint();
          }
        }
      }
    });
    cbImg_.setPreferredSize(new Dimension(200, cbImg_.getPreferredSize().height));
    cbImg_.setMaximumSize(cbImg_.getPreferredSize());
    cbImg_.setToolTipText(PivResource.getS("Affiche l'image s�lectionn�e"));
    cbImg_.setEnabled(false);
    cbImg_.setRenderer(new DefaultListCellRenderer() {

      @Override
      public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        String name = null;
        if (value != null && (name = value.toString()).startsWith(PivProject.BACKGROUND_IMAGE_NAME_PREFIX)) {
          name = name.substring(PivProject.BACKGROUND_IMAGE_NAME_PREFIX.length()) + " (" + PivResource.getS("Fond") + ")";
        }
        
        return super.getListCellRendererComponent(list, name, index, isSelected, cellHasFocus);
      }
    });
    
    btShowStabImages_ = new JToggleButton(PivResource.PIV.getIcon("stabilization.gif"));
    btShowStabImages_.setToolTipText(PivResource.getS("Affiche les images stabilis�es � la place des images sources"));
    btShowStabImages_.setBorder(BorderFactory.createEmptyBorder(5, 3, 5, 3));
    btShowStabImages_.setEnabled(false);
    btShowStabImages_.addActionListener((e) -> {
      majTools();
      mdlOriginalImage.showStabilizedImages(btShowStabImages_.isSelected());
      mdlOriginalImage.setSelectedImage(cbImg_.getSelectedIndex());
      pnLayers_.getVueCalque().repaint();
    });
  }
  
  
  
  /**
   * Mise a jour les outils.
   */
  private void majTools() {
    enableEvents_=false;
    
    // La liste d�roulante des images, avec s�lection d'une image diff�rente si inexistante.
    Object o=cbImg_.getSelectedItem();
    cbImg_.removeAllItems();
    cbImg_.setEnabled(false);

    if (prj_ != null) {
      if (prj_.getStabilizedImageFiles().length == 0) {
        btShowStabImages_.setSelected(false);
        btShowStabImages_.setEnabled(false);
      }
      else {
        btShowStabImages_.setEnabled(true);
      }
      
      File[] imgs;
      if (btShowStabImages_.isSelected()) {
        imgs = prj_.getStabilizedImageFiles();
      }
      else {
        imgs = prj_.getReadySrcImageFiles();
      }
      
      
      for (File f : imgs) {
        cbImg_.addItem(f.getName());
      }
      cbImg_.setEnabled(imgs.length > 0);
    }
    
    boolean found=false;
    if (o != null) {
      for (int i=0; i<cbImg_.getItemCount(); i++) {
        if (cbImg_.getItemAt(i).equals(o)) {
          cbImg_.setSelectedItem(o);
          found=true;
          break;
        }
      }
    }
    if (!found) {
      cbImg_.setSelectedItem(cbImg_.getItemCount()==0?-1:0);
    }
    
    enableEvents_=true;
  }
  
  /**
   * @param _index L'index de l'image a afficher. Si l'index est sup�rieur au nombre d'images,
   * on affiche pas d'images.
   */
  public void setSelectedImage(int _index) {
    cbImg_.setSelectedIndex(_index < cbImg_.getItemCount() ? _index:-1);
  }
  

  /**
   * Rend visible le calque des d�bits.
   * @param _b True : Le calque est visible.
   */
  public void setFlowAreaLayerVisible(boolean _b) {
    cqStabAreas_.setVisible(_b);
  }
  
  
  @Override
  public void projectStateChanged(PivProject _prj, String _prop) {

    if ("transfParams".equals(_prop)) {
      changeViewSystemCoords();
    }
    else if ("pgmImages".equals(_prop)) {
      majTools();
      mdlOriginalImage.showStabilizedImages(btShowStabImages_.isSelected());
      mdlOriginalImage.setSelectedImage(cbImg_.getSelectedIndex());
    }
    else if ("stabImages".equals(_prop)) {
      majTools();
      mdlOriginalImage.showStabilizedImages(btShowStabImages_.isSelected());
      mdlOriginalImage.setSelectedImage(cbImg_.getSelectedIndex());
    }
    else if("orthoPoints".equals(_prop)) {
      mdlOrthoPoints.update();
    }
    else if ("orthoParameters".equals(_prop)) {
      mdlScalingPoints.update();
    }
    else if ("stabilizationParams".equals(_prop)) {
      mdlStabAreas.update();
    }
    
    pnLayers_.getVueCalque().repaint();
  }

  @Override
  public String getTitle() {
    return TITLE;
  }

  @Override
  public JComponent[] getSpecificTools() {
    return new JComponent[]{cbImg_, btShowStabImages_};
  }

  @Override
  public EbliCoordinateDefinition[] getCoordinateDefinitions() {
    return new EbliCoordinateDefinition[]{DEF_COOR_I,DEF_COOR_J};
  }

  @Override
  public void restoreLayerProperties(Map<String, EbliUIProperties> _props) {
    for (BCalqueAffichage cq : layers_) {
      restoreLayerProperties(_props, cq);
    }
  }

  @Override
  public Map<String, EbliUIProperties> saveLayerProperties() {
    HashMap<String, EbliUIProperties> props=new HashMap<String, EbliUIProperties>();
    for (BCalqueAffichage cq : layers_) {
      saveLayerProperties(props, cq);
    }
    return props;
  }
  
  /**
   * Restore les propri�t�s graphiques recursivement.
   * @param _props Les propri�t�s
   * @param _cq Le calque a restaurer
   */
  private void restoreLayerProperties(Map<String, EbliUIProperties> _props, BCalque _cq) {
    if (_cq instanceof BGroupeCalque) {
      for (BCalque cq : ((BGroupeCalque)_cq).getCalques()) {
        restoreLayerProperties(_props, cq);
      }
    }
    else {
      _cq.initFrom(_props.get(_cq.getName()));
    }
  }
  
  /**
   * Sauve les propri�t�s graphiques recursivement.
   * @param _props Les propri�t�s
   * @param _cq Le calque a sauver
   */
  private void saveLayerProperties(Map<String, EbliUIProperties> _props, BCalque _cq) {
    if (_cq instanceof BGroupeCalque) {
      for (BCalque cq : ((BGroupeCalque)_cq).getCalques()) {
        saveLayerProperties(_props, cq);
      }
    }
    else {
      _props.put(_cq.getName(),_cq.saveUIProperties());
    }
  }
  
  public ZCalquePointEditable getOrthoPointsLayer() {
    return cqOrthoPoints_;
  }
  
  /**
   * Change le repere.
   */
  protected void changeViewSystemCoords() {
    majLayers();
  }

  @Override
  public EbliActionAbstract getActivationAction() {
    if (actOriginalView_==null) {
      actOriginalView_=new PivOriginalViewAction((PivImplementation)pnLayers_.getCtuluUI());
    }
    return actOriginalView_;
  }

  /**
   * Retourne l'action pour la saisie des points de r�f�rence.
   * @return L'action.
   */
  public PivOrthoGRPAction getOrthoGRPAction() {
    if (actOrthoGRP_==null) {
      actOrthoGRP_=new PivOrthoGRPAction(pnLayers_);
    }
    return actOrthoGRP_;
  }

  @Override
  public void nextImage() {
    int nextInd = cbImg_.getSelectedIndex() + 1;
    if (nextInd < cbImg_.getItemCount()) {
      cbImg_.setSelectedIndex(nextInd);
    }
  }

  @Override
  public void previousImage() {
    int previousInd = cbImg_.getSelectedIndex() -1;
    if (previousInd >= 0) {
      cbImg_.setSelectedIndex(previousInd);
    }
  }
}
