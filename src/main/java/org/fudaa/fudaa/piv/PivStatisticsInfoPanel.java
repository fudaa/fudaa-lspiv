package org.fudaa.fudaa.piv;

import java.awt.event.ItemEvent;

import javax.swing.table.AbstractTableModel;

import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivResultsI;
import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;
import org.fudaa.fudaa.piv.utils.PivMath;

import com.memoire.fu.FuLog;

/**
 * Un panneau de visualisation des statistiques
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivStatisticsInfoPanel extends CtuluDialogPanel {

  /**
   * Un modele bas� sur les r�sultats
   * @author marchand@deltacad.fr
   *
   */
  public static class ResultTableModel extends AbstractTableModel {
    private static String[] headers = new String[] {
        PivResource.getS("R�sultat"),
        PivResource.getS("Min"),
        PivResource.getS("Max"),
        PivResource.getS("Moyenne"),
        PivResource.getS("M�diane"),
        PivResource.getS("Ecart type")
    };
    
    // Les resultats, pour chaque temps.
    PivResultsI[] res;
    // L'indice du resultat
    int selInd;
    
    public void setResults(PivResultsI[] _res) {
      res = _res;
      fireTableDataChanged();
    }
    
    public void setSelectedResult(int _isel) {
      selInd = _isel;
      fireTableDataChanged();
    }
    
    @Override
    public int getRowCount() {
      return res == null ? 0 : res[0].getResults().length;
    }

    @Override
    public int getColumnCount() {
      return headers.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      // Le nom des r�sultats
      if (columnIndex == 0) {
        return res[0].getResults()[rowIndex].getName();
      }
      
      // Les valeurs de r�sultat
      else {
        return computeValue(rowIndex, columnIndex);
      }
    }

    @Override
    public String getColumnName(int columnIndex) {
      return headers[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      return columnIndex == 0 ? String.class : Double.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return false;
    }
    
    protected double computeValue(int _row, int _col) {
      if (res == null) 
        return 0;
      
      // Le type du resultat.
      ResultType resType = res[0].getResults()[_row];
          
      double[] vals;
      
      // Tous les r�sultats sont concern�s
      if (selInd == -1) {
        vals = new double[0];
        for (int i = 0; i<res.length; i++) {
          double[] resVals = res[i].getValues(resType);
          double[] fullArray = new double[vals.length + resVals.length];
          
          System.arraycopy(vals, 0, fullArray, 0, vals.length);
          System.arraycopy(resVals, 0, fullArray, vals.length, resVals.length);
          vals = fullArray;
        }
      }
      
      // On utilise que le resultat selectionn�
      else {
        vals = res[selInd].getValues(resType);
      }
      
      switch (_col) {
      // Min
      case 1:
        return PivMath.min(vals);
      // Max
      case 2:
        return PivMath.max(vals);
      // Moyenne
      case 3:
        return PivMath.average(vals);
      // Mediane
      case 4:
        return PivMath.median(vals);
      // Ecart type
      case 5:
        return PivMath.standardDeviation(vals);
      default:
        return Double.NaN;
      }
    }
  }

  PivImplementation impl_;
  ResultTableModel rawModel = new ResultTableModel();
  ResultTableModel filteredModel = new ResultTableModel();

  /**
   * Constructeur.
   */
  public PivStatisticsInfoPanel(PivImplementation _impl) {
    impl_ = _impl;
    initComponents();
    customize();
  }

  private void customize() {
    rbAllResults.setText(PivResource.getS("Pour tous les r�sultats"));
    rbSelectedResult.setText(PivResource.getS("Pour le r�sultat"));
    pnRawResults.setBorder(javax.swing.BorderFactory.createTitledBorder(PivResource.getS("R�sultats bruts")));
    pnFilteredResults.setBorder(javax.swing.BorderFactory.createTitledBorder(PivResource.getS("R�sultats filtr�s")));

    PivProject prj = impl_.getCurrentProject();

    rbSelectedResult.addItemListener(evt -> {
      coSelectedResult.setEnabled(evt.getStateChange() == ItemEvent.SELECTED);
      updateResults(coSelectedResult.isEnabled() ? coSelectedResult.getSelectedIndex() : -1);
    });

    // Liste des r�sultats.
    coSelectedResult.removeAllItems();
    for (int i = 0; i < prj.getInstantRawResults().length; i++) {
      coSelectedResult.addItem(PivResource.getS("R�sultat") + " : " + (i + 1));
    }
    coSelectedResult.addItemListener(evt -> {
      if (evt.getStateChange() == ItemEvent.SELECTED) {
        updateResults(coSelectedResult.getSelectedIndex());
      }
    });

    rbSelectedResult.doClick();
    rbAllResults.doClick();
    
    rawModel.setResults(prj.getInstantRawResults());
    filteredModel.setResults(prj.getInstantFilteredResults());

    tbRowResults.setModel(rawModel);
    tbFilteredResults.setModel(filteredModel);
//    tbFilteredResults.setFillsViewportHeight(true);
    
    setFilteredPanelEnabled(prj.getInstantFilteredResults() != null);
  }
  
  /**
   * Active/desactive le panneau des r�sultats filtr�s
   * @param _b True : Panneau actif.
   */
  protected void setFilteredPanelEnabled(boolean _b) {
    tbFilteredResults.setEnabled(_b);
    if (_b)
      spFilteredResults.setToolTipText(null);
    else
      spFilteredResults.setToolTipText(PivResource.getS("Aucun r�sultat filtr�"));

    pnFilteredResults.setEnabled(_b);
  }

  /**
   * Met a jour les resultats en fonction de l'indice de r�sultat selectionn�.
   * 
   * @param _ind L'indice du r�sultat selectionn�. -1 : Moyenne a faire sur tous
   *             les r�sultats
   */
  private void updateResults(int _ind) {
    FuLog.trace("updateResults");
    
    rawModel.setSelectedResult(_ind);
    filteredModel.setSelectedResult(_ind);
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgContext = new javax.swing.ButtonGroup();
        pnRawResults = new javax.swing.JPanel();
        spRawResults = new javax.swing.JScrollPane();
        tbRowResults = new javax.swing.JTable();
        rbAllResults = new javax.swing.JRadioButton();
        rbSelectedResult = new javax.swing.JRadioButton();
        coSelectedResult = new javax.swing.JComboBox<>();
        pnFilteredResults = new javax.swing.JPanel();
        spFilteredResults = new javax.swing.JScrollPane();
        tbFilteredResults = new javax.swing.JTable();

        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        pnRawResults.setBorder(javax.swing.BorderFactory.createTitledBorder("R�sultats bruts"));

        tbRowResults.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"VX", null, null, null, null, null},
                {"VY", null, null, null, null, null},
                {"NORME", null, null, null, null, null},
                {"CORREL", null, null, null, null, null},
                {"OMEGA", null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "R�sultat", "Min", "Max", "Moy.", "M�diane", "Ecart type"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        spRawResults.setViewportView(tbRowResults);

        javax.swing.GroupLayout pnRawResultsLayout = new javax.swing.GroupLayout(pnRawResults);
        pnRawResults.setLayout(pnRawResultsLayout);
        pnRawResultsLayout.setHorizontalGroup(
            pnRawResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spRawResults, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        pnRawResultsLayout.setVerticalGroup(
            pnRawResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spRawResults, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
        );

        bgContext.add(rbAllResults);
        rbAllResults.setSelected(true);
        rbAllResults.setText("Pour tous les r�sultats");

        bgContext.add(rbSelectedResult);
        rbSelectedResult.setText("Pour le r�sultat");

        coSelectedResult.setMaximumRowCount(2);
        coSelectedResult.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "R�sultat : 1" }));
        coSelectedResult.setEnabled(false);

        pnFilteredResults.setBorder(javax.swing.BorderFactory.createTitledBorder("R�sultats filtr�s"));

        tbFilteredResults.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"VX", null, null, null, null, null},
                {"VY", null, null, null, null, null},
                {"NORME", null, null, null, null, null},
                {"CORREL", null, null, null, null, null},
                {"OMEGA", null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "R�sultat", "Min", "Max", "Moy.", "M�diane", "Ecart type"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        spFilteredResults.setViewportView(tbFilteredResults);

        javax.swing.GroupLayout pnFilteredResultsLayout = new javax.swing.GroupLayout(pnFilteredResults);
        pnFilteredResults.setLayout(pnFilteredResultsLayout);
        pnFilteredResultsLayout.setHorizontalGroup(
            pnFilteredResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spFilteredResults, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        pnFilteredResultsLayout.setVerticalGroup(
            pnFilteredResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spFilteredResults, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnRawResults, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(rbAllResults)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(rbSelectedResult)
                .addGap(18, 18, 18)
                .addComponent(coSelectedResult, 0, 287, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(pnFilteredResults, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(rbAllResults)
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rbSelectedResult)
                    .addComponent(coSelectedResult, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnRawResults, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnFilteredResults, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgContext;
    private javax.swing.JComboBox<String> coSelectedResult;
    private javax.swing.JPanel pnFilteredResults;
    private javax.swing.JPanel pnRawResults;
    private javax.swing.JRadioButton rbAllResults;
    private javax.swing.JRadioButton rbSelectedResult;
    private javax.swing.JScrollPane spFilteredResults;
    private javax.swing.JScrollPane spRawResults;
    private javax.swing.JTable tbFilteredResults;
    private javax.swing.JTable tbRowResults;
    // End of variables declaration//GEN-END:variables
}
