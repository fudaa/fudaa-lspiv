package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;

import javax.swing.JTabbedPane;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;

import com.memoire.fu.FuLog;

/**
 * Un panneau de saisie des param�tres pour la mise a l'echelle des images.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivScalingParamPanel extends CtuluDialogPanel {
  PivImplementation impl_;
  private PivScalingResolutionSubPanel pnResol_;
  private PivScalingTransformationSubPanel pnTransf_;
  private PivScalingLimitsSubPanel pnOthers_;
  // Les param�tres, restitu�s si le bouton cancel est press�.
  private PivOrthoParameters params_;
  // Les param�tres, stock�s a chaque apply, pour etre sur qu'en cas de modif, la modification est appliqu�e.
  private PivOrthoParameters lastParams_;
  
  boolean eventEnabled_ = true;

  /**
   * Constructeur.
   */
  public PivScalingParamPanel(PivImplementation _impl) {
    impl_ = _impl;
    try {
      eventEnabled_ = false;
      customize();

      params_ = new PivOrthoParameters(impl_.getCurrentProject().getOrthoParameters());
      lastParams_ = new PivOrthoParameters(params_);
      setParams(lastParams_);
    }
    finally {
      eventEnabled_ = true;
    }
  }

  private void customize() {
    pnResol_ = new PivScalingResolutionSubPanel(impl_.get2dFrame().getVisuPanel(), this);
    pnTransf_ = new PivScalingTransformationSubPanel(impl_.get2dFrame().getVisuPanel(), impl_.getCurrentProject(), this);
    pnOthers_ = new PivScalingLimitsSubPanel(impl_, this);

    final JTabbedPane tbParams=new JTabbedPane();
    tbParams.addTab(PivResource.getS("R�solution"), pnResol_);
    tbParams.addTab(PivResource.getS("Transformation"), pnTransf_);
    tbParams.addTab(PivResource.getS("Autres"), pnOthers_);
    tbParams.addChangeListener((evt) -> {
      if (tbParams.getSelectedIndex()==2) {
        pnOthers_.updatePanel();
      }
      if (tbParams.getSelectedIndex() == 0) {
        pnResol_.updatePanel();
      }
    });
    
    setLayout(new BorderLayout());
    add(tbParams, BorderLayout.CENTER);
    
    String helpText=PivResource.getS("<b>R�solution</b><p>Vous pouvez d�finir la r�solution soit directement, soit en saisissant des couples de points et moyennation.<p><u>Saisie par couple de points</u> :<br>Saisissez 2 points � l'�cran, puis renseignez les coordonn�es ou la distance dans la table (suivant l'option choisie)<p><p>");
    helpText+=PivResource.getS("<b>Transformation</b><p>Pour d�finir une transformation, saisissez 2 points � l'�cran, puis renseignez les coordonn�es dans la table");

    setHelpText("<html>"+helpText+"</html>");
  }

  /**
   * Rempli le panneau depuis les donn�es du projet.
   * @param _params L'objet m�tier pour l'orthorectification.
   */
  public void setParams(PivOrthoParameters _params) {
    pnResol_.setParams(_params);
    pnTransf_.setParams(_params);
    pnOthers_.setParams(_params);
  }

  /**
   * @return Le panneau de la resolution.
   */
  public PivScalingResolutionSubPanel getResolutionPanel() {
    return pnResol_;
  }
  
  /**
   * @return Le panneau de la transformation
   */
  public PivScalingTransformationSubPanel getTransformationPanel() {
    return pnTransf_;
  }
  
  public PivScalingLimitsSubPanel getLimitsPanel() {
    return pnOthers_;
  }
  
  /**
   * Met a jour les parametres d'ortho rectification donn�s.
   * @param _params Les parametres d'ortho rectification.
   */
  public void retrieveParams(PivOrthoParameters _params) {
    pnResol_.retrieveParams(_params);
    pnTransf_.retrieveParams(_params);
    pnOthers_.retrieveParams(_params);
  }

  @Override
  public boolean isDataValid() {
    boolean bok=pnResol_.isDataValid(true);
    bok=bok && pnTransf_.isDataValid(true);
    bok=bok && pnOthers_.isDataValid(true);
    
    return bok;
  }

  /**
   * Notifie que les param�tres ont chang� (les param�tres peuvent �tre d�grad�s, c'est a dire pas completement remplis)
   */
  public void fireParametersChanged() {
    if (!eventEnabled_)
      return;
    
    PivOrthoParameters params = new PivOrthoParameters();
    retrieveParams(params);
    impl_.getCurrentProject().setOrthoParameters(params);
  }
  
  /**
   * Surcharg� pour pouvoir pr�visualiser l'image transform�e sans sortir du 
   * dialogue.
   * @return 
   */
  @Override
  public boolean apply() {
    PivOrthoParameters params=new PivOrthoParameters();
    retrieveParams(params);

    if (params.equals(lastParams_))
      return true;
    
    lastParams_ = params;
    impl_.getCurrentProject().setOrthoParameters(lastParams_);

    CtuluLog ana=new CtuluLog();
    ana.setDesc(PivResource.getS("Pr�visualisation des images transform�es"));
    PivExeLauncher.instance().computeScalingImages(ana, impl_.getCurrentProject(),true, null);
    if (ana.containsErrorOrSevereError()) {
      impl_.error(ana.getResume());
      return true;
    }
    
    impl_.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_TRANSF_VIEW);
    impl_.get2dFrame().getVisuPanel().restaurer();
    
    return true;
  }

  @Override
  public boolean cancel() {
    // Les param�tres originaux sont restitu�s.
    impl_.getCurrentProject().setOrthoParameters(params_);
    return true;
  }
}
