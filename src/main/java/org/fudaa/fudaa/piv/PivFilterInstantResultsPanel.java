package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.metier.PivComputeParameters;
import org.fudaa.fudaa.piv.metier.PivFilterParameters;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuPanel;


/**
 * Un panneau pour filtrer les r�sultats instantan�s utilis�s pour faire le calcul de moyenne.
 * Les resultats filtr�s et selectionn�s sont sauv�s dans le projet.
 *  
 * @author marchand@deltacad.fr
 * @version $Id$
 */
public class PivFilterInstantResultsPanel extends CtuluDialogPanel {

  PivImplementation impl_;
  CtuluDialog diProgress_;
  
  private JTextField tfNormalMinLimit_;
  private JTextField tfNormalMaxLimit_;
  private JTextField tfVxMinLimit_;
  private JTextField tfVxMaxLimit_;
  private JTextField tfVyMinLimit_;
  private JTextField tfVyMaxLimit_;
  private JTextField tfCorrelMinLimit_;
  private JTextField tfCorrelMaxLimit_;
  private JTextField tfMedianTestEpsilon_;
  private JTextField tfMedianTestR0min_;
  private JTextField tfCorrelRcut_;
  private JTextField tfCorrelRh0max_;
  private JTextField tfVelocityDistribNstdvel_;
  private JTextField tfVelocityDispersionCovmax_;
  private JTextField tfAngularDispersionCircvarmax_;
  
  private JCheckBox cbVelFlt_;
  private JCheckBox cbCorrelFlt_;
  private JCheckBox cbCorrelPeakFlt_;
  private JCheckBox cbVelocityDistribFlt_;
  private JCheckBox cbVelocityDispersionFlt_;
  private JCheckBox cbAngularDispersionFlt_;
  private JCheckBox cbMedianTestFlt_;
  private JCheckBox cbMedianTestAdvanced_;
  private JTextField tfMedianTestNneighbor_;
  private JTextField tfMedianTestDistmax_;
  
  private JTabbedPane tpFilters_;
  private ButtonTabComponent pnVelTitle_;
  private ButtonTabComponent pnCorrelTitle_;
  private ButtonTabComponent pnCorrelPeakTitle_;
  private ButtonTabComponent pnVelocityDistribTitle_;
  private ButtonTabComponent pnVelocityDispersionTitle_;
  private ButtonTabComponent pnAngularDispersionTitle_;
  private ButtonTabComponent pnMedianTestTitle_;

  /**
   * Un composant onglet avec une case � cocher.
   */
  class ButtonTabComponent extends JPanel {
    
    // Important
    // Aucun tooltip ne doit �tre ajout� � ce composant : 
    // Un tooltip met en place un mouse listener, et si un mouselistener est ajout� sur un composant Java, alors l'evenement n'est pas
    // transmis sur le composant plac� en dessous (le JTabbedPane). Si tel est le cas, le click sur l'onglet pour changer de panneau est
    // alors sans effet.
    
    JCheckBox cb = new JCheckBox();
    JLabel lb = new JLabel();
    
    public ButtonTabComponent(JTabbedPane pane, String title, Icon _icon) {
      this.setOpaque(false);
      this.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));

      cb.setOpaque(false);
      cb.addItemListener((evt) -> {
        cb.setToolTipText(cb.isSelected() ? PivResource.getS("Filtre actif") : PivResource.getS("Filtre inactif"));
      });
      cb.setSelected(true);
      cb.setSelected(false);
      
      lb.setText(title);
      lb.setIcon(_icon);
      lb.setOpaque(false);
      
      add(cb);
      add(lb);
    }
    
    public void addItemListener(ItemListener l) {
      cb.addItemListener(l);
    }
    
    public void setSelected(boolean b) {
      cb.setSelected(b);
    }
    
    public boolean isSelected() {
      return cb.isSelected();
    }
  }
    
  
  /**
   * Create the panel.
   */
  public PivFilterInstantResultsPanel(PivImplementation _impl) {
    impl_ = _impl;
    
    initComponents();
    customize();
    
  }
  
  protected void initComponents() {
    
    // Wrap, sinon, la fenetre est trop large.
    String text = PivUtils.wrapMessage("<html>"+
        PivResource.getS("<u>Vitesses</u><br>Les seuils de vitesse minimale et maximale, en amplitude et en composantes Vx et Vy, sont � manier avec pr�caution de fa�on � ne filtrer que les vitesses aberrantes. L'effet de ce filtre en post-traitement est le m�me que le dimensionnement de l'aire de recherche (SA), qui a l'avantage suppl�mentaire de limiter la dur�e des calculs.<p><p>")+
        PivResource.getS("<u>Corr�lation</u><br>La corr�lation est un important indicateur de la qualit� des r�sultats de vitesse. Le seuil de corr�lation minimale pourra �tre de l'ordre de 0.7 pour des traceurs ind�formables, et de l'ordre de 0.4 pour des traceurs se d�formant (figures de turbulence, �cume). Le seuil de corr�lation maximale (0.98, typiquement) permet d'�carter les vitesses correspondant � des motifs statiques (berge, bord de l'image) ou presque (reflets/ombres, vagues stationnaires).")+
        "</html>", 132);
    setHelpText(text);

    setLayout(new BorderLayout(0, 0));
    
    tpFilters_ = new JTabbedPane();
    tpFilters_.setTabPlacement(JTabbedPane.LEFT);
    
    // Filtre pour la vitesse
    
    JPanel pnVelocity = new JPanel();
    pnVelocity.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
    
    cbVelFlt_ = new JCheckBox(PivResource.getS("Filtrer les vitesses"));
    cbVelFlt_.addItemListener((evt) -> setVelPanelActive(cbVelFlt_.isSelected()));
    
    // Filtre median Test
    
    JPanel pnMedianTest = new JPanel();
    pnMedianTest.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
    
    cbMedianTestFlt_ = new JCheckBox(PivResource.getS("Filtrer par test m�dian"));
    cbMedianTestFlt_.addItemListener((evt) -> setMedianTestPanelActive(cbMedianTestFlt_.isSelected()));
    
    JPanel pnMedianTestParameters = new JPanel();
    pnMedianTestParameters.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Param�tres")));
    JLabel lbMedianTestR0min = new JLabel(PivResource.getS("Ecart max. � la m�diane normalis�e") + " (pix):");
    JLabel lbMedianTestEpsilon = new JLabel(PivResource.getS("Tol�rance fluctuations locales") + " (pix):");
    tfMedianTestEpsilon_ = new JTextField();
    tfMedianTestEpsilon_.setEnabled(false);
    tfMedianTestR0min_ = new JTextField();
    tfMedianTestR0min_.setEnabled(false);
    
    cbMedianTestAdvanced_ = new JCheckBox(PivResource.getS("Param�tres avanc�s"));
    cbMedianTestAdvanced_.setEnabled(false);
    cbMedianTestAdvanced_.addItemListener((evt) -> {
      tfMedianTestNneighbor_.setEnabled(cbMedianTestAdvanced_.isSelected());
      tfMedianTestDistmax_.setEnabled(cbMedianTestAdvanced_.isSelected());
    });
    
    JLabel lbMedianTestNneighbor = new JLabel(PivResource.getS("n_neighbor:"));
    JLabel lbMedianTestDistmax = new JLabel(PivResource.getS("dist_max:"));
    tfMedianTestNneighbor_ = new JTextField();
    tfMedianTestNneighbor_.setEnabled(false);
    tfMedianTestDistmax_ = new JTextField();
    tfMedianTestDistmax_.setEnabled(false);
    
    GroupLayout gl_pnMedianTestParameters = new GroupLayout(pnMedianTestParameters);
    gl_pnMedianTestParameters.setHorizontalGroup(
      gl_pnMedianTestParameters.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnMedianTestParameters.createSequentialGroup()
          .addGroup(gl_pnMedianTestParameters.createParallelGroup(Alignment.LEADING)
            .addGroup(gl_pnMedianTestParameters.createSequentialGroup()
              .addContainerGap()
              .addGroup(gl_pnMedianTestParameters.createParallelGroup(Alignment.LEADING)
                .addComponent(lbMedianTestDistmax)
                .addComponent(lbMedianTestNneighbor)
                .addComponent(lbMedianTestR0min)
                .addComponent(lbMedianTestEpsilon))
              .addPreferredGap(ComponentPlacement.RELATED, 10, GroupLayout.PREFERRED_SIZE)
              .addGroup(gl_pnMedianTestParameters.createParallelGroup(Alignment.LEADING)
                .addComponent(tfMedianTestNneighbor_, GroupLayout.DEFAULT_SIZE, 465, Short.MAX_VALUE)
                .addComponent(tfMedianTestR0min_, GroupLayout.DEFAULT_SIZE, 475, Short.MAX_VALUE)
                .addComponent(tfMedianTestEpsilon_, GroupLayout.DEFAULT_SIZE, 475, Short.MAX_VALUE)
                .addComponent(tfMedianTestDistmax_, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)))
            .addComponent(cbMedianTestAdvanced_))
          .addContainerGap())
    );
    gl_pnMedianTestParameters.setVerticalGroup(
      gl_pnMedianTestParameters.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnMedianTestParameters.createSequentialGroup()
          .addGroup(gl_pnMedianTestParameters.createParallelGroup(Alignment.BASELINE)
            .addComponent(tfMedianTestEpsilon_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addComponent(lbMedianTestEpsilon))
          .addPreferredGap(ComponentPlacement.RELATED)
          .addGroup(gl_pnMedianTestParameters.createParallelGroup(Alignment.BASELINE)
            .addComponent(lbMedianTestR0min)
            .addComponent(tfMedianTestR0min_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addComponent(cbMedianTestAdvanced_)
          .addPreferredGap(ComponentPlacement.RELATED, 2, Short.MAX_VALUE)
          .addGroup(gl_pnMedianTestParameters.createParallelGroup(Alignment.LEADING)
            .addGroup(gl_pnMedianTestParameters.createSequentialGroup()
              .addGap(5)
              .addComponent(lbMedianTestNneighbor))
            .addGroup(gl_pnMedianTestParameters.createSequentialGroup()
              .addPreferredGap(ComponentPlacement.RELATED)
              .addComponent(tfMedianTestNneighbor_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          .addGap(6)
          .addGroup(gl_pnMedianTestParameters.createParallelGroup(Alignment.BASELINE)
            .addComponent(tfMedianTestDistmax_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addComponent(lbMedianTestDistmax))
          .addGap(49))
    );
    pnMedianTestParameters.setLayout(gl_pnMedianTestParameters);
    
        GroupLayout gl_pnMedianTest = new GroupLayout(pnMedianTest);
        gl_pnMedianTest.setHorizontalGroup(
          gl_pnMedianTest.createParallelGroup(Alignment.TRAILING)
            .addGroup(gl_pnMedianTest.createSequentialGroup()
              .addContainerGap()
              .addGroup(gl_pnMedianTest.createParallelGroup(Alignment.LEADING)
                .addComponent(cbMedianTestFlt_)
                .addComponent(pnMedianTestParameters, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 566, Short.MAX_VALUE))
              .addContainerGap())
        );
        gl_pnMedianTest.setVerticalGroup(
          gl_pnMedianTest.createParallelGroup(Alignment.LEADING)
            .addGroup(gl_pnMedianTest.createSequentialGroup()
              .addComponent(cbMedianTestFlt_)
              .addPreferredGap(ComponentPlacement.RELATED)
              .addComponent(pnMedianTestParameters, GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(233, Short.MAX_VALUE))
        );
        pnMedianTest.setLayout(gl_pnMedianTest);

    tpFilters_.addTab(PivResource.getS("Test m�dian"), pnMedianTest);
    
    JPanel pnNormalLimits = new JPanel();
    pnNormalLimits.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Limites de la norme de vitesse (m/s)")));
    JLabel lbNormalMinLimit = new JLabel(PivResource.getS("Min:"));
    JLabel lbNormalMaxLimit = new JLabel(PivResource.getS("Max:"));
    tfNormalMinLimit_ = new JTextField();
    tfNormalMinLimit_.setEnabled(false);
    tfNormalMaxLimit_ = new JTextField();
    tfNormalMaxLimit_.setEnabled(false);
    
    JPanel pnVxLimits = new JPanel();
    pnVxLimits.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Limites de la composante Vx (m/s)")));
    JLabel lbVxMinLimit = new JLabel(PivResource.getS("Min:"));
    JLabel lbVxMaxLimit = new JLabel(PivResource.getS("Max:"));
    tfVxMinLimit_ = new JTextField();
    tfVxMinLimit_.setEnabled(false);
    tfVxMaxLimit_ = new JTextField();
    tfVxMaxLimit_.setEnabled(false);
    
    JPanel pnVyLimits = new JPanel();
    pnVyLimits.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Limites de la composante Vy (m/s)")));
    JLabel lbVyMinLimit = new JLabel(PivResource.getS("Min:"));
    JLabel lbVyMaxLimit = new JLabel(PivResource.getS("Max:"));
    tfVyMinLimit_ = new JTextField();
    tfVyMinLimit_.setEnabled(false);
    tfVyMaxLimit_ = new JTextField();
    tfVyMaxLimit_.setEnabled(false);
    
    GroupLayout gl_pnVxLimits = new GroupLayout(pnVxLimits);
    gl_pnVxLimits.setHorizontalGroup(
      gl_pnVxLimits.createParallelGroup(Alignment.LEADING)
        .addGap(0, 291, Short.MAX_VALUE)
        .addGroup(gl_pnVxLimits.createSequentialGroup()
          .addGroup(gl_pnVxLimits.createParallelGroup(Alignment.LEADING)
            .addComponent(lbVxMaxLimit)
            .addComponent(lbVxMinLimit))
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addGroup(gl_pnVxLimits.createParallelGroup(Alignment.LEADING)
            .addComponent(tfVxMinLimit_, GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
            .addComponent(tfVxMaxLimit_, GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE))
          .addContainerGap())
    );
    gl_pnVxLimits.setVerticalGroup(
      gl_pnVxLimits.createParallelGroup(Alignment.LEADING)
        .addGap(0, 72, Short.MAX_VALUE)
        .addGroup(gl_pnVxLimits.createSequentialGroup()
          .addGroup(gl_pnVxLimits.createParallelGroup(Alignment.BASELINE)
            .addComponent(lbVxMinLimit)
            .addComponent(tfVxMinLimit_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addPreferredGap(ComponentPlacement.RELATED)
          .addGroup(gl_pnVxLimits.createParallelGroup(Alignment.BASELINE)
            .addComponent(lbVxMaxLimit)
            .addComponent(tfVxMaxLimit_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addContainerGap(13, Short.MAX_VALUE))
    );
    pnVxLimits.setLayout(gl_pnVxLimits);
    
    GroupLayout gl_pnVyLimits = new GroupLayout(pnVyLimits);
    gl_pnVyLimits.setHorizontalGroup(
      gl_pnVyLimits.createParallelGroup(Alignment.LEADING)
        .addGap(0, 311, Short.MAX_VALUE)
        .addGap(0, 291, Short.MAX_VALUE)
        .addGroup(gl_pnVyLimits.createSequentialGroup()
          .addGroup(gl_pnVyLimits.createParallelGroup(Alignment.LEADING)
            .addComponent(lbVyMaxLimit)
            .addComponent(lbVyMinLimit))
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addGroup(gl_pnVyLimits.createParallelGroup(Alignment.LEADING)
            .addComponent(tfVyMinLimit_, GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
            .addComponent(tfVyMaxLimit_, GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE))
          .addContainerGap())
    );
    gl_pnVyLimits.setVerticalGroup(
      gl_pnVyLimits.createParallelGroup(Alignment.LEADING)
        .addGap(0, 72, Short.MAX_VALUE)
        .addGap(0, 72, Short.MAX_VALUE)
        .addGroup(gl_pnVyLimits.createSequentialGroup()
          .addGroup(gl_pnVyLimits.createParallelGroup(Alignment.BASELINE)
            .addComponent(lbVyMinLimit)
            .addComponent(tfVyMinLimit_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addPreferredGap(ComponentPlacement.RELATED)
          .addGroup(gl_pnVyLimits.createParallelGroup(Alignment.BASELINE)
            .addComponent(lbVyMaxLimit)
            .addComponent(tfVyMaxLimit_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addContainerGap(13, Short.MAX_VALUE))
    );
    pnVyLimits.setLayout(gl_pnVyLimits);
    GroupLayout gl_pnVelocity = new GroupLayout(pnVelocity);
    gl_pnVelocity.setHorizontalGroup(
      gl_pnVelocity.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnVelocity.createSequentialGroup()
          .addGroup(gl_pnVelocity.createParallelGroup(Alignment.LEADING)
            .addGroup(gl_pnVelocity.createSequentialGroup()
              .addGap(10)
              .addGroup(gl_pnVelocity.createParallelGroup(Alignment.LEADING)
                .addComponent(pnNormalLimits, GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
                .addGroup(gl_pnVelocity.createSequentialGroup()
                  .addComponent(cbVelFlt_)
                  .addPreferredGap(ComponentPlacement.RELATED, 257, Short.MAX_VALUE))))
            .addGroup(gl_pnVelocity.createSequentialGroup()
              .addContainerGap()
              .addComponent(pnVxLimits, GroupLayout.DEFAULT_SIZE, 498, Short.MAX_VALUE))
            .addGroup(gl_pnVelocity.createSequentialGroup()
              .addContainerGap()
              .addComponent(pnVyLimits, GroupLayout.DEFAULT_SIZE, 498, Short.MAX_VALUE)))
          .addContainerGap())
    );
    gl_pnVelocity.setVerticalGroup(
      gl_pnVelocity.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnVelocity.createSequentialGroup()
          .addComponent(cbVelFlt_)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(pnNormalLimits, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(pnVxLimits, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(pnVyLimits, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
          .addContainerGap(176, Short.MAX_VALUE))
    );
    
    GroupLayout gl_pnNormalLimits = new GroupLayout(pnNormalLimits);
    gl_pnNormalLimits.setHorizontalGroup(
      gl_pnNormalLimits.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnNormalLimits.createSequentialGroup()
          .addGroup(gl_pnNormalLimits.createParallelGroup(Alignment.LEADING)
            .addComponent(lbNormalMaxLimit)
            .addComponent(lbNormalMinLimit))
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addGroup(gl_pnNormalLimits.createParallelGroup(Alignment.LEADING)
            .addComponent(tfNormalMinLimit_, GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
            .addComponent(tfNormalMaxLimit_, GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE))
          .addContainerGap())
    );
    gl_pnNormalLimits.setVerticalGroup(
      gl_pnNormalLimits.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnNormalLimits.createSequentialGroup()
          .addGroup(gl_pnNormalLimits.createParallelGroup(Alignment.BASELINE)
            .addComponent(lbNormalMinLimit)
            .addComponent(tfNormalMinLimit_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addPreferredGap(ComponentPlacement.RELATED)
          .addGroup(gl_pnNormalLimits.createParallelGroup(Alignment.BASELINE)
            .addComponent(lbNormalMaxLimit)
            .addComponent(tfNormalMaxLimit_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addContainerGap(13, Short.MAX_VALUE))
    );
    pnNormalLimits.setLayout(gl_pnNormalLimits);
    pnVelocity.setLayout(gl_pnVelocity);
    
    tpFilters_.addTab(PivResource.getS("Vitesse"), pnVelocity);

    // Filtre pour la correlation
    
    JPanel pnCorrel = new JPanel();
    pnCorrel.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
    
    cbCorrelFlt_ = new JCheckBox(PivResource.getS("Filtrer les corr�lations"));
    cbCorrelFlt_.addItemListener((evt) -> setCorrelPanelActive(cbCorrelFlt_.isSelected()));
    
    JPanel pnCorrelLimits = new JPanel();
    pnCorrelLimits.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Corr�lation")));
    JLabel lbCorrelMinLimit=new JLabel(PivResource.getS("Min:"));
    JLabel lbCorrelMaxLimit=new JLabel(PivResource.getS("Max:"));
    tfCorrelMinLimit_ = new JTextField();
    tfCorrelMinLimit_.setEnabled(false);
    tfCorrelMaxLimit_ = new JTextField();
    tfCorrelMaxLimit_.setEnabled(false);
    
    GroupLayout gl_pnCorrelLimits = new GroupLayout(pnCorrelLimits);
    gl_pnCorrelLimits.setHorizontalGroup(
      gl_pnCorrelLimits.createParallelGroup(Alignment.LEADING)
        .addGap(0, 227, Short.MAX_VALUE)
        .addGroup(gl_pnCorrelLimits.createSequentialGroup()
          .addGroup(gl_pnCorrelLimits.createParallelGroup(Alignment.LEADING)
            .addComponent(lbCorrelMaxLimit)
            .addComponent(lbCorrelMinLimit))
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addGroup(gl_pnCorrelLimits.createParallelGroup(Alignment.LEADING)
            .addComponent(tfCorrelMinLimit_, GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
            .addComponent(tfCorrelMaxLimit_, GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE))
          .addContainerGap())
    );
    gl_pnCorrelLimits.setVerticalGroup(
      gl_pnCorrelLimits.createParallelGroup(Alignment.LEADING)
        .addGap(0, 72, Short.MAX_VALUE)
        .addGroup(gl_pnCorrelLimits.createSequentialGroup()
          .addGroup(gl_pnCorrelLimits.createParallelGroup(Alignment.BASELINE)
            .addComponent(lbCorrelMinLimit)
            .addComponent(tfCorrelMinLimit_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addPreferredGap(ComponentPlacement.RELATED)
          .addGroup(gl_pnCorrelLimits.createParallelGroup(Alignment.BASELINE)
            .addComponent(lbCorrelMaxLimit)
            .addComponent(tfCorrelMaxLimit_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addContainerGap(13, Short.MAX_VALUE))
    );
    pnCorrelLimits.setLayout(gl_pnCorrelLimits);
    GroupLayout gl_pnCorrel = new GroupLayout(pnCorrel);
    gl_pnCorrel.setHorizontalGroup(
      gl_pnCorrel.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnCorrel.createSequentialGroup()
          .addGap(10)
          .addGroup(gl_pnCorrel.createParallelGroup(Alignment.LEADING)
            .addComponent(pnCorrelLimits, GroupLayout.DEFAULT_SIZE, 566, Short.MAX_VALUE)
            .addComponent(cbCorrelFlt_))
          .addContainerGap())
    );
    gl_pnCorrel.setVerticalGroup(
      gl_pnCorrel.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnCorrel.createSequentialGroup()
          .addComponent(cbCorrelFlt_)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(pnCorrelLimits, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
          .addContainerGap(329, Short.MAX_VALUE))
    );
    pnCorrel.setLayout(gl_pnCorrel);
    tpFilters_.addTab(PivResource.getS("Corr�lation"), pnCorrel);
    
    // Filtre pour le pic de correlation
    
    JPanel pnCorrelPeak = new JPanel();
    pnCorrelPeak.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
    
    cbCorrelPeakFlt_ = new JCheckBox(PivResource.getS("Filtrer par largeur du pic de corr�lation"));
    cbCorrelPeakFlt_.addItemListener((evt) -> setCorrelPeakPanelActive(cbCorrelPeakFlt_.isSelected()));

    JPanel pnCorrelPeakParameters = new JPanel();
    pnCorrelPeakParameters.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Param�tres")));
    JLabel lbCorrelRh0max = new JLabel(PivResource.getS("Rapport max. largeur pic corr�lation vs. d�placement:"));
    JLabel lbCorrelRcut = new JLabel("rcut:");
    lbCorrelRcut.setEnabled(false);
    tfCorrelRcut_ = new JTextField();
    tfCorrelRcut_.setEnabled(false);
    tfCorrelRh0max_ = new JTextField();
    tfCorrelRh0max_.setEnabled(false);
    
    GroupLayout gl_pnCorrelPeakParameters = new GroupLayout(pnCorrelPeakParameters);
    gl_pnCorrelPeakParameters.setHorizontalGroup(
      gl_pnCorrelPeakParameters.createParallelGroup(Alignment.LEADING)
        .addGap(0, 239, Short.MAX_VALUE)
        .addGroup(gl_pnCorrelPeakParameters.createSequentialGroup()
          .addGroup(gl_pnCorrelPeakParameters.createParallelGroup(Alignment.LEADING)
            .addComponent(lbCorrelRh0max)
            .addComponent(lbCorrelRcut))
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addGroup(gl_pnCorrelPeakParameters.createParallelGroup(Alignment.LEADING)
            .addComponent(tfCorrelRcut_, GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
            .addComponent(tfCorrelRh0max_, GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE))
          .addContainerGap())
    );
    gl_pnCorrelPeakParameters.setVerticalGroup(
      gl_pnCorrelPeakParameters.createParallelGroup(Alignment.LEADING)
        .addGap(0, 75, Short.MAX_VALUE)
        .addGroup(gl_pnCorrelPeakParameters.createSequentialGroup()
          .addGroup(gl_pnCorrelPeakParameters.createParallelGroup(Alignment.BASELINE)
            .addComponent(lbCorrelRcut)
            .addComponent(tfCorrelRcut_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addPreferredGap(ComponentPlacement.RELATED)
          .addGroup(gl_pnCorrelPeakParameters.createParallelGroup(Alignment.BASELINE)
            .addComponent(lbCorrelRh0max)
            .addComponent(tfCorrelRh0max_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    pnCorrelPeakParameters.setLayout(gl_pnCorrelPeakParameters);

    GroupLayout gl_pnCorrelPeak = new GroupLayout(pnCorrelPeak);
    gl_pnCorrelPeak.setHorizontalGroup(
      gl_pnCorrelPeak.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnCorrelPeak.createSequentialGroup()
          .addContainerGap()
          .addGroup(gl_pnCorrelPeak.createParallelGroup(Alignment.LEADING)
            .addComponent(cbCorrelPeakFlt_)
            .addComponent(pnCorrelPeakParameters, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 566, Short.MAX_VALUE))
          .addContainerGap())
    );
    gl_pnCorrelPeak.setVerticalGroup(
      gl_pnCorrelPeak.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnCorrelPeak.createSequentialGroup()
          .addComponent(cbCorrelPeakFlt_)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(pnCorrelPeakParameters, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
          .addContainerGap(312, Short.MAX_VALUE))
    );
    pnCorrelPeak.setLayout(gl_pnCorrelPeak);
    tpFilters_.addTab(PivResource.getS("Pic de corr�lation"), pnCorrelPeak);
    
    // Filtre de distribution temporelle de la vitesse.
    
    JPanel pnVelocityDistrib = new JPanel();
    pnVelocityDistrib.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
    
    cbVelocityDistribFlt_ = new JCheckBox(PivResource.getS("Filtrer par la distribution temporelle de vitesse"));
    cbVelocityDistribFlt_.addItemListener((evt) -> setVelocityDistribPanelActive(cbVelocityDistribFlt_.isSelected()));

    JPanel pnVelocityDistribParameters = new JPanel();
    pnVelocityDistribParameters.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Param�tres")));
    JLabel lbVelocityDistribNstdvel = new JLabel(PivResource.getS("Nb d'�carts-types au-del� de la moyenne:"));
    tfVelocityDistribNstdvel_ = new JTextField();
    tfVelocityDistribNstdvel_.setEnabled(false);
        
    GroupLayout gl_pnVelocityDistribParameters = new GroupLayout(pnVelocityDistribParameters);
    gl_pnVelocityDistribParameters.setHorizontalGroup(
      gl_pnVelocityDistribParameters.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnVelocityDistribParameters.createSequentialGroup()
          .addComponent(lbVelocityDistribNstdvel)
          .addGap(13)
          .addComponent(tfVelocityDistribNstdvel_, GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
          .addContainerGap())
    );
    gl_pnVelocityDistribParameters.setVerticalGroup(
      gl_pnVelocityDistribParameters.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnVelocityDistribParameters.createSequentialGroup()
          .addGroup(gl_pnVelocityDistribParameters.createParallelGroup(Alignment.BASELINE)
            .addComponent(lbVelocityDistribNstdvel)
            .addComponent(tfVelocityDistribNstdvel_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addContainerGap(33, Short.MAX_VALUE))
    );
    pnVelocityDistribParameters.setLayout(gl_pnVelocityDistribParameters);

    GroupLayout gl_pnVelocityDistrib = new GroupLayout(pnVelocityDistrib);
    gl_pnVelocityDistrib.setHorizontalGroup(
      gl_pnVelocityDistrib.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnVelocityDistrib.createSequentialGroup()
          .addGroup(gl_pnVelocityDistrib.createParallelGroup(Alignment.LEADING)
            .addGroup(gl_pnVelocityDistrib.createSequentialGroup()
              .addGap(10)
              .addComponent(cbVelocityDistribFlt_))
            .addGroup(Alignment.TRAILING, gl_pnVelocityDistrib.createSequentialGroup()
              .addContainerGap()
              .addComponent(pnVelocityDistribParameters, GroupLayout.DEFAULT_SIZE, 566, Short.MAX_VALUE)))
          .addContainerGap())
    );
    gl_pnVelocityDistrib.setVerticalGroup(
      gl_pnVelocityDistrib.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnVelocityDistrib.createSequentialGroup()
          .addComponent(cbVelocityDistribFlt_)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(pnVelocityDistribParameters, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
          .addContainerGap(338, Short.MAX_VALUE))
    );
    pnVelocityDistrib.setLayout(gl_pnVelocityDistrib);
    tpFilters_.addTab(PivResource.getS("Distribution des vitesses"), pnVelocityDistrib);
    
    // Filtre dispersion de la vitesse dans le sens du courant 
    
    JPanel pnVelocityDispersion = new JPanel();
    pnVelocityDispersion.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
    
    cbVelocityDispersionFlt_ = new JCheckBox(PivResource.getS("Filtrer par la dispersion de vitesse dans le sens du courant"));
    cbVelocityDispersionFlt_.addItemListener((evt) -> setVelocityDispersionPanelActive(cbVelocityDispersionFlt_.isSelected()));
    
    JPanel pnVelocityDispersionParameters = new JPanel();
    pnVelocityDispersionParameters.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Param�tres")));
    JLabel pnVelocityDispersionCovmax = new JLabel(PivResource.getS("COVmax:"));
    tfVelocityDispersionCovmax_ = new JTextField();
    tfVelocityDispersionCovmax_.setEnabled(false);
    
    GroupLayout gl_pnVelocityDispersionParameters = new GroupLayout(pnVelocityDispersionParameters);
    gl_pnVelocityDispersionParameters.setHorizontalGroup(
      gl_pnVelocityDispersionParameters.createParallelGroup(Alignment.LEADING)
        .addGap(0, 245, Short.MAX_VALUE)
        .addGroup(gl_pnVelocityDispersionParameters.createSequentialGroup()
          .addComponent(pnVelocityDispersionCovmax)
          .addGap(13)
          .addComponent(tfVelocityDispersionCovmax_, GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
          .addContainerGap())
    );
    gl_pnVelocityDispersionParameters.setVerticalGroup(
      gl_pnVelocityDispersionParameters.createParallelGroup(Alignment.LEADING)
        .addGap(0, 49, Short.MAX_VALUE)
        .addGroup(gl_pnVelocityDispersionParameters.createSequentialGroup()
          .addGroup(gl_pnVelocityDispersionParameters.createParallelGroup(Alignment.BASELINE)
            .addComponent(pnVelocityDispersionCovmax)
            .addComponent(tfVelocityDispersionCovmax_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addContainerGap(33, Short.MAX_VALUE))
    );
    pnVelocityDispersionParameters.setLayout(gl_pnVelocityDispersionParameters);
    
    GroupLayout gl_pnVelocityDispersion = new GroupLayout(pnVelocityDispersion);
    gl_pnVelocityDispersion.setHorizontalGroup(
      gl_pnVelocityDispersion.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnVelocityDispersion.createSequentialGroup()
          .addGroup(gl_pnVelocityDispersion.createParallelGroup(Alignment.LEADING)
            .addGroup(gl_pnVelocityDispersion.createSequentialGroup()
              .addGap(10)
              .addComponent(cbVelocityDispersionFlt_))
            .addGroup(gl_pnVelocityDispersion.createSequentialGroup()
              .addContainerGap()
              .addComponent(pnVelocityDispersionParameters, GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE)))
          .addGap(2))
    );
    gl_pnVelocityDispersion.setVerticalGroup(
      gl_pnVelocityDispersion.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnVelocityDispersion.createSequentialGroup()
          .addComponent(cbVelocityDispersionFlt_)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(pnVelocityDispersionParameters, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
          .addContainerGap(338, Short.MAX_VALUE))
    );
    pnVelocityDispersion.setLayout(gl_pnVelocityDispersion);
//    tpFilters_.addTab(PivResource.getS("Dispersion des vitesses"), pnVelocityDispersion);
    
    // Filtre dispersion angulaire
    
    JPanel pnAngularDispersion = new JPanel();
    pnAngularDispersion.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
    
    cbAngularDispersionFlt_ = new JCheckBox(PivResource.getS("Filtrer par la dispersion angulaire"));
    cbAngularDispersionFlt_.addItemListener((evt) -> setAngularDispersionPanelActive(cbAngularDispersionFlt_.isSelected()));
    
    JPanel pnAngularDispersionParameters = new JPanel();
    pnAngularDispersionParameters.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Param�tres")));
    JLabel lbAngularDispersionCircvarmax = new JLabel(PivResource.getS("Variance circulaire max.:"));
    tfAngularDispersionCircvarmax_ = new JTextField();
    tfAngularDispersionCircvarmax_.setEnabled(false);
    
    GroupLayout gl_pnAngularDispersionParameters = new GroupLayout(pnAngularDispersionParameters);
    gl_pnAngularDispersionParameters.setHorizontalGroup(
      gl_pnAngularDispersionParameters.createParallelGroup(Alignment.LEADING)
        .addGap(0, 241, Short.MAX_VALUE)
        .addGap(0, 245, Short.MAX_VALUE)
        .addGroup(gl_pnAngularDispersionParameters.createSequentialGroup()
          .addComponent(lbAngularDispersionCircvarmax)
          .addGap(13)
          .addComponent(tfAngularDispersionCircvarmax_, GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
          .addContainerGap())
    );
    gl_pnAngularDispersionParameters.setVerticalGroup(
      gl_pnAngularDispersionParameters.createParallelGroup(Alignment.LEADING)
        .addGap(0, 49, Short.MAX_VALUE)
        .addGap(0, 49, Short.MAX_VALUE)
        .addGroup(gl_pnAngularDispersionParameters.createSequentialGroup()
          .addGroup(gl_pnAngularDispersionParameters.createParallelGroup(Alignment.BASELINE)
            .addComponent(lbAngularDispersionCircvarmax)
            .addComponent(tfAngularDispersionCircvarmax_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addContainerGap(33, Short.MAX_VALUE))
    );
    pnAngularDispersionParameters.setLayout(gl_pnAngularDispersionParameters);

    GroupLayout gl_pnAngularDispersion = new GroupLayout(pnAngularDispersion);
    gl_pnAngularDispersion.setHorizontalGroup(
      gl_pnAngularDispersion.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnAngularDispersion.createSequentialGroup()
          .addGroup(gl_pnAngularDispersion.createParallelGroup(Alignment.LEADING)
            .addGroup(gl_pnAngularDispersion.createSequentialGroup()
              .addGap(10)
              .addComponent(cbAngularDispersionFlt_, GroupLayout.PREFERRED_SIZE, 241, GroupLayout.PREFERRED_SIZE))
            .addGroup(Alignment.TRAILING, gl_pnAngularDispersion.createSequentialGroup()
              .addContainerGap()
              .addComponent(pnAngularDispersionParameters, GroupLayout.DEFAULT_SIZE, 566, Short.MAX_VALUE)))
          .addContainerGap())
    );
    gl_pnAngularDispersion.setVerticalGroup(
      gl_pnAngularDispersion.createParallelGroup(Alignment.LEADING)
        .addGroup(gl_pnAngularDispersion.createSequentialGroup()
          .addComponent(cbAngularDispersionFlt_)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(pnAngularDispersionParameters, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
          .addContainerGap(338, Short.MAX_VALUE))
    );
    pnAngularDispersion.setLayout(gl_pnAngularDispersion);
    tpFilters_.addTab(PivResource.getS("Dispersion angulaire"), pnAngularDispersion);
    
    add(tpFilters_, BorderLayout.CENTER);
    setPreferredSize(new Dimension(730, 400));
  }

  protected void customize() {
    List<ButtonTabComponent> tabTitleComponents = new ArrayList<>();
    
    int icpt = 0;
    pnMedianTestTitle_ = new ButtonTabComponent(tpFilters_, PivResource.getS("Coh�rence spatiale"), PivResource.PIV.getIcon("filter_median_test.png"));
    pnMedianTestTitle_.addItemListener((evt) -> setMedianTestPanelActive(pnMedianTestTitle_.isSelected()));
    tpFilters_.setTabComponentAt(icpt++, pnMedianTestTitle_);
    tabTitleComponents.add(pnMedianTestTitle_);
    
    pnVelTitle_ = new ButtonTabComponent(tpFilters_, PivResource.getS("Vitesse"), PivResource.PIV.getIcon("filter_vel.png"));
    pnVelTitle_.addItemListener((evt) -> setVelPanelActive(pnVelTitle_.isSelected()));
    tpFilters_.setTabComponentAt(icpt++, pnVelTitle_);
    tabTitleComponents.add(pnVelTitle_);
    
    pnCorrelTitle_ = new ButtonTabComponent(tpFilters_, PivResource.getS("Qualit� des traceurs"), PivResource.PIV.getIcon("filter_corr.png"));
    pnCorrelTitle_.addItemListener((evt) -> setCorrelPanelActive(pnCorrelTitle_.isSelected()));
    tpFilters_.setTabComponentAt(icpt++, pnCorrelTitle_);
    tabTitleComponents.add(pnCorrelTitle_);
    
    pnCorrelPeakTitle_ = new ButtonTabComponent(tpFilters_, PivResource.getS("Pic de corr�lation"), PivResource.PIV.getIcon("filter_corr_peak.png"));
    pnCorrelPeakTitle_.addItemListener((evt) -> setCorrelPeakPanelActive(pnCorrelPeakTitle_.isSelected()));
    tpFilters_.setTabComponentAt(icpt++, pnCorrelPeakTitle_);
    tabTitleComponents.add(pnCorrelPeakTitle_);
    
    pnVelocityDistribTitle_ = new ButtonTabComponent(tpFilters_, PivResource.getS("Coh�rence temporelle (vitesses)"), PivResource.PIV.getIcon("filter_vel_distrib.png"));
    pnVelocityDistribTitle_.addItemListener((evt) -> setVelocityDistribPanelActive(pnVelocityDistribTitle_.isSelected()));
    tpFilters_.setTabComponentAt(icpt++, pnVelocityDistribTitle_);
    tabTitleComponents.add(pnVelocityDistribTitle_);
    
//    pnVelocityDispersionTitle_ = new ButtonTabComponent(tpFilters_, PivResource.getS("Dispersion des vitesses"), PivResource.PIV.getIcon("filter_vel_dispersion.png"));
//    pnVelocityDispersionTitle_.addItemListener((evt) -> setVelocityDispersionPanelActive(pnVelocityDispersionTitle_.isSelected()));
//    tpFilters_.setTabComponentAt(icpt++, pnVelocityDispersionTitle_);
//    tabTitleComponents.add(pnVelocityDispersionTitle_);
    
    pnAngularDispersionTitle_ = new ButtonTabComponent(tpFilters_, PivResource.getS("Coh�rence temporelle (angles)"), PivResource.PIV.getIcon("filter_angular_dispersion.png"));
    pnAngularDispersionTitle_.addItemListener((evt) -> setAngularDispersionPanelActive(pnAngularDispersionTitle_.isSelected()));
    tpFilters_.setTabComponentAt(icpt++, pnAngularDispersionTitle_);
    tabTitleComponents.add(pnAngularDispersionTitle_);
    
    // Redimensionnement de tous les composants onglets, pour les caler � gauche.
    Dimension maxDim = new Dimension();
    for (ButtonTabComponent cp : tabTitleComponents) {
      Dimension d = cp.getPreferredSize();
      maxDim.setSize(Math.max(maxDim.width, d.width+2), Math.max(maxDim.height, d.height));
    }
    for (ButtonTabComponent cp : tabTitleComponents) {
      cp.setPreferredSize(maxDim);
    }
  }
  
  public void setMedianTestPanelActive(boolean b) {
    tfMedianTestEpsilon_.setEnabled(b);
    tfMedianTestR0min_.setEnabled(b);
    cbMedianTestAdvanced_.setEnabled(b);
    
    tfMedianTestNneighbor_.setEnabled(cbMedianTestAdvanced_.isSelected() && b);
    tfMedianTestDistmax_.setEnabled(cbMedianTestAdvanced_.isSelected() && b);

    cbMedianTestFlt_.setSelected(b);
    pnMedianTestTitle_.setSelected(b);
  }
  
  public void setVelPanelActive(boolean b) {
    tfNormalMinLimit_.setEnabled(b);
    tfNormalMaxLimit_.setEnabled(b);
    tfVxMinLimit_.setEnabled(b);
    tfVxMaxLimit_.setEnabled(b);
    tfVyMinLimit_.setEnabled(b);
    tfVyMaxLimit_.setEnabled(b);
    
    cbVelFlt_.setSelected(b);
    pnVelTitle_.setSelected(b);
  }
  
  public void setCorrelPanelActive(boolean b) {
    tfCorrelMinLimit_.setEnabled(b);
    tfCorrelMaxLimit_.setEnabled(b);

    cbCorrelFlt_.setSelected(b);
    pnCorrelTitle_.setSelected(b);
  }
  
  public void setCorrelPeakPanelActive(boolean b) {
    // Ce param�tre ne peut pas �tre modifi�.
//  tfCorrelRcut_.setEnabled(b);
    tfCorrelRh0max_.setEnabled(b);

    cbCorrelPeakFlt_.setSelected(b);
    pnCorrelPeakTitle_.setSelected(b);
  }
  
  public void setVelocityDistribPanelActive(boolean b) {
    tfVelocityDistribNstdvel_.setEnabled(b);

    cbVelocityDistribFlt_.setSelected(b);
    pnVelocityDistribTitle_.setSelected(b);
  }
  
  public void setVelocityDispersionPanelActive(boolean b) {
    tfVelocityDispersionCovmax_.setEnabled(b);

    cbVelocityDispersionFlt_.setSelected(b);
    pnVelocityDispersionTitle_.setSelected(b);
  }
  
  public void setAngularDispersionPanelActive(boolean b) {
    tfAngularDispersionCircvarmax_.setEnabled(b);

    cbAngularDispersionFlt_.setSelected(b);
    pnAngularDispersionTitle_.setSelected(b);
  }
  
  /**
   * Definit les valeurs pour les filtres
   * @param _params Les param�tres de filtres.
   */
  public void setFilterValues(PivFilterParameters _params) {
    if (_params==null) return;
    
    cbVelFlt_.setSelected(_params.isVelocityFilterEnabled());
    tfNormalMinLimit_.setText(""+_params.getSmin());
    tfNormalMaxLimit_.setText(""+_params.getSmax());
    tfVxMinLimit_.setText(""+_params.getVxmin());
    tfVxMaxLimit_.setText(""+_params.getVxmax());
    tfVyMinLimit_.setText(""+_params.getVymin());
    tfVyMaxLimit_.setText(""+_params.getVymax());

    cbCorrelFlt_.setSelected(_params.isCorrelationFilterEnabled());
    tfCorrelMinLimit_.setText(""+_params.getMinCorrelation());
    tfCorrelMaxLimit_.setText(""+_params.getMaxCorrelation());
    
    cbMedianTestFlt_.setSelected(_params.isMedianTestFilterEnabled());
    tfMedianTestEpsilon_.setText(""+_params.getEpsilon());
    tfMedianTestR0min_.setText(""+_params.getR0min());
    
    cbMedianTestAdvanced_.setSelected(_params.isMedianTestAdvancedFilterEnabled());
    tfMedianTestNneighbor_.setText(""+_params.getNneighbor());
    tfMedianTestDistmax_.setText(""+_params.getDistmax());
    
    cbCorrelPeakFlt_.setSelected(_params.isCorrelationPeakFilterEnabled());
    tfCorrelRcut_.setText(""+_params.getRcut());
    tfCorrelRh0max_.setText(""+_params.getRhomax());
    
    cbVelocityDistribFlt_.setSelected(_params.isVelocityDistributionFilterEnabled());
    tfVelocityDistribNstdvel_.setText(""+_params.getNstdvel());
    
    cbVelocityDispersionFlt_.setSelected(_params.isVelocityDispersionFilterEnabled());
    tfVelocityDispersionCovmax_.setText(""+_params.getCovmax());
    
    cbAngularDispersionFlt_.setSelected(_params.isAngularDispersionFilterEnabled());
    tfAngularDispersionCircvarmax_.setText(""+_params.getCircvarmax());
  }
  
  /**
   * Recupere les valeurs pour les filtres.
   * @param _params Les param�tres de filtee.
   */
  public void retrieveFilterValues(PivFilterParameters _params) {
    if (_params==null) return;
    
    if (cbVelFlt_.isSelected()) {
      _params.setSmin(Double.parseDouble(tfNormalMinLimit_.getText()));
      _params.setSmax(Double.parseDouble(tfNormalMaxLimit_.getText()));
      _params.setVxmin(Double.parseDouble(tfVxMinLimit_.getText()));
      _params.setVxmax(Double.parseDouble(tfVxMaxLimit_.getText()));
      _params.setVymin(Double.parseDouble(tfVyMinLimit_.getText()));
      _params.setVymax(Double.parseDouble(tfVyMaxLimit_.getText()));
    }
    else {
      _params.resetVelocityFilter();
    }
    _params.setVelocityFilterEnabled(cbVelFlt_.isSelected());

    if (cbCorrelFlt_.isSelected()) {
      _params.setMinCorrelation(Double.parseDouble(tfCorrelMinLimit_.getText()));
      _params.setMaxCorrelation(Double.parseDouble(tfCorrelMaxLimit_.getText()));
    }
    else {
      _params.resetCorrelationFilter();
    }
    _params.setCorrelationFilterEnabled(cbCorrelFlt_.isSelected());
    
    if (cbMedianTestFlt_.isSelected()) {
      _params.setEpsilon(Double.parseDouble(tfMedianTestEpsilon_.getText()));
      _params.setR0min(Double.parseDouble(tfMedianTestR0min_.getText()));
      
      if (cbMedianTestAdvanced_.isSelected()) {
        _params.setNneighbor(Integer.parseInt(tfMedianTestNneighbor_.getText()));
        _params.setDistmax(Double.parseDouble(tfMedianTestDistmax_.getText()));
      }
      else {
        _params.resetMedianTestAdvancedFilter();
      }
      _params.setMedianTestFilterAdvancedEnabled(cbMedianTestAdvanced_.isSelected());
    }
    else {
      _params.resetMedianTestFilter();
      _params.setMedianTestFilterAdvancedEnabled(false);
    }
    _params.setMedianTestFilterEnabled(cbMedianTestFlt_.isSelected());
    
    if (cbCorrelPeakFlt_.isSelected()) {
      _params.setRcut(Double.parseDouble(tfCorrelRcut_.getText()));
      _params.setRhomax(Double.parseDouble(tfCorrelRh0max_.getText()));
    }
    else {
      _params.resetCorrelationPeakFilter();
    }
    _params.setCorrelationPeakFilterEnabled(cbCorrelPeakFlt_.isSelected());
    
    if (cbVelocityDistribFlt_.isSelected()) {
      _params.setNstdvel(Integer.parseInt(tfVelocityDistribNstdvel_.getText()));
    }
    else {
      _params.resetVelocityDistributionFilter();
    }
    _params.setVelocityDistributionFilterEnabled(cbVelocityDistribFlt_.isSelected());
    
    if (cbVelocityDispersionFlt_.isSelected()) {
      _params.setCovmax(Double.parseDouble(tfVelocityDispersionCovmax_.getText()));
    }
    else {
      _params.resetVelocityDispersionFilter();
    }
    _params.setVelocityDispersionFilterEnabled(cbVelocityDispersionFlt_.isSelected());
    
    if (cbAngularDispersionFlt_.isSelected()) {
      _params.setCircvarmax(Double.parseDouble(tfAngularDispersionCircvarmax_.getText()));
    }
    else {
      _params.resetAngularDispersionFilter();
    }
    _params.setAngularDispersionFilterEnabled(cbAngularDispersionFlt_.isSelected());
  }

  @Override
  public boolean apply() {
    boolean compute = false;
    
    PivProject prj = impl_.getCurrentProject();
    PivComputeParameters params=new PivComputeParameters(prj.getComputeParameters());
    retrieveFilterValues(params.getFilters());
    
    if (!params.equals(prj.getComputeParameters())) {
      if (prj.hasAverageResults() && !impl_.question(PivResource.getS("Suppression des r�sultats"), 
          PivResource.getS("Attention : des r�sultats existent et seront supprim�s si vous modifiez les param�tres.\nVoulez-vous continuer ?"))) {
        return false;
      }
      
      prj.setComputeParameters(params);
      compute = true;
    }
    
    else if (!prj.hasInstantFilteredResults()) {
      compute = true;
    }
    
    if (compute) {

      // La tache a ex�cuter.
      PivTaskAbstract r = new PivTaskAbstract(PivResource.getS("Filtrage des r�sultats instantan�s")) {

        public boolean act(PivTaskObserverI _observer) {
          CtuluLog ana = new CtuluLog();

          // Filtrage
          PivExeLauncher.instance().computeFilteredInstantResultats(ana, impl_.getCurrentProject(), _observer);
          if (ana.containsErrorOrSevereError()) {
            impl_.error(ana.getResume());
            return false;
          }

          // Lanc� � la fin, car l'interface se bloque si on ne le fait pas.
          // Probl�me de thread swing probablement...
          SwingUtilities.invokeLater(new Runnable() {
            public void run() {
              impl_.message(PivResource.getS("Calcul termin�"), PivResource.getS("Le calcul s'est termin� avec succ�s"),
                  false);
              impl_.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);
              impl_.get2dFrame().getVisuPanel().setFilteredVelocitiesLayerVisible(true);
            }
          });
          
          return true;
        }
      };

      PivProgressionPanel pnProgress_ = new PivProgressionPanel(r);
      diProgress_ = pnProgress_.createDialog(impl_.getParentComponent());
      diProgress_.setOption(CtuluDialog.ZERO_OPTION);
      diProgress_.setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
      diProgress_.setTitle(r.getName());

      r.start();
      diProgress_.afficheDialogModal();
    }

    return true;
  }
  
  @Override
  public boolean isDataValid() {

    // Filtre test m�dian
    if (cbMedianTestFlt_.isSelected()) {
      if (PivUtils.isRealInInterval(this, tfMedianTestEpsilon_.getText(), PivResource.getS("epsilon"), false, 0, false, 5) == null) {
        return false;
      }
      if (PivUtils.isRealInInterval(this, tfMedianTestR0min_.getText(), PivResource.getS("r0min"), false, 0, false, 10) == null) {
        return false;
      }
      if (cbMedianTestAdvanced_.isSelected()) {
        if (PivUtils.isPositiveInteger(this, tfMedianTestNneighbor_.getText(), PivResource.getS("n_neighbor"), 1) == null) {
          return false;
        }
        if (PivUtils.isStrictPositiveReal(this, tfMedianTestDistmax_.getText(), PivResource.getS("dist_max"), 0) == null) {
          return false;
        }
      }
    }
    
    // Filtre vitesses
    if (cbVelFlt_.isSelected()) {
      Double min;
      Double max;
      
      if ((min=PivUtils.isPositiveReal(this, tfNormalMinLimit_.getText(), PivResource.getS("Min norme de vitesse")))==null) {
        return false;
      }
      
      if ((max=PivUtils.isPositiveReal(this, tfNormalMaxLimit_.getText(), PivResource.getS("Max norme de vitesse")))==null) {
        return false;
      }
      if (min>=max) {
        setErrorText(PivResource.getS("{0}: Min doit �tre inf�rieur � Max",
            PivResource.getS("Norme de vitesse")));
        return false;
      }
      
      if ((min=PivUtils.isReal(this, tfVxMinLimit_.getText(), PivResource.getS("Min Vx")))==null) {
        return false;
      }
      if ((max=PivUtils.isReal(this, tfVxMaxLimit_.getText(), PivResource.getS("Max Vx")))==null) {
        return false;
      }
      if (min>=max) {
        setErrorText(PivResource.getS("{0}: Min doit �tre inf�rieur � Max",
            PivResource.getS("Vx")));
        return false;
      }
      
      if ((min=PivUtils.isReal(this, tfVyMinLimit_.getText(), PivResource.getS("Min Vy")))==null) {
        return false;
      }
      if ((max=PivUtils.isReal(this, tfVyMaxLimit_.getText(), PivResource.getS("Max Vy")))==null) {
        return false;
      }
      if (min>=max) {
        setErrorText(PivResource.getS("{0}: Min doit �tre inf�rieur � Max",
            PivResource.getS("Vy")));
        return false;
      }
    }
    
    // Filtre correlations
    if (cbCorrelFlt_.isSelected()) {
      Double min;
      Double max;

      if ((min=PivUtils.isReal(this, tfCorrelMinLimit_.getText(), PivResource.getS("Min corr�lation")))==null) {
        return false;
      }
      if ((max=PivUtils.isReal(this, tfCorrelMaxLimit_.getText(), PivResource.getS("Max corr�lation")))==null) {
        return false;
      }
      if (min>=max) {
        setErrorText(PivResource.getS("{0}: Min doit �tre inf�rieur � Max", PivResource.getS("Corr�lation")));
        return false;
      }
    }

    // Filtre pic de correlation
    if (cbCorrelPeakFlt_.isSelected()) {
      if (PivUtils.isRealInInterval(this, tfCorrelRcut_.getText(), PivResource.getS("rcut"), false, 0, false, 1) == null) {
        return false;
      }
      if (PivUtils.isRealInInterval(this, tfCorrelRh0max_.getText(), PivResource.getS("rh0max"), false, 0, false, 1) == null) {
        return false;
      }
    }
    
    // Filtre vitesses distributions
    if (cbVelocityDistribFlt_.isSelected()) {
      if (PivUtils.isIntegerInInterval(this, tfVelocityDistribNstdvel_.getText(), PivResource.getS("Nstdvel"), false, 1, false, 10) == null) {
        return false;
      }
    }

    // Filtre de dispersion des vitesses dans le sens du courant.
    if (cbVelocityDispersionFlt_.isSelected()) {
      if (PivUtils.isRealInInterval(this, tfVelocityDispersionCovmax_.getText(), PivResource.getS("COVmax"), false, 0, false, 1) == null) {
        return false;
      }
    }

    // Filtre de dispersion angulaire.
    if (cbAngularDispersionFlt_.isSelected()) {
      if (PivUtils.isRealInInterval(this, tfAngularDispersionCircvarmax_.getText(), PivResource.getS("circvarmax"), false, 0, false, 1) == null) {
        return false;
      }
    }

    return true;
  }
  
  public static void main(String[] _args) {
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    BuPanel panel = new BuPanel();
    PivFilterInstantResultsPanel pn = new PivFilterInstantResultsPanel(null);
    pn.setFilterValues(new PivFilterParameters());
    
    panel.setLayout(new BuBorderLayout());
    panel.add(pn, BuBorderLayout.CENTER);
    
    frame.getContentPane().add(panel);
    frame.pack();
    frame.setVisible(true);
    
  }
}
