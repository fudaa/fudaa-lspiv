/*
 * @creation     20 janv. 08
 * @modification $Date: 2009-03-29 17:52:05 +0200 (dim., 29 mars 2009) $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv;

import java.awt.Dimension;

import javax.swing.JComponent;

import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.ZEbliFilleCalques;

/**
 * La fenetre interne vue 2D des donn�es de l'application. Elle construit le
 * composant arbre de calques {@link org.fudaa.ebli.calque.BArbreCalque}. La
 * plupart des traitements est d�l�gu�e au composant
 * {@link org.fudaa.ebli.calque.ZEbliCalquesPanel} encapsul�.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivFille2d extends ZEbliFilleCalques /*implements BuUndoRedoInterface, CtuluExportDataInterface,
    CtuluImageImporter, CtuluUndoRedoInterface, FudaaSavable, BuCutCopyPasteInterface*/ {
  
  public PivFille2d(PivImplementation _ui) {
    super(new PivVisuPanel(_ui), _ui, null);
    setName("mdlMainFille");
    changeTitle(null);
    setPreferredSize(new Dimension(600, 600));
//    cutCopyPasteManager_=new MdlCutCopyPasteManager(this, (MdlSceneEditor) getMdlVisuPanel().getEditor().getSceneEditor());
  }

  @Override
  public JComponent[] getSpecificTools() {
    JComponent[] superCmps=super.getSpecificTools();
    JComponent[] cmps=getVisuPanel().getSpecificTools();
    JComponent[] tools=new JComponent[cmps.length+superCmps.length];
    System.arraycopy(superCmps, 0, tools, 0, superCmps.length);
    System.arraycopy(cmps, 0, tools, superCmps.length, cmps.length);
    
    return tools;
  }

//  public void saveIn(final FudaaSaveZipWriter _writer, final ProgressionInterface _prog) {
//    new FudaaFilleVisuPersistence(this).saveIn(_writer, _prog);
//  }
//
//  public void saveIn(final ObjectContainer _db, final ProgressionInterface _prog) {
//    new FudaaFilleVisuPersistence(this).saveIn(_db, _prog);
//  }

  /**
   * Retourne le panneau interne des calques.
   * @return Le panneau des calques.
   */
  @Override
  public PivVisuPanel getVisuPanel() {
    return (PivVisuPanel)super.getVisuPanel();
  }
  
  public BArbreCalque getArbreCalque() {
    createPanelComponent();
    return arbre_;
  }
  
  /**
   * Modifie le titre de la fenetre, en tenant compte de la vue courante.
   * @param _view Le nom de la vue courante.
   */
  protected void changeTitle(String _view) {
    String titre=PivResource.getS("Vue 2D");
    if (_view!=null) {
      titre+=" - "+_view;
    }
    setTitle(titre);
  }

  @Override
  public final String[] getEnabledActions() {
    String[] acts=super.getEnabledActions();
    String[] rets=new String[acts.length+2];
    System.arraycopy(acts, 0, rets, 0, acts.length);
    rets[rets.length-2]=CtuluLibImage.SNAPSHOT_COMMAND;
    rets[rets.length-1]=CtuluLibImage.SNAPSHOT_CLIPBOARD_COMMAND;
    return rets;
  }

//  public void importImage() {
//    getMdlVisuPanel().importImage();
//
//  }
//
//  public void redo() {
//    getMdlVisuPanel().redo();
//  }
//
//  public void startExport(CtuluUI _impl) {
//    getMdlVisuPanel().getEditor().getExportAction().actionPerformed(null);
//  }
//
//  public void undo() {
//    getMdlVisuPanel().undo();
//  }
//
//  public void clearCmd(final CtuluCommandManager _source) {
//    getMdlVisuPanel().clearCmd(_source);
//  }
//
//  public CtuluCommandManager getCmdMng() {
//    return getMdlVisuPanel().getCmdMng();
//  }
//
//  public void setActive(boolean _active) {
//    getMdlVisuPanel().setActive(_active);
//
//  }

//  public void copy() {
//    cutCopyPasteManager_.copy();
//  }
//
//  public void cut() {
//    cutCopyPasteManager_.cut();
//  }
//
//  public void duplicate() {
//    getMdlVisuPanel().duplicate();
//  }
//
//  public void paste() {
//    cutCopyPasteManager_.paste();
//  }
  
}
