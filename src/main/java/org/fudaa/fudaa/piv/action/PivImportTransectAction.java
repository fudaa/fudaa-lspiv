package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.io.PivBathyReader;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;
import org.fudaa.fudaa.piv.metier.PivTransect;
import org.fudaa.fudaa.piv.utils.PivUtils;

/**
 * Une action pour importer un transect.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivImportTransectAction extends EbliActionSimple {
  PivImplementation impl;
  CtuluFileChooser fcTransect;

  /**
   * Constructeur
   * @param _impl L'implementation.
   */
  public PivImportTransectAction(PivImplementation _impl) {
    super(PivResource.getS("Import de transect(s)..."), null, "IMPORT_TRANSECT");

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Affiche une boite de dialogue pour permettre de selectionner le fichier
   * d'import.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }

    // Selection du fichier
    if (fcTransect == null) {
      fcTransect=new CtuluFileChooser(true);
      fcTransect.setAcceptAllFileFilterUsed(true);
      fcTransect.setFileFilter(PivUtils.FILE_FLT_BATHY);
      fcTransect.setSelectedFile(new File("trans0001.bth_dat"));
      fcTransect.setMultiSelectionEnabled(true);
      fcTransect.setDialogTitle(PivResource.getS("S�lection de fichier(s) transect(s)"));
    }
    
    if (fcTransect.showOpenDialog(impl.getFrame()) == CtuluFileChooser.CANCEL_OPTION) {
      return;
    }
    File[] transectFiles=fcTransect.getSelectedFiles();
    ArrayList<PivTransect> transects = new ArrayList<>();

    for (File transectFile : transectFiles) {
      CtuluIOResult<PivTransect> ret = new PivBathyReader().read(transectFile, null);
      if (ret.getAnalyze().containsErrorOrSevereError()) {
        impl.error(ret.getAnalyze().getResume());
        return;
      }

      transects.add(ret.getSource());
    }
      
    GrMorphisme toData = impl.getCurrentProject().getTransformationParameters().getToData();
    for (PivTransect trans : transects) {
      trans.getStraight().autoApplique(toData);

      impl.getCurrentProject().addTransect(trans);
    }

    impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Il doit exister au moins une image transform�e");
  }
}
