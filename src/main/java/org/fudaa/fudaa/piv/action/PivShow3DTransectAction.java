/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivShow3DTransectPanel;

/**
 * Une action pour afficher le transect selectionné suivant un graphe 3D.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivShow3DTransectAction extends EbliActionSimple implements ZSelectionListener {
  PivImplementation impl;
  PivShow3DTransectPanel pn;
  
  public PivShow3DTransectAction(PivImplementation _impl) {
    super(PivResource.getS("Vue 3D du(des) transect(s)..."), null, "SHOW_3D_GRAPH_TRANS");
    impl=_impl;
    _impl.get2dFrame().getVisuPanel().getScene().addSelectionListener(this);
    
    setEnabled(false);
  }

  /**
   * Affiche le panneau des paramètres de calcul.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    int[] isels=impl.get2dFrame().getVisuPanel().getRealView().getTransectLayer().getSelectedIndex();
//    
//    if (pn==null)
      pn=new PivShow3DTransectPanel(impl);
    pn.setSelectedTransects(isels);
//
    pn.afficheModale(impl.getFrame(), PivResource.getS("Vue 3D du(des) transect(s)"),CtuluDialog.OK_OPTION);
  }
  
  @Override
  public String getEnableCondition() {
    return PivResource.getS("Sélectionner au moins un transect");
  }

  @Override
  public void selectionChanged(ZSelectionEvent _evt) {
    boolean b=false;
    BCalque cq=impl.get2dFrame().getVisuPanel().getCalqueActif();

    if (cq==impl.get2dFrame().getVisuPanel().getRealView().getTransectLayer() && 
        ((ZCalqueLigneBriseeEditable)cq).getLayerSelection() !=null &&
        ((ZCalqueLigneBriseeEditable)cq).getLayerSelection().getNbSelectedIndex()>0) {
      b=true;
    }
    super.setEnabled(b);
  }
}
