/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteraction;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteractionTargetI;
import org.fudaa.ebli.commun.EbliActionChangeState;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;

/**
 * Une action pour saisir le point centre des IA/SA.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivDefineAreaCenterAction extends EbliActionChangeState {

  private PivVisuPanel pnCalques_;
  /** Le calque d'�dition. */
  private ZCalqueEditionInteraction cqEdition_;
  /** Le controller d'edition */
  private ZCalqueEditionInteractionTargetI controller_;
  /** Le listener sur le statut gele du calque d'�dition */
  private PropertyChangeListener listener_;

  /**
   * Constructeur.
   * @param _pn Le panneau des calques.
   */
  public PivDefineAreaCenterAction(PivVisuPanel _pn) {
    super(PivResource.getS("Position..."), null, "DEFINE_IA_CENTER");
    setDefaultToolTip(PivResource.getS("Saisissez la position du centre"));
    pnCalques_=_pn;
    
    // Pour ecouter le statut du calque d'edition
    listener_=new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        setSelected(!(Boolean)evt.getNewValue());
      }
    };
    
    setCalqueInteraction(_pn.getEditionLayer());
  }

  /**
   * Definit le calque d'interaction associ� � l'action.
   * @param _cq Le calque. Peut �tre <tt>null</tt>.
   */
  private void setCalqueInteraction(ZCalqueEditionInteraction _cq) {
    if (cqEdition_==_cq) return;
    
    if (cqEdition_!=null)
      cqEdition_.removePropertyChangeListener("gele", listener_);
    
    cqEdition_=_cq;
    
    if (cqEdition_!=null)
      cqEdition_.addPropertyChangeListener("gele", listener_);
  }
  
  /**
   * Definit le controlleur d'edition.
   * @param _ctrl Le controlleur.
   */
  public void setEditionController(ZCalqueEditionInteractionTargetI _ctrl) {
    controller_=_ctrl;
  }

  /**
   * Surcharge pour activer/desactiver le calque d'edition.
   */
  @Override
  public void changeAction() {
    if (isSelected()) {
      pnCalques_.setViewMode(PivVisuPanel.MODE_TRANSF_VIEW);
      cqEdition_.setListener(controller_);
      cqEdition_.setTypeForme(DeForme.POINT);
      cqEdition_.setFormEndedByDoubleClic(true);
      pnCalques_.setCalqueInteractionActif(cqEdition_);
    }
    else {
      pnCalques_.unsetCalqueInteractionActif(cqEdition_);
      cqEdition_.cancelEdition();
    }
  }


  @Override
  public String getEnableCondition() {
    return PivResource.getS("Il doit exister au moins une image transform�e");
  }
}
