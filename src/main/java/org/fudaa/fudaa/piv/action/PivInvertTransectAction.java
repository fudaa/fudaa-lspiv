/*
 * @creation     14 nov. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivTransect;

/**
 * Action permettant le changement de sens des géométries selectionnées.
 * @author Bertrand Marchand (marchand@detacad.fr)
 */
public class PivInvertTransectAction extends EbliActionSimple implements ZSelectionListener {
  PivImplementation impl_;
  
  public PivInvertTransectAction(PivImplementation _impl) {
    super(PivResource.getS("Inverser des transects"), null, "INVERSER_TRANSECT");
    impl_=_impl;
    setEnabled(false);
  }

  public void actionPerformed(final ActionEvent _e) {
    int[] selIds=impl_.get2dFrame().getVisuPanel().getScene().getLayerSelection().getSelectedIndex();
    
    PivTransect[] trans=impl_.getCurrentProject().getTransects();
    for (int id : selIds) {
      trans[id].invertGeometry();
    }
    
    // Pour forcer le rafraichissement d'écran
    impl_.getCurrentProject().setTransects(trans);
  }
  
  @Override
  public String getEnableCondition() {
    return PivResource.getS("Sélectionner un ou plusieurs transects");
  }

  @Override
  public void selectionChanged(ZSelectionEvent _evt) {
    boolean b=false;
    BCalque cq=impl_.get2dFrame().getVisuPanel().getCalqueActif();

    if (cq==impl_.get2dFrame().getVisuPanel().getRealView().getTransectLayer() && 
        ((ZCalqueLigneBriseeEditable)cq).getLayerSelection() !=null &&
        !((ZCalqueLigneBriseeEditable)cq).getLayerSelection().isEmpty()) {
      b=true;
    }
    super.setEnabled(b);
  }
}
