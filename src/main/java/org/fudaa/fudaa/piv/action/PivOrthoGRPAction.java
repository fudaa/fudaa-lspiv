/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.Container;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.SwingUtilities;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.BPaletteEdition;
import org.fudaa.ebli.calque.edition.BPaletteEditionClientInterface;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteraction;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteraction.FormDelegate;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteractionTargetI;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.calque.edition.ZEditorLigneBriseePanel;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.layer.PivOrthoPointsModel;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDesktop;
import com.memoire.fu.FuLog;

/**
 * Une action pour saisir les points de r�f�rence au sol. Cette action est 
 * utilis�e comme controller � la palette et au calque d'�dition d�di�.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivOrthoGRPAction extends EbliActionPaletteAbstract {

  private PivVisuPanel pnCalques_;
  /** Le calque d'�dition. */
  private ZCalqueEditionInteraction cqEdition_;
  /** Le calsue des ortho points */
  private ZCalquePointEditable cqOrthoPoints_;
  /** La palette */
  private BPaletteEdition palette_;
  /** Le panneau d'edition des param�tres du multipoint */
  private ZEditorLigneBriseePanel pnParams;
  /** Le controller d'edition */
  private EditionController controller_;
  /** Le listener de propri�t� du calque d'interaction */
  private EditionLayerPropertyListener propListener_;
  /** Les points existans avant edition */
  private PivOrthoPoint[] points_;
  /** Indique si l'action a �t� annul�e ou non. */
  private boolean cancel_;

  /**
   * Le controlleur pour l'�dition. L'�dition se fait sur le calque des GRP directement.
   */
  class EditionController implements ZCalqueEditionInteractionTargetI {

    
    public void atomicChanged() {
      // On notifie la palette que la forme a chang�, pour activer ou non le bouton fin d'edition.
      FuLog.debug("atomicChanged");
      pnParams.atomicChanged();
      
      FormDelegate multi = cqEdition_.getFormeEnCours();
      // Attention : La polyligne peut �tre nulle.
      GrPolyligne pl = (GrPolyligne)multi.getFormeEnCours();
      
      ZEditionAttributesDataI data = multi.getData();
      setPoints(pl, data, true);
            
      cqOrthoPoints_.repaint();
    }

    private boolean setPoints(GrPolyligne _pg, ZEditionAttributesDataI _data, boolean _inverse)  {
      GrMorphisme toData=pnCalques_.getProject().getTransformationParameters().getToData();
     
      PivOrthoPoint[] pts = null;

      if (_pg != null) {
        CtuluAnalyze ana = new CtuluAnalyze();
        if (!pnCalques_.getOriginalView().getOrthoPointsLayer().getModelEditable()
            .isDataValid(_pg.toCoordinateSequence(), _data, ana)) {
          pnCalques_.getCtuluUI().error(ana.getFatalError());
          return false;
        }
        
        pts = new PivOrthoPoint[_pg.sommets_.nombre()];
        
        GrPoint realPt;
        GrPoint imgPt;
        
        for (int i = 0; i < _pg.sommets_.nombre(); i++) {
          // Valeurs des points r�els.
          Double x = (Double) _data.getValue(PivOrthoPointsModel.ATT_IND_XR, i);
          Double y = (Double) _data.getValue(PivOrthoPointsModel.ATT_IND_YR, i);
          Double z = (Double) _data.getValue(PivVisuPanel.ATT_IND_ZR, i);
          realPt = new GrPoint(x.doubleValue(), y.doubleValue(), z.doubleValue());
          realPt.autoApplique(toData);
          
          // Point image
          imgPt = _pg.sommet(i);
          
          pts[i] = new PivOrthoPoint(realPt, imgPt);
        }
      }
      
      pnCalques_.getProject().setOrthoPoints(pts);
      return true;
    }
    
    
    /**
     * Ajoute la forme en cours au calque.
     * @param _pg Le multipoint, sous forme de polyligne
     * @param _data Les valeurs associ�es a chaque point.
     * @return Si false, la saisie n'est pas stopp�e.
     */
    public boolean addNewMultiPoint(GrPolyligne _pg, ZEditionAttributesDataI _data) {
      cancel_ = false;

      if (!setPoints(_pg, _data, true)) {
        return false;
      }
      hideWindow();

      return true;
    }

    /** Non utilis� */
    public void setMessage(String _s) {
    }

    /** Non utilis� */
    public void unsetMessage() {
    }

    /** Non utilis� */
    public boolean addNewPolygone(GrPolygone _pg, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewPoint(GrPoint _point, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewRectangle(GrPoint _pt, GrPoint _pt2, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewPolyligne(GrPolyligne _pt, ZEditionAttributesDataI _data) {
      return true;
    }

    public void pointMove(double _x, double _y) {
      // On notifie la palette que la forme a chang�, pour activer ou non le bouton fin d'edition.
      FuLog.debug("pointMove");
      pnParams.atomicChanged();
      
      FormDelegate multi = cqEdition_.getFormeEnCours();
      GrPolyligne pl = (GrPolyligne)multi.getFormeEnCours();
      
      ZEditionAttributesDataI data = multi.getData();
      setPoints(pl, data, true);
            
      cqOrthoPoints_.repaint();
    }
  }
  
  /**
   * Le listener qui r�agit en fonction du gel du calque d'interaction.
   */
  class EditionLayerPropertyListener implements PropertyChangeListener {

    public void propertyChange(PropertyChangeEvent evt) {
      if (evt.getPropertyName().equals("gele")) {
        if (pnParams!=null)
          pnParams.calqueInteractionChanged();
      }
    }
  }

  /**
   * Classe g�rant la disparition et la r�apparition du panel quand la fen�tre
   * principale est r�duite.
   */
  class Piv2dFrameListener extends InternalFrameAdapter {
    public void internalFrameActivated(final InternalFrameEvent _e) {
      if(window_!=null)
        window_.setVisible(true);
    }
    public void internalFrameDeactivated(final InternalFrameEvent _e) {
      if(window_!=null)
        window_.setVisible(false);
    }
  }

  /**
   * Constructeur de l'action, avec le panneau des calques.
   * @param _pn Le panneau des calques.
   */
  public PivOrthoGRPAction(PivVisuPanel _pn) {
    super(PivResource.getS("Nouveaux points..."), null, "CREATE_GRP");
    pnCalques_=_pn;
    
    controller_=new EditionController();
    propListener_=new EditionLayerPropertyListener();
    setCalqueInteraction(_pn.getEditionLayer());
    
    setPaletteResizable(true);
    setEnabled(false);
  }

  /**
   * Definit le calque d'interaction associ� � l'action.
   * @param _cq Le calque. Peut �tre <tt>null</tt>.
   */
  private void setCalqueInteraction(ZCalqueEditionInteraction _cq) {
    if (cqEdition_!=null) {
      cqEdition_.removePropertyChangeListener("gele", propListener_);
    }
    
    cqEdition_=_cq;

    if (cqEdition_!=null) {
      cqEdition_.addPropertyChangeListener("gele", propListener_);
    }
  }

  /**
   * Definit le calque contenant les points.
   * @param _cq Le calque des points.
   */
  public void setCalqueOrthoPoints(ZCalquePointEditable _cq) {
    cqOrthoPoints_=_cq;
    // Pour que la palette soit reconstruite correctement.
    palette_=null;
  }

  /**
   * Surcharg� pour demander � l'utilisateur s'il souhaite ajouter des points.
   */
  @Override
  public void changeAction() {
    if (isSelected())
      pnCalques_.setViewMode(PivVisuPanel.MODE_ORIGINAL_VIEW);
        
    // On conserve les points, pour les restituer en cas d'abandon.
    points_ = pnCalques_.getProject().getOrthoPoints();
    
    super.changeAction();
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Il doit exister au moins une image source");
  }

  /**
   * Surchag� pour d�finir le desktop au moment de l'affichage.
   */
  @Override
  public void showWindow() {
    // Necessaire, sinon la palette est visualis�e en externe. Ne peut �tre fait avant, car le desktop n'est pas
    // encore existant.
    setDesktop((BuDesktop)SwingUtilities.getAncestorOfClass(BuDesktop.class,pnCalques_));
    super.showWindow();
    
    // #5588 : On rend le calque d'interaction invisible, les points saisis le sont dans le calque des GRP.
    cqEdition_.setVisible(false);
  }

  /**
   * Surcharg� pour activer le calque d'�dition apr�s affichage.
   */
  @Override
  public void doAfterDisplay() {
    cqEdition_.setListener(controller_);
    cqEdition_.setTypeForme(DeForme.MULTI_POINT);
    cqEdition_.setFormEndedByDoubleClic(false);
    pnCalques_.setCalqueInteractionActif(cqEdition_);
    
    // On ajoute les points existants directement dans le tableau.
    if (points_ != null) {
      for (PivOrthoPoint pt : points_) {
        pnParams.getTableModel().addRow(pt.getImgPoint().x_, pt.getImgPoint().y_, pt.getRealPoint().x_, pt.getRealPoint().y_, pt.getRealPoint().z_);
      }
    }
    
    cancel_ = true;
  }

  /**
   * Surcharg� pour rendre inactif le calque d'�dition apr�s que la palette a
   * �t� ferm�e.
   */
  @Override
  public void hideWindow() {
    // Le calque d'interaction est rendu inactif.
    pnCalques_.unsetCalqueInteractionActif(cqEdition_);
    if (cqEdition_!=null)
      cqEdition_.cancelEdition();
    setSelected(false);
    
    if (cancel_) {
      pnCalques_.getProject().setOrthoPoints(points_);
    }
    
    super.hideWindow();
    
    // #5588 : On rend le calque d'interaction visible, pour le reste du programme.
    cqEdition_.setVisible(true);
  }

  @Override
  public JComponent buildContentPane() {
    if (palette_==null){
      palette_=new BPaletteEdition(pnCalques_.getScene());
      palette_.buildComponents();
      palette_.setTargetClient(new BPaletteEditionClientInterface() {
        public void doAction(String _com, BPaletteEdition _emitter) {
          if (BPaletteEdition.ADD_MULTIPOINT_ACTION.equals(_com)) {
            AbstractButton bt=_emitter.getSelectedButton();
            if (bt==null) {
              palette_.setEditorPanel(null);
            }
            else {
              BuButton btValider=new BuButton(PivResource.PIV.getIcon("valider"),PivResource.getS("Fin de saisie"));
              btValider.setToolTipText(PivResource.getS("Terminer la saisie des points et valider"));
              
              cqEdition_.setTypeForme(DeForme.MULTI_POINT);
              pnParams=new ZEditorLigneBriseePanel(palette_,cqEdition_);
              pnParams.replaceAddButton(btValider);
              pnParams.setCoordinateDefinitions(pnCalques_.getCoordinateDefinitions());
              pnParams.targetChanged(cqOrthoPoints_);
              pnParams.setPreferredSize(new Dimension(360,300));
              palette_.setEditorPanel(pnParams.getComponent());
              cqEdition_.setFeatures(pnParams.getAttributeContainer());
            }
            Object o = SwingUtilities.getAncestorOfClass(JInternalFrame.class, palette_);
            if (o == null) {
              o = SwingUtilities.getAncestorOfClass(JDialog.class, palette_);
              if (o != null) {
                ((JDialog) o).pack();
              }
            }
            else {
              ((JInternalFrame) o).pack();
            }
          }
        }
      });
      palette_.setButtonVisible(null, false);
      palette_.setButtonSelected(BPaletteEdition.ADD_MULTIPOINT_ACTION);

      // Ecoute de la fen�tre principale pour pouvoir se r�duire avec elle
      final Container c = SwingUtilities.getAncestorOfClass(JInternalFrame.class, pnCalques_);
      if (c instanceof JInternalFrame)
        ((JInternalFrame) c).addInternalFrameListener(new Piv2dFrameListener());
      
    }
    return palette_.getComponent();
  }
}
