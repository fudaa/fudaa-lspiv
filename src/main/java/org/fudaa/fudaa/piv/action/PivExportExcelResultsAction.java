/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.HashMap;

import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliTableInfoPanel;
import org.fudaa.ebli.commun.EbliTableInfoTarget;
import org.fudaa.fudaa.piv.PivExportImageFilesPanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.metier.PivResultsI;

import com.memoire.bu.BuResource;
import com.memoire.fu.FuLog;

/**
 * Une action pour exporter les r�sultats sous fichier excel.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivExportImagesAction.java 9523 2017-01-24 15:39:14Z bmarchan $
 */
public class PivExportExcelResultsAction extends EbliActionSimple {
  
  static final String TXTEXPORTERRAWRES=PivResource.getS("bruts");
  static final String TXTEXPORTERFLTRES=PivResource.getS("filtr�s");
  static final String TXTEXPORTERAVERES=PivResource.getS("moyenn�s");

  public enum ResultsType {
    RAW,
    FILTERED,
    AVERAGE
  }
  
  PivImplementation impl;
  PivExportImageFilesPanel pnExportImages;
  CtuluDialog diProgress_;
  ResultsType type_;
  CtuluFileChooser fcExportRes_;
  
  class ExportExcelPanel extends CtuluDialogPanel {
    EbliTableInfoPanel pnTable_;
    
    public ExportExcelPanel(PivImplementation _impl, EbliTableInfoTarget cq, ZEbliCalquesPanel pnVisu) {
      pnTable_ = new EbliTableInfoPanel(_impl, cq, pnVisu.getCoordinateDefinitions());
      add(pnTable_, BorderLayout.CENTER);
    }

    @Override
    public boolean apply() {
      pnTable_.exportTable();
      return true;
    }
    
  }
  
  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivExportExcelResultsAction(PivImplementation _impl, ResultsType _type) {
    super((_type == ResultsType.RAW ? TXTEXPORTERRAWRES: _type == ResultsType.FILTERED ? TXTEXPORTERFLTRES: TXTEXPORTERAVERES)+"...", null, 
          _type == ResultsType.RAW ? "EXPORT_RAW_RESULTS": _type == ResultsType.FILTERED ? "EXPORT_FILTERED_RESULTS":"EXPORT_AVERAGE_RESULTS");
    
    type_=_type;
    impl=_impl;
    setEnabled(false);
  }

  /**
   * Exporte les images transform�es au format pgm.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }
    
    PivVisuPanel pnVisu = impl.get2dFrame().getVisuPanel();
    
    EbliTableInfoTarget cq;
    switch (type_) {
    case RAW:
    default:
      cq = pnVisu.getRealView().getVelRawResultsLayer();
      break;
    case FILTERED:
      cq = pnVisu.getRealView().getVelFilteredResultsLayer();
      break;
    case AVERAGE:
      cq = pnVisu.getRealView().getVelAverageResultsLayer();
      break;
    }
    
    ExportExcelPanel pn = new ExportExcelPanel(impl, cq, impl.get2dFrame().getVisuPanel());
    CtuluDialog di = new CtuluDialog(impl.getFrame(), pn);
    di.setOption(CtuluDialog.APPLY_CANCEL_OPTION);
    di.setButtonText(CtuluDialog.APPLY_OPTION, EbliLib.getS("Exporter..."));
    di.setButtonIcon(CtuluDialog.APPLY_OPTION, BuResource.BU.getMenuIcon("exporter"));
    di.setButtonText(CtuluDialog.CANCEL_OPTION, PivResource.getS("Termin�"));
    di.setButtonIcon(CtuluDialog.CANCEL_OPTION, BuResource.BU.getIcon("valider"));
    di.setTitle(PivResource.getS("Export des r�sultats vers Excel"));
    di.afficheDialogModal();
  }
  
  /**
   * @return Le mapping de nom entre les variables et les noms T�lemac.
   * Necessaire pour la reconnaissance des donn�es pour l'affichage des vecteurs.
   * 
   */
  protected HashMap<CtuluVariable, String> getTelemacVarNamesMapping() {
    HashMap<CtuluVariable, String> var2TelemacNames=new HashMap<>();
    
    var2TelemacNames.put(PivResultsI.ResultType.VX, "VELOCITY U");
    var2TelemacNames.put(PivResultsI.ResultType.VY, "VELOCITY V");
    var2TelemacNames.put(PivResultsI.ResultType.NORME, "SCALAR VELOCITY");
    // Les autres variables n'ont pas d'�quivalence sp�cifiques.
    
    return var2TelemacNames;
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Les r�sultats doivent exister");
  }
}
