/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;
import java.io.File;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.io.PivHReader;
import org.fudaa.fudaa.piv.io.PivImgRefReader;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;
import org.fudaa.fudaa.piv.metier.PivProject.OrthoMode;
import org.fudaa.fudaa.piv.utils.PivUtils;

/**
 * Une action pour importer les parametres de transformation.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivImportGridAction.java 9083 2015-03-17 13:05:54Z bmarchan $
 */
public class PivImportOrthoParamAction extends EbliActionSimple {
  PivImplementation impl;
  CtuluFileChooser fcParams;

  /**
   * Constructeur
   * @param _impl L'implementation.
   */
  public PivImportOrthoParamAction(PivImplementation _impl) {
    super(PivResource.getS("Import des param�tres d'orthorectification..."), null, "IMPORT_ORTHO_PARAM");

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Selectionne le fichier d'import, et remplace les parametres.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }

    // Selection du fichier
    if (fcParams == null) {
      fcParams=new CtuluFileChooser(true);
      fcParams.setAcceptAllFileFilterUsed(true);
      fcParams.setFileFilter(PivUtils.FILE_FLT_TRANSF_PARAMS);
      // Pas terrible, mais evite de redonner le nom manuellement.
      fcParams.setSelectedFile(new File(PivUtils.FILE_FLT_TRANSF_PARAMS.getFirstExt()));
      fcParams.setMultiSelectionEnabled(false);
      String mode = impl.getCurrentProject().getOrthoMode() == OrthoMode.ORTHO_2D ? PivResource.getS("Ortho 2D"):PivResource.getS("Ortho 3D");
      fcParams.setDialogTitle(PivResource.getS("S�lection d'un fichier de parametres de transformation") + " (" + mode + ")");
    }
    
    if (fcParams.showOpenDialog(impl.getFrame()) == CtuluFileChooser.CANCEL_OPTION) {
      return;
    }
    File imgrefFile=fcParams.getSelectedFile();

    CtuluIOResult<PivOrthoParameters> ret = new PivImgRefReader().read(imgrefFile, null);
    if (ret.getAnalyze().containsErrorOrSevereError()) {
      impl.error(ret.getAnalyze().getResume());
      return;
    }
    
    PivOrthoParameters params=ret.getSource();
    
    // Lecture du fichier h.dat associ�.
    File hFile = new File(imgrefFile.getParentFile(), "h.dat");
    if (hFile.exists()) {
      CtuluIOResult<Double> ret2 = new PivHReader().read(hFile, null);
      if (ret.getAnalyze().containsErrorOrSevereError()) {
        impl.error(ret.getAnalyze().getResume());
        return;
      }

      params.setWaterElevation(ret2.getSource());
    }
    else {
      impl.warn(PivResource.getS("Fichier manquant"), PivResource.getS("Le fichier h.dat correspondant est manquant.\nLe niveau d'eau ne sera pas initialis�."));
    }

    impl.getCurrentProject().setOrthoParameters(params);

    impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Il doit exister au moins une image source");
  }
}
