/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluFileChooserTestWritable;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivExportGaugingReportFillValuesPanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivPreferences;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivShow3DTransectPanel;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.metier.PivComputeParameters;
import org.fudaa.fudaa.piv.metier.PivFlowResults;
import org.fudaa.fudaa.piv.metier.PivGlobalFlowResults;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;
import org.fudaa.fudaa.piv.metier.PivSamplingVideoParameters;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters.StabilizationPointsDensity;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters.StabilizationTransformationModel;
import org.fudaa.fudaa.piv.metier.PivStatisticParameters;
import org.fudaa.fudaa.piv.metier.PivStatisticParameters.PivStatisticCalcultationMethod;
import org.fudaa.fudaa.piv.metier.PivTransect;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.bu.BuPreferences;
import com.memoire.fu.FuLog;

/**
 * Une action pour exporter le rapport de jaugeage
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivExportImagesAction.java 9320 2016-03-23 15:04:50Z bmarchan $
 */
public class PivExportGaugingReportAction extends EbliActionSimple {
  
  public static interface AbstractCellData {
    public abstract Object getValue(int _row, int _col);
    
    public abstract int getNbCols();
    
    public abstract int getNbRows();
  }
  
  public static abstract class AbstractScalarCellData implements AbstractCellData {
    public AbstractScalarCellData() {}
    
    @Override
    public int getNbCols() {
      return 1;
    }
    
    @Override
    public int getNbRows() {
      return 1;
    }
  }
  
  public static class UserCellData extends AbstractScalarCellData {
    public String name;
    public String value;
    public String description;
    
    public UserCellData(String _name, String _description) {
      super();
      name = _name;
      description=_description;
    }
    
    public void setValue(String _value) {
      value=_value;
    }

    @Override
    public String getValue(int _row, int _col) {
      return value;
    }
  }
  
  /**
   * Un data retournant une image.
   * @author Bertrand Marchand (marchand@detacad.fr)
   */
  public abstract static class ImageCellData extends AbstractScalarCellData {
    @Override
    public abstract byte[] getValue(int _row, int _col);
  }
  
  PivImplementation impl;
//  CtuluDialog diProgress_;
  CtuluFileChooser fcReport_;
  /** Les variables users (inconnues du systeme) du modele Excel. */
  HashMap<String,UserCellData> userVars=new HashMap<>();
  /** Le nom fichier projet pour changer le nom du rapport. */
  File projectFile_;
  
  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivExportGaugingReportAction(PivImplementation _impl) {
    super(PivResource.getS("Exporter le rapport de jaugeage..."), null, "EXPORT_GAUGING_REPORT");

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Exporte les images transform�es au format pgm.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }

    try {
      // Nettoie les valeurs � chaque changement du nom de projet.
      if (isProjectFileModified())
        userVars.clear();

      computeNeededDatas();
      HashMap<String, AbstractCellData> systemDatas=defineSystemVariables();

      String templateDir=System.getProperty(PivPreferences.PIV_TEMPLATES_PATH, System.getProperty("user.dir") + "/templates");
      String templateName="template_gauging_" + BuPreferences.BU.getStringProperty("locale.language", System.getProperty("piv.lang", "fr")) + ".xlsx";

      File ftemplate=new File(templateDir, templateName);
      if (!ftemplate.exists()) {
        impl.error(PivResource.getS("Erreur mod�le"), PivResource.getS("Le mod�le {0} n'existe pas", ftemplate));
        return;
      }

      Workbook wb = null;
      try (FileInputStream fis = new FileInputStream(ftemplate)) {
        wb = new XSSFWorkbook(fis);
      }
      CreationHelper ch = wb.getCreationHelper();
      
      // Le nombre de cellules nomm�es
      int nbnames=wb.getNumberOfNames();

      // Recup�ration depuis le template des variables utilisateurs.
      
      for (Name namedCell : wb.getAllNames()) {
//      for (int i=0; i < nbnames; i++) {
//        Name namedCell=wb.getNameAt(i);

        String cellname=namedCell.getNameName();
        FuLog.debug(cellname);
        
        int ind;
        if ((ind=cellname.indexOf('.')) != -1) {
          cellname=cellname.substring(0, ind);
        }

        if (!systemDatas.containsKey(cellname)) {
          String desc=namedCell.getComment();
          if (desc == null || desc.isEmpty())
            desc=cellname;

          if (!userVars.containsKey(cellname))
            userVars.put(cellname, new UserCellData(cellname, desc));
        }
      }

      // Saisie des valeurs depuis la table des cellules nomm�es utilisateur.
      
      if (!userVars.isEmpty()) {
        PivExportGaugingReportFillValuesPanel diValues=new PivExportGaugingReportFillValuesPanel(impl, userVars.values(), systemDatas.keySet());
        if (diValues.afficheModale(impl.getFrame(), PivResource.getS("Champs � renseigner")) == JOptionPane.CANCEL_OPTION)
          return;
      }

      HashMap<String,AbstractCellData> allVars = new HashMap<>();
      allVars.putAll(systemDatas);
      allVars.putAll(userVars);

      // Remplissage des cellules nomm�es du template depuis les datas
      
      HashMap<String, HashMap<String, CellRangeAddress>> rowCol2CellAdress=new HashMap<>();

      for (Name namedCell : wb.getAllNames()) {
//      for (int i=0; i < nbnames; i++) {
//        Name namedCell=wb.getNameAt(i);

        String cellname=namedCell.getNameName();
        int ind;
        if ((ind=cellname.indexOf('.')) != -1) {
          cellname=cellname.substring(0, ind);
        }

        // Recuperation la premiere cellule de la zone nomm�e
        
        AreaReference aref=new AreaReference(namedCell.getRefersToFormula(), SpreadsheetVersion.EXCEL2007);
        // Generalement TOP/LEFT
        CellReference crefTL=aref.getFirstCell();
        // Generalement BOTTOM/RIGHT
        CellReference crefBR=aref.getLastCell();
        Sheet currentSheet=wb.getSheet(crefTL.getSheetName());
        Drawing drawing = currentSheet.createDrawingPatriarch();

        // Ces map servent a faire le lien entre des cellules et des cellules fusionn�es. Utilis� pour les tableaux.
        HashMap<String, CellRangeAddress> rc2CellAdress=rowCol2CellAdress.get(crefTL.getSheetName());
        if (rc2CellAdress == null) {
          rc2CellAdress=new HashMap<>();
          for (int imr=0; imr < currentSheet.getNumMergedRegions(); imr++) {
            CellRangeAddress cra=currentSheet.getMergedRegion(imr);
            rc2CellAdress.put(cra.getFirstRow() + "_" + cra.getFirstColumn(), cra);
          }

          rowCol2CellAdress.put(crefTL.getSheetName(), rc2CellAdress);
        }

        int firstRow=crefTL.getRow();
        int firstCol=crefTL.getCol();

        // Ajoute toutes les valeurs de la variable dans les zones nomm�es.
        
        AbstractCellData cellWrapper=allVars.get(cellname);
        
        // Une donn�e de type image.
        if (cellWrapper instanceof ImageCellData) {
          ImageCellData imgWrapper = (ImageCellData)cellWrapper;
          byte[] bytes=imgWrapper.getValue(0, 0);
          
          if (bytes==null) {
            Row r=currentSheet.getRow(firstRow);
            Cell c=r.getCell(firstCol);
            c.setCellValue("-");
          }
          else {
            int ipict=wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
            ClientAnchor anchor=ch.createClientAnchor();
            anchor.setRow1(firstRow);
            anchor.setCol1(firstCol);
            anchor.setRow2(crefBR.getRow() + 1);
            anchor.setCol2(crefBR.getCol() + 1);

            drawing.createPicture(anchor, ipict);
          }
        }
        
        // Une donn�e de type valeur.
        else {
          int currentRow=firstRow;
          for (int irow=0; irow < cellWrapper.getNbRows(); irow++) {
            int currentCol=firstCol;
            for (int icol=0; icol < cellWrapper.getNbCols(); icol++) {
              Row r=currentSheet.getRow(currentRow);
              Cell c=r.getCell(currentCol);
              if (c != null) {
                Object val=cellWrapper.getValue(irow, icol);
                c.setCellValue(val == null ? "-" : val.toString().trim());
              }
              CellRangeAddress cra=rc2CellAdress.get(currentRow + "_" + currentCol);
              if (cra == null) {
                currentCol++;
              }
              else {
                currentCol+=cra.getLastColumn() - cra.getFirstColumn() + 1;
              }
            }
            CellRangeAddress cra=rc2CellAdress.get(currentRow + "_" + firstCol);
            if (cra == null) {
              currentRow++;
            }
            else {
              currentRow+=cra.getLastRow() - cra.getFirstRow() + 1;
            }
          }
        }
      }

      // Lock et ecriture du fichier final
      
      for (int i=0; i<wb.getNumberOfSheets(); i++) {
        XSSFSheet sheet = (XSSFSheet)wb.getSheetAt(i);
        sheet.protectSheet("__LSPIV__");
        sheet.lockDeleteColumns(true);
        sheet.lockDeleteRows(true);
        sheet.lockFormatCells(true);
        sheet.lockFormatColumns(true);
        sheet.lockFormatRows(true);
        sheet.lockInsertColumns(true);
        sheet.lockInsertRows(true);
        sheet.enableLocking();
      }
      
      saveReport(wb);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Sauve le rapport sur le fichier selectionn�, et l'ouvre automatiquement.
   * @param wb
   * @throws IOException
   */
  protected void saveReport(Workbook wb) throws IOException {
    
    if (fcReport_ == null) {
      fcReport_=new CtuluFileChooser(true);
      fcReport_.setTester(new CtuluFileChooserTestWritable(impl));
      fcReport_.setAcceptAllFileFilterUsed(true);
      fcReport_.setFileFilter(PivUtils.FILE_FLT_XLSX);
      fcReport_.setMultiSelectionEnabled(false);
      fcReport_.setDialogTitle(PivResource.getS("Ficher Excel d'export"));
    }

    // Changement de projet => On change le nom du rapport
    if (isProjectFileModified()) {
      projectFile_=impl.getSavedProjectFile();
      
      String fsave = PivUtils.getFilenameWithoutExtension(impl.getSavedProjectFile().getName(), PivUtils.FILE_FLT_PROJ.getExtensions());
      fcReport_.setSelectedFile(new File(fsave + "_" + PivResource.getS("rapport") + ".xlsx"));
    }

    if (fcReport_.showSaveDialog(impl.getFrame()) == JFileChooser.CANCEL_OPTION) {
      return;
    }

    try (FileOutputStream fs = new FileOutputStream(fcReport_.getSelectedFile())) {
      wb.write(fs);
    }
    
    // Ouverture directe de l'application associ�e au fichier .xlsx
    if (Desktop.isDesktopSupported()) {
      try {
        Desktop.getDesktop().open(fcReport_.getSelectedFile());
      }
      catch (Exception _exc) {
      }
    }
  }

  /**
   * Calcule les donn�es non stock�es dans le projet, necessaires � la r�alisation du template.
   */
  protected void computeNeededDatas() {
    // Calcul des erreurs
    if (impl.getCurrentProject().getOrthoPoints() != null) {
      CtuluLog ana=new CtuluLog();
      ana.setDesc(PivResource.getS("V�rification des points d'orthorectification"));
//      PivExeLauncher.instance().computeOrthoCoefs(ana, impl.getCurrentProject(), null);
//      if (ana.containsErrorOrSevereError()) {
//        impl.error(ana.getResume());
//        return;
//      }
      PivExeLauncher.instance().computeVerifOrtho(ana, impl.getCurrentProject(), null);
      if (ana.containsErrorOrSevereError()) {
        impl.error(ana.getResume());
        return;
      }
    }
  }
  
  /**
   * Definit les variables connues du systeme.
   */
  public HashMap<String, AbstractCellData> defineSystemVariables() {
    HashMap<String, AbstractCellData> systemVars_=new HashMap<>();
    
    final PivTransect[] transects=impl.getCurrentProject().getTransects();
    final PivComputeParameters pivParams = impl.getCurrentProject().getComputeParameters();
    final PivFlowResults[] flowResults = impl.getCurrentProject().getFlowResults();
    final PivOrthoPoint[] orthoPoints = impl.getCurrentProject().getOrthoPoints();
    final PivOrthoParameters orthoParams = impl.getCurrentProject().getOrthoParameters();
    final PivSamplingVideoParameters samplingParams = impl.getCurrentProject().getSamplingParameters();
    final PivStabilizationParameters stabParams = impl.getCurrentProject().getStabilizationParameters();
    final PivStatisticParameters statParams = impl.getCurrentProject().getStatisticParameters();
    
    final PivGlobalFlowResults globRes = new PivGlobalFlowResults(flowResults);
    final Double meanCoef = globRes.getAverageScalarResult(ResultType.COMPUTED_VEL_COEFFICIENT);
    final Double meanDisch = globRes.getAverageScalarResult(ResultType.FULL_DISCHARGE);
    final Double meanArea = globRes.getAverageScalarResult(ResultType.WETTED_AREA);
    final Double meanMeanV = globRes.getAverageScalarResult(ResultType.MEAN_VELOCITY);
    final Double meanMeasuredDisch = globRes.getAverageScalarResult(ResultType.MEASURED_DISCHARGE);
    
    final HashMap<String,Object> imgOptions = new HashMap<>();
    imgOptions.put(CtuluLibImage.PARAMS_FILL_BACKGROUND_BOOLEAN, Boolean.FALSE);
    
    final DecimalFormat fmt3Digit = CtuluLib.getDecimalFormat(3);
    final DecimalFormat fmt1Digit = CtuluLib.getDecimalFormat(1);
    final DecimalFormat fmtEcart = CtuluLib.getNoEffectDecimalFormat();
    fmtEcart.setMaximumFractionDigits(1);
    fmtEcart.setMinimumFractionDigits(1);
    fmtEcart.setPositivePrefix("+");
    final DecimalFormat fmtExp=new DecimalFormat("#");
    fmtExp.setMaximumFractionDigits(3);
    fmtExp.setMinimumFractionDigits(2);
    
    systemVars_.put("__AVERAGE_METHOD", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        if (statParams==null)
          return null;
        
        return statParams.getMethod() == PivStatisticCalcultationMethod.MEAN ? PivResource.getS("Moyenne") : PivResource.getS("M�diane");
      }
    });
    systemVars_.put("__COEF_VITESSE_MAX", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        if (flowResults==null || flowResults.length==0)
          return null;
        
        double val=Double.NEGATIVE_INFINITY;
        for (PivFlowResults res : flowResults) {
          val=Math.max(val, res.getValue(-1, ResultType.COMPUTED_VEL_COEFFICIENT));
        }
        return format(fmt3Digit,val,null);
      }
    });
    systemVars_.put("__COEF_VITESSE_MIN", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        if (flowResults==null || flowResults.length==0)
          return null;
        
        double val=Double.POSITIVE_INFINITY;
        for (PivFlowResults res : flowResults) {
          val=Math.min(val, res.getValue(-1, ResultType.COMPUTED_VEL_COEFFICIENT));
        }
        return format(fmt3Digit,val,null);
      }
    });
    systemVars_.put("__CORREL_ACTIF", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams!=null && pivParams.getFilters().isCorrelationFilterEnabled() ? PivResource.getS("Actif") : PivResource.getS("Non actif");
      }
    });
    systemVars_.put("__CORREL_MAX",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || !pivParams.getFilters().isCorrelationFilterEnabled() ? null:""+format(fmt3Digit,pivParams.getFilters().getMaxCorrelation(),null);
      }
    });
    systemVars_.put("__CORREL_MIN",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || !pivParams.getFilters().isCorrelationFilterEnabled() ? null:""+format(fmt3Digit,pivParams.getFilters().getMinCorrelation(),null);
      }
    });
    systemVars_.put("__DEBIT_MOYEN",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return meanDisch==null ? null:format(fmt3Digit,meanDisch,"m�/s");
      }
    });
    systemVars_.put("__DEBUT_VIDEO", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return samplingParams==null ? null:format(fmt3Digit,samplingParams.getSamplingBeginTime(),"s");
      }
    });
    systemVars_.put("__DISPERS_ANGLE_ACTIF", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams!=null && pivParams.getFilters().isAngularDispersionFilterEnabled() ? PivResource.getS("Actif") : PivResource.getS("Non actif");
      }
    });
    systemVars_.put("__DISPERS_ANGLE_CIRCVARMAX",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || !pivParams.getFilters().isAngularDispersionFilterEnabled() ? null:""+format(fmt3Digit,pivParams.getFilters().getCircvarmax(),null);
      }
    });
    systemVars_.put("__DISPERS_VITESSE_ACTIF", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams!=null && pivParams.getFilters().isVelocityDispersionFilterEnabled() ? PivResource.getS("Actif") : PivResource.getS("Non actif");
      }
    });
    systemVars_.put("__DISPERS_VITESSE_COVMAX",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || !pivParams.getFilters().isVelocityDispersionFilterEnabled() ? null:""+format(fmt3Digit,pivParams.getFilters().getCovmax(),null);
      }
    });
    systemVars_.put("__DISTRIB_VITESSE_ACTIF", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams!=null && pivParams.getFilters().isVelocityDistributionFilterEnabled() ? PivResource.getS("Actif") : PivResource.getS("Non actif");
      }
    });
    systemVars_.put("__DISTRIB_VITESSE_NSTDVEL",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || !pivParams.getFilters().isVelocityDistributionFilterEnabled() ? null:""+pivParams.getFilters().getNstdvel();
      }
    });
    systemVars_.put("__ERREUR_MAX",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        if (orthoPoints==null)
          return null;
        
        double maxi=Double.NEGATIVE_INFINITY;
        for (PivOrthoPoint pt : orthoPoints) {
          maxi=Math.max(maxi, pt.getError());
        }
        return format(fmt3Digit,maxi,"m");
      }
    });
    systemVars_.put("__FIN_VIDEO", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return samplingParams==null ? null:format(fmt3Digit,samplingParams.getSamplingEndTime(),"s");
      }
    });
    systemVars_.put("__HAUTEUR",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return orthoParams==null ? null:format(fmt3Digit,orthoParams.getWaterElevation(),"m");
      }
    });
    systemVars_.put("__HAUTEUR_VIDEO", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return samplingParams==null ? null:samplingParams.getSamplingSize().height + " pix";
      }
    });
    systemVars_.put("__IA_M",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return (pivParams==null || pivParams.getIASize() == null || orthoParams==null) ? null:format(fmt3Digit,pivParams.getIASize()*orthoParams.getResolution(),"m");
      }
    });
    systemVars_.put("__IA_PIX",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || pivParams.getIASize() == null ? null:pivParams.getIASize()+" pix";
      }
    });
    systemVars_.put("__INTERTEMPS",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || pivParams.getTimeInterval() == null ? null:format(fmt3Digit,pivParams.getTimeInterval(),"s");
      }
    });
    systemVars_.put("__LARGEUR_VIDEO", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return samplingParams==null ? null:samplingParams.getSamplingSize().width + " pix";
      }
    });
    systemVars_.put("__MODE_ORTHO", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        switch (impl.getCurrentProject().getOrthoMode()) {
        case SCALING:
          return PivResource.getS("Mise � l'�chelle");
        default:
        case ORTHO_2D:
          return PivResource.getS("Ortho 2D");
        case ORTHO_3D:
          return PivResource.getS("Ortho 3D");
        }
      }
    });
    systemVars_.put("__NOM_PROJET",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return impl.getCurrentProjectFile()==null ? null:impl.getCurrentProjectFile().getName();
      }
    });
    systemVars_.put("__NOM_VIDEO", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return samplingParams==null ? null:samplingParams.getVideoFile().getName();
      }
    });
    systemVars_.put("__NOMBRE_GRPS", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return orthoPoints==null ? null:orthoPoints.length+"";
      }
    });
    systemVars_.put("__NOMBRE_IMAGES", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return ""+impl.getCurrentProject().getSrcImageFiles(PivProject.SRC_IMAGE_TYPE.CALCULATION).length;
      }
    });
    systemVars_.put("__NOMBRE_IMAGES_SECONDE", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || pivParams.getTimeInterval() == null  ? null:format(fmt3Digit,1./pivParams.getTimeInterval(),null);
      }
    });
    systemVars_.put("__NOMBRE_TRANSECTS", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return transects==null ? null:transects.length+"";
      }
    });
    systemVars_.put("__NORME_ACTIF", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams!=null && pivParams.getFilters().isVelocityFilterEnabled() ? PivResource.getS("Actif") : PivResource.getS("Non actif");
      }
    });
    systemVars_.put("__NORME_MAX", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || !pivParams.getFilters().isVelocityFilterEnabled() ? null:format(fmt3Digit,pivParams.getFilters().getSmax(),"m/s");
      }
    });
    systemVars_.put("__NORME_MIN", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || !pivParams.getFilters().isVelocityFilterEnabled() ? null:format(fmt3Digit,pivParams.getFilters().getSmin(),"m/s");
      }
    });
    systemVars_.put("__PAS_INTERPOLATION_MAX", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        if (transects==null || transects.length==0)
          return null;
        
        double val=Double.NEGATIVE_INFINITY;
        for (PivTransect trans : transects) {
          val=Math.max(val, trans.getParams().getInterpolationStep());
        }
        return format(fmt3Digit,val,"m");
      }
    });
    systemVars_.put("__PAS_INTERPOLATION_MIN", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        if (transects==null || transects.length==0)
          return null;
        
        double val=Double.POSITIVE_INFINITY;
        for (PivTransect trans : transects) {
          val=Math.min(val, trans.getParams().getInterpolationStep());
        }
        return format(fmt3Digit,val,"m");
      }
    });
    systemVars_.put("__PIC_CORREL_ACTIF", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams!=null && pivParams.getFilters().isCorrelationPeakFilterEnabled() ? PivResource.getS("Actif") : PivResource.getS("Non actif");
      }
    });
    systemVars_.put("__PIC_CORREL_RCUT",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || !pivParams.getFilters().isCorrelationPeakFilterEnabled() ? null:""+format(fmt3Digit,pivParams.getFilters().getRcut(),null);
      }
    });
    systemVars_.put("__PIC_CORREL_RH0MAX",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || !pivParams.getFilters().isCorrelationPeakFilterEnabled() ? null:""+format(fmt3Digit,pivParams.getFilters().getRhomax(),null);
      }
    });
    systemVars_.put("__PERIODE_VIDEO", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return samplingParams==null ? null:"1 / "+samplingParams.getSamplingPeriod();
      }
    });
    systemVars_.put("__RATIO_DEBIT_MESURE", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return meanMeasuredDisch==null ? null:format(fmt1Digit,meanMeasuredDisch/meanDisch*100, "%");
      }
    });
    systemVars_.put("__RAYON_RECHERCHE_MAX", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        if (transects==null || transects.length==0)
          return null;
        
        double val=Double.NEGATIVE_INFINITY;
        for (PivTransect trans : transects) {
          val=Math.max(val, trans.getParams().getRadiusX());
        }
        return format(fmt3Digit,val,"m");
      }
    });
    systemVars_.put("__RAYON_RECHERCHE_MIN", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        if (transects==null || transects.length==0)
          return null;
        
        double val=Double.POSITIVE_INFINITY;
        for (PivTransect trans : transects) {
          val=Math.min(val, trans.getParams().getRadiusX());
        }
        return format(fmt3Digit,val,"m");
      }
    });
    systemVars_.put("__RESOLUTION", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return orthoParams==null ? null:format(fmt3Digit,orthoParams.getResolution(),"m/pix");
      }
    });
    systemVars_.put("__TB_RESULTATS_DEBIT", new AbstractCellData() {
      @Override
      public String getValue(int rowIndex, int _col) {
        if (flowResults==null || flowResults.length==0)
          return null;
        
        // Une ligne blanche entre les r�sultats et la moyenne
        if (rowIndex==flowResults.length)
          return "";
        
        Double val;
        
        switch (_col) {
        default:
        case 0:
          return rowIndex==flowResults.length+1? PivResource.getS("Moyenne"):""+(rowIndex+1);
        case 1:
          val=rowIndex==flowResults.length+1 ? meanCoef : flowResults[rowIndex].getValue(-1,  ResultType.COMPUTED_VEL_COEFFICIENT); 
          return format(fmt3Digit, val,null);
        case 2:
          val=rowIndex==flowResults.length+1? meanDisch:flowResults[rowIndex].getDischarge();
          return format(fmt3Digit, val,null);
        case 3:
          val=rowIndex==flowResults.length+1? meanDisch:flowResults[rowIndex].getDischarge();
          val = (val-meanDisch)/meanDisch*100;
          return format(fmtEcart, val,null);
        case 4:
          val = rowIndex==flowResults.length+1? meanArea:flowResults[rowIndex].getWettedArea();
          return format(fmt3Digit, val,null);
        case 5:
          val=rowIndex==flowResults.length+1? meanArea:flowResults[rowIndex].getWettedArea();
          val = (val-meanArea)/meanArea*100;
          return format(fmtEcart, val,null);
        case 6:
          val = rowIndex==flowResults.length+1? meanMeanV:flowResults[rowIndex].getMeanVelocity();
          return format(fmt3Digit, val,null);
        case 7:
          val=rowIndex==flowResults.length+1? meanMeanV:flowResults[rowIndex].getMeanVelocity();
          val = (val-meanMeanV)/meanMeanV*100;
          return format(fmtEcart, val,null);
//        case 7:
//          val = rowIndex==flowResults.length+1? meanMeasuredDisch:flowResults[rowIndex].getValue(-1,ResultType.MEASURED_DISCHARGE);
//          return format(fmt3Digit, val,null);
        case 8:
          val=rowIndex==flowResults.length+1? meanMeasuredDisch/meanDisch:flowResults[rowIndex].getValue(-1,ResultType.MEASURED_DISCHARGE)/flowResults[rowIndex].getDischarge();
          val*=100;
          return format(fmt1Digit, val,null);
        }
      }

      @Override
      public int getNbCols() {
        return 9;
      }

      @Override
      public int getNbRows() {
        return transects==null ? 0 : transects.length+2;
      }
    });
    systemVars_.put("__SIM_MS", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        if (orthoParams!=null && pivParams!=null) {
          double val;
          if (pivParams.getTimeInterval()==null || pivParams.getSim() == null || pivParams.getTimeInterval() == 0) {
            val = 0;
          }
          else {
            val = orthoParams.getResolution()*pivParams.getSim()/pivParams.getTimeInterval();
          }
          
          return format(fmt3Digit, val,"m/s");
        }
        return null;
      }
    });
    systemVars_.put("__SIM_PIX", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || pivParams.getSim() == null ? null : pivParams.getSim() +" pix";
      }
    });
    systemVars_.put("__SIP_MS", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        if (orthoParams!=null && pivParams!=null) {
          double val;
          if (pivParams.getTimeInterval()==null || pivParams.getSip() == null || pivParams.getTimeInterval()==0) {
            val = 0;
          }
          else {
            val = orthoParams.getResolution()*pivParams.getSip()/pivParams.getTimeInterval();
          }
          
          return format(fmt3Digit, val,"m/s");
        }
        return null;
      }
    });
    systemVars_.put("__SIP_PIX", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || pivParams.getSip() == null  ? null:pivParams.getSip()+" pix";
      }
    });
    systemVars_.put("__SJM_MS", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        if (orthoParams!=null && pivParams!=null) {
          double val;
          if (pivParams.getTimeInterval()==null || pivParams.getSjm() == null || pivParams.getTimeInterval()==0) {
            val = 0;
          }
          else {
            val = orthoParams.getResolution()*pivParams.getSjm()/pivParams.getTimeInterval();
          }
          
          return format(fmt3Digit, val,"m/s");
        }
        return null;
      }
    });
    systemVars_.put("__SJM_PIX", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || pivParams.getSjm()==null ? null:pivParams.getSjm()+" pix";
      }
    });
    systemVars_.put("__SJP_MS", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        if (orthoParams!=null && pivParams!=null) {
          double val;
          if (pivParams.getTimeInterval()==null || pivParams.getSjp() == null || pivParams.getTimeInterval()==0) {
            val = 0;
          }
          else {
            val = orthoParams.getResolution()*pivParams.getSjp()/pivParams.getTimeInterval();
          }
          
          return format(fmt3Digit, val,"m/s");
        }
        return null;
      }
    });
    systemVars_.put("__SJP_PIX", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || pivParams.getSjp() == null ? null:pivParams.getSjp()+" pix";
      }
    });
    systemVars_.put("__STABILISATION_ACTIVE", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return stabParams != null && stabParams.isActive() ? PivResource.getS("Active") : PivResource.getS("Non active");
      }
    });
    systemVars_.put("__STABILISATION_DENSITE", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        if (stabParams == null) {
          return null;
        }
        else if (stabParams.getDensity() == StabilizationPointsDensity.HIGH) {
          return PivResource.getS("Forte");
        }
        else if (stabParams.getDensity() == StabilizationPointsDensity.MEDIUM) {
          return PivResource.getS("Interm�diaire");
        }
        else if (stabParams.getDensity() == StabilizationPointsDensity.LOW) {
          return PivResource.getS("Faible");
        }
        return null;
      }
    });
    systemVars_.put("__STABILISATION_MODELE", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        if (stabParams == null) {
          return null;
        }
        else if (stabParams.getModel() == StabilizationTransformationModel.PROJECTIVE) {
          return PivResource.getS("Projective");
        }
        else if (stabParams.getModel() == StabilizationTransformationModel.SIMILARITY) {
          return PivResource.getS("Similitude");
        }
        return null;
      }
    });
    systemVars_.put("__TEST_MEDIAN_ACTIF", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams!=null && pivParams.getFilters().isMedianTestFilterEnabled() ? PivResource.getS("Actif") : PivResource.getS("Non actif");
      }
    });
    systemVars_.put("__TEST_MEDIAN_EPSILON",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || !pivParams.getFilters().isMedianTestFilterEnabled() ? null:""+format(fmt3Digit,pivParams.getFilters().getEpsilon(),null);
      }
    });
    systemVars_.put("__TEST_MEDIAN_R0MIN",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || !pivParams.getFilters().isMedianTestFilterEnabled() ? null:""+format(fmt3Digit,pivParams.getFilters().getR0min(),null);
      }
    });
    systemVars_.put("__VERSION_APPLICATION",new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return impl.getInformationsSoftware().version;
      }
    });
    systemVars_.put("__VUE_2D", new ImageCellData() {
      
      @Override
      public byte[] getValue(int _row, int _col) {
        impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);
        BufferedImage img=impl.get2dFrame().getVisuPanel().produceImage(imgOptions);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
          ImageIO.write(img, "png", bos);
          return bos.toByteArray();
        }
        catch (IOException e) {
          return null;
        }
      }
    });
    systemVars_.put("__VUE_3D", new ImageCellData() {
      
      @Override
      public byte[] getValue(int _row, int _col) {
        
        PivShow3DTransectPanel pn=new PivShow3DTransectPanel(impl);
        
        if (transects != null && transects.length != 0) {
          int[] isels=new int[transects.length];
          for (int i=0; i < transects.length; i++) {
            isels[i]=i;
          }
          pn.setSelectedTransects(isels);
        }
        
        pn.get3dGraph().setSize(500, 500);
        BufferedImage img=CtuluLibImage.produceImageForComponent(pn.get3dGraph(),imgOptions);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
          ImageIO.write(img, "png", bos);
          return bos.toByteArray();
        }
        catch (IOException e) {
          return null;
        }
      }
    });
    systemVars_.put("__VX_ACTIF", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams!=null && pivParams.getFilters().isVelocityFilterEnabled() ? PivResource.getS("Actif") : PivResource.getS("Non actif");
      }
    });
    systemVars_.put("__VX_MAX", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || !pivParams.getFilters().isVelocityFilterEnabled() ? null:format(fmt3Digit, pivParams.getFilters().getVxmax(), "m/s");
      }
    });
    systemVars_.put("__VX_MIN", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || !pivParams.getFilters().isVelocityFilterEnabled() ? null:format(fmt3Digit, pivParams.getFilters().getVxmin(), "m/s");
      }
    });
    systemVars_.put("__VY_ACTIF", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams!=null && pivParams.getFilters().isVelocityFilterEnabled() ? PivResource.getS("Actif") : PivResource.getS("Non actif");
      }
    });
    systemVars_.put("__VY_MAX", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || !pivParams.getFilters().isVelocityFilterEnabled() ? null:format(fmt3Digit, pivParams.getFilters().getVymax(),"m/s");
      }
    });
    systemVars_.put("__VY_MIN", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return pivParams==null || !pivParams.getFilters().isVelocityFilterEnabled() ? null:format(fmt3Digit, pivParams.getFilters().getVymin(),"m/s");
      }
    });
    systemVars_.put("__X_MAX", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return orthoParams==null ? null:format(fmt3Digit, orthoParams.getXmax(),"m");
      }
    });
    systemVars_.put("__X_MIN", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return orthoParams==null ? null:format(fmt3Digit, orthoParams.getXmin(),"m");
      }
    });
    systemVars_.put("__Y_MAX", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return orthoParams==null ? null:format(fmt3Digit, orthoParams.getYmax(),"m");
      }
    });
    systemVars_.put("__Y_MIN", new AbstractScalarCellData() {
      @Override
      public String getValue(int _row, int _col) {
        return orthoParams==null ? null:format(fmt3Digit, orthoParams.getYmin(),"m");
      }
    });
    
    return systemVars_;
  }
  
  /**
   * Formatte le champs double en entr�e
   * @param _fmt Le format
   * @param _val La valeur a formatter.
   * @param _unit L'unit�, ou null si aucune
   * @return Si la valeur vaux Double.NaN, retourne NaN.
   */
  private String format(DecimalFormat _fmt, Double _val, String _unit) {
    String ret;
    if (_val.isNaN()) {
      ret=_val.toString();
    }
    else {
      ret=_fmt.format(_val);
    }
    
    // C'est pas terrible, mais on passe en notation scientifique pour les grands nombres.
    if (ret.length()>10) {
      ret=String.format(Locale.US, "%10.3E", _val);
    }
    
    if (_unit!=null) {
      ret+=" "+_unit;
    }
    return ret;
  }

  /**
   * @return True si le nom du projet a �t� modifi�.
   */
  private boolean isProjectFileModified() {
    return !impl.getSavedProjectFile().equals(projectFile_);
  }
  
  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Les images doivent exister");
  }
}
