/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivExportImageFilesPanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivProgressionPanel;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivTaskAbstract;
import org.fudaa.fudaa.piv.PivTaskObserverI;
import org.fudaa.fudaa.piv.imageio.PivPGMWriter;
import org.fudaa.fudaa.piv.metier.PivProject;

import com.memoire.fu.FuLog;

/**
 * Une action pour eporter les images transformées ou sources
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivExportImagesAction extends EbliActionSimple {
  
  PivImplementation impl;
  PivExportImageFilesPanel pnExportImages;
  CtuluDialog diProgress_;
  int bexport;
  int exportType_;

  public final static int EXPORT_SRC = 0;
  public final static int EXPORT_TRANSF = 1;
  public final static int EXPORT_STAB = 2;
  
  static final String TXTEXPORTERSRC=PivResource.getS("Exporter les images sources");
  static final String TXTEXPORTERTRANSF=PivResource.getS("Exporter les images transformées");
  static final String TXTEXPORTERSTAB=PivResource.getS("Exporter les images stabilisées");
  static final String TXTEXPORTSRC=PivResource.getS("Export des images sources");
  static final String TXTEXPORTTRANSF=PivResource.getS("Export des images transformées");
  static final String TXTEXPORTSTAB=PivResource.getS("Export des images stabilisées");
  
  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivExportImagesAction(PivImplementation _impl, int _exportType) {
    super((_exportType == 0 ? TXTEXPORTERSRC: _exportType == 1 ? TXTEXPORTERTRANSF:TXTEXPORTERSTAB)+"...", null, _exportType == 0 ? "EXPORT_SRC_IMAGES" : _exportType == 1 ?  "EXPORT_TRANSF_IMAGES":"EXPORT_STAB_IMAGES");
    exportType_=_exportType;

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Exporte les images transformées au format pgm.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }

    // Selection des images
    if (pnExportImages==null)
      pnExportImages = new PivExportImageFilesPanel(impl);
    
    if (exportType_ == 0) {
      pnExportImages.setFiles(impl.getCurrentProject().getSrcImageFiles(PivProject.SRC_IMAGE_TYPE.ALL));
    }
    else if (exportType_ == 1) {
      pnExportImages.setFiles(impl.getCurrentProject().getTransfImageFiles());
    }
    else {
      pnExportImages.setFiles(impl.getCurrentProject().getStabilizedImageFiles());
    }
    if (!pnExportImages.afficheModaleOk(impl.getFrame(), (exportType_ == 0 ? TXTEXPORTERSRC : exportType_ == 1 ? TXTEXPORTERTRANSF:TXTEXPORTERSTAB), CtuluDialog.OK_CANCEL_OPTION)) {
      return;
    }

    // Export des images, eventuellement dans un autre format.
    PivTaskAbstract r=new PivTaskAbstract((exportType_ == 0 ? TXTEXPORTERSRC : exportType_ == 1 ? TXTEXPORTERTRANSF:TXTEXPORTERSTAB)) {

      @Override
      public boolean act(PivTaskObserverI _observer) {
        try {
          if (observer_ != null) {
            _observer.setDesc((exportType_ == 0 ? TXTEXPORTERSRC : exportType_ == 1 ? TXTEXPORTERTRANSF : TXTEXPORTERSTAB));
            _observer.setProgression(0);
          }
          
          File[] exportImgFiles=pnExportImages.getSelectedFiles();
          File destinationDir=pnExportImages.getDestinationDir();
          // Correspond aux extensions
          String format=pnExportImages.getFormat();
          
          for (int i=0; i<exportImgFiles.length; i++) {
            File cacheFile=impl.getCurrentProject().getCacheImageFile(exportImgFiles[i]);
            // Chaque image dans son format d'origine
            if (format.equals("ORIGIN")) {
              File destFile=new File(destinationDir,exportImgFiles[i].getName());
              String ext=CtuluLibFile.getExtension(exportImgFiles[i].getName());
              
              if (exportImgFiles[i].equals(cacheFile)) {
                CtuluLibFile.copyFile(cacheFile, destFile);
              }
              else {
                BufferedImage img = ImageIO.read(cacheFile);
                if (ext.equals("pgm")) {
                  new PivPGMWriter().write(destFile, img);
                }
                else {
                  CtuluImageExport.export(img, destFile, ext, impl);
                }
              }
            }
            else {
              File destFile=CtuluLibFile.changeExtension(new File(destinationDir,exportImgFiles[i].getName()),format);

              // Le format d'export est le même que le format de stockage
              if (format.equals(CtuluLibFile.getExtension(cacheFile.getName()))) {
                CtuluLibFile.copyFile(cacheFile, destFile);
              }
              // Sinon, transfert du format de stockage vers format d'export.
              else {
                BufferedImage img = ImageIO.read(cacheFile);
                if (format.equals("pgm")) {
                  new PivPGMWriter().write(destFile, img);
                }
                else {
                  CtuluImageExport.export(img, destFile, format, impl);
                }
              }
            }
            if (observer_ != null) {
              observer_.setProgression(((i + 1) * 100) / exportImgFiles.length);
              if (observer_.isStopRequested())
                return false;
            }
          }
        }
        catch (IOException e) {
          FuLog.error("Erreur lors de l'export des images");
          return false;
        }
        return true;
      }
    };

    PivProgressionPanel pnProgress_=new PivProgressionPanel(r);
    diProgress_ = pnProgress_.createDialog(impl.getParentComponent());
    diProgress_.setOption(CtuluDialog.ZERO_OPTION);
    diProgress_.setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
    diProgress_.setTitle(r.getName());

    r.start();
    diProgress_.afficheDialogModal();
  }

  /**
   * @return true Si toutes les données sont présentes pour un lancement.
   */
  public boolean isValide() {
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Les images doivent exister");
  }
}
