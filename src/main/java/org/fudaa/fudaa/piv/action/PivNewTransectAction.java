/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.SwingUtilities;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.BPaletteEdition;
import org.fudaa.ebli.calque.edition.BPaletteEditionClientInterface;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteraction;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteractionTargetI;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.calque.edition.ZEditorLigneBriseePanel;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.metier.PivTransect;
import org.fudaa.fudaa.piv.metier.PivTransectParams;
import org.fudaa.fudaa.piv.utils.PivValueEditorGlobalCoefVelocity;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDesktop;
import com.memoire.fu.FuLog;

/**
 * Une action pour cr�er un transect. Cette action est 
 * utilis�e comme controlleur � la palette et au calque d'�dition d�di�.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivNewTransectAction extends EbliActionPaletteAbstract {

  private PivVisuPanel pnCalques_;
  /** Le calque d'�dition. */
  private ZCalqueEditionInteraction cqEdition_;
  /** Le calsue des ortho points */
  private ZCalqueLigneBriseeEditable cqTransect_;
  /** La palette */
  private BPaletteEdition palette_;
  /** Le panneau d'edition des param�tres du multipoint */
  private ZEditorLigneBriseePanel pnParams;
  /** Le controller d'edition */
  private EditionController controller_;
  /** Le listener de propri�t� du calque d'interaction */
  private EditionLayerPropertyListener propListener_;

  /**
   * Le controlleur pour l'�dition.
   */
  class EditionController implements ZCalqueEditionInteractionTargetI {

    public void atomicChanged() {
      // On notifie la palette que la forme a chang�, pour activer ou non le bouton fin d'edition.
      FuLog.debug("atomicChanged");
      pnParams.atomicChanged();
      
      cqTransect_.repaint();
    }

    /** Non utilis� */
    public boolean addNewMultiPoint(GrPolyligne _pg, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public void setMessage(String _s) {
    }

    /** Non utilis� */
    public void unsetMessage() {
    }

    /** Non utilis� */
    public boolean addNewPolygone(GrPolygone _pg, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewPoint(GrPoint _point, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewRectangle(GrPoint _pt, GrPoint _pt2, ZEditionAttributesDataI _data) {
      return true;
    }

    /**
     * Ajoute la forme en cours au calque.
     * @param _pl La polyligne
     * @param _data Les valeurs associ�es a chaque point.
     * @return Si false, la saisie n'est pas stopp�e.
     */
    public boolean addNewPolyligne(GrPolyligne _pl, ZEditionAttributesDataI _data) {
      if (_pl.sommets_.nombre() < 2) {
        pnCalques_.getCtuluUI().error(PivResource.getS("Le nombre de points doit �tre sup�rieur � 1."));
        return false;
      }
      
      double globalCoef = (Double)_data.getValue(PivVisuPanel.ATT_COEF_VIT, 0);
      double[] ptsCoeffs = new double[_data.getNbVertex()];
      for (int i=0; i<_pl.sommets_.nombre(); i++) {
        // Initialisation du Z polyligne
        _pl.sommet(i).z_=(Double) _data.getValue(PivVisuPanel.ATT_IND_ZR, i);
        // Et des coefs de vitesse
        if (globalCoef == PivValueEditorGlobalCoefVelocity.UNSET_VALUE) {
          ptsCoeffs[i] = (Double)_data.getValue(PivVisuPanel.ATT_P_COEF_VIT, i);
        }
        else {
          ptsCoeffs[i] = globalCoef;
        }
      }
            
      // Warnings
      Double waterLevel = pnCalques_.getProject().getOrthoParameters().getWaterElevation();
      if (_pl.sommet(0).z_<waterLevel ||
          _pl.sommet(_pl.nombre()-1).z_<waterLevel) {
        pnCalques_.getCtuluUI().warn(PivResource.getS("Attention"), PivResource.getS("La cote d'un des deux points extr�mit�s est inf�rieure au niveau d'eau"), false);
      }
      boolean bok = false;
      for (int i=0; i<_pl.sommets_.nombre(); i++) {
        if (_pl.sommet(i).z_<waterLevel) {
          bok = true;
          break;
        }
      }
      if (!bok) {
        pnCalques_.getCtuluUI().warn(PivResource.getS("Attention"), PivResource.getS("Le niveau d'eau est inf�rieur au point le plus bas du transect"), false);
      }
      
      // Transformation vers repere courant.
      GrMorphisme toData=pnCalques_.getProject().getTransformationParameters().getToData();
      _pl.autoApplique(toData);

      // Initialisation du Z polyligne
      for (int i=0; i<_pl.sommets_.nombre(); i++) {
        _pl.sommet(i).z_=(Double) _data.getValue(PivVisuPanel.ATT_IND_ZR, i);
      }

      
      PivTransectParams params=new PivTransectParams();
      params.setInterpolationStep((Double)_data.getValue(PivVisuPanel.ATT_INTER, 0));
      params.setRadiusX((Double)_data.getValue(PivVisuPanel.ATT_RX, 0));
      params.setRadiusY((Double)_data.getValue(PivVisuPanel.ATT_RY, 0));
      params.setSurfaceCoef((Double)_data.getValue(PivVisuPanel.ATT_COEF_VIT, 0));
      params.setMaxPoints((Integer)_data.getValue(PivVisuPanel.ATT_MAX_POINT_NUMBER, 0));
      
      PivTransect transect=new PivTransect();
      transect.setStraight(_pl);
      transect.setParams(params);
      transect.setCoefs(ptsCoeffs);
      
      pnCalques_.getProject().addTransect(transect);
      hideWindow();

      return true;
    }

    /** Non utilis� */
    public void pointMove(double _x, double _y) {
    }
  }
  
  /**
   * Le listener qui r�agit en fonction du gel du calque d'interaction.
   */
  class EditionLayerPropertyListener implements PropertyChangeListener {

    public void propertyChange(PropertyChangeEvent evt) {
      if (evt.getPropertyName().equals("gele")) {
        if (pnParams!=null)
          pnParams.calqueInteractionChanged();
      }
    }
  }

  /**
   * Classe g�rant la disparition et la r�apparition du panel quand la fen�tre
   * principale est r�duite.
   */
  class Piv2dFrameListener extends InternalFrameAdapter {
    @Override
    public void internalFrameActivated(final InternalFrameEvent _e) {
      if(window_!=null)
        window_.setVisible(true);
    }
    @Override
    public void internalFrameDeactivated(final InternalFrameEvent _e) {
      if(window_!=null)
        window_.setVisible(false);
    }
  }

  /**
   * Constructeur de l'action, avec le panneau des calques.
   * @param _pn Le panneau des calques.
   */
  public PivNewTransectAction(PivVisuPanel _pn) {
    super(PivResource.getS("Nouveau transect..."), null, "CREATE_TRANSECT");
    pnCalques_=_pn;
    controller_=new EditionController();
    propListener_=new EditionLayerPropertyListener();
    setCalqueInteraction(_pn.getEditionLayer());
    
    setPaletteResizable(true);
    setEnabled(false);
  }

  /**
   * Definit le calque d'interaction associ� � l'action.
   * @param _cq Le calque. Peut �tre <tt>null</tt>.
   */
  private void setCalqueInteraction(ZCalqueEditionInteraction _cq) {
    if (cqEdition_!=null) {
      cqEdition_.removePropertyChangeListener("gele", propListener_);
    }
    
    cqEdition_=_cq;

    if (cqEdition_!=null) {
      cqEdition_.addPropertyChangeListener("gele", propListener_);
    }
  }

  /**
   * Definit le calque contenant le transect.
   * @param _cq Le calque du transect.
   */
  public void setTransectLayer(ZCalqueLigneBriseeEditable _cq) {
    cqTransect_=_cq;
    // Pour que la palette soit reconstruite correctement.
    palette_=null;
  }

  /**
   * Surcharg� pour demander � l'utilisateur s'il souhaite modifier le contour.
   */
  @Override
  public void changeAction() {
    if (isSelected())
      pnCalques_.setViewMode(PivVisuPanel.MODE_REAL_VIEW);
    
    // Si pas de points ou ok pour les supprimer, on lance l'action.
//    if (pnCalques_.getProject().getTransects()==null ||
//        pnCalques_.getCtuluUI().question(
//              PivResource.getS("Modification du transect"),
//              PivResource.getS("Voulez-vous supprimer les transects existants ?"))) {
//      pnCalques_.getProject().setTransects(null);
//    }
    super.changeAction();
//    }
//    // Pas de modification.
//    else {
//      setSelected(false);
//    }
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Il doit exister au moins une image transform�e");
  }

  /**
   * Surchag� pour d�finir le desktop au moment de l'affichage.
   */
  @Override
  public void showWindow() {
    // Necessaire, sinon la palette est visualis�e en externe. Ne peut �tre fait avant, car le desktop n'est pas
    // encore existant.
    setDesktop((BuDesktop)SwingUtilities.getAncestorOfClass(BuDesktop.class,pnCalques_));
    super.showWindow();
  }

  /**
   * Surcharg� pour activer le calque d'�dition apr�s affichage.
   */
  @Override
  public void doAfterDisplay() {
    cqEdition_.setListener(controller_);
    cqEdition_.setTypeForme(DeForme.LIGNE_BRISEE);
    cqEdition_.setFormEndedByDoubleClic(true);
    cqEdition_.forceKeyModifier(KeyEvent.VK_SHIFT);
    pnCalques_.setCalqueInteractionActif(cqEdition_);
  }

  /**
   * Surcharg� pour rendre inactif le calque d'�dition apr�s que la palette a
   * �t� ferm�e.
   */
  @Override
  public void hideWindow() {
    // Le calque d'interaction est rendu inactif.
    pnCalques_.unsetCalqueInteractionActif(cqEdition_);
    if (cqEdition_!=null) {
      cqEdition_.cancelEdition();
      cqEdition_.releaseKeyModifier(KeyEvent.VK_SHIFT);
    }
    setSelected(false);
    super.hideWindow();
  }

  @Override
  protected void buildWindow() {
    // BM : Un peu pourri, mais si la fenetre n'est pas reconstruite, le composant GlobalCoef, qui est egalement utilis� dans la fenetre d'edition,
    // disparait definitivement.
    window_ = null;
    content = null;
    super.buildWindow();
  }

  @Override
  public JComponent buildContentPane() {
    // BM : Un peu pourri, mais si la fenetre n'est pas reconstruite, le composant GlobalCoef, qui est egalement utilis� dans la fenetre d'edition,
    // disparait definitivement.
//    if (palette_==null){
      palette_=new BPaletteEdition(pnCalques_.getScene());
      palette_.buildComponents();
      palette_.setTargetClient(new BPaletteEditionClientInterface() {
        public void doAction(String _com, BPaletteEdition _emitter) {
          if (BPaletteEdition.ADD_POLYLINE_ACTION.equals(_com)) {
            AbstractButton bt=_emitter.getSelectedButton();
            if (bt==null) {
              palette_.setEditorPanel(null);
            }
            else {
              BuButton btValider=new BuButton(PivResource.PIV.getIcon("valider"),PivResource.getS("Fin de saisie"));
              btValider.setToolTipText(PivResource.getS("Terminer la saisie des points et valider"));
              
              cqEdition_.setTypeForme(DeForme.LIGNE_BRISEE);
              pnParams=new ZEditorLigneBriseePanel(palette_,cqEdition_);
              pnParams.replaceAddButton(btValider);
              pnParams.setCoordinateDefinitions(pnCalques_.getCoordinateDefinitions());
              pnParams.targetChanged(cqTransect_);
              pnParams.setPreferredSize(new Dimension(300,300));
              palette_.setEditorPanel(pnParams.getComponent());
              cqEdition_.setFeatures(pnParams.getAttributeContainer());
            }
            Object o = SwingUtilities.getAncestorOfClass(JInternalFrame.class, palette_);
            if (o == null) {
              o = SwingUtilities.getAncestorOfClass(JDialog.class, palette_);
              if (o != null) {
                ((JDialog) o).pack();
              }
            }
            else {
              ((JInternalFrame) o).pack();
            }
          }
        }
      });
      palette_.setButtonVisible(null, false);
      palette_.setButtonSelected(BPaletteEdition.ADD_POLYLINE_ACTION);

      // Ecoute de la fen�tre principale pour pouvoir se r�duire avec elle
      final Container c = SwingUtilities.getAncestorOfClass(JInternalFrame.class, pnCalques_);
      if (c instanceof JInternalFrame)
        ((JInternalFrame) c).addInternalFrameListener(new Piv2dFrameListener());
//    }
    return palette_.getComponent();
  }
}
