/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivExePanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivImportVideoParamPanel;
import org.fudaa.fudaa.piv.PivProgressionPanel;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivTaskAbstract;
import org.fudaa.fudaa.piv.PivTaskObserverI;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivProject.SRC_IMAGE_TYPE;
import org.fudaa.fudaa.piv.utils.PivUtils;
import org.fudaa.fudaa.piv.metier.PivSamplingVideoParameters;

/**
 * Une action pour importer une vid�o.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivImportVideoAction extends EbliActionSimple {
  PivImplementation impl_;
  PivImportVideoParamPanel pnImport_;
  CtuluDialog diProgress_;

  /**
   * Constructeur
   * @param _impl L'implementation.
   */
  public PivImportVideoAction(PivImplementation _impl) {
    super(PivResource.getS("Importer les images depuis une vid�o..."), PivResource.PIV.getIcon("video"), "IMPORT_VIDEO");

    impl_=_impl;
    setEnabled(false);
  }

  /**
   * Selectionne le fichier d'import, et propose � l'utilisateur de remplacer ou
   * combiner les nouveaux points.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }
    
    pnImport_=new PivImportVideoParamPanel(impl_);

    if (pnImport_.afficheModale(impl_.getFrame(), PivResource.getS("Import des images"))==0) {
      
      if (!PivExeLauncher.instance().areExeOK()) {
        PivExePanel pnExe = new PivExePanel();

        if (!pnExe.afficheModaleOk(impl_.getFrame(), PivResource.getS("R�pertoire contenant les executables"))) {
          return;
        }
      }
      
      // La tache a ex�cuter.
      PivTaskAbstract r=new PivTaskAbstract(PivResource.getS("Echantillonnage")) {

        public boolean act(PivTaskObserverI _observer) {

          File tmpOutDir=null;
          
          try {
            CtuluLog ana=new CtuluLog();
            ana.setDesc(getName());

            // Les images sont cr��es dans un repertoire temporaire du projet dezipp�, pour ne
            // pas �craser les images existantes avant validation.
            tmpOutDir = new File(impl_.getCurrentProject().getTempDirectory(), "img_sampling");
            tmpOutDir.mkdirs();
            
            PivSamplingVideoParameters params = pnImport_.getSamplingParameters();

            int nbImgs = (int)(pnImport_.getFrameRate()*(params.getSamplingEndTime()-params.getSamplingBeginTime()));
            PivExeLauncher.instance().computeSampling(ana, impl_.getCurrentProject(), params.getVideoFile(), tmpOutDir, params.getSamplingPeriod(), nbImgs, params.getSamplingBeginTime(),
                params.getSamplingEndTime(), params.getSamplingSize(), _observer);
            if (ana.containsErrorOrSevereError()) {
              impl_.error(ana.getResume());
              return false;
            }
            
            File[] fimgs=tmpOutDir.listFiles(PivProject.FLT_FILES);
            if (fimgs == null) {
              fimgs=new File[0];
            }
            else {
              Arrays.sort(fimgs);
              
              // #5668 : On supprime les premi�re et derni�re images, qui ne semblent pas respecter l'intervalle de temps.
              ArrayList<File> lfimgs = new ArrayList<>(Arrays.asList(fimgs));
              int nbOutFiles = fimgs.length;
              if (nbOutFiles > 1)
                lfimgs.remove(0);
              if (nbOutFiles > 2)
                lfimgs.remove(lfimgs.size()-1);
              
              fimgs = lfimgs.toArray(new File[0]);
//              if (nbOutFiles > 1)
//                new File(_tmpOutDir, "image0001.png").delete();
//              if (nbOutFiles > 2)
//                new File(_tmpOutDir, "image" + PivUtils.formatOn4Chars(nbOutFiles) + ".png").delete();
//              // Et on renomme a partir de 1.
//              for (int i = 0; i< nbOutFiles-2; i++) {
//                File srcFile = new File(_tmpOutDir, "image" + PivUtils.formatOn4Chars(i + 2) + ".png");
//                File dstFile = new File(_tmpOutDir, "image" + PivUtils.formatOn4Chars(i + 1) + ".png");
//                srcFile.renameTo(dstFile);
//
//              }

            }
            
            SRC_IMAGE_TYPE[] fileTypes = new SRC_IMAGE_TYPE[fimgs.length];
            for (int i=0; i<fileTypes.length; i++) {
              fileTypes[i] = SRC_IMAGE_TYPE.CALCULATION;
            }
            
            // Pour �tre sur d'effacer toutes les images pr�c�dentes
            impl_.getCurrentProject().setSrcImagesFiles(new File[0], fileTypes, _observer, ana);
            
            impl_.getCurrentProject().setSrcImagesFiles(fimgs, fileTypes, _observer, ana);
            
            // Et initialisation de l'intervalle de temps.
            impl_.getCurrentProject().setTimeStep(1./pnImport_.getFrameRate());
            
            // Et stocke les infos de sampling, pour le rapport de jaugeage.
            impl_.getCurrentProject().setSamplingParameters(params);
          }
//          catch (IOException _exc) {
//            _exc.printStackTrace();
//          }
          finally {
            if (tmpOutDir!=null)
              CtuluLibFile.deleteDir(tmpOutDir);
          }

          impl_.message(PivResource.getS("L'�chantillonnage s'est termin� avec succ�s"));
          return true;
        }
      };

      PivProgressionPanel pnProgress_=new PivProgressionPanel(r);
      diProgress_=pnProgress_.createDialog(impl_.getFrame());
      diProgress_.setOption(CtuluDialog.ZERO_OPTION);
      diProgress_.setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
      diProgress_.setTitle(r.getName());

      r.start();
      diProgress_.afficheDialogModal();
      
      impl_.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_ORIGINAL_VIEW);
      impl_.get2dFrame().restaurer();
    }
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Le projet doit �tre cr��");
  }
}
