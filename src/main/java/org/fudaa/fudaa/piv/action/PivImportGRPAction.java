/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Arrays;

import javax.swing.JFileChooser;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.io.PivGRPReader;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;
import org.fudaa.fudaa.piv.utils.PivUtils;

/**
 * Une action pour importer les points de r�f�rence au sol.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivImportGRPAction extends EbliActionSimple {
  PivImplementation impl;
  CtuluFileChooser fcGRP;

  /**
   * Constructeur
   * @param _impl L'implementation.
   */
  public PivImportGRPAction(PivImplementation _impl) {
    super(PivResource.getS("Import des points de r�f�rence..."), null, "IMPORT_GRP");

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Affiche une boite de dialogue pour permettre de selectionner le fichier
   * d'import.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }

    boolean keepPreviousPts=false;
    if (impl.getCurrentProject().getOrthoPoints()!=null) {
      keepPreviousPts=!impl.question(
              PivResource.getS("Import des points de r�f�rence"),
              PivResource.getS("Des points existent d�j�.\nVoulez-vous les supprimer avant ajout des nouveaux ?"));
    }

    // Selection du fichier
    if (fcGRP == null) {
      fcGRP=new CtuluFileChooser(true);
      fcGRP.setAcceptAllFileFilterUsed(true);
      fcGRP.setFileFilter(PivUtils.FILE_FLT_GRP);
      // Pas terrible, mais evite de redonner le nom manuellement.
      fcGRP.setSelectedFile(new File(PivUtils.FILE_FLT_GRP.getFirstExt()));
      fcGRP.setMultiSelectionEnabled(false);
      fcGRP.setDialogTitle(PivResource.getS("S�lection d'un fichier GRP"));
    }
    
    if (fcGRP.showOpenDialog(impl.getFrame()) == JFileChooser.CANCEL_OPTION) {
      return;
    }
    File grpFile=fcGRP.getSelectedFile();

    CtuluIOResult<PivOrthoPoint[]> ret = new PivGRPReader(impl.getCurrentProject().getSrcImageSize()).read(grpFile, null);
    if (ret.getAnalyze().containsErrorOrSevereError()) {
      impl.error(ret.getAnalyze().getResume());
      return;
    }

    // Transformation des points reels vers data.
    PivOrthoPoint[] pts=ret.getSource();
    GrMorphisme toData=impl.getCurrentProject().getTransformationParameters().getToData();
    for (PivOrthoPoint pt : pts) {
      pt.getRealPoint().autoApplique(toData);
    }
    
    PivOrthoPoint[] newpts;
    if (keepPreviousPts) {
      PivOrthoPoint[] oldpts=impl.getCurrentProject().getOrthoPoints();
      newpts=Arrays.copyOf(oldpts,oldpts.length+pts.length);
      for (int i=0; i<pts.length; i++) {
        newpts[i+oldpts.length]=pts[i];
      }
    }
    else {
      newpts=pts;
    }
    
    impl.getCurrentProject().setOrthoPoints(newpts);

    impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_ORIGINAL_VIEW);
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Il doit exister au moins une image source");
  }
}
