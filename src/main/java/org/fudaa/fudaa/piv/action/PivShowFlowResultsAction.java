/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivFlowInfoPanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;

/**
 * Une action pour afficher les resultats de calcul de d�bit (sans refaire le calcul).
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivComputeFlowAction.java 9133 2015-06-16 16:36:57Z bmarchan $
 */
public class PivShowFlowResultsAction extends EbliActionSimple {
  PivImplementation impl;
  CtuluDialog diProgress_;

  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivShowFlowResultsAction(PivImplementation _impl) {
    super(PivResource.getS("Afficher les r�sultats du calcul..."), null, "SHOW_FLOW_RESULTS");

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Affiche la fenetre resultats.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }
    
    PivFlowInfoPanel pnInfo=new PivFlowInfoPanel(impl.getCurrentProject().getFlowResults(), impl.getCurrentProject().getTransects(), impl.getCurrentProject().getOrthoParameters());
    pnInfo.afficheModale(impl.getFrame(), PivResource.getS("R�sultats du calcul"), CtuluDialog.OK_OPTION);
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    if (impl.getCurrentProject().getFlowResults()==null) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Il n'existe pas de r�sultats de calcul de d�bit"));
      return false;
    }
    
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Les r�sultats de calcul de d�bit doivent exister");
  }
}
