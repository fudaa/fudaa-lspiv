/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;

import com.memoire.bu.BuResource;
import com.memoire.fu.FuLog;

/**
 * Une action pour faire d�filier vers l'image pr�c�dente (utile pour la saisie des vitesses manuelles).
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivPreviousImageAction extends EbliActionSimple {
  PivImplementation impl;
  CtuluDialog diProgress_;

  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivPreviousImageAction(PivImplementation _impl) {
    super(PivResource.getS("Image pr�c�dente"), BuResource.BU.getIcon("monter"), "PREVIOUS_IMAGE");
    setKey(KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0));

    impl=_impl;
    //  Attention : L'action est activ�e ici, et est desactiv�e au demarrage de l'appli.
    // Si on la met a desactiv�e et qu'on l'active dans l'appli par SetEnableForAction par la siute, le key mapping ne fonctionne pas...
    setEnabled(true);
  }

  /**
   * Lance l'analyse par PIV, dans un thread s�par�.
   * @param _e L'evenement pour l'action.
   */
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }
    
    FuLog.trace("Image pr�c�dente");
    if (impl.get2dFrame() != null  && impl.get2dFrame().getVisuPanel() != null && impl.get2dFrame().getVisuPanel().getCurrentView() != null)
      impl.get2dFrame().getVisuPanel().getCurrentView().previousImage();
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
//    if (impl.getCurrentProject().getComputeGrid()==null) {
//      impl.error(PivResource.getS("Erreur"), PivResource.getS("Les points de grille n'ont pas �t� d�finis"));
//      return false;
//    }
//    if (impl.getCurrentProject().getComputeParameters()==null || !impl.getCurrentProject().getComputeParameters().isFilledForComputing()) {
//      impl.error(PivResource.getS("Erreur"), PivResource.getS("Les param�tres de calcul n'ont pas �t� donn�s"));
//      return false;
//    }
//    if (impl.getCurrentProject().getTransfImageFiles().length<2) {
//      impl.error(PivResource.getS("Erreur"), PivResource.getS("Le projet doit contenir au moins 2 images transform�es"));
//      return false;
//    }
    
    return true;
  }

  public String getEnableCondition() {
    return PivResource.getS("Le projet doit �tre cr�� et comporter au moins 2 images");
  }
}
