/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import javax.swing.KeyStroke;
import org.fudaa.ebli.commun.EbliActionChangeState;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivTransfView;
import org.fudaa.fudaa.piv.PivVisuPanel;

/**
 * Une action pour mettre la vue en configuration de visualisation d'image d'origine.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivTransfViewAction extends EbliActionChangeState {
  PivImplementation impl;

  /**
   * Constructeur
   * @param _impl L'implementation.
   */
  public PivTransfViewAction(PivImplementation _impl) {
    super(PivTransfView.TITLE, null, "PIV_TRANSF_VIEW");
    setKey(KeyStroke.getKeyStroke('T'));

    impl=_impl;
    setEnabled(true);
  }

//
//  public void updateForSelectionChanged() {
//    ZScene scn=sceneEditor_.getScene();
////    SceneSelectionHelper hlp=sceneEditor_.getScene().getSelectionHelper();
////    int idGeom=-1;
//
//    boolean b=true;
//    // Si la selection n'est pas nulle
//    b=b && !scn.isSelectionEmpty();
//    // Si atomique
//    String acname=b?(scn.isAtomicMode()?"Projeter les sommets sur un semis":"Projeter les g�om�tries sur un semis"):"Projeter sur un semis";
//    putValue(Action.NAME, BuResource.BU.getString(acname));
//
//    setEnabled(b);
//  }

//  public String getEnableCondition() {
//    return PivResource.getS("Les param�tres doivent �tre renseign�s et les images transform�es doivent exister");
//  }

  @Override
  public void changeAction() {
    impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_TRANSF_VIEW);
    impl.updateActionsState();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.calque.ZSelectionListener#selectionChanged(org.fudaa.ebli.calque.ZSelectionEvent)
   */
//  public void selectionChanged(ZSelectionEvent _evt) {
//    updateForSelectionChanged();
//  }
}
