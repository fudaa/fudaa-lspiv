/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivOrthoModePanel;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivProject.OrthoMode;

/**
 * Une action pour choisir le mode opératoire de l'othorectification.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivTransectParamAction.java 9132 2015-06-16 14:54:20Z bmarchan $
 */
public class PivChooseOrthoModeAction extends EbliActionSimple {
  PivImplementation impl;
  
  public PivChooseOrthoModeAction(PivImplementation _impl) {
    super(PivResource.getS("Mode d'orthorectification..."), null, "DEFINE_ORTHO_MODE");
    impl=_impl;
    setEnabled(false);
  }

  /**
   * Affiche le panneau des paramètres de calcul.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    PivOrthoModePanel pn=new PivOrthoModePanel(impl);
    pn.setModeFullOrtho(impl.getCurrentProject().getOrthoMode() != OrthoMode.SCALING);

    if (pn.afficheModale(impl.getFrame(), PivResource.getS("Mode d'orthorectification"),CtuluDialog.OK_CANCEL_OPTION)==CtuluDialog.OK_OPTION) {
      impl.get2dFrame().getVisuPanel().setScalingPointsLayerVisible(!pn.isModeFullOrtho());
      impl.get2dFrame().getVisuPanel().setOrthoPointsLayerVisible(pn.isModeFullOrtho());
    }
  }
  
  @Override
  public String getEnableCondition() {
    return PivResource.getS("Il doit exister au moins une image source");
  }
}
