package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.List;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.io.PivTransectAbsZReader;
import org.fudaa.fudaa.piv.metier.PivTransect;
import org.fudaa.fudaa.piv.utils.PivUtils;

/**
 * Une action pour importer un transect en abscisse/cote.
 * Cette action est partielle, le traitement des donn�es etant d�port� dans le listener associ�.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivImportTransectAbsZAction extends EbliActionSimple {
  PivImplementation impl;
  CtuluFileChooser fcTransect;
  ImportListener listener_;
  
  /**
   * Ub listener pour les points import�s.
   * @author marchand
   */
  public interface ImportListener {
    public void dataImported(List<Double[]> _points);
  }

  /**
   * Constructeur
   * @param _impl L'implementation.
   */
  public PivImportTransectAbsZAction(PivImplementation _impl) {
    super(PivResource.getS("Import d'un transect en abscisse/cote..."), PivResource.PIV.getIcon("importer"), "IMPORT_TRANSECT_ABS_Z");

    impl=_impl;
    setEnabled(true);
  }
  
  public void setImportListener(ImportListener _listener) {
    listener_ = _listener;
  }

  /**
   * Affiche une boite de dialogue pour permettre de selectionner le fichier
   * d'import.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }

    // Selection du fichier
    if (fcTransect == null) {
      fcTransect=new CtuluFileChooser(true);
      fcTransect.setAcceptAllFileFilterUsed(true);
      fcTransect.setFileFilter(PivUtils.FILE_FLT_TRANS_ABS_Z);
      fcTransect.setDialogTitle(PivResource.getS("S�lection d'un fichier transect en abscisse/cote"));
    }
    
    if (fcTransect.showOpenDialog(impl.getFrame()) == CtuluFileChooser.CANCEL_OPTION) {
      return;
    }
    File transectFile=fcTransect.getSelectedFile();
    CtuluIOResult<List<Double[]>> ret = new PivTransectAbsZReader().read(transectFile, null);
    if (ret.getAnalyze().containsErrorOrSevereError()) {
      impl.error(ret.getAnalyze().getResume());
      return;
    }
    
    if (listener_ != null) {
      listener_.dataImported(ret.getSource());
    }
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    return true;
  }
}
