/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;
import java.io.File;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.io.PivParamReader;
import org.fudaa.fudaa.piv.metier.PivComputeParameters;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.utils.PivUtils;

/**
 * Une action pour importer les parametres de calcul.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivImportGridAction.java 9083 2015-03-17 13:05:54Z bmarchan $
 */
public class PivImportComputeParamAction extends EbliActionSimple {
  PivImplementation impl;
  CtuluFileChooser fcParams;

  /**
   * Constructeur
   * @param _impl L'implementation.
   */
  public PivImportComputeParamAction(PivImplementation _impl) {
    super(PivResource.getS("Import des param�tres de calcul..."), null, "IMPORT_PIV_PARAM");

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Selectionne le fichier d'import, et remplace les parametres.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }
    
    PivProject prj = impl.getCurrentProject();

// BM 08/02/2024 : Les filtres ne sont plus stock�s dans ce fichier, on ne demande plus si l'import des filtres est utile.
// Par contre, ils sont quand m�me import�s pour les fichiers PIV_param.dat < version 4.
//    if (!impl.question(PivResource.getS("Param�tres de calcul"), PivResource.getS("Les param�tres de filtres seront aussi import�s.\nVoulez-vous continuer ?"))) {
//      return;
//    }

    // Selection du fichier
    if (fcParams == null) {
      fcParams=new CtuluFileChooser(true);
      fcParams.setAcceptAllFileFilterUsed(true);
      fcParams.setFileFilter(PivUtils.FILE_FLT_COMPUTE_PARAMS);
      // Pas terrible, mais evite de redonner le nom manuellement.
      fcParams.setSelectedFile(new File(PivUtils.FILE_FLT_COMPUTE_PARAMS.getFirstExt()));
      fcParams.setMultiSelectionEnabled(false);
      fcParams.setDialogTitle(PivResource.getS("S�lection d'un fichier de parametres de calcul"));
    }
    
    if (fcParams.showOpenDialog(impl.getFrame()) == CtuluFileChooser.CANCEL_OPTION) {
      return;
    }
    File paramsFile=fcParams.getSelectedFile();
    
    if (prj.hasInstantRawResults() && !impl.question(PivResource.getS("Suppression des r�sultats"), 
        PivResource.getS("Attention : des r�sultats existent et seront supprim�s si vous importez.\nVoulez-vous continuer ?"))) {
      return;
    }

    CtuluIOResult<Object[]> ret = new PivParamReader().read(paramsFile, null);
    if (ret.getAnalyze().containsErrorOrSevereError()) {
      impl.error(ret.getAnalyze().getResume());
      return;
    }
    
    PivComputeParameters params=(PivComputeParameters)ret.getSource()[0];
    // On n'importe pas l'intervalle de temps
    if (impl.getCurrentProject().getComputeParameters() != null) {
      params.setTimeInterval(prj.getComputeParameters().getTimeInterval());
    }
    
    prj.setComputeParameters(params);
    prj.setInstantRawResults(null);

    impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_TRANSF_VIEW);
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Il doit exister au moins une image transform�e");
  }
}
