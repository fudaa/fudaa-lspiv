package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.impl.FudaaPanelTask;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.particles.PivComputeParticlesTaskModel;
import org.fudaa.fudaa.piv.particles.PivParticlesLayer;

/**
 * Permet d'afficher le dialogue pour les trajectoire/ligne de courant
 */
public class PivCreateParticleLinesAction extends EbliActionSimple implements TreeSelectionListener {

  protected PivVisuPanel pnCalque_;
  protected FudaaCommonImplementation impl_;
  protected PivParticlesLayer destLayer_;
  PivParticlesLayer[] layers_;
  PivComputeParticlesTaskModel taskTrajectories = null;
  
  public PivCreateParticleLinesAction(final PivVisuPanel panel, FudaaCommonImplementation impl, PivParticlesLayer[] _layers) {
    super(PivResource.getS("Calcul des lignes de courant/trajectoires..."), PivResource.PIV.getIcon("crystal_oscilloscope.png"), "TRAJ");
    impl_ = impl;
    pnCalque_ = panel;
    layers_=_layers;
    setEnabled(false);
    
    panel.getArbreCalqueModel().addTreeSelectionListener(this);
  }
  
  public void setLayers(PivParticlesLayer[] _layers) {
    layers_=_layers;
  }
  
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (destLayer_.getSource().getGrid()==null) {
      impl_.error(PivResource.getS("Pas de r�sultats disponibles pour le trac� de particules"));
      return;
    }
    taskTrajectories = new PivComputeParticlesTaskModel(pnCalque_,destLayer_);
    new FudaaPanelTask(pnCalque_.getCtuluUI(), taskTrajectories).afficheDialog();
  }

  @Override
  public void valueChanged(TreeSelectionEvent e) {
    for (int i=0; i<layers_.length; i++) {
      BCalque cqActif=pnCalque_.getCalqueActif();
      if (layers_[i]==cqActif) {
        destLayer_ =layers_[i];
        setEnabled(true);
        return;
      }
    }
    setEnabled(false);
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("S�lectionnez un calque particule");
  }
}
