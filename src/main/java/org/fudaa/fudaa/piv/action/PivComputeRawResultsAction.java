/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import javax.swing.SwingUtilities;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivExePanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivProgressionPanel;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivTaskAbstract;
import org.fudaa.fudaa.piv.PivTaskObserverI;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.utils.PivUtils;

/**
 * Une action pour lancer l'analyse par PIV.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivComputeRawResultsAction extends EbliActionSimple {
  PivImplementation impl;
  CtuluDialog diProgress_;

  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivComputeRawResultsAction(PivImplementation _impl) {
    super(PivResource.getS("Calcul des r�sultats instantan�s"), null, "COMPUTE_PIV");

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Lance l'analyse par PIV, dans un thread s�par�.
   * @param _e L'evenement pour l'action.
   */
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }

//    // Wrap, sinon, la fenetre est trop large.
//    String questionText = PivUtils.wrapMessage(
//        PivResource.getS("La dur�e des calculs de vitesse est proportionnelle au nombre de pixels dans l'IA (carr� du c�t�), au nombre de pixels dans la SA, au nombre de noeuds de la grille, et au nombre de paires d'images � traiter.\n\n")+
//        PivResource.getS("Voulez-vous calculer les r�sultats instantan�s (Les temps de calcul peuvent �tre long) ?"), 132);
//    if (!impl.question(PivResource.getS("Calcul des r�sultats instantan�s "), questionText)) {
//      return;
//    }
    
    if (!PivExeLauncher.instance().areExeOK()) {
      PivExePanel pnExe = new PivExePanel();

      if (!pnExe.afficheModaleOk(impl.getFrame(), PivResource.getS("R�pertoire contenant les executables"))) {
        return;
      }
    }


    // La tache a ex�cuter.
    PivTaskAbstract r=new PivTaskAbstract(PivResource.getS("Calcul des r�sultats instantan�s")) {

      public boolean act(PivTaskObserverI _observer) {
        CtuluLog ana = new CtuluLog();
        ana.setDesc(this.getName());
        PivExeLauncher.instance().computeRawInstantResults(ana, impl.getCurrentProject(), false, _observer);
        if (ana.containsErrorOrSevereError()) {
          impl.error(ana.getResume());
          return false;
        }

        // Lanc� � la fin, car l'interface se bloque si on ne le fait pas.
        // Probl�me de thread swing probablement...
        SwingUtilities.invokeLater(new Runnable() {
          public void run() {
            impl.message(PivResource.getS("Calcul termin�"), PivResource.getS("Le calcul s'est termin� avec succ�s"), false);
            impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);
            impl.get2dFrame().getVisuPanel().setRawVelocitiesLayerVisible(true);
          }
        });
        
        return true;
      }
    };

    PivProgressionPanel pnProgress_=new PivProgressionPanel(r);
    diProgress_ = pnProgress_.createDialog(impl.getParentComponent());
    diProgress_.setOption(CtuluDialog.ZERO_OPTION);
    diProgress_.setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
    diProgress_.setTitle(r.getName());

    r.start();
    diProgress_.afficheDialogModal();
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    if (impl.getCurrentProject().getComputeGrid()==null) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Les points de grille n'ont pas �t� d�finis"));
      return false;
    }
    if (impl.getCurrentProject().getComputeParameters()==null || !impl.getCurrentProject().getComputeParameters().isFilledForComputing()) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Les param�tres de calcul n'ont pas �t� donn�s"));
      return false;
    }
    if (impl.getCurrentProject().getTransfImageFiles().length<2) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Le projet doit contenir au moins 2 images transform�es"));
      return false;
    }
    
    return true;
  }

  public String getEnableCondition() {
    return PivResource.getS("Les param�tres doivent �tre renseign�s et les images transform�es doivent exister");
  }
}
