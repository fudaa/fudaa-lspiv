package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.layer.PivEditableModel;

/**
 * Une action pour �diter la g�om�trie. Le projet est mis a jour suite
 * � l'�dition.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivEditAction extends EbliActionSimple implements ZSelectionListener {
  PivVisuPanel pnCalques_;

  /**
   * Constructeur.
   * @param _pn Le panneau des calques.
   */
  public PivEditAction(PivVisuPanel _pn) {
    super(PivResource.getS("Editer"), PivResource.PIV.getIcon("editer"), "GIS_EDIT");
    pnCalques_=_pn;
    setDefaultToolTip(PivResource.getS("Editer la g�om�trie s�lectionn�e"));
    setEnabled(false);
  }

  /**
   * Affiche la fenetre d'�dition pour les g�om�tries autoris�es.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    pnCalques_.getEditor().edit();
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("S�lectionner une g�om�trie");
  }

  /**
   * Lorsque la s�lection de g�om�trie, l'action est gris�e ou non.
   * @param _evt L'evenement de selection.
   */
  public void selectionChanged(ZSelectionEvent _evt) {
    boolean b=false;
    if (pnCalques_.getScene().getCalqueActif() instanceof ZCalqueEditable) {
      ZCalqueEditable cq=(ZCalqueEditable) pnCalques_.getScene().getCalqueActif();
      
      b=cq.getLayerSelection()!=null && !cq.getLayerSelection().isEmpty();
      b&=cq.getEditor()!=null && (cq.getModelEditable() instanceof PivEditableModel);
    }
    super.setEnabled(b);
  }
}
