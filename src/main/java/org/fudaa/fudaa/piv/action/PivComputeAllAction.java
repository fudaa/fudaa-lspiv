/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivComputeTrackingPanel;
import org.fudaa.fudaa.piv.PivExePanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivTaskAbstract;
import org.fudaa.fudaa.piv.io.PivExeLauncher;

/**
 * Une action pour lancer toute la chaine de calculs.
 * Seuls les calculs dont les param�tres ont chang� sont relanc�s.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivComputeAllAction extends EbliActionSimple {
  PivImplementation impl;
  CtuluDialog diProgress_;

  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivComputeAllAction(PivImplementation _impl) {
    super(PivResource.getS("Relancer tous les calculs..."), null, "COMPUTE_ALL");
    setKey(KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_MASK | KeyEvent.SHIFT_MASK));

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Lance l'analyse par PIV, dans un thread s�par�.
   * @param _e L'evenement pour l'action.
   */
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }

    if (!PivExeLauncher.instance().areExeOK()) {
      PivExePanel pnExe = new PivExePanel();

      if (!pnExe.afficheModaleOk(impl.getFrame(), PivResource.getS("R�pertoire contenant les executables"))) {
        return;
      }
    }

    PivComputeTrackingPanel pnProgress_=new PivComputeTrackingPanel(impl);
    diProgress_ = pnProgress_.createDialog(impl.getParentComponent());
    diProgress_.setOption(CtuluDialog.OK_CANCEL_OPTION);
    diProgress_.setButtonText(CtuluDialog.OK_OPTION, PivResource.getS("Oui"));
    diProgress_.setTitle(PivResource.getS("Relancer tous les calculs"));
    diProgress_.afficheDialogModal();
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
//    if (impl.getCurrentProject().getComputeGrid()==null) {
//      impl.error(PivResource.getS("Erreur"), PivResource.getS("Les points de grille n'ont pas �t� d�finis"));
//      return false;
//    }
//    if (impl.getCurrentProject().getComputeParameters()==null || !impl.getCurrentProject().getComputeParameters().isFilledForComputing()) {
//      impl.error(PivResource.getS("Erreur"), PivResource.getS("Les param�tres de calcul n'ont pas �t� donn�s"));
//      return false;
//    }
//    if (impl.getCurrentProject().getTransfImageFiles().length<2) {
//      impl.error(PivResource.getS("Erreur"), PivResource.getS("Le projet doit contenir au moins 2 images transform�es"));
//      return false;
//    }
    
    return true;
  }

  public String getEnableCondition() {
    return PivResource.getS("Le projet doit �tre cr�� et comporter au moins 2 images");
  }
}
