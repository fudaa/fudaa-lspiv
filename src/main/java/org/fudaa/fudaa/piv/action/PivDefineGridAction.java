/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.Container;

import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.SwingUtilities;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteraction;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteractionTargetI;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.piv.PivComputeGridDefinitionPanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivSimplifiedEditionPalette;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.metier.PivCntGrid;
import org.fudaa.fudaa.piv.metier.PivGrid;

import com.memoire.bu.BuDesktop;

/**
 * Une action pour saisir le contour de grille et la grille de calcul.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivDefineGridAction extends EbliActionPaletteAbstract {

  private PivVisuPanel pnCalques_;
  /** Le calque d'�dition. */
  private ZCalqueEditionInteraction cqEdition_;
  /** La palette pour la reprise. */
  private PivSimplifiedEditionPalette palette_;
  /** Le controller d'edition */
  private EditionController controller_;
  /** La grille, avant modification. Sera restitu�e si on annule. */
  private PivGrid oldGrid_;

  /**
   * Un editeur qui r�agit � la saisie d'une forme pour la grille.
   *
   * @author Bertrand Marchand (marchand@deltacad.fr)
   * @version $Id$
   */
  class EditionController implements ZCalqueEditionInteractionTargetI {

    public void atomicChanged() {
      // On notifie la palette que la forme a chang�, pour activer ou non le bouton fin d'edition.
      GrPolyligne pl=(GrPolyligne) cqEdition_.getFormeEnCours().getFormeEnCours();
      
      // #5631 : On stoppe le polygone au bout de 4 clics.
      if (pl != null && pl.nombre() == 4) {
        addContour(pl.toGrPolygone());
      }
      
      palette_.getButton(PivSimplifiedEditionPalette.BUTTON_END).setEnabled(pl != null);
    }

    /**
     * Ajoute la forme en cours au calque, et affiche la fenetre de param�tres.
     * @param _pg Le polygone
     * @param _data Les valeurs associ�es a chaque point.
     * @return true.
     */
    public boolean addNewPolygone(GrPolygone _pg, ZEditionAttributesDataI _data) {
      CtuluAnalyze ana=new CtuluAnalyze();
      if (!pnCalques_.getTransfView().getCntGridLayer().modeleDonnees().isDataValid(_pg.toCoordinateSequence(), _data, ana)) {
        pnCalques_.getCtuluUI().error(ana.getFatalError());
        cqEdition_.cancelEdition();
        atomicChanged();
        return true;
      }
      
      addContour(_pg);
      return true;
    }
    
    private void addContour(GrPolygone _pg) {
      PivCntGrid cntGrid=pnCalques_.getProject().getComputeCntGrid();
      if (cntGrid==null) {
        cntGrid=new PivCntGrid();
      }
      cntGrid.setContour(_pg);

      pnCalques_.getProject().setComputeCntGrid(cntGrid);

      hideWindow();
      showPanelParameters();
    }

    /** Non utilis� */
    public boolean addNewPoint(GrPoint _point, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewRectangle(GrPoint _pt, GrPoint _pt2, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewPolyligne(GrPolyligne _pt, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewMultiPoint(GrPolyligne _pt, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public void setMessage(String _s) {
    }

    /** Non utilis� */
    public void unsetMessage() {
    }

    /** Non utilis� */
    public void pointMove(double _x, double _y) {
    }
  }

  /**
   * Classe g�rant la disparition et la r�apparition du panel quand la fen�tre
   * principale est r�duite.
   */
  class Piv2dFrameListener extends InternalFrameAdapter {
    @Override
    public void internalFrameActivated(final InternalFrameEvent _e) {
      if(window_!=null)
        window_.setVisible(true);
    }
    @Override
    public void internalFrameDeactivated(final InternalFrameEvent _e) {
      if(window_!=null)
        window_.setVisible(false);
    }
  }

  /**
   * Constructeur.
   * @param _pn Le panneau des calques.
   */
  public PivDefineGridAction(PivVisuPanel _pn) {
    super(PivResource.getS("D�finition des points de grille..."), null, "DEFINE_GRID");
    pnCalques_=_pn;
    controller_=new EditionController();
    setCalqueInteraction(_pn.getEditionLayer());
    
    setPaletteResizable(true);
    setEnabled(false);
  }

  /**
   * Definit le calque d'interaction associ� � l'action.
   * @param _cq Le calque. Peut �tre <tt>null</tt>.
   */
  private void setCalqueInteraction(ZCalqueEditionInteraction _cq) {
    cqEdition_=_cq;
  }

  /**
   * Surcharg� pour demander � l'utilisateur s'il souhaite modifier le contour.
   */
  @Override
  public void changeAction() {
    if (isSelected()) {
      pnCalques_.setViewMode(PivVisuPanel.MODE_TRANSF_VIEW);
      // On remettra la grille si il y a annulation.
      oldGrid_ = pnCalques_.getProject().getComputeGrid();
      pnCalques_.getProject().setComputeCntGrid(null);

      if (pnCalques_.getProject().getComputeGrid()==null ||
        pnCalques_.getCtuluUI().question(
           PivResource.getS("D�finition des points de grille"),
           PivResource.getS("Des points de grille existent d�j�.\nVoulez-vous les supprimer avant ajout des nouveaux ?"))) {
        pnCalques_.getProject().setComputeGrid(null);
      }
    }
    else {
      pnCalques_.getProject().setComputeGrid(oldGrid_);
    }
    
    super.changeAction();
  }


  @Override
  public String getEnableCondition() {
    return PivResource.getS("Il doit exister au moins une image transform�e");
  }

  /**
   * Surchag� pour d�finir le desktop au moment de l'affichage.
   */
  @Override
  public void showWindow() {
    // Necessaire, sinon la palette est visualis�e en externe. Ne peut �tre fait avant, car le desktop n'est pas
    // encore existant.
    setDesktop((BuDesktop)SwingUtilities.getAncestorOfClass(BuDesktop.class,pnCalques_));
    super.showWindow();
  }

  /**
   * Surcharg� pour activer le calque d'�dition apr�s affichage.
   */
  @Override
  public void doAfterDisplay() {
    cqEdition_.setListener(controller_);
    cqEdition_.setTypeForme(DeForme.POLYGONE);
    cqEdition_.setFormEndedByDoubleClic(true);
    pnCalques_.setCalqueInteractionActif(cqEdition_);
    palette_.getButton(PivSimplifiedEditionPalette.BUTTON_END).setEnabled(false);
  }

  /**
   * Surcharg� pour rendre inactif le calque d'�dition apr�s que la palette a
   * �t� ferm�e.
   */
  @Override
  public void hideWindow() {
    pnCalques_.unsetCalqueInteractionActif(cqEdition_);
    cqEdition_.cancelEdition();
    setSelected(false);
//    if (pnCalques_.getController().getCqSelectionI()!=null)
//      pnCalques_.getController().getCqSelectionI().setGele(false);
    super.hideWindow();
  }

  /**
   * Affiche la fenetre pour la saisie des param�tres de grille.
   */
  public void showPanelParameters() {
    final PivCntGrid params=pnCalques_.getProject().getComputeCntGrid();
    final PivGrid previousGrid = pnCalques_.getProject().getComputeGrid();
    PivComputeGridDefinitionPanel pn=new PivComputeGridDefinitionPanel() {

      @Override
      public boolean apply() {
        
        retrieveParams(params);
        PivGrid grid=PivGrid.computeFromShape(params.getContour(), params.getNbXPoints(), params.getNbYPoints());
        grid.merge(previousGrid);
        pnCalques_.getProject().setComputeGrid(grid);
        return true;
      }

      @Override
      public boolean cancel() {
        pnCalques_.getProject().setComputeGrid(oldGrid_);
        pnCalques_.getProject().setComputeCntGrid(null);
        return super.cancel();
      }

      @Override
      public boolean ok() {
        if (pnCalques_.getProject().hasInstantRawResults()
            && !pnCalques_.getCtuluUI().question(PivResource.getS("Suppression des r�sultats"), PivResource.getS("Attention : des r�sultats existent et seront supprim�s si vous modifiez la grille.\nVoulez-vous continuer ?"))) {
          return false;
        }

        pnCalques_.getProject().setComputeCntGrid(null);
        
        pnCalques_.getProject().setInstantRawResults(null);
        return super.ok();
      }
      
      
      
    };
    pn.setParams(params);

//    pnCalques_.get
//    if (pn.afficheModaleOk(pnCalques_.getCtuluUI().getParentComponent(), PivResource.getS("D�finition du nombre de points sur la grille"))) {
    pn.affiche(pnCalques_, PivResource.getS("D�finition du nombre de points sur la grille"), CtuluDialog.OK_CANCEL_APPLY_OPTION);
  }
  
  @Override
  public JComponent buildContentPane() {
    if (palette_==null){
      // Cr�ation de l'internal frame de reprise
      palette_=new PivSimplifiedEditionPalette(pnCalques_, PivResource.getS("Contour de grille"));
      palette_.setCalqueInteraction(cqEdition_);
      // Ecoute de la fen�tre principale pour pouvoir se r�duire avec elle
      final Container c = SwingUtilities.getAncestorOfClass(JInternalFrame.class, pnCalques_);
      if (c instanceof JInternalFrame)
        ((JInternalFrame) c).addInternalFrameListener(new Piv2dFrameListener());
    }
    return palette_.getComponent();
  }
}
