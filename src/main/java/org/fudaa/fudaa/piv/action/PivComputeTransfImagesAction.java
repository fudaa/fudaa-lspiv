/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivExePanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivProgressionPanel;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivTaskAbstract;
import org.fudaa.fudaa.piv.PivTaskObserverI;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivProject.OrthoMode;
import org.fudaa.fudaa.piv.utils.PivUtils;

/**
 * Une action pour lancer l'orthorectification.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivComputeTransfImagesAction extends EbliActionSimple {
  PivImplementation impl;
  CtuluDialog diProgress_;
  
  public PivComputeTransfImagesAction(PivImplementation _impl) {
    super(PivResource.getS("Transformer les images"), null, "COMPUTE_ORTHO");

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Lancement de l'orthorectification sous forme d'un thread s�par�.
   * @param _e L'evenement pour l'action.
   */
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }

    // Wrap, sinon, la fenetre est trop large.
//    String questionText = PivUtils.wrapMessage(
//        PivResource.getS("La dur�e du calcul de transformation des images est inversement proportionnelle � la taille des ortho-images demand�e (r�solution et emprise), et au nombre d'images � transformer.\n\n")+
//        PivResource.getS("Voulez-vous lancer le calcul de transformation des images ?"), 132);
//    if (!impl.question(PivResource.getS("Calcul de transformation des images"), questionText)) {
//      return;
//    }
    
    
    if (!PivExeLauncher.instance().areExeOK()) {
      PivExePanel pnExe = new PivExePanel();

      if (!pnExe.afficheModaleOk(impl.getFrame(), PivResource.getS("R�pertoire contenant les executables"))) {
        return;
      }
    }

    // La tache a ex�cuter.
    PivTaskAbstract r=new PivTaskAbstract(PivResource.getS("Transformation des images")) {

      public boolean act(PivTaskObserverI _observer) {
        if (impl.getCurrentProject().getOrthoMode() != OrthoMode.SCALING) {
          CtuluLog ana = new CtuluLog();
          ana.setDesc(getName());
//            PivExeLauncher.instance().computeOrthoCoefs(ana, impl.getCurrentProject(), this);
//            if (ana.containsErrorOrSevereError()) {
//              impl.error(ana.getResume());
//              return;
//            }

          // Sp�cifique orthorectification, quand tous les points ont la m�me cote.
          Double z = PivOrthoPoint.getIdenticalZ(impl.getCurrentProject().getOrthoPoints());
          PivOrthoParameters params = impl.getCurrentProject().getOrthoParameters();
          if (impl.getCurrentProject().getOrthoMode() != OrthoMode.SCALING && z != null && !z.equals(params.getWaterElevation())) {
            // TODO Deplacer ce message qui devrait etre en dehors de la tache.
            impl.message(
                PivResource.getS("La cote du plan d'eau va �tre modifi�e pour correspondre aux cotes Z des GRP"));
            params.setWaterElevation(z);
            impl.getCurrentProject().setOrthoParameters(params);
          }

          PivExeLauncher.instance().computeTransfImg(ana, impl.getCurrentProject(), false, _observer);
          if (ana.containsErrorOrSevereError()) {
            impl.error(ana.getResume());
            return false;
          }
        }
        else {
          CtuluLog ana = new CtuluLog();
          ana.setDesc(getName());
          PivExeLauncher.instance().computeScalingImages(ana, impl.getCurrentProject(), false, _observer);
          if (ana.containsErrorOrSevereError()) {
            impl.error(ana.getResume());
            return false;
          }
        }

        impl.message(PivResource.getS("La transformation des images s'est termin�e avec succ�s"));
        return true;
      }
    };

    PivProgressionPanel pnProgress_=new PivProgressionPanel(r);
    diProgress_=pnProgress_.createDialog(impl.getParentComponent());
    diProgress_.setOption(CtuluDialog.ZERO_OPTION);
    diProgress_.setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
    diProgress_.setTitle(r.getName());

    r.start();
    diProgress_.afficheDialogModal();
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    // Controle des points ortho uniquement pour full ortho.
    if (impl.getCurrentProject().getOrthoMode() != OrthoMode.SCALING) {
      if (impl.getCurrentProject().getOrthoPoints() == null) {
        impl.error(PivResource.getS("Erreur"), PivResource.getS("Aucun point de r�f�rence au sol n'a �t� d�fini"));
        return false;
      }
      String mes=PivOrthoPoint.areOrthoPointsOk(impl.getCurrentProject().getOrthoPoints());
      if (mes != null) {
        impl.error(PivResource.getS("Erreur"), mes);
        return false;
      }
    }
    if (impl.getCurrentProject().getOrthoParameters()==null) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Les param�tres d'orthorectification n'ont pas �t� donn�s"));
      return false;
    }
    if (impl.getCurrentProject().isStabilizationActive() && impl.getCurrentProject().getStabilizedImageFiles().length == 0) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Le projet ne contient aucune image stabilis�e"));
      return false;
    }
    else if (!impl.getCurrentProject().hasSrcImages(PivProject.SRC_IMAGE_TYPE.CALCULATION)) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Le projet ne contient aucune image source"));
      return false;
    }
    
    return true;
  }

  public String getEnableCondition() {
    return PivResource.getS("Les param�tres et points de r�f�rence doivent �tre renseign�s");
  }
}
