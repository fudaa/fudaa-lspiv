/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivExePanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivManageTransformedImagesPanel;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.io.PivExeLauncher;

/**
 * Une action pour manager les images transformées.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivManageTransformedImagesAction extends EbliActionSimple {
  PivImplementation impl;
  PivManageTransformedImagesPanel pnMngImages;
  CtuluDialog diProgress_;

  /**
   * Constructeur.
   * 
   * @param _impl L'implementation.
   */
  public PivManageTransformedImagesAction(PivImplementation _impl) {
    super(PivResource.getS("Sous échantillonnage des images transformées..."), null, "MANAGE_TRANSFORMED_IMAGES");

    impl = _impl;
    setEnabled(false);
  }

  /**
   * Selectionne les images, pour les reconditionne dans un thread séparé.
   * 
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }

    if (!PivExeLauncher.instance().areExeOK()) {
      PivExePanel pnExe = new PivExePanel();

      if (!pnExe.afficheModaleOk(impl.getFrame(), PivResource.getS("Répertoire contenant les executables"))) {
        return;
      }
    }

    // Selection des images
    if (pnMngImages == null)
      pnMngImages = new PivManageTransformedImagesPanel(impl);
    
    pnMngImages.setTimeStep(impl.getCurrentProject().getTimeStep());
    pnMngImages.setSamplingPeriod(impl.getCurrentProject().getComputeSamplingPeriod());

    if (!pnMngImages.afficheModaleOk(impl.getFrame(), PivResource.getS("Sous échantillonnage des images transformées"),
        CtuluDialog.OK_CANCEL_OPTION)) {
      return;
    }
  }

  /**
   * @return true Si toutes les données sont présentes pour un lancement.
   */
  public boolean isValide() {

    if (impl.getCurrentProject().getTransfImageFiles().length < 2) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Le projet doit contenir au moins 2 images transformées"));
      return false;
    }
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Le projet doit contenir au moins 2 images transformées");
  }
}
