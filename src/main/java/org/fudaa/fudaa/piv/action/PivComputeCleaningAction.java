/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivComputeCleaningPanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;

/**
 * Une action pour lancer toute la chaine de calculs. Seuls les calculs dont les param�tres ont chang� sont relanc�s.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivComputeCleaningAction extends EbliActionSimple {
  PivImplementation impl;
  CtuluDialog diProgress_;

  /**
   * Constructeur.
   * 
   * @param _impl L'implementation.
   */
  public PivComputeCleaningAction(PivImplementation _impl) {
    super(PivResource.getS("Nettoyer les r�sultats de calculs..."), null, "CLEAN_COMPUTATIONS");
    setKey(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_MASK | KeyEvent.SHIFT_MASK));

    impl = _impl;
    setEnabled(false);
  }

  /**
   * Lance l'analyse par PIV, dans un thread s�par�.
   * 
   * @param _e L'evenement pour l'action.
   */
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }


    PivComputeCleaningPanel pnProgress_ = new PivComputeCleaningPanel(impl);
    diProgress_ = pnProgress_.createDialog(impl.getParentComponent());
    diProgress_.setOption(CtuluDialog.OK_CANCEL_OPTION);
//    diProgress_.setButtonText(CtuluDialog.OK_OPTION, PivResource.getS("Oui"));
    diProgress_.setTitle(PivResource.getS("Nettoyer les r�sultats de calculs"));
    diProgress_.afficheDialogModal();
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
//    if (impl.getCurrentProject().getComputeGrid()==null) {
//      impl.error(PivResource.getS("Erreur"), PivResource.getS("Les points de grille n'ont pas �t� d�finis"));
//      return false;
//    }
//    if (impl.getCurrentProject().getComputeParameters()==null || !impl.getCurrentProject().getComputeParameters().isFilledForComputing()) {
//      impl.error(PivResource.getS("Erreur"), PivResource.getS("Les param�tres de calcul n'ont pas �t� donn�s"));
//      return false;
//    }
//    if (impl.getCurrentProject().getTransfImageFiles().length<2) {
//      impl.error(PivResource.getS("Erreur"), PivResource.getS("Le projet doit contenir au moins 2 images transform�es"));
//      return false;
//    }

    return true;
  }

  public String getEnableCondition() {
    return PivResource.getS("Le projet doit �tre cr�� et comporter au moins 2 images");
  }
}
