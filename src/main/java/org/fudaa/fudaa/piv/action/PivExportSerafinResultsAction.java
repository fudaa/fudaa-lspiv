/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.HashMap;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluFavoriteFiles;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModelDefault;
import org.fudaa.fudaa.commun.impl.FudaaPanelTask;
import org.fudaa.fudaa.commun.impl.FudaaPanelTaskModel;
import org.fudaa.fudaa.meshviewer.MvSelectionNodeOrEltData;
import org.fudaa.fudaa.meshviewer.export.MvExportActSerafin;
import org.fudaa.fudaa.meshviewer.export.MvExportFactory;
import org.fudaa.fudaa.piv.PivExportImageFilesPanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.layer.PivResultsModel;
import org.fudaa.fudaa.piv.metier.PivResultsI;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.bu.BuFileFilter;

/**
 * Une action pour exporter les r�sultats sous fichier serafin.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivExportImagesAction.java 9523 2017-01-24 15:39:14Z bmarchan $
 */
public class PivExportSerafinResultsAction extends EbliActionSimple {
  
  static final String TXTEXPORTERRAWRES=PivResource.getS("bruts");
  static final String TXTEXPORTERFLTRES=PivResource.getS("filtr�s");
  static final String TXTEXPORTERAVERES=PivResource.getS("moyenn�s");

  public enum ResultsType {
    RAW,
    FILTERED,
    AVERAGE
  }
  
  interface ResultsAdapter {
    public FudaaCourbeTimeListModelDefault getTimes();
    public ListModel<CtuluVariable> getVariables();
    public EfGridData getGridData();
  }
    
  /**
   * Pour les r�sultats 2D
   * @author marchand
   */
  class D2ResultsAdapter implements ResultsAdapter {
    
    PivResultsI[] res;
    FudaaCourbeTimeListModelDefault lsTime;
    ListModel<CtuluVariable> vars;
    PivResultsModel mdl;
    
    public D2ResultsAdapter(PivResultsI[] _res) {
      res = _res;
    }

    @Override
    public FudaaCourbeTimeListModelDefault getTimes() {
      if (lsTime == null) {
        double[] times=new double[res.length];
        for (int i=0; i < times.length; i++) {
//          times[i]=i * impl.getCurrentProject().getComputeParameters().getTimeInterval();
          times[i]=i+1;
        }
        
        lsTime = new FudaaCourbeTimeListModelDefault(times, CtuluNumberFormatDefault.DEFAULT_FMT);
      }
      
      return lsTime;
    }

    @Override
    public ListModel<CtuluVariable> getVariables() {
      if (vars == null) {
        vars=new ListModel<CtuluVariable>() {

          @Override
          public int getSize() {
            return res.length == 0 ? 0 : res[0].getResults().length;
          }

          @Override
          public CtuluVariable getElementAt(int index) {
            if (res.length == 0)
              return null;

            return res[0].getResults()[index];
          }

          @Override
          public void addListDataListener(ListDataListener l) {
          }

          @Override
          public void removeListDataListener(ListDataListener l) {
          }
        };
      }
      return vars;
    }

    @Override
    public EfGridData getGridData() {
      if (mdl==null) {
        mdl = new PivResultsModel(impl.getCurrentProject(), null, res);
      }
      return mdl;
    }
  }
  
  PivImplementation impl;
  PivExportImageFilesPanel pnExportImages;
  CtuluDialog diProgress_;
  ResultsType type_;
  CtuluFileChooser fcExportRes_;
  
  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivExportSerafinResultsAction(PivImplementation _impl, ResultsType _type) {
    super((_type == ResultsType.RAW ? TXTEXPORTERRAWRES: _type == ResultsType.FILTERED ? TXTEXPORTERFLTRES: TXTEXPORTERAVERES)+"...", null, 
          _type == ResultsType.RAW ? "EXPORT_RAW_RESULTS": _type == ResultsType.FILTERED ? "EXPORT_FILTERED_RESULTS":"EXPORT_AVERAGE_RESULTS");
    
    type_=_type;
    impl=_impl;
    setEnabled(false);
  }

  /**
   * Exporte les images transform�es au format pgm.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }
    
    String fsave = PivUtils.getFilenameWithoutExtension(impl.getSavedProjectFile().getName(), PivUtils.FILE_FLT_PROJ.getExtensions());
    fsave += "." + PivUtils.FILE_FLT_SERA.getFirstExt();
    
    // BM : On ne demande pas le nom du fichier resultat � suavegarder, il est par d�faut dans la fenetre d'export.
//    if (fcExportRes_ == null) {
//      fcExportRes_=new CtuluFileChooser(true);
//      fcExportRes_.setTester(new CtuluFileChooserTestWritable(impl));
//      fcExportRes_.setAcceptAllFileFilterUsed(true);
//      fcExportRes_.setFileFilter(PivUtils.FILE_FLT_SERA);
//      fcExportRes_.setMultiSelectionEnabled(false);
//      fcExportRes_.setDialogTitle(PivResource.getS("Export des r�sultats"));
//    }
//    String fsave=impl.getSavedProjectFile().getName();
//    fsave=fsave.substring(0, fsave.indexOf(PivUtils.FILE_FLT_PROJ.getFirstExt()) - 1);
//    fcExportRes_.setSelectedFile(new File(fsave + ".ser"));

//    if (fcExportRes_.showSaveDialog(impl.getFrame()) == JFileChooser.CANCEL_OPTION) {
//      return;
//    }
//    File defaultFile = fcExportRes_.getSelectedFile();;
    
    
    // Les formats autoris�s pour l'export de r�sultats.
    BuFileFilter seraFlt = PivUtils.FILE_FLT_SERA;
    
    // Le nom du fichier par d�faut.
    File defaultFile = new File(CtuluFavoriteFiles.getLastDir(),fsave);
    
    ResultsAdapter adapter;
    switch (type_) {
    case RAW:
    default:
      adapter = new D2ResultsAdapter(impl.getCurrentProject().getInstantRawResults());
      break;
    case FILTERED:
      adapter = new D2ResultsAdapter(impl.getCurrentProject().getInstantFilteredResults());
      break;
    case AVERAGE:
      adapter = new D2ResultsAdapter(new PivResultsI[] {impl.getCurrentProject().getAverageResults()});
      break;
    }
    
    MvExportFactory fact=new MvExportFactory(impl, adapter.getVariables(), adapter.getTimes(), adapter.getGridData(), null, null, 0) {
      public FudaaPanelTaskModel createExporter(final File _initFile, final BuFileFilter _filter, final MvSelectionNodeOrEltData _selection) {
        boolean isSerafinVolume = false;
        
        return MvExportActSerafin.createSerafinTaskModel(impl, this, _initFile, _selection, isSerafinVolume, true, false, getTelemacVarNamesMapping());
      }
    };
    
    final FudaaPanelTaskModel task = fact.createExporter(defaultFile, seraFlt, null);
    if (task != null) {
      new FudaaPanelTask(impl, task).afficheDialog();
    }
  }
  
  /**
   * @return Le mapping de nom entre les variables et les noms T�lemac.
   * Necessaire pour la reconnaissance des donn�es pour l'affichage des vecteurs.
   * 
   */
  protected HashMap<CtuluVariable, String> getTelemacVarNamesMapping() {
    HashMap<CtuluVariable, String> var2TelemacNames=new HashMap<>();
    
    var2TelemacNames.put(PivResultsI.ResultType.VX, "VELOCITY U");
    var2TelemacNames.put(PivResultsI.ResultType.VY, "VELOCITY V");
    var2TelemacNames.put(PivResultsI.ResultType.NORME, "SCALAR VELOCITY");
    // Les autres variables n'ont pas d'�quivalence sp�cifiques.
    
    return var2TelemacNames;
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Les r�sultats doivent exister");
  }
}
