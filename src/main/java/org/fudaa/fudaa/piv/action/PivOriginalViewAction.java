/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import javax.swing.KeyStroke;

import org.fudaa.ebli.commun.EbliActionChangeState;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivOriginalView;
import org.fudaa.fudaa.piv.PivVisuPanel;

/**
 * Une action pour mettre la vue en configuration de visualisation d'image d'origine.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivOriginalViewAction extends EbliActionChangeState {
  PivImplementation impl;

  /**
   * Constructeur
   * @param _impl L'implementation.
   */
  public PivOriginalViewAction(PivImplementation _impl) {
    super(PivOriginalView.TITLE, null, "PIV_ORIGINAL_VIEW");
    setKey(KeyStroke.getKeyStroke('I'));

    impl=_impl;
    setEnabled(true);
  }

  @Override
  public void changeAction() {
    impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_ORIGINAL_VIEW);
    impl.updateActionsState();
  }
}
