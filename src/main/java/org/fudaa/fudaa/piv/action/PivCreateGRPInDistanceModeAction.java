/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivCreateGRPInDistanceModePanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;

/**
 * Une action pour cr�er des .
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivSelectImagesAction.java 9500 2017-01-09 17:22:31Z bmarchan $
 */
public class PivCreateGRPInDistanceModeAction extends EbliActionSimple {
  PivImplementation impl;
  PivCreateGRPInDistanceModePanel pnParams;
  CtuluDialog diProgress_;

  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivCreateGRPInDistanceModeAction(PivImplementation _impl) {
    super(PivResource.getS("Mode 4 points en distances..."), null, "DISTANCE_MODE_GRP");

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Selectionne les images, pour les reconditionne dans un thread s�par�.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }
    
    impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_ORIGINAL_VIEW);
    
    if (impl.getCurrentProject().getOrthoPoints()!=null) {
      if (!impl.question(PivResource.getS("Mode 4 points en distances"),
          PivResource.getS("Des points existent d�j�. Ils vont �tre supprim�s.\nVoulez vous continuer ?"))) {
        return;
      }
    }

    // Affichage du panneau
    pnParams = new PivCreateGRPInDistanceModePanel(impl);
    pnParams.affiche(impl.getFrame(), PivResource.getS("Mode 4 points en distances"), CtuluDialog.OK_CANCEL_OPTION);
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Il doit exister au moins une image source");
  }
}
