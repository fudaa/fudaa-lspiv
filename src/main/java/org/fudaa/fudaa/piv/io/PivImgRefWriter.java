package org.fudaa.fudaa.piv.io;

import java.awt.Dimension;
import java.io.PrintWriter;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;


/**
 * Une classe pour ecrire les parametres d'orthorectification sur fichier.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivImgRefWriter extends FileCharSimpleWriterAbstract<Object[]> {

  /**
   * Ecrit les paramètres d'orthorectification.
   * param _o Un tableau de PivOrthoPoint[].
   */
  protected void internalWrite(final Object[] _o) {
    if (_o.length!=2 ||
        !(_o[0] instanceof PivOrthoParameters) ||
        !(_o[1] instanceof Dimension)) {
      donneesInvalides(_o);
      return;
    }

    PivOrthoParameters params = (PivOrthoParameters)_o[0];
    Dimension imgSize = (Dimension)_o[1];

    final PrintWriter writer = new PrintWriter(out_);

    try {
      // xmin, ymin
      writer.println("xmin,ymin");
      writer.println(params.getXmin()+" "+params.getYmin());
      // xmax, ymax
      writer.println("xmax,ymax");
      writer.println(params.getXmax()+" "+params.getYmax());
      // Résolution
      writer.println("resolution");
      writer.println(params.getResolution());
      // Taille des images de base
      writer.println("taille des images de base : ni - nj");
      writer.println(imgSize.height+" "+imgSize.width);
    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
  }
}
