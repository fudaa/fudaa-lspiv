/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;
import org.fudaa.fudaa.piv.PivResource;

/**
 * Un lecteur pour les fichiers transect en abscisse/cote.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivTransectAbsZReader extends FileCharSimpleReaderAbstract<List<Double[]>> implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;
  /** Nb d'octets du fichier a lire pour stat sur pourcentage effectu� */
  int nbOctets;

  /**
   * Constructeur.
   */
  public PivTransectAbsZReader() {
  }

  /**
   * Lit les points et retourne le transect.
   * @return Le transect
   */
  @Override
  protected List<Double[]> internalRead() {
    return readParams();
  }

  @Override
  protected void processFile(final File _f) {
    nbOctets = (int) _f.length();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized List<Double[]> readParams() {
    List<Double[]> points = new ArrayList<>();

    if (super.in_ == null) {
      analyze_.addErrorFromFile(PivResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      int lu=0;
      boolean afficheAvance = false;
      if ((progress_ != null) && (nbOctets > 0)) {
        afficheAvance = true;
        progress_.setProgression(0);
      }
      int pourcentageEnCours = 0;

      in_.setJumpBlankLine(true);

      // Boucle jusque fin de fichier. Exception EOF si fin.
      while (true) {
        in_.readFields();
        
        points.add(new Double[] { in_.doubleField(0), in_.doubleField(1) });
        lu+=26; // Si le formattage en fortran

        if ((afficheAvance) && ((lu * 100 / nbOctets) >= (pourcentageEnCours + 20))) {
          pourcentageEnCours += 20;
          progress_.setProgression(pourcentageEnCours);
        }
      }
    }
    // Sortie normale
    catch (final EOFException e) {
      return points;
    }
    catch (final IOException | NumberFormatException e) {
      analyze_.addErrorFromFile(PivResource.getS("Une erreur de lecture s'est produite"), in_.getLineNumber());
      return points;
    }
    finally {
      if (progress_ != null) {
        progress_.setProgression(100);
      }
    }
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  @Override
  public void stop() {
    bstop_ = true;
  }
}
