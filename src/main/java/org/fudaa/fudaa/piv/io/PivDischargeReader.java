/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import gnu.trove.TDoubleArrayList;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivFlowResults;
import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;

/**
 * Un lecteur pour les fichiers des r�sultats de calcul de d�bit.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivDischargeReader extends FileCharSimpleReaderAbstract<PivFlowResults> implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;
  /** Nb d'octets du fichier a lire pour stat sur pourcentage effectu� */
  int nbOctets;

  /**
   * Le constructeur.
   */
  public PivDischargeReader() {
  }

  /**
   * Lit les r�sultats de debit et les retourne.
   * @return Les r�sultats
   */
  protected PivFlowResults internalRead() {
    return readResults();
  }

  @Override
  protected void processFile(final File _f) {
    nbOctets = (int) _f.length();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized PivFlowResults readResults() {
    PivFlowResults res = new PivFlowResults();
    TDoubleArrayList lx=new TDoubleArrayList();
    TDoubleArrayList ly=new TDoubleArrayList();
    TDoubleArrayList lvx=new TDoubleArrayList();
    TDoubleArrayList lvy=new TDoubleArrayList();
    TDoubleArrayList lmode=new TDoubleArrayList();

    if (super.in_ == null) {
      analyze_.addErrorFromFile(PivResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      int lu=0;
      boolean afficheAvance = false;
      if ((progress_ != null) && (nbOctets > 0)) {
        afficheAvance = true;
        progress_.setProgression(0);
      }
      int pourcentageEnCours = 0;

      in_.setJumpBlankLine(true);

      // 1ere ligne
      in_.readFields();
      // Le niveau d'eau
      res.setWaterElevation(in_.doubleField(0));
      // Debit
      res.setDischarge(in_.doubleField(1));
      // Surface mouill�e
      res.setWettedArea(in_.doubleField(2));
      // Vitesse moyenne
      res.setMeanVelocity(in_.doubleField(3));
      // D�bit mesur� (Fudaa-LSPIV > 1.5.0)
      if (in_.getNumberOfFields()>4) {
        res.setValue(0, ResultType.MEASURED_DISCHARGE, in_.doubleField(4));
      }
      // Coefficient de vitesse calcul� (Fudaa-LSPIV > 1.7.2)
      if (in_.getNumberOfFields()>5) {
        res.setValue(0, ResultType.COMPUTED_VEL_COEFFICIENT, in_.doubleField(5));
      }

      // Boucle jusque fin de fichier. Exception EOF si fin.
      while (true) {
        in_.readFields();

          // X
          lx.add(in_.doubleField(2));
          // Y
          ly.add(in_.doubleField(3));
          // VX
          lvx.add(in_.doubleField(5));
          // VY
          lvy.add(in_.doubleField(6));
          // Extrapolation
          lmode.add(in_.doubleField(8));

        lu+=84; // Si le formattage en fortran

        if ((afficheAvance) && ((lu * 100 / nbOctets) >= (pourcentageEnCours + 20))) {
          pourcentageEnCours += 20;
          progress_.setProgression(pourcentageEnCours);
        }
      }
    }
    // Sortie normale
    catch (final EOFException e) {
      res.setX(lx.toNativeArray());
      res.setY(ly.toNativeArray());
      res.setVx(lvx.toNativeArray());
      res.setVy(lvy.toNativeArray());
      res.setComputeMode(lmode.toNativeArray());
    }
    catch (final IOException e) {
      analyze_.addErrorFromFile(PivResource.getS("Une erreur de lecture s'est produite"), in_.getLineNumber());
    }
    catch (final NumberFormatException e) {
      analyze_.addErrorFromFile(PivResource.getS("Une erreur de lecture s'est produite"), in_.getLineNumber());
    }
    
    if (progress_ != null) {
      progress_.setProgression(100);
    }

    return res;
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  public void stop() {
    bstop_ = true;
  }
}
