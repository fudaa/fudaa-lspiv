package org.fudaa.fudaa.piv.io;

import java.io.PrintWriter;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;


/**
 * Une classe pour ecrire la cote d'eau sur fichier.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivHWriter extends FileCharSimpleWriterAbstract<Double> {

  /**
   * Ecrit les paramètres d'orthorectification.
   * param _o Un tableau de PivOrthoPoint[].
   */
  protected void internalWrite(final Double _o) {
    Double cote = _o;

    final PrintWriter writer = new PrintWriter(out_);

    try {
      // Cote d'eau
      writer.println(cote);
    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }

    writer.close();
  }
}
