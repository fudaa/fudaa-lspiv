/*
 * @creation 15 f�vr. 07
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.awt.Dimension;
import java.io.PrintWriter;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.ebli.geometrie.VecteurGrPoint;
import org.fudaa.fudaa.piv.metier.PivSamplingVideoParameters;

/**
 * La classe pour ecrire le fichier de param�tres d'�chantillonnage video.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivSamplingVideoParamWriter extends FileCharSimpleWriterAbstract<PivSamplingVideoParameters> {

  /**
   * Ecrit les param�tres.
   */
  protected void internalWrite(final PivSamplingVideoParameters _pars) {

    final PrintWriter writer = new PrintWriter(out_);

    try {
      // Temps de d�but
      writer.println(_pars.getSamplingBeginTime());
      // Dur�e
      writer.println(_pars.getSamplingEndTime()-_pars.getSamplingBeginTime());
      // La periode de conservation des images
      writer.println(_pars.getSamplingPeriod());
      // Les dimensions de l'image �chantillonn�e
      Dimension size = _pars.getSamplingSize();
      writer.println(size.width + " " + size.height);
      // Le chemin complet vers le fichier vid�o
      writer.println("\"" + _pars.getVideoFile().getAbsolutePath() + "\"");

    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
  }
}
