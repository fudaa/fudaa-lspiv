package org.fudaa.fudaa.piv.io;

import java.io.PrintWriter;
import java.util.Locale;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.fudaa.piv.metier.PivResultsI;
import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;


/**
 * Une classe pour ecrire un fichier des r�sultats scalaires instantan�es filtr�s en espace r�el.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivParamWriter.java 9455 2016-11-18 11:05:02Z bmarchan $
 */
public class PivInstantScalWriter extends FileCharSimpleWriterAbstract<PivResultsI> {

  /**
   * Ecrit les resultats.
   */
  @Override
  protected void internalWrite(final PivResultsI _res) {
    if (!_res.hasResult(ResultType.NORME)  ||
        !_res.hasResult(ResultType.CORREL) ||
        !_res.hasResult(ResultType.OMEGA)  ||
        !_res.hasResult(ResultType.DIVERG)) {
      donneesInvalides(_res);
      return;
    }

    final PrintWriter writer = new PrintWriter(out_);

    try {
      
      for (int i=0; i<_res.getNbPoints(); i++) {
        // Format fixe.
        String s=String.format(Locale.US, "%8.4f %8.4f %8.4f %8.4f %8.4f %8.4f", _res.getX(i),_res.getY(i),_res.getValue(i, ResultType.NORME),_res.getValue(i, ResultType.CORREL),_res.getValue(i, ResultType.OMEGA),_res.getValue(i, ResultType.DIVERG));
        writer.println(s);
      }
    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
  }
}
