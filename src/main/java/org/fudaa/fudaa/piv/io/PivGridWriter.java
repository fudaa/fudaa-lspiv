package org.fudaa.fudaa.piv.io;

import java.io.PrintWriter;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.fudaa.piv.metier.PivGrid;

/**
 * Une classe pour ecrire les points de grille sur fichier.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivGridWriter extends FileCharSimpleWriterAbstract<PivGrid> {

  /**
   * Ecrit les paramètres d'orthorectification.
   * param _o Un tableau de PivOrthoPoint[].
   */
  protected void internalWrite(final PivGrid _o) {
    PivGrid grid = _o;

    final PrintWriter writer = new PrintWriter(out_);

    try {

      // Boucle sur tous les points, ecriture des coordonnées en entier (images)
      for (int i=0; i<grid.getNbGeometries(); i++) {
        writer.println(((int)(grid.getX(i)+0.5))+" "+((int)(grid.getY(i)+0.5)));
      }

    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
  }
}
