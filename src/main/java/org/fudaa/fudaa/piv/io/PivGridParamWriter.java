package org.fudaa.fudaa.piv.io;

import java.io.PrintWriter;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.fudaa.piv.metier.PivCntGrid;

/**
 * Une classe pour ecrire un contour de grille sur fichier.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivGridParamWriter extends FileCharSimpleWriterAbstract<PivCntGrid> {

  /**
   * Ecrit les paramètres d'orthorectification.
   * param _o Un tableau de PivOrthoPoint[].
   */
  protected void internalWrite(final PivCntGrid _o) {
    PivCntGrid cntGrid = _o;

    final PrintWriter writer = new PrintWriter(out_);

    try {
      // Boucle sur les points du contour.
      writer.println("coordinates of 4 following corners of the grid (x,y per line)");
      for (int i=0; i<cntGrid.getContour().nombre(); i++) {
        writer.println((int)cntGrid.getContour().sommet(i).x_+" "+(int)cntGrid.getContour().sommet(i).y_);
      }

      writer.println("Number of steps on the segments 1 and 3");
      writer.println(""+cntGrid.getNbXPoints());

      writer.println("Number of steps on the segments 2 and 4");
      writer.println(""+cntGrid.getNbYPoints());

    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
  }
}
