/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivSamplingVideoParameters;

/**
 * Un lecteur pour un fichier de param�tres d'�chantillonnage video.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivSamplingVideoParamReader extends FileCharSimpleReaderAbstract<PivSamplingVideoParameters> implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;

  /**
   * Lit les parametres.
   */
  protected PivSamplingVideoParameters internalRead() {
    return readParams();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized PivSamplingVideoParameters readParams() {
    PivSamplingVideoParameters pars = new PivSamplingVideoParameters();

    if (super.in_ == null) {
      analyze_.addError(PivResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      if (progress_ != null) {
        progress_.setProgression(0);
      }

      in_.setJumpBlankLine(true);

      // Temps de d�but
      in_.readFields();
      double beginTime = in_.doubleField(0);
      // Dur�e de l'�chantillonage.
      in_.readFields();
      double delay = in_.doubleField(0);
      // La periode de conservation des images
      in_.readFields();
      int samplingPeriod = in_.intField(0);
      // Les dimensions de l'image �chantillonn�e
      in_.readFields();
      int width = in_.intField(0);
      int height = in_.intField(1);
      // Le chemin complet vers le fichier vid�o
      in_.readFields();
      String videoFile = in_.getLine().trim();
      if (videoFile.startsWith("\""))
        videoFile = videoFile.substring(1);
      if (videoFile.endsWith("\""))
        videoFile = videoFile.substring(0, videoFile.length()-1);

      pars.setSamplingBeginTime(beginTime);
      pars.setSamplingEndTime(beginTime + delay);
      pars.setSamplingPeriod(samplingPeriod);
      pars.setSamplingSize(new Dimension(width, height));
      pars.setVideoFile(new File(videoFile));

      if (progress_ != null) {
        progress_.setProgression(100);
      }
      
      return pars;
    }
    catch (final IOException e) {
      analyze_.manageException(e);
    }
    catch (final NumberFormatException e) {
      analyze_.manageException(e);
    }
    
    return null;
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  public void stop() {
    bstop_ = true;
  }
}
