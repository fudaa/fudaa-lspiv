/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivResultsDefault;
import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;

/**
 * Un lecteur pour les fichiers des r�sultats instantan�es filtr�s en espace r�el.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivInstantFilteredReader extends FileCharSimpleReaderAbstract<PivResultsDefault> implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;
  /** Nb d'octets du fichier a lire pour stat sur pourcentage effectu� */
  int nbOctets;

  /**
   * Le constructeur.
   */
  public PivInstantFilteredReader() {
  }

  /**
   * Lit les r�sultats de vitesses et les retourne.
   * @return Les r�sultats
   */
  protected PivResultsDefault internalRead() {
    return readVelocities();
  }

  protected void processFile(final File _f) {
    nbOctets = (int) _f.length();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized PivResultsDefault readVelocities() {
    // TODO Bertrand : A verifier
    PivResultsDefault res = new PivResultsDefault(new ResultType[]{ResultType.VX,ResultType.VY,ResultType.CORREL});

    if (super.in_ == null) {
      analyze_.addErrorFromFile(PivResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      int lu=0;
      boolean afficheAvance = false;
      if ((progress_ != null) && (nbOctets > 0)) {
        afficheAvance = true;
        progress_.setProgression(0);
      }
      int pourcentageEnCours = 0;

      in_.setJumpBlankLine(true);

      // Boucle jusque fin de fichier. Exception EOF si fin.
      while (true) {
        in_.readFields();

        double[] vals=new double[res.getResults().length];
        for (int i=0; i<res.getResults().length; i++) {
          vals[i]=in_.doubleField(i+2);
        }
        res.addPoint(in_.doubleField(0), in_.doubleField(1), vals);

        lu+=26; // Si le formattage en fortran

        if ((afficheAvance) && ((lu * 100 / nbOctets) >= (pourcentageEnCours + 20))) {
          pourcentageEnCours += 20;
          progress_.setProgression(pourcentageEnCours);
        }
      }
    }
    // Sortie normale
    catch (final EOFException e) {
    }
    catch (final IOException e) {
      analyze_.addErrorFromFile(PivResource.getS("Une erreur de lecture s'est produite"), in_.getLineNumber());
    }
    catch (final NumberFormatException e) {
      analyze_.addErrorFromFile(PivResource.getS("Une erreur de lecture s'est produite"), in_.getLineNumber());
    }
    
    if (progress_ != null) {
      progress_.setProgression(100);
    }

    return res;
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  public void stop() {
    bstop_ = true;
  }
}
