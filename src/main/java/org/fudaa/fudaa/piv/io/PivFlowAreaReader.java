/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.io.EOFException;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.fudaa.piv.PivResource;

/**
 * Un lecteur pour les fichiers PIV de points de zone d'�coulement.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivFlowAreaReader extends FileCharSimpleReaderAbstract<GrPolygone> implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;

  /**
   * Lit les points et les retourne.
   * @return La collection de points
   */
  protected GrPolygone internalRead() {
    return readPolygone();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized GrPolygone readPolygone() {
    GrPolygone pg = new GrPolygone();

    if (super.in_ == null) {
      analyze_.addError(PivResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      int lu = 0;
      boolean afficheAvance = false;
      if (progress_ != null) {
        afficheAvance = true;
        progress_.setProgression(0);
      }
      int pourcentageEnCours = 0;

      in_.setJumpBlankLine(true);

//      // Entete
//      in_.readFields();
//      String entete = in_.stringField(0);
//      if (!"GRP".equalsIgnoreCase(entete)) {
//        analyze_.addError(PivResource.getS("Format de fichier invalide : Format FlowArea attendu"), 0);
//        return null;
//      }
      // Nombre de points
      in_.readFields();
      int nbPts = in_.intField(0);

      lu += 3; // Si tous les champs remplis au maxi

      // Les points, jusqu'� fin de fichier.
      for (int i = 0; i < nbPts; i++) {
        if (bstop_) {
          return null;
        }

        in_.readFields();
        GrPoint pt = new GrPoint(in_.doubleField(0), in_.doubleField(1), 0);
        pg.sommets_.ajoute(pt);
        
        lu += 1; // Approximatif

        if ((afficheAvance) && ((lu * 100 / (nbPts+3)) >= (pourcentageEnCours + 20))) {
          pourcentageEnCours += 20;
          progress_.setProgression(pourcentageEnCours);
        }
      }
    }
    catch (final EOFException e) {
      analyze_.manageException(e);
    }
    catch (final IOException e) {
      analyze_.manageException(e);
    }
    catch (final NumberFormatException e) {
      analyze_.manageException(e);
    }
    
    return pg;
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  public void stop() {
    bstop_ = true;
  }
}
