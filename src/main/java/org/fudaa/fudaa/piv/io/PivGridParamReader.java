/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivCntGrid;

/**
 * Un lecteur pour les fichiers de contour de grille.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivGridParamReader extends FileCharSimpleReaderAbstract<PivCntGrid> implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;
  /** Nb d'octets du fichier a lire pour stat sur pourcentage effectu� */
  int nbOctets;

  /**
   * Le constructeur.
   */
  public PivGridParamReader() {
  }

  /**
   * Lit le contour de grille et le retourne.
   * @return Le contour.
   */
  protected PivCntGrid internalRead() {
    return readParams();
  }

  protected void processFile(final File _f) {
    nbOctets = (int) _f.length();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized PivCntGrid readParams() {
    PivCntGrid params = new PivCntGrid();

    if (super.in_ == null) {
      analyze_.addError(PivResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      if (progress_ != null) {
        progress_.setProgression(0);
      }

      in_.setJumpBlankLine(true);

      // Entete
      in_.readLine();

      // Lecture des points de polygone.
      GrPolygone pg=new GrPolygone();
      for (int i=0; i<4; i++) {
        in_.readFields();
        pg.sommets_.ajoute(in_.intField(0),in_.intField(1),0);
      }
      params.setContour(pg);

      // Number of steps on the segments 1 and 3
      in_.readLine();
      in_.readFields();
      params.setNbXPoints(in_.intField(0));

      // Number of steps on the segments 2 and 4
      in_.readLine();
      in_.readFields();
      params.setNbYPoints(in_.intField(0));
    }
    // Sortie normale
    catch (final EOFException e) {
    }
    catch (final IOException e) {
      analyze_.manageException(e);
    }
    catch (final NumberFormatException e) {
      analyze_.manageException(e);
    }
    
    if (progress_ != null) {
      progress_.setProgression(100);
    }

    return params;
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  public void stop() {
    bstop_ = true;
  }
}
