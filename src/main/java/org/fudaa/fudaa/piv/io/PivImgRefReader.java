/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.io.EOFException;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;

/**
 * Un lecteur pour les fichiers img_ref.dat des parametres pour l'orthorectification.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivImgRefReader extends FileCharSimpleReaderAbstract<PivOrthoParameters> implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;

  /**
   * Le constructeur.
   */
  public PivImgRefReader() {
  }

  /**
   * Lit les param�tres d'orthorectification et les retourne.
   * @return Les parametres
   */
  protected PivOrthoParameters internalRead() {
    return readParams();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized PivOrthoParameters readParams() {
    PivOrthoParameters params = new PivOrthoParameters();

    if (super.in_ == null) {
      analyze_.addError(PivResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      if (progress_ != null) {
        progress_.setProgression(0);
      }

      in_.setJumpBlankLine(true);

      // xmin,ymin
      in_.readLine();
      in_.readFields();
      params.setXmin(in_.doubleField(0));
      params.setYmin(in_.doubleField(1));

      // xmax,ymax
      in_.readLine();
      in_.readFields();
      params.setXmax(in_.doubleField(0));
      params.setYmax(in_.doubleField(1));

      // resolution
      in_.readLine();
      in_.readFields();
      params.setResolution(in_.doubleField(0));

      // Taille des images de base (on ne la stocke pas, calcul�e �
      // partir des fichiers images)
      in_.readLine();
      in_.readFields();

      if (progress_ != null) {
        progress_.setProgression(100);
      }
    }
    catch (final EOFException e) {
      analyze_.manageException(e);
    }
    catch (final IOException e) {
      analyze_.manageException(e);
    }
    catch (final NumberFormatException e) {
      analyze_.manageException(e);
    }
    return params;
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  public void stop() {
    bstop_ = true;
  }
}
