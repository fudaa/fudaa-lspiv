/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.io.EOFException;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters;

/**
 * Un lecteur pour les fichiers PIV de points de zone de stabilisation.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivStabAreasReader extends FileCharSimpleReaderAbstract<PivStabilizationParameters> implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;

  /**
   * Lit les zones et les retourne dans les parametres; Les autres param�tres n'ont pas d'utilit� (sauf la nature de zone.
   * @return La collection de points
   */
  protected PivStabilizationParameters internalRead() {
    return readZones();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized PivStabilizationParameters readZones() {
    PivStabilizationParameters params = new PivStabilizationParameters();

    if (super.in_ == null) {
      analyze_.addError(PivResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      int lu = 0;
      boolean afficheAvance = false;
      if (progress_ != null) {
        afficheAvance = true;
        progress_.setProgression(0);
      }
      int pourcentageEnCours = 0;

      in_.setJumpBlankLine(true);

      // Entete (inutilis�)
      in_.readFields();
      
      // Nature des zones
      in_.readFields();
      in_.readFields();
      params.setFlowAreas(in_.intField(0) == 1);
      
      // Nombre des zones
      in_.readFields();
      in_.readFields();
      int nbAreas = in_.intField(0);

      // Boucle sur les zones
      
      in_.readFields();
      
      lu += 3;
      // Le nombre de points approximatif pour l'avancement
      int nbPtsApprox = nbAreas * 10;

      for (int iarea = 0; iarea < nbAreas; iarea++) {
        GrPolygone pg = new GrPolygone();

        // Nombre de points
        in_.readFields();
        int nbPts = in_.intField(0);

        // Les points.
        for (int i = 0; i < nbPts; i++) {
          if (bstop_) {
            return null;
          }

          in_.readFields();
          GrPoint pt = new GrPoint(in_.doubleField(0), in_.doubleField(1), 0);
          pg.sommets_.ajoute(pt);

          lu += 1; // Approximatif

          if ((afficheAvance) && ((lu * 100 / (nbPtsApprox + 3)) >= (pourcentageEnCours + 20))) {
            pourcentageEnCours += 20;
            pourcentageEnCours = Math.max(pourcentageEnCours, 100);
            progress_.setProgression(pourcentageEnCours);
          }
        }
        params.getOutlines().add(pg);
      }
    }
    catch (final EOFException e) {
      analyze_.manageException(e);
    }
    catch (final IOException e) {
      analyze_.manageException(e);
    }
    catch (final NumberFormatException e) {
      analyze_.manageException(e);
    }
    
    return params;
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  public void stop() {
    bstop_ = true;
  }
}
