/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.io.EOFException;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivComputeParameters;
import org.fudaa.fudaa.piv.metier.PivTransectParams;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.fu.FuLog;

/**
 * Un lecteur pour les fichiers des parametres pour le calcul par PIV.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivParamReader extends FileCharSimpleReaderAbstract<Object[]> implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;
  
  /** 
   * Version du fichier <br>
   * Version 1 : Version initiale<br>
   * Version 2 : Ajout des limites des VY<br>
   * Version 3 : Ajout de la p�riode de sous echantillonage<br>
   * Version 4 : Suppression des filtres => d�plac�s dans filters.dat
   * */
  String version_="1.0";

  /**
   * Le constructeur.
   */
  public PivParamReader() {
  }

  /**
   * Lit les param�tres de calcul PIV et d�bit et les retourne sous forme d'un
   * tableau de PivComputeParameters et PivFlowParameters. Les objets en retour
   * peuvent �tre null si un champ n'est pas correctement renseign� (-1 par
   * exemple)
   * 
   * @return Les parametres
   */
  protected Object[] internalRead() {
    return readParams();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized Object[] readParams() {
    PivComputeParameters params = new PivComputeParameters();
    PivTransectParams paramsFlow = new PivTransectParams();

    if (super.in_ == null) {
      analyze_.addError(PivResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      if (progress_ != null) {
        progress_.setProgression(0);
      }

      in_.setJumpBlankLine(true);
      
      // Entete "# FileVersion <num> / IA size"
      in_.readFields();
      if ("#".equals(in_.stringField(0)) && "FileVersion".equals(in_.stringField(1))) {
        version_ = in_.stringField(2);
      }
      FuLog.debug("Version du fichier PIV_param.dat="+version_);
      
      // IA size
      in_.readFields();
      params.setIASize(in_.intField(0));

      // SA size : sim,sip,sjm,sjp
      in_.readLine();
      in_.readFields();
      params.setSim(in_.intField(0));
      in_.readFields();
      params.setSip(in_.intField(0));
      in_.readFields();
      params.setSjm(in_.intField(0));
      in_.readFields();
      params.setSjp(in_.intField(0));

      // Time interval / Sampling period
      in_.readLine();
      in_.readFields();
      params.setTimeInterval(in_.doubleField(0));
      if (version_.compareTo("3.0") >= 0) {
        params.setUnderSamplingPeriod(in_.intField(1));
      }
      else {
        params.setUnderSamplingPeriod(1);
      }

      if (version_.compareTo("4.0") < 0) {
        // Filtre correlation
        in_.readLine();
        in_.readFields();
        double minCorrel = in_.doubleField(0);
        in_.readLine();
        in_.readFields();
        double maxCorrel = in_.doubleField(0);
        // Le filtre correlation est il actif ?
        boolean isCorrelationEnabled = (minCorrel != PivUtils.FORTRAN_DOUBLE_MIN || maxCorrel != PivUtils.FORTRAN_DOUBLE_MAX);
        if (isCorrelationEnabled) {
          params.getFilters().setMinCorrelation(minCorrel);
          params.getFilters().setMaxCorrelation(maxCorrel);
        }
        params.getFilters().setCorrelationFilterEnabled(isCorrelationEnabled);
      }

      // Taille des images transform�es (on ne la stocke pas, calcul�e �
      // partir des fichiers images).
      in_.readLine();
      in_.readFields();
      in_.readFields();

      if (version_.compareTo("4.0") < 0) {
        // Filtre sur les vitesses
        // Seuils limites de norme de vitesse smin, smax
        in_.readLine();
        in_.readFields();
        double smin = in_.doubleField(0);
        in_.readFields();
        double smax = in_.doubleField(0);
        // Seuils composante VX vmin, vmax
        in_.readLine();
        in_.readFields();
        double vxmin = in_.doubleField(0);
        in_.readFields();
        double vxmax = in_.doubleField(0);

        double vymin;
        double vymax;
        if (version_.compareTo("2.0") >= 0) {
          // Seuils composante Vy vmin, vmax
          in_.readLine();
          in_.readFields();
          vymin = in_.doubleField(0);
          in_.readFields();
          vymax = in_.doubleField(0);
        }
        else {
          vymin = vxmin;
          vymax = vxmax;
        }
        // Le filtre vitesse est il actif ?
        boolean isVelocityEnabled = 
          smin != PivUtils.FORTRAN_DOUBLE_MIN || smax != PivUtils.FORTRAN_DOUBLE_MAX ||
          vxmin != PivUtils.FORTRAN_DOUBLE_MIN || vxmax != PivUtils.FORTRAN_DOUBLE_MAX ||
          vymin != PivUtils.FORTRAN_DOUBLE_MIN || vymax != PivUtils.FORTRAN_DOUBLE_MAX;
        if (isVelocityEnabled) {
          params.getFilters().setSmin(smin);
          params.getFilters().setSmax(smax);
          params.getFilters().setVxmin(vxmin);
          params.getFilters().setVxmax(vxmax);
          params.getFilters().setVymin(vymin);
          params.getFilters().setVymax(vymax);
        }
        params.getFilters().setVelocityFilterEnabled(isVelocityEnabled);
      }

      // Coefficient de surface
      in_.readLine();
      in_.readFields();
      paramsFlow.setSurfaceCoef(in_.doubleField(0));

      // Rayon de recherche des vitesses autour du point de bathy.
      in_.readLine();
      in_.readFields();
      paramsFlow.setRadiusX(in_.doubleField(0));
      if (in_.getNumberOfFields() > 1) {
        paramsFlow.setRadiusY(in_.doubleField(1));
      }
      // Si 1 seule valeur lue, on donne a la 2ieme la meme valeur.
      else {
        paramsFlow.setRadiusY(in_.doubleField(0));
      }

      // Pas d'interpolation de la bathy/ Nb maxi de points sur le transect (reconditionnement)
      in_.readLine();
      in_.readFields();
      paramsFlow.setInterpolationStep(in_.doubleField(0));
      if (version_.compareTo("3.0") >= 0) {
        paramsFlow.setMaxPoints(in_.intField(1));
      }

      if (progress_ != null) {
        progress_.setProgression(100);
      }
    }
    catch (final EOFException e) {
      analyze_.manageException(e);
    }
    catch (final IOException e) {
      analyze_.manageException(e);
    }
    catch (final NumberFormatException e) {
      analyze_.manageException(e);
    }
    
    // -1 : Les param�tres n'ont pas �t� renseign�s.
    if (params.getIASize()==-1)
      params.clearComputingParameters();
    if (params.getTimeInterval()==-1)
      params.setTimeInterval(null);
    if (paramsFlow.getRadiusX()==-1)
      paramsFlow=null;
    
    return new Object[]{params,paramsFlow};
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  public void stop() {
    bstop_ = true;
  }
}
