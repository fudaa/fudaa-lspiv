package org.fudaa.fudaa.piv.io;

import java.io.PrintWriter;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters;


/**
 * Une classe pour ecrire sur fichier les parametres de stabilisation.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivStabParamsWriter extends FileCharSimpleWriterAbstract<PivStabilizationParameters> {

  /**
   * Ecrit les param�tres.
   * 
   * param _o Un tableau contenant 3 objets PivComputeParameters, PivFlowParameters et Dimension.
   * 
   * Les objets peuvent �tre null (s'ils n'ont pas �t� d�finis). Dans ce cas, -1 sera
   * inscrit dans les champs correspondant pour indiquer que cette partie des infos
   * n'a pas �t� renseign�e.
   */
  protected void internalWrite(final PivStabilizationParameters _o) {

    final PrintWriter writer = new PrintWriter(out_);

    try {
      // Density
      writer.println("Keypoints density (0: Low, 1: Medium, 2: High)");
      writer.println(_o.getDensity().ordinal());
      
      // Transformation model
      writer.println("Transformation model (0: Projective, 1: Similarity)");
      writer.println(_o.getModel().ordinal());
    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
  }
}
