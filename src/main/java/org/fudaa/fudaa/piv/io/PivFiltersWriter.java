package org.fudaa.fudaa.piv.io;

import java.io.PrintWriter;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.fudaa.piv.metier.PivFilterParameters;
import org.fudaa.fudaa.piv.metier.PivStatisticParameters;
import org.fudaa.fudaa.piv.metier.PivStatisticParameters.PivStatisticCalcultationMethod;


/**
 * Une classe pour ecrire sur fichier les filtres post calcul.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivFiltersWriter extends FileCharSimpleWriterAbstract<Object[]> {
  /** Version du fichier */
  public static final String VERSION="1.0";

  /**
   * Ecrit les paramètres de filtre.
   * 
   */
  protected void internalWrite(final Object[] _o) {
    if (_o.length!=2 ||
        !(_o[0] instanceof PivFilterParameters) ||
        !(_o[1] instanceof PivStatisticParameters)) {
      donneesInvalides(_o);
      return;
    }

    PivFilterParameters paramFilters = (PivFilterParameters)_o[0];
    PivStatisticParameters paramsStats = (PivStatisticParameters)_o[1];
        
    final PrintWriter writer = new PrintWriter(out_);

    try {
      // Entete
      writer.println("# V" + VERSION + " Filter parameters");
      
      // Correlation
      writer.println("# Correlation (active, min, max)");
      writer.print(paramFilters.isCorrelationFilterEnabled());
      writer.print(" ");
      writer.print(paramFilters.getMinCorrelation());
      writer.print(" ");
      writer.print(paramFilters.getMaxCorrelation());
      writer.println();
      
      // Norme de vitesse
      writer.println("# Velocity norm (active, min, max)");
      writer.print(paramFilters.isVelocityFilterEnabled());
      writer.print(" ");
      writer.print(paramFilters.getSmin());
      writer.print(" ");
      writer.print(paramFilters.getSmax());
      writer.println();
      
      // Vx de vitesse
      writer.println("# Vx component limit (active, min, max)");
      writer.print(paramFilters.isVelocityFilterEnabled());
      writer.print(" ");
      writer.print(paramFilters.getVxmin());
      writer.print(" ");
      writer.print(paramFilters.getVxmax());
      writer.println();
      
      // Vx de vitesse
      writer.println("# Vy component limit (active, min, max)");
      writer.print(paramFilters.isVelocityFilterEnabled());
      writer.print(" ");
      writer.print(paramFilters.getVymin());
      writer.print(" ");
      writer.print(paramFilters.getVymax());
      writer.println();
      
      // Test median
      writer.println("# Median test (active, epsilon, r0min, n_neighbor, dist_max)");
      writer.print(paramFilters.isMedianTestFilterEnabled());
      writer.print(" ");
      writer.print(paramFilters.getEpsilon());
      writer.print(" ");
      writer.print(paramFilters.getR0min());
      writer.print(" ");
      writer.print(paramFilters.getNneighbor());
      writer.print(" ");
      writer.print(paramFilters.getDistmax());
      writer.println();
      
      // Pic de correlation
      writer.println("# Correlation peak width (active, rcut, rh0max)");
      writer.print(paramFilters.isCorrelationPeakFilterEnabled());
      writer.print(" ");
      writer.print(paramFilters.getRcut());
      writer.print(" ");
      writer.print(paramFilters.getRhomax());
      writer.println();
      
      // Distribution des vitesses
      writer.println("# Velocity temporal distribution (active, nstdvel)");
      writer.print(paramFilters.isVelocityDistributionFilterEnabled());
      writer.print(" ");
      writer.print(paramFilters.getNstdvel());
      writer.println();
      
      // Dispersion des vitesses
      writer.println("# Streamwise velocity dispersion (active, COVmax)");
      writer.print(paramFilters.isVelocityDispersionFilterEnabled());
      writer.print(" ");
      writer.print(paramFilters.getCovmax());
      writer.println();
      
      // Dispersion angulaire
      writer.println("# Angular dispersion (active, circvarmax)");
      writer.print(paramFilters.isAngularDispersionFilterEnabled());
      writer.print(" ");
      writer.print(paramFilters.getCircvarmax());
      writer.println();
      
      // Aggregation at point
      writer.println("# Aggregation at point (0:median / 1:mean)");
      writer.print("" + (paramsStats.getMethod()==PivStatisticCalcultationMethod.MEDIAN ? 0:1));
      writer.println();
    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
  }
}
