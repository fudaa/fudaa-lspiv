package org.fudaa.fudaa.piv.io;

import java.util.ArrayList;
import java.util.List;

import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.fudaa.piv.metier.PivStatisticParameters;
import org.fudaa.fudaa.piv.metier.PivFilterParameters;
import org.fudaa.fudaa.piv.metier.PivSamplingVideoParameters;

/**
 * Un param�tre utilis� dans la lecture/ecriture du fichier des donn�es globales. 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivGlobalXmlParam {
  
  /**
   * La facon daont la resolution est donn�e.
   * @author Bertrand Marchand (marchand@detacad.fr)
   */
  public enum ResolutionType {
    DIRECT,
    DISTANCES,
    COORDINATES
  }
  
  /** Le centre de l'IA/SA */
  public GrPoint iaCenter;
  /** La liste ordonn�e des noms des images source */
  public String[] srcImages;
  /** Translation X */
  public double tx;
  /** Translation Y */
  public double ty;
  /** Translation Z */
  public double tz;
  /** Le coordonn�e X du point centre de la rotation suivant Z */
  public double xcenter;
  /** Le coordonn�e Y du point centre de la rotation suivant Z */
  public double ycenter;
  /** Rotation Z */
  public double rz;
  /** Le calcul par PIV est il actif ? */
  public boolean isPivActive = true;
  /** Mise � l'echelle : Les points en coordonn�es image pour la transformation ([0] si aucune transformation) */
  public GrPoint[] scalingImgPoints;
  /** Mise � l'echelle : Les points en coordonn�es r�elle pour la transformation ([0] si aucune transformation) */
  public GrPoint[] scalingRealPoints;
  /** Mise � l'echelle : Les points en coordonn�es image pour la resolution ([0] si aucun points) */
  public List<GrPoint[]> scalingResolutionImgPoints = new ArrayList<>();
  /** Mise � l'echelle : Les points en coordonn�es r�elle pour la resolution ([0] si aucun points) */
  public List<GrPoint[]> scalingResolutionRealPoints = new ArrayList<>();
  /** Mise � l'echelle : Les distances pour la resolution ([0] si aucune distance) */
  public List<Double> scalingResolutionDistances = new ArrayList<>();
  /** Mise � l'echelle : Les altitudes des segments pour le calcul de resolution suivant un drone ([0] si le mode drone est inactif) */
  public List<Double> scalingZSegments = new ArrayList<>();
  /** Le polygone pour la zone d'�coulement (stabilisation). Si null, pas de zone d�finie; lu pour les versions < 1.9 */
  public GrPolygone stabilizationFlowArea = null;
  /** Les parametrres de sampling; lu pour les versions < 1.9 */
  public PivSamplingVideoParameters samplingParams;
  /** Les points pour le calcul direct des vitesses */
  public List<GrPoint[]> displacementPoints = new ArrayList<>();
  /** Les indices des images associ�es */
  public List<Integer[]> imgIndexes = new ArrayList<>();
  /** Mise � l'echelle : L'altitude du drone si celle ci est donn�e. null : Le mode drone n'est pas utlis� */
  public Double scalingZDrone;
  /** Les parametres de filtres */
  public PivFilterParameters filterParams;
  /** Les parametres de calcul de moyenne/m�dinae */
  public PivStatisticParameters statisticParams;
    
}