package org.fudaa.fudaa.piv.io;

import org.fudaa.ctulu.CtuluXmlWriter;
import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.piv.metier.PivStatisticParameters.PivStatisticCalcultationMethod;

/**
 * Une classe pour �crire des donn�es globales du projet.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivGlobalXmlWriter extends FileCharSimpleWriterAbstract<PivGlobalXmlParam> {
  
  @Override
  protected void internalWrite(PivGlobalXmlParam _param) {

    final CtuluXmlWriter writer;

    try {
      writer = new CtuluXmlWriter(out_);
      
      writer.writeComment("This file contains global data for the project");
      writer.setMainTag("document");
      
      // La version 2.0
      writer.startTag("version");
      writer.writeEntry("2.0");
      writer.endTag("version");
      
      // Le centre de l'IA
      
      if (_param.iaCenter!=null) {
        writer.startTag("iaCenter");
        writer.writeEntry("jCoord", "" + (int) _param.iaCenter.x_);
        writer.writeEntry("iCoord", "" + (int) _param.iaCenter.y_);
        writer.endTag("iaCenter");
      }

      // L'ordre des images sources du projet
      
      writer.startTag("srcImages");
      for (int i=0; i<_param.srcImages.length; i++) {
        writer.startTagWithAttrs("image", new String[]{"ind"}, new String[]{""+i});
        writer.writeEntry(_param.srcImages[i]);
        writer.endTag("image");
      }
      writer.endTag("srcImages");
      
      // La rotation/translation appliqu�s � l'espace r�el.
      
      writer.startTagWithAttrs("transfMatrix", new String[]{"tx","ty","tz","rz","xcenter","ycenter"},
          new String[]{ "" + _param.tx,"" + _param.ty,"" + _param.tz,"" + _param.rz,"" + _param.xcenter,"" + _param.ycenter});
      writer.endTag("transfMatrix");
      
      // Mode de transformation echelle
      
      writer.startTag("scaling");
      
      // Les parametres de transformation pour une mise � l'echelle
      
      if (_param.scalingImgPoints != null) {
        writer.startTag("transfPoints");
        for (int i=0; i < _param.scalingImgPoints.length; i++) {
          writer.startTagWithAttrs("transfPoint", new String[]{"i","j","x","y"},
              new String[]{"" + (int) _param.scalingImgPoints[i].x_,"" + (int) _param.scalingImgPoints[i].y_,"" + (int) _param.scalingRealPoints[i].x_,"" + (int) _param.scalingRealPoints[i].y_});
          writer.endTag("transfPoint");
        }
        writer.endTag("transfPoints");
      }

      // Le type de resolution, le mode drone ou non, l'altitude du drone
            
      PivGlobalXmlParam.ResolutionType type = _param.scalingResolutionImgPoints.size() == 0 ? PivGlobalXmlParam.ResolutionType.DIRECT : _param.scalingResolutionRealPoints.size() == 0 ? PivGlobalXmlParam.ResolutionType.DISTANCES : PivGlobalXmlParam.ResolutionType.COORDINATES;
      writer.startTagWithAttrs("resolution", new String[]{"type", "altitudeDrone"}, new String[] { type.toString(), _param.scalingZDrone == null ? "" : "" + _param.scalingZDrone });
      writer.endTag("resolution");
      
      // Les points de resolution pour une mise � l'echelle
      
      writer.startTag("resolutionCouples");
      for (int i=0; i < _param.scalingResolutionImgPoints.size(); i++) {
        GrPoint[] imgPts = _param.scalingResolutionImgPoints.get(i);
        
        int i1 = (int)imgPts[0].x_;
        int j1 = (int)imgPts[0].y_;
        int i2 = (int)imgPts[1].x_;
        int j2 = (int)imgPts[1].y_;
        int x1 = 0;
        int y1 = 0;
        int x2 = 0;
        int y2 = 0;
        double distance = 0;
        double zSegment = 0;

        if (_param.scalingZDrone != null) {
          zSegment = _param.scalingZSegments.get(i);
        }
        
        if (type == PivGlobalXmlParam.ResolutionType.DISTANCES) {
          distance = _param.scalingResolutionDistances.get(i);
        }
        else {
          GrPoint[] realPts = _param.scalingResolutionRealPoints.get(i);
          x1 = (int)realPts[0].x_;
          y1 = (int)realPts[0].y_;
          x2 = (int)realPts[1].x_;
          y2 = (int)realPts[1].y_;
        }
        writer.startTagWithAttrs("resolutionCouple", new String[]{"i1","j1","x1","y1","i2","j2","x2","y2","distance","zSegment"},
            new String[]{""+i1, ""+j1, ""+x1, ""+y1, ""+i2, ""+j2, ""+x2, ""+y2, ""+distance, "" + zSegment});
        writer.endTag("resolutionCouple");
      }
      writer.endTag("resolutionCouples");

      writer.endTag("scaling");
      
      // Calcul par PIV
      writer.startTagWithAttrs("pivComputation", new String[] { "active" },
          new String[] { "" + _param.isPivActive });
      writer.endTag("pivComputation");
      
      // Vitesses manuelles
      writer.startTag("directVelocities");
      
      // Les couples de points
      if (_param.displacementPoints != null) {
        writer.startTag("displacementPoints");
        
        double j1 = 0;
        double i1 = 0;
        double j2 = 0;
        double i2 = 0;
        int img1 = 0;
        int img2 = 0;
        
        for (int i=0; i < _param.displacementPoints.size(); i++) {
          GrPoint[] displacementPts = _param.displacementPoints.get(i);
          Integer[] imgIndexes = _param.imgIndexes.get(i);
          
          j1 = displacementPts[0].x_;
          i1 = displacementPts[0].y_;
          j2 = displacementPts[1].x_;
          i2 = displacementPts[1].y_;
          img1 = imgIndexes[0];
          img2 = imgIndexes[1];
          
          writer.startTagWithAttrs("displacementPoint", new String[]{"j1","i1","j2","i2","img1","img2"},
              new String[]{"" + j1,"" + i1,"" + j2, "" + i2, "" + img1, "" + img2});
          writer.endTag("displacementPoint");
        }
        
        writer.endTag("displacementPoints");
      }
      
      // Int�gration ou non des vitesses calcul�es manuellement
      writer.writeEntry("combined", "" + (_param.statisticParams != null ? _param.statisticParams.isManualFieldUsed() : false));
      
      writer.endTag("directVelocities");
      
      // La zone d'ecoulement pour la stabilisation
//      
//      if (_param.stabilizationFlowArea != null) {
//        writer.startTag("flowAreaPoints");
//        for (int i=0; i<_param.stabilizationFlowArea.nombre(); i++) {
//          GrPoint pt = _param.stabilizationFlowArea.sommet(i);
//          writer.startTagWithAttrs("flowAreaPoint", new String[] { "i", "j" },
//              new String[] { "" + (int) pt.x_, "" + (int) pt.y_ });
//          writer.endTag("flowAreaPoint");
//        }
//        writer.endTag("flowAreaPoints");
//      }
      
//      // Les param�tres de sampling 
//      if (_param.samplingParams != null) {
//        writer.startTag("sampling");
//        writer.writeEntry("samplingVideoFile", _param.samplingParams.getVideoFile().getAbsolutePath());
//        writer.startTagWithAttrs("samplingSize", new String[] {"width", "height"}, 
//            new String[] {""+_param.samplingParams.getSamplingSize().width, ""+_param.samplingParams.getSamplingSize().height});
//        writer.endTag("samplingSize");
//        writer.startTagWithAttrs("samplingTime", new String[] {"begin", "end"}, 
//            new String[] {""+_param.samplingParams.getSamplingBeginTime(), ""+_param.samplingParams.getSamplingEndTime()});
//        writer.endTag("samplingTime");
//        writer.writeEntry("samplingPeriod", ""+_param.samplingParams.getSamplingPeriod());
//        writer.endTag("sampling");
//      }
      
      // Les parametres de filtre
      if (_param.filterParams != null) {
        writer.startTag("filters");
        // Norme de vitesse
        writer.startTagWithAttrs("velocityNorm", new String[] { "active", "min", "max" },
            new String[] { "" + _param.filterParams.isVelocityFilterEnabled(), "" + _param.filterParams.getSmin(), "" + _param.filterParams.getSmax() });
        writer.endTag("velocityNorm");
        // Vx
        writer.startTagWithAttrs("vx", new String[] { "active", "min", "max" },
            new String[] { "" + _param.filterParams.isVelocityFilterEnabled(), "" + _param.filterParams.getVxmin(), "" + _param.filterParams.getVxmax() });
        writer.endTag("vx");
        // Vy
        writer.startTagWithAttrs("vy", new String[] { "active", "min", "max" },
            new String[] { "" + _param.filterParams.isVelocityFilterEnabled(), "" + _param.filterParams.getVymin(), "" + _param.filterParams.getVymax() });
        writer.endTag("vy");
        // Correlation
        writer.startTagWithAttrs("correlation", new String[] { "active", "min", "max" },
            new String[] { "" + _param.filterParams.isCorrelationFilterEnabled(), "" + _param.filterParams.getMinCorrelation(), "" + _param.filterParams.getMaxCorrelation() });
        writer.endTag("correlation");
        // Test median
        writer.startTagWithAttrs("medianTest", new String[] { "active", "epsilon", "r0min", "advanced", "n_neighbor", "dist_max" },
            new String[] { "" + _param.filterParams.isMedianTestFilterEnabled(), "" + _param.filterParams.getEpsilon(), "" + _param.filterParams.getR0min(), "" + _param.filterParams.isMedianTestAdvancedFilterEnabled(), "" + _param.filterParams.getNneighbor(), "" + _param.filterParams.getDistmax() });
        writer.endTag("medianTest");
        // Correlation peak
        writer.startTagWithAttrs("correlationPeak", new String[] { "active", "rcut", "rh0max" },
            new String[] { "" + _param.filterParams.isCorrelationPeakFilterEnabled(), "" + _param.filterParams.getRcut(), "" + _param.filterParams.getRhomax() });
        writer.endTag("correlationPeak");
        // Velocity distribution
        writer.startTagWithAttrs("velocityDistribution", new String[] { "active", "nstdvel" },
            new String[] { "" + _param.filterParams.isVelocityDistributionFilterEnabled(), "" + _param.filterParams.getNstdvel() });
        writer.endTag("velocityDistribution");
        // Velocity distribution
        writer.startTagWithAttrs("velocityDispersion", new String[] { "active", "covmax" },
            new String[] { "" + _param.filterParams.isVelocityDispersionFilterEnabled(), "" + _param.filterParams.getCovmax() });
        writer.endTag("velocityDispersion");
        // Angle distribution
        writer.startTagWithAttrs("angularDispersion", new String[] { "active", "circvarmax" },
            new String[] { "" + _param.filterParams.isAngularDispersionFilterEnabled(), "" + _param.filterParams.getCircvarmax() });
        writer.endTag("angularDispersion");

        writer.endTag("filters");
      }
      
      // Parametres statistiques
      if (_param.statisticParams != null) {
        writer.writeComment("Parameters for average/median computation");
        String method;
        if (_param.statisticParams.getMethod() == PivStatisticCalcultationMethod.MEDIAN) {
          method = "median";
        }
        else {
          method = "mean";
        }
        writer.startTagWithAttrs("statistics", new String[] { "method" },
            new String[] { method });
        writer.endTag("statistics");
      }
      
      writer.finish();
    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
    
  }
  
//  /**
//   * L'�criture des transects avec leurs propri�t�s
//   * @param writer Le fichier de sortie.
//   * @param _trans Les transects
//   * @throws IOException
//   */
//  void writeTransects(CtuluXmlWriter writer, PivTransect[] _trans) throws IOException {
//    writer.startTag("transects");
//
//    for (PivTransect trans : _trans) {
//      writer.startTagWithAttrs("transect", 
//          new String[] { "coef", "step", "radius" }, 
//          new String[] { "" + trans.getParams().getSurfaceCoef(), 
//                         "" + trans.getParams().getInterpolationStep(),
//                         "" + trans.getParams().getRadius() });
//      // writer.startTag("transect");
//      // writer.writeEntry("coef", "" + trans.getParams().getSurfaceCoef());
//      // writer.writeEntry("interp", "" +
//      // trans.getParams().getInterpolationStep());
//      // writer.writeEntry("dmax", "" + trans.getParams().getDmax());
//
//      for (int i = 0; i < trans.getStraight().nombre(); i++) {
//        GrPoint pt = trans.getStraight().sommet(i);
//        writer.startTagWithAttrs("point", new String[] { "x", "y", "z" }, new String[] { "" + pt.x_, "" + pt.y_, "" + pt.z_ });
//        writer.endTag("point");
//      }
//      writer.endTag("transect");
//    }
//    writer.endTag("transects");
//  }
}
