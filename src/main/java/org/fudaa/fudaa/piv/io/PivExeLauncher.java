package org.fudaa.fudaa.piv.io;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.fudaa.fudaa.piv.PivPreferences;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivTaskObserverI;
import org.fudaa.fudaa.piv.imageio.PivPGMWriter;
import org.fudaa.fudaa.piv.metier.PivCompositeResults;
import org.fudaa.fudaa.piv.metier.PivComputeParameters;
import org.fudaa.fudaa.piv.metier.PivConcatenatedResults;
import org.fudaa.fudaa.piv.metier.PivFlowResults;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivResultsDefault;
import org.fudaa.fudaa.piv.metier.PivResultsI;
import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;
import org.fudaa.fudaa.piv.metier.PivTransect;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLib.ProcessMessagesObserverI;
import com.memoire.fu.FuLog;

/**
 * Une classe pour lancer les executables externes. Les exe sont sous la forme
 * d'un script ou directement d'un executable. La plupart des exes sont lanc�s
 * dans des threads s�par�s. Ils s'executent pour la plupart directement dans
 * le repertoire temporaire du projet.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivExeLauncher {
  
  /** Les exe pouvant �tre lanc�s */
  private enum EXE {
    ortho_plan,
    transf_a,
    PIV,
    verif_ortho,
    moy_ec,
    filter,
    bathy_compute,
    Q_compute,
    vel_transf,
    vel_scal,
    moy_scal,
    stab_img,
    ffmpeg,
    ffprobe,
    gen_img_ref,
    manual_vel
  }
  
  /** Les scripts pouvant etre lanc�s */
  private enum CMD {
  }
  
  /**
   * Une classe d'observation et de traitement des messages envoy�s par l'exe de filtre des r�sultats.
   * @author Bertrand Marchand
   */
  public class FilterProcessMessagesObserver implements ProcessMessagesObserverI {
    PivTaskObserverI task;
    Pattern pattern = Pattern.compile("^ *(.*?)... *([0-9.]*) *%$", Pattern.CASE_INSENSITIVE);
    int startedProgression_;
    String step = null;
    
    public FilterProcessMessagesObserver(PivTaskObserverI _task) {
      task = _task;
      startedProgression_ = task.getProgression();
    }

    @Override
    public void appendOut(String _line) {
      
      Matcher match = pattern.matcher(_line);
      if (match.find()) {
        if (!match.group(1).equals(step)) {
          step = match.group(1);
        }
        String spercent = match.group(2);
        try {
          int prog = (int)Double.parseDouble(spercent);
          
          task.setDesc(step);
          task.setProgression(prog);
          FuLog.trace("Step="+step+", percent="+prog+"%");
        }
        catch (NumberFormatException exc) {}
      }
    }

    @Override
    public void appendErr(String _line) {
    }

    @Override
    public void finished() {
      task.completed();
    }
  }
  
  /**
   * Une classe d'observation et de traitement des messages envoy�s par l'exe stab images.
   * @author Bertrand Marchand
   */
  public class StabProcessMessagesObserver implements ProcessMessagesObserverI {
    PivTaskObserverI task;
    Pattern pattern = Pattern.compile("^([0-9.]*)% image[0-9]{4}\\.pgm", Pattern.CASE_INSENSITIVE);
    int startedProgression_;
    
    public StabProcessMessagesObserver(PivTaskObserverI _task) {
      task = _task;
      startedProgression_ = task.getProgression();
    }

    @Override
    public void appendOut(String _line) {
      
      Matcher match = pattern.matcher(_line);
      if (match.find()) {
        String spercent = match.group(1);
        try {
          int prog = startedProgression_ + (int)((100.-startedProgression_)*Double.parseDouble(spercent)/100.);
          task.setProgression(prog);
        }
        catch (NumberFormatException exc) {}
//        FuLog.trace("Progression=" + match.group(1) + "%");
      }
    }

    @Override
    public void appendErr(String _line) {
    }

    @Override
    public void finished() {
      task.completed();
    }
  }
  
  public class SamplingProcessMessagesObserver implements ProcessMessagesObserverI {
    PivTaskObserverI task;
    int nbTotalImgs;
    Pattern pattern = Pattern.compile("^frame=\\s*(\\d*)", Pattern.CASE_INSENSITIVE);
    
    public SamplingProcessMessagesObserver(PivTaskObserverI _task, int _nbImgs) {
      nbTotalImgs = _nbImgs;
      task = _task;
    }

    @Override
    public void appendOut(String _line) {
      System.out.println("TRC=" + _line);
    }
    
    @Override
    public void appendErr(String _line) {
      System.err.println("ERR=" + _line);
      
      Matcher match = pattern.matcher(_line);
      if (match.find()) {
        String snbTreated = match.group(1);
        try {
          int nbTreated = Integer.parseInt(snbTreated);
          int prog = (int)(100.*nbTreated/nbTotalImgs);
          task.setProgression(prog);
        }
        catch (NumberFormatException exc) {}
//        FuLog.trace("Progression=" + match.group(1) + "%");
      }
    }

    @Override
    public void finished() {
      // TODO Auto-generated method stub
      
    }
    
  }
  
  /**
   * Un thread pour tuer (kill) le process exe lanc� par ailleurs.
   * Utilis� sur les exe sur lesquels on ne peut envoyer de commande d'interruption.
   * 
   * @author Bertrand Marchand
   */
  public class KillProcessThread extends Thread {
    PivTaskObserverI task_;
    Process proc_;
    
    public KillProcessThread(PivTaskObserverI _task, Process _proc) {
      task_ = _task;
      proc_ = _proc;
    }
    
    @Override
    public void run() {
      while (!task_.isStopRequested()) {
        try {
          Thread.sleep(10);
        }
        catch (InterruptedException e) {
        }
      }
      proc_.destroy();
      
      FuLog.trace("Interruption");
    }
  }

  /** */
  private static final String OUTPUT_DIR="outputs.dir";
  /** Le repertoire des images sources */
  private static final String IMG_PGM_DIR="img_pgm";
  /** Le repertoire des images stabilis�es */
  private static final String IMG_STAB_DIR="img_stab";
  /** Le repertoire des images transform�es */
  private static final String IMG_TRANSF_DIR="img_transf";
  private static final String VEL_RAW_DIR="vel_raw";
  private static final String VEL_REAL_DIR="vel_real";
  private static final String VEL_FILTER_DIR="vel_filter";
  private static final String VEL_SCAL_DIR="vel_scal";
  /** Le r�pertoire contenant les transects */
  private static final String TRANSECTS_DIR="transects";

  /** L'extension pour le lancement d'un exe */
  private static final String EXE_EXT=FuLib.isWindows()?".exe":".x";
  private static final String CMD_EXT=FuLib.isWindows()?".bat":".sh";

  /** Le repertoire des exe */
  private File exePath=null;
  
  /** Le singleton */
  private static PivExeLauncher instance_;

  /**
   * Le constructeur du lanceur d'ex�cutables.
   * @param _exePath Le chemin du r�pertoire des exes.
   */
  private PivExeLauncher(File _exePath) {
    exePath=_exePath;
  }
  
  public static PivExeLauncher instance() {
    // Le chemin des exes peut �tre donn� en propri�t�, auquel cas il est prioritaire sur
    // celui r�cup�r� du fichier des pr�f�rences
    String exeRep=System.getProperty(PivPreferences.PIV_EXE_PATH);
    if (exeRep==null) {
      exeRep=PivPreferences.PIV.getStringProperty(PivPreferences.PIV_EXE_PATH);
    }
    
    if (instance_==null)
      instance_=new PivExeLauncher(new File(exeRep));
    return instance_;
  }

  /**
   * Definit le chemin du r�pertoire des exes. Tous les ex�cutables doivent s'y
   * trouver.
   * @param _exePath Le chemin des exes.
   */
  public void setExePath(File _exePath) {
    exePath=_exePath;
  }
  
  /**
   * Retourne le chemin du repertoire des exes.
   * @return Le chemin.
   */
  public File getExePath() {
    return exePath;
  }

  /**
   * Retourne vrai si tous les exe sont pr�sents dans le repertoire sp�cifi�.
   * @return false si certains exe ne sont pas pr�sents dans le r�pertoire donn�.
   */
  public boolean areExeOK() {
    return getMissingExeOrCmd().length==0;
  }

  /**
   * Retourne les noms des exes manquants dans le r�pertoire.
   * @return Les noms des exes, ou un tableau vide si tous sont pr�sents.
   */
  public String[] getMissingExeOrCmd() {
    return getMissingExeOrCmdIn(exePath);
  }
  
  /**
   * Retourne les noms des exes manquants dans le r�pertoire pass� en argument
   * @param _exePath Le r�pertoire � tester.
   * @return Les noms des exes, ou un tableau vide si tous sont pr�sents.
   */
  public String[] getMissingExeOrCmdIn(File _exePath) {
    ArrayList<String> list=new ArrayList<String>();

    for (EXE exe : EXE.values()) {
      if (!new File(_exePath,exe.toString()+EXE_EXT).exists()) {
        list.add(exe.toString()+EXE_EXT);
      }
    }

    for (CMD cmd : CMD.values()) {
      if (!new File(_exePath,cmd.toString()+CMD_EXT).exists()) {
        list.add(cmd.toString()+CMD_EXT);
      }
    }
    return list.toArray(new String[0]);
  }

  /**
   * Une methode pour copier un fichier d'un repertoire vers un autre, sous un
   * nom diff�rent.
   * @param _srcDir Le repertoire source.
   * @param _dstDir Le repertoire destination
   * @param _srcFileName Le nom du fichier a copier.
   * @param _dstFileName Le nom du fichier une fois copi�.
   */
  private void copyFile(File _srcDir, File _dstDir, String _srcFileName, String _dstFileName) {
    File srcGrpFile=new File(_srcDir,_srcFileName);
    File dstGrpFile=new File(_dstDir,_dstFileName);
    CtuluLibFile.copyFile(srcGrpFile, dstGrpFile);
  }

  /**
   * Lance le calcul d'echantillonnage d'images depuis un fichier vid�o.
   *
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _task La tache en cours d'execution.
   * @param _tdeb Le temps de d�but d'�chantillonage Si null, commence � 0.
   * @param _tfin Le temps de fin d'�chantillonnage Si null, fini a la fin de la vid�o.
   * @param _size La taille des images echantillonn�es. Si null, la taille des images est celle de la vid�o.
   * @param _nbImgs Le nombre d'images que l'echantillonage va produire (a peu pr�s, pour le suivi de l'avancement de l'�chantillonage). Si -1 : On ne connait pas le nombre, et la progression sera indetermin�e
   * @return true : Tout s'est bien d�roul�.
   * @see #launchOrthoPlan
   */
  public boolean computeSampling(CtuluLog _ana, PivProject _prj, File _fvideo, File _tmpOutDir, int _periodImgs, int _nbImgs, Double _tdeb, Double _tfin, Dimension _size, PivTaskObserverI _task) {
    File prjRoot=_prj.getRoot();

    String desc=PivResource.getS("Lancement de l'�chantillonnage...");
    FuLog.trace(desc);
    if (_task!=null) {
      _task.setDesc(desc);
      _task.setProgression(0);
    }

    try {
      ArrayList<String> cmdArray=new ArrayList<>();
      cmdArray.add(exePath+File.separator+EXE.ffmpeg + EXE_EXT);
      cmdArray.add("-i");
      cmdArray.add(_fvideo.getAbsolutePath());
      // #5405 : Cette option produit 3 images de d�but erron�es.
//      cmdArray.add("-r");
//      cmdArray.add(""+_periodImgs);
      cmdArray.add("-vf");
      cmdArray.add("framestep="+_periodImgs);
      if (_tdeb!=null) {
        cmdArray.add("-ss");
        cmdArray.add(""+_tdeb);
      }
      if (_tfin!=null) {
        cmdArray.add("-to");
        cmdArray.add(""+_tfin);
      }
      if (_size!=null) {
        cmdArray.add("-s");
        cmdArray.add(_size.width + "x" + _size.height);
      }
      cmdArray.add(new File(_tmpOutDir,"image%4d.png").getAbsolutePath());
      FuLog.trace("Launched command : "+ String.join(" ", cmdArray));
      
      SamplingProcessMessagesObserver obs = new SamplingProcessMessagesObserver(_task, _nbImgs);
     
      final StringBuffer sberr = new StringBuffer();
      final StringBuffer sbout = new StringBuffer();
      Process proc = FuLib.runProgramAsync(cmdArray.toArray(new String[0]), prjRoot, sbout, sberr, obs);
      
      // Un thread pour tuer le programme.
      KillProcessThread th = new KillProcessThread(_task, proc);
      th.start();
      
      try {
        proc.waitFor();
      }
      catch (InterruptedException e) {}
      
      if (!sberr.toString().isEmpty()) {
        String text=PivUtils.trunkErrorMessage(sberr.toString(),132);
        _ana.addError(PivResource.getS("Erreur dans l'�chantillonnage:\n")+text);
        return false;
      }

      if (_task!=null && _task.isStopRequested()) {
        _ana.addError(PivResource.getS("Echantillonnage interrompu"));
        return false;
      }

      FuLog.trace(PivResource.getS("Echantillonnage ok."));
    }
    catch (IOException ex) {
      _ana.addError(ex.getMessage());
      return false;
    }
    
    if (_task!=null)
      _task.setProgression(100);

    return true;
  }

  /**
   * Lance le calcul de stabilisation d'images.
   *
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _task La tache en cours d'execution.
   * @return true : Tout s'est bien d�roul�.
   * @see #launchOrthoPlan
   */
  public boolean computeStabilizedImages(CtuluLog _ana, PivProject _prj, final PivTaskObserverI _task) {
    
    File prjRoot=_prj.getRoot();
    File prjOutputs=new File(prjRoot,OUTPUT_DIR);
    File prjPgmImgs = new File(_prj.getTempDirectory(), IMG_PGM_DIR);
    File prjStabImgs = new File(prjRoot, IMG_STAB_DIR);
    
    String desc=PivResource.getS("Stabilisation des images...");
    
    // Nettoyage du repertoire temporaire ou on va stocker toutes les images PGM pour le calcul de stabilisation.
    prjPgmImgs.mkdirs();
    for (File pgmImg : prjPgmImgs.listFiles())
      pgmImg.delete();
    
    // Nettoyage du repertoire des images stabilis�es
    prjStabImgs.mkdirs();
    for (File stabImg : prjStabImgs.listFiles())
      stabImg.delete();
    
    int prog = 1;
    
    FuLog.trace(desc);
    if (_task!=null) {
      _task.setDesc(desc);
      _task.setProgression(prog);
    }

    // Ecriture du fichier stabilisation
    File stabParamFile = new File(prjOutputs, "stab_param.dat");
    new PivStabParamsWriter().write(_prj.getStabilizationParameters(), stabParamFile, _task);
    // Ecriture du fichier mask
    File maskfile = new File(prjOutputs, "mask.dat");
    new PivStabMaskWriter().write(new Object[]{_prj.getStabilizationParameters(), _prj.getSrcImageSize()}, maskfile, _task);
    
    if (_task!=null) {
      _task.setProgression(prog += 2);
    }
    
    try {

      // De 5 � 20 %
      if (_task != null)
        _task.setDesc(PivResource.getS("Cr�ation des images pgm..."));

      // Transfert de toutes les images sources vers le format PGM dans un repertoire d�di�.
      File[] srcImgs = _prj.getSrcImageFiles(PivProject.SRC_IMAGE_TYPE.CALCULATION);
      int i = 1;
      for (File srcImg : srcImgs) {
        File cache = _prj.getCacheImageFile(srcImg);
        BufferedImage buffer = ImageIO.read(cache);
        new PivPGMWriter().write(new File(prjPgmImgs, "image"+PivUtils.formatOn4Chars(i++)+".pgm"), buffer);
        
        if (_task!=null) {
          _task.setProgression(3 + i*10/srcImgs.length);
        }
      }
    
      ArrayList<String> cmdArray=new ArrayList<>();
      cmdArray.add(exePath+File.separator+EXE.stab_img + EXE_EXT);
      cmdArray.add(prjPgmImgs.getAbsolutePath()+File.separator);

      FuLog.trace("Launched command : "+ String.join(" ", cmdArray));
      if (_task != null)
        _task.setDesc(PivResource.getS("Stabilisation des images..."));
      
      // Un observer des sorties du process, pour recuperer la progression.
      StabProcessMessagesObserver obs = new StabProcessMessagesObserver(_task);
      
      final StringBuffer sberr = new StringBuffer();
      final StringBuffer sbout = new StringBuffer();
      Process proc = FuLib.runProgramAsync(cmdArray.toArray(new String[0]), prjRoot, sbout, sberr, obs);
//      
//      Process proc = Runtime.getRuntime().exec(cmdArray.toArray(new String[0]), null, prjRoot);
//      
      // Un thread pour interrompre l'exe de stabilisation (pas d'autre moyen pour interrompre.
      KillProcessThread th = new KillProcessThread(_task, proc);
      th.start();
      
//      Thread th = new Thread() {
//        @Override
//        public void run() {
//          while (!_task.isStopRequested()) {
//            try {
//              Thread.sleep(10);
//            }
//            catch (InterruptedException e) {
//            }
//          }
//          proc.destroy();
//          
//          FuLog.trace("Interruption");
//        }
//      };
//      th.start();
//      
//      final StringBuffer sberr = new StringBuffer();
//      final StringBuffer sbout = new StringBuffer();
//      
//      new Thread(new Runnable() {
//
//        @Override
//        public void run() {
//          _task.setDesc(PivResource.getS("Stabilisation des images..."));
//          
//          // Pour retrouver les pourcentage d'avancement
//          Pattern pattern = Pattern.compile("^([0-9]*) image[0-9]{4}\\.pgm", Pattern.CASE_INSENSITIVE);
//          LineNumberReader outs = new LineNumberReader(new InputStreamReader(proc.getInputStream()));
//          LineNumberReader errs = new LineNumberReader(new InputStreamReader(proc.getErrorStream()));
//          
//          exiting = false;
//
//          while (!exiting) {
//            String line;
//            try {
//              while (outs.ready() && (line = outs.readLine()) != null) {
//                sbout.append(line);
//                
//                Matcher match = pattern.matcher(line);
//                if (match.find()) {
//                  String spercent = match.group(1);
//                  try {
//                    prog = 10 + 90*Integer.parseInt(spercent)/100;
//                    _task.setProgression(prog);
//                  }
//                  catch (NumberFormatException exc) {}
////                  FuLog.trace("Progression=" + match.group(1) + "%");
//                }
//              }
//              
//              while (errs.ready() && (line = errs.readLine()) != null) {
//                sberr.append(line);
//              }
//              
//            } 
//            catch (IOException e) {
//              FuLog.trace("Exception");
//            }
//          }
//
//        }
//      }).start();
      
      proc.waitFor();
      
//      exiting = true;
      
      
//      String outs = FuLib.runProgram(cmdArray.toArray(new String[0]), prjRoot);
      if (!sberr.toString().isEmpty()) {
        String text=PivUtils.trunkErrorMessage(sberr.toString(),132);
        _ana.addError(PivResource.getS("Erreur dans la stabilisation:\n")+text);
        return false;
      }

      if (_task!=null && _task.isStopRequested()) {
        _ana.addError(PivResource.getS("Stabilisation interrompue"));
        return false;
      }
      
//      // On supprime la premi�re image, qui ne semble pas respecter l'intervalle de temps.
//      new File(_tmpOutDir, "image0001.png").delete();
      
      FuLog.trace(PivResource.getS("Stabilisation ok."));
    }
    catch (Exception ex) {
      _ana.addError(ex.getMessage());
      return false;
    }
    
    finally {
      // On supprime tout ce qui ne devra pas �tre sauvegard�.
      maskfile.delete();
      for (File srcImg : prjPgmImgs.listFiles()) {
        srcImg.delete();
      }
      prjPgmImgs.delete();
    }
    
    _prj.setStabilizedImagesChanged(_task);
    if (_task!=null)
      _task.setProgression(90);


    return true;
  }

  /**
   * Retourne les propri�t�s d'un fichier vid�o (dont le frame rate)
   *
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _task La tache en cours d'execution.
   * @return true : Tout s'est bien d�roul�.
   * @see #launchOrthoPlan
   */
  public Properties getVideoProperties(CtuluLog _ana, PivProject _prj, File _fvideo, PivTaskObserverI _task) {
    Properties ret = new Properties();
    
    File prjRoot=_prj.getRoot();

    String desc=PivResource.getS("Recup�ration des infos vid�o...");
    FuLog.trace(desc);
    if (_task!=null) {
      _task.setDesc(desc);
      _task.setProgression(5);
    }

    try {
      // Recup�ration de la plupart des informations
      
      // ffprobe -v error -select_streams v:0 -show_streams -of default=noprint_wrappers=1 C:\projets\fudaa\devel\exemples\lspiv\1.6\20160528_175553.mp4
      ArrayList<String> cmdArray=new ArrayList<>();
      cmdArray.add(exePath+File.separator+EXE.ffprobe + EXE_EXT);
      cmdArray.add("-v");
      cmdArray.add("error");
      cmdArray.add("-select_streams");
      cmdArray.add("v:0");
      cmdArray.add("-show_streams");
      cmdArray.add("-of");
      cmdArray.add("default=noprint_wrappers=1");
      cmdArray.add(_fvideo.getAbsolutePath());
      
      FuLog.trace("Launched command : "+ String.join(" ", cmdArray));
      
      StringBuffer sout = new StringBuffer();
      StringBuffer serr = new StringBuffer();
      String outs = FuLib.runProgram(cmdArray.toArray(new String[0]), prjRoot, sout, serr);
      if (FuLib.lastRunStatus!=0) {
        String text=PivUtils.trunkErrorMessage(serr.toString(),132);
        _ana.addError(PivResource.getS("Erreur dans la r�cup�ration des informations de la vid�o:\n")+text);
        return null;
      }
      
      try (InputStream is = new ByteArrayInputStream(outs.getBytes(StandardCharsets.UTF_8))) {
        ret.load(is);
      }

      // Recup�ration de la dur�e, pour une evaluation du nombre d'images de la vid�o.
      
      cmdArray.clear();
      cmdArray.add(exePath+File.separator+EXE.ffprobe + EXE_EXT);
      cmdArray.add("-v");
      cmdArray.add("error");
      cmdArray.add("-show_entries");
      cmdArray.add("format=duration");
      cmdArray.add("-of");
      cmdArray.add("default=noprint_wrappers=1:nokey=1");
      cmdArray.add(_fvideo.getAbsolutePath());
      
      FuLog.trace("Launched command : "+ String.join(" ", cmdArray));
      
      sout.setLength(0);
      serr.setLength(0);
      outs = FuLib.runProgram(cmdArray.toArray(new String[0]), prjRoot, sout, serr);
      if (FuLib.lastRunStatus!=0) {
        String text=PivUtils.trunkErrorMessage(serr.toString(),132);
        _ana.addError(PivResource.getS("Erreur dans la r�cup�ration des informations de la vid�o:\n")+text);
        return null;
      }
      
      try (BufferedReader ir = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(outs.getBytes(StandardCharsets.UTF_8))))) {
        ret.put("video-duration", ir.readLine());
      }
      
      if (_task!=null && _task.isStopRequested()) {
        _ana.addError(PivResource.getS("R�cup�ration interrompue"));
        return null;
      }

      FuLog.trace(PivResource.getS("R�cup�ration ok."));
    }
    catch (IOException ex) {
      _ana.addError(ex.getMessage());
      return null;
    }
    
    
    if (_task!=null)
      _task.setProgression(90);


    return ret;
  }

  /**
   * Lance le calcul de transformation des images sources a partir des parametres de scaling
   * d'orthorectification.
   *
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _task La tache en cours d'execution.
   * @param _apercu true : Ne calcule qu'une seule image, pour l'apercu en espace
   * r�el.
   * @return true : Tout s'est bien d�roul�.
   */
  public boolean computeScalingImages(CtuluLog _ana, PivProject _prj, boolean _apercu, PivTaskObserverI _task) {
    File prjRoot=_prj.getRoot();
    File prjImgOut=new File(prjRoot,IMG_TRANSF_DIR);

    // Nettoyage eventuel du repertoire des images transform�es.
    prjImgOut.mkdirs();
    for (File f : _prj.getTransfImageFiles()) f.delete();

    try {
      // Les images sources
      // Les images : Soit sources, soit stabilis�es.
      File[] imgInp = null;
      if (_prj.isStabilizationActive()) {
        imgInp = _prj.getStabilizedImageFiles();
      }
      else {
        imgInp = _prj.getSrcImageFiles(PivProject.SRC_IMAGE_TYPE.CALCULATION);
      }
      
      if (_task!=null)
        _task.setProgression(10);
      int prog=0;

      PivOrthoParameters params=_prj.getOrthoParameters();
      
      int maxIter=(_apercu?1:imgInp.length);
      for (int i = 0; i <maxIter ; i++) {
        try {
          if (_prj.cacheExists(imgInp[i])) {
            File cache = _prj.getCacheImageFile(imgInp[i]);
            BufferedImage inputBuffer = ImageIO.read(cache);
            
            int imin=(int) (params.getXmin() / params.getResolution());
            int jmin=(int) (params.getYmin() / params.getResolution());
            int imax=(int) (params.getXmax() / params.getResolution());
            int jmax=(int) (params.getYmax() / params.getResolution());
            BufferedImage outputBuffer=new BufferedImage(imax - imin, jmax - jmin, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g=(Graphics2D) outputBuffer.getGraphics();
            
            // Pas de transformation
            if (_prj.getOrthoParameters().getScalingTranformationImgPoints().length == 0) {
              g.translate(-imin, inputBuffer.getHeight()-jmin);
              g.scale(1, -1);
              g.drawImage(inputBuffer, 0, 0, null);
            }
            
            // Translation / rotation
            else {
              GrPoint p1final = new GrPoint(params.getScalingTranformationRealPoints()[0].x_ / params.getResolution(), params.getScalingTranformationRealPoints()[0].y_ / params.getResolution(), 0);
              
              // Calcul de l'angle de rotation depuis les points saisis.
              GrVecteur v1=new GrVecteur(params.getScalingTranformationImgPoints()[1].x_-params.getScalingTranformationImgPoints()[0].x_, params.getScalingTranformationImgPoints()[1].y_-params.getScalingTranformationImgPoints()[0].y_, 0);
              GrVecteur v2=new GrVecteur(params.getScalingTranformationRealPoints()[1].x_-params.getScalingTranformationRealPoints()[0].x_, params.getScalingTranformationRealPoints()[1].y_-params.getScalingTranformationRealPoints()[0].y_, 0);
              double rotation=v1.getAngleXY(v2);
              
              // Le premier point saisi dans le repere apr�s rotation
              GrMorphisme toRotation=GrMorphisme.rotationZ(rotation);
              GrPoint p1rotation = params.getScalingTranformationImgPoints()[0].applique(toRotation);
              
              // La transformation de l'espace de depart vers l'espace final (le 2ieme point n'est pas forcement la ou c'�tait demand�).
              GrMorphisme toFinal = GrMorphisme.rotationZ(rotation).composition(GrMorphisme.translation(p1final.x_-p1rotation.x_-imin, p1final.y_-p1rotation.y_-jmin, 0));
              AffineTransform transf = toFinal.creeAffineTransorm2D();
              
              g.transform(transf);
              // Retournement de l'image (car espace Graphics2D avec (0,0) en haut � gauche de l'image)
              g.translate(0, inputBuffer.getHeight());
              g.scale(1, -1);
              g.drawImage(inputBuffer, 0, 0, null);
            }
            
            new PivPGMWriter().write(new File(prjImgOut, "image" + PivUtils.formatOn4Chars(i + 1) + "_transf.pgm"), outputBuffer);
          }

          if (_task!=null && _task.isStopRequested()) {
            _ana.addError(PivResource.getS("Transformation interrompue"));
            return false;
          }
        }
        catch (IOException ex) {
          _ana.addError(ex.getMessage());
          return false;
        }
        prog=((i+1)*90)/maxIter;
        if (_task!=null)
          _task.setProgression(prog);

      }
      FuLog.trace(PivResource.getS("Transformation des images ok."));
    }

    // Peut arriver si la resolution de l'image demand�e est trop grande.
    catch (OutOfMemoryError _exc) {
      _ana.addError(PivResource.getS("Le nombre de pixels des images transform�es est trop important.\nAugmentez la r�solution ou diminuez les dimensions."));
      return false;
    }
    
    // On signale au projet que les images transform�es ont chang�.
    finally {

      _prj.setTransfImagesChanged(_task);
      if (_task!=null)
        _task.setProgression(100);
    }
    
    return true;
  }
  
  protected double getMin(double[] _vals) {
    double xmin = Double.POSITIVE_INFINITY;
    
    for (double x : _vals) {
      xmin = Math.min(xmin,x);
    }
    
    return xmin;
  }
  
  
  /**
   * Lance le calcul des coefficients d'orthorectification.
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _pts Les points modifi�s pour que le coordonn�es soient optimis�es
   * @param _task La tache en cours d'execution.
   * @return true : Tout s'est bien d�roul�.
   */
  private boolean computeOrthoCoefs(CtuluLog _ana, PivProject _prj, PivOrthoPoint[] _pts, PivTaskObserverI _task) {
    
    File prjRoot=_prj.getRoot();
    File prjOutputs=new File(prjRoot,OUTPUT_DIR);

    String desc=PivResource.getS("Calcul des coefficients...");
    FuLog.trace(desc);
    if (_task!=null) {
      _task.setDesc(desc);
      _task.setProgression(5);
    }

    // Sauvegarde des donn�es du projet.
    File grpFile=new File(prjOutputs,"GRP.dat");
    Dimension imgSize = _prj.getSrcImageSize();
    new PivGRPWriter().write(new Object[] {_pts, imgSize }, grpFile, _task);

    // Lancement
    try {
      String outs=FuLib.runProgram(new String[]{exePath+File.separator+EXE.ortho_plan+EXE_EXT}, prjRoot);
      if (!outs.trim().equals("")) {
        _ana.addError(PivResource.getS("Erreur de calcul des coefficients :\n")+outs);
        return false;
      }

      if (_task!=null && _task.isStopRequested()) {
        _ana.addError(PivResource.getS("Calcul interrompu"));
        return false;
      }
      if (_task!=null)
        _task.setProgression(90);

      FuLog.trace(PivResource.getS("Calcul des coefficients ok."));
    }
    catch (IOException ex) {
      _ana.addError(ex.getMessage());
      return false;
    }

    return true;
  }

  /**
   * Lance le calcul de transformation des images sources a partir des coefficients
   * d'orthorectification.
   *
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _task La tache en cours d'execution.
   * @param _apercu true : Ne calcule qu'une seule image, pour l'apercu en espace
   * r�el.
   * @return true : Tout s'est bien d�roul�.
   */
  public boolean computeTransfImg(CtuluLog _ana, PivProject _prj, boolean _apercu, PivTaskObserverI _task) {
    File prjRoot=_prj.getRoot();
    File prjImgInp=new File(prjRoot,IMG_PGM_DIR);
    File prjImgOut=new File(prjRoot,IMG_TRANSF_DIR);
    File prjOutputs=new File(prjRoot,OUTPUT_DIR);
    
    File fimgInpExe=new File(prjImgInp,"image1.pgm");
    File fimgInpTmp=new File(prjImgInp,"image1.pgm_temp");

    PivOrthoPoint[] realPts = _prj.getOrthoPoints();
    
    // Un d�calage est calcul� pour les coordonnn�es tr�s �lev�es.
    double xmin = Double.POSITIVE_INFINITY;
    double ymin = Double.POSITIVE_INFINITY;
    for (int i=0; i<realPts.length; i++) {
      xmin = Math.min(xmin, realPts[i].getRealPoint().x_);
      ymin = Math.min(ymin, realPts[i].getRealPoint().y_);
    }
    
    // Les points temporaires modifi�s.
    PivOrthoPoint[] tmpPts = new PivOrthoPoint[realPts.length];
    for (int i=0; i<tmpPts.length; i++) {
      tmpPts[i] = new PivOrthoPoint(realPts[i]);
      tmpPts[i].getRealPoint().x_=tmpPts[i].getRealPoint().x_-xmin;
      tmpPts[i].getRealPoint().y_=tmpPts[i].getRealPoint().y_-ymin;
    }
    
    // Les parametres temporaires, modifi�s.
    PivOrthoParameters tmpParams = new PivOrthoParameters(_prj.getOrthoParameters());
    tmpParams.setXmin(tmpParams.getXmin()-xmin);
    tmpParams.setXmax(tmpParams.getXmax()-xmin);
    tmpParams.setYmin(tmpParams.getYmin()-ymin);
    tmpParams.setYmax(tmpParams.getYmax()-ymin);
    
    // Calcul des coefficients
    if (!computeOrthoCoefs(_ana, _prj, tmpPts, _task))
      return false;

    // Nettoyage eventuel du repertoire des images transform�es.
    prjImgOut.mkdirs();
    for (File f : _prj.getTransfImageFiles()) f.delete();

    try {

      // Sauvegarde des donn�es du projet.
      File imgRefFile = new File(prjOutputs, "img_ref.dat");
      Object[] params=new Object[]{tmpParams,_prj.getSrcImageSize()};
      new PivImgRefWriter().write(params, imgRefFile, null);

      File hFile = new File(prjOutputs, "h.dat");
      new PivHWriter().write(tmpParams.getWaterElevation(), hFile, null);

      // Le ficher coeff.dat est d�ja pr�sent.
//    copyFile(prjOutputs, exeOutputs, "coeff.dat");

      // Les images : Soit sources, soit stabilis�es.
      File[] imgInp = null;
      if (_prj.isStabilizationActive()) {
        imgInp = _prj.getStabilizedImageFiles();
      }
      else {
        imgInp = _prj.getSrcImageFiles(PivProject.SRC_IMAGE_TYPE.CALCULATION);
      }
      
      // Si une image1.pgm existe, on la renomme pour eviter qu'elle soit �cras�e.
      int indImgInpExe=Arrays.asList(imgInp).indexOf(fimgInpExe);
      if (indImgInpExe!=-1) {
        imgInp[indImgInpExe].renameTo(fimgInpTmp);
        imgInp[indImgInpExe]=fimgInpTmp;
      }
      
      if (_task!=null)
        _task.setProgression(10);
      int prog=0;

      int maxIter=(_apercu?1:imgInp.length);
      for (int i = 0; i <maxIter ; i++) {
        try {
          if (_prj.cacheExists(imgInp[i])) {
            File cache = _prj.getCacheImageFile(imgInp[i]);
            BufferedImage buffer = ImageIO.read(cache);
            new PivPGMWriter().write(new File(prjImgInp, fimgInpExe.getName()), buffer);
          }
          else {
            // TODO : Ne devrait plus passer par la...
            // Attention : Le nom ne doit pas etre le m�me que les images d�ja
            // stock�es
            String srcFileName = imgInp[i].getName();
            copyFile(imgInp[i].getParentFile(), prjImgInp, srcFileName, fimgInpExe.getName());
          }

          String desc = PivResource.getS("Transformation de l'image {0} sur {1}...", i + 1, maxIter);
          FuLog.trace(desc);
          if (_task != null) {
            _task.setDesc(desc);
            _task.setProgression(prog);
          }

          String outs = FuLib.runProgram(new String[]{exePath + File.separator + EXE.transf_a + EXE_EXT}, prjRoot);
          if (!outs.trim().equals("")) {
            _ana.addError(PivResource.getS("Erreur de transformation de l'image {0}:\n",i+1)+outs);
            return false;
          }

          if (_task!=null && _task.isStopRequested()) {
            _ana.addError(PivResource.getS("Transformation interrompue"));
            return false;
          }
        }
        catch (IOException ex) {
          _ana.addError(ex.getMessage());
          return false;
        }
        prog=((i+1)*90)/maxIter;
        if (_task!=null)
          _task.setProgression(prog);

        new File(prjImgOut,"image1_transf.pgm").renameTo(new File(prjImgOut,"image"+PivUtils.formatOn4Chars(i+1)+"_transf.pgm"));
      }
      FuLog.trace(PivResource.getS("Transformation des images ok."));
    }

    // On supprime l'image temporaires
    // On signale au projet que les images transform�es ont chang�.
    finally {
      fimgInpExe.delete();
      if (fimgInpTmp.exists()) {
        boolean b=fimgInpTmp.renameTo(fimgInpExe);
        b&=b;
      }

      _prj.setTransfImagesChanged(_task);
      if (_task!=null)
        _task.setProgression(100);
    }
    
    return true;
  }
  
  /**
   * Lance le calcul des resultats bruts
   * 
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _task La tache en cours d'execution.
   * @param _apercu true : Ne calcule qu'un seul r�sultat, pour l'apercu en espace r��l.
   * @return true : Tout s'est bien d�roul�.
   */
  public boolean computeRawInstantResults(CtuluLog _ana, PivProject _prj, boolean _apercu, PivTaskObserverI _task) {
    _prj.removeInstantRawResultsOnFS();
    
    computePiv(_ana, _prj, _apercu, _task);
    if (_ana.containsErrorOrSevereError()) {
      return false;
    }

    computeRealRawResults(_ana, _prj, _task);
    if (_ana.containsErrorOrSevereError()) {
      return false;
    }

    return true;
  }

  /**
   * Lance le calcul de la velocimetrie par PIV. Les images transform�es sont
   * analys�es 2 par 2.
   *
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _task La tache en cours d'execution.
   * @param _apercu true : Ne calcule qu'un seul r�sultat, pour l'apercu en espace r��l.
   * @return true : Tout s'est bien d�roul�.
   */
  private boolean computePiv(CtuluLog _ana, PivProject _prj, boolean _apercu, PivTaskObserverI _task) {
    File prjRoot=_prj.getRoot();
    File prjImgOut=new File(prjRoot,IMG_TRANSF_DIR);
    File prjOutputs=new File(prjRoot,OUTPUT_DIR);
    File prjVelRaw=new File(prjRoot,VEL_RAW_DIR);

    try {
      // Sauvegarde des donn�es du projet.
      File paramFile=new File(prjOutputs, "PIV_param.dat");
      // #5584 : Les filtres sont reinitialis�s, sinon PIV en tient compte (il ne devrait pas)...
      PivComputeParameters computeParams = new PivComputeParameters(_prj.getComputeParameters());
      computeParams.getFilters().setVelocityFilterEnabled(false);
      computeParams.getFilters().setCorrelationFilterEnabled(false);
      
      Object[] params=new Object[]{computeParams, null, _prj.getTransfImageSize()};
      new PivParamWriter().write(params, paramFile, null);

      File gridFile=new File(prjOutputs, "grid.dat");
      new PivGridWriter().write(_prj.getComputeGrid(), gridFile, null);

      // Les images transform�es
      File[] imgOut=_prj.getTransfImageFiles();

      int prog=5;
      
      int maxIter=(_apercu?1:imgOut.length-1);
      for (int i=0; i<maxIter; i++) {
        
        try {
          if (_prj.cacheExists(imgOut[i])) {
            File cache1=_prj.getCacheImageFile(imgOut[i]);
            BufferedImage buffer1 = ImageIO.read(cache1);
            new PivPGMWriter().write(new File(prjImgOut, "image1_transf.pgm"), buffer1);
          }
          else {
            String srcFileName1=imgOut[i].getName();
            copyFile(prjImgOut, prjImgOut, srcFileName1, "image1_transf.pgm");
          }
          if (_prj.cacheExists(imgOut[i+1])) {
            File cache2 = _prj.getCacheImageFile(imgOut[i+1]);
            BufferedImage buffer2 = ImageIO.read(cache2);
            new PivPGMWriter().write(new File(prjImgOut, "image2_transf.pgm"), buffer2);
          }
          else {
            String srcFileName2=imgOut[i+1].getName();
            copyFile(prjImgOut, prjImgOut, srcFileName2, "image2_transf.pgm");
          }

          String desc = PivResource.getS("Analyse du couple d'images {0} sur {1}...", i + 1, maxIter);
          FuLog.trace(desc);
          if (_task != null) {
            _task.setDesc(desc);
            _task.setProgression(prog);
          }

          String outs=FuLib.runProgram(new String[]{exePath+File.separator+EXE.PIV+EXE_EXT}, prjRoot);
          if (!outs.trim().equals("")) {
            _ana.addError(PivResource.getS("Erreur d'analyse du couple d'images {0},{1}:\n",i+1,i+2)+outs);
            return false;
          }

          if (_task!=null && _task.isStopRequested()) {
            _ana.addError(PivResource.getS("Analyse interrompue"));
            return false;
          }
        }
        catch (IOException ex) {
          _ana.addError(ex.getMessage());
          return false;
        }

        // On renomme le fichier
        new File(prjVelRaw,"piv.dat").renameTo(new File(prjVelRaw,"piv"+PivUtils.formatOn4Chars(i+1)+".dat"));

        prog=(i+1)*90/maxIter+5;
        if (_task!=null)
          _task.setProgression(prog);
      }
      FuLog.trace(PivResource.getS("Analyse des images ok."));
      if (_task!=null)
        _task.setProgression(100);
    }

    // On supprime les images temporaires
    finally {
      new File(prjImgOut, "image1_transf.pgm").delete();
      new File(prjImgOut, "image2_transf.pgm").delete();
      
      if (_ana.containsErrorOrSevereError()) {
        _prj.setInstantRawResults(null);
      }
    }
    
    return true;
  }

  /**
   * Lance le controle du calcul de l'orthorectification par comparaison des
   * des points donn�es et des points recalcul�s.
   * Doit �tre lanc� apr�s le calcul des coefficients d'orthorectification.
   *
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _task La tache en cours d'execution.
   * @return true : Tout s'est bien d�roul�.
   * @see #launchOrthoPlan
   */
  public boolean computeVerifOrtho(CtuluLog _ana, PivProject _prj, PivTaskObserverI _task) {
    File prjRoot=_prj.getRoot();
    File prjOutputs=new File(prjRoot,OUTPUT_DIR);

    PivOrthoPoint[] realPts = _prj.getOrthoPoints();
    
    // Un d�calage est calcul� pour les coordonnn�es tr�s �lev�es.
    double xmin = Double.POSITIVE_INFINITY;
    double ymin = Double.POSITIVE_INFINITY;
    for (int i=0; i<realPts.length; i++) {
      xmin = Math.min(xmin, realPts[i].getRealPoint().x_);
      ymin = Math.min(ymin, realPts[i].getRealPoint().y_);
    }
    
    // Les points temporaires modifi�s.
    PivOrthoPoint[] tmpPts = new PivOrthoPoint[realPts.length];
    for (int i=0; i<tmpPts.length; i++) {
      tmpPts[i] = new PivOrthoPoint(realPts[i]);
      tmpPts[i].getRealPoint().x_=tmpPts[i].getRealPoint().x_-xmin;
      tmpPts[i].getRealPoint().y_=tmpPts[i].getRealPoint().y_-ymin;
    }
    
    // Calcul des coefficients
    if (!computeOrthoCoefs(_ana, _prj, tmpPts, _task))
      return false;

    String desc=PivResource.getS("Lancement de la v�rification de l'orthorectification...");
    FuLog.trace(desc);
    if (_task!=null) {
      _task.setDesc(desc);
      _task.setProgression(5);
    }

    try {
      String outs = FuLib.runProgram(new String[]{exePath+File.separator+EXE.verif_ortho + EXE_EXT}, prjRoot);
      if (!outs.trim().equals("")) {
        _ana.addError(PivResource.getS("Erreur de v�rification de l'orthorectification:\n")+outs);
        return false;
      }

      if (_task!=null && _task.isStopRequested()) {
        _ana.addError(PivResource.getS("Verification interrompue"));
        return false;
      }

      // R�cup�ration des donn�es du projet.
      File grpFile = new File(prjOutputs, "GRP_test_ortho.dat");
      CtuluIOResult<PivOrthoPoint[]> ret = new PivGRPReader(_prj.getSrcImageSize(), true).read(grpFile, null);
      PivOrthoPoint[] computePts=ret.getSource();
      
      // On red�cale les points calcul�s
      for (int i=0; i<computePts.length; i++) {
        computePts[i].getComputeRealPoint().x_ = computePts[i].getComputeRealPoint().x_+xmin;
        computePts[i].getComputeRealPoint().y_ = computePts[i].getComputeRealPoint().y_+ymin;
      }
      
      // On conserve les points d'origine, on ne modifie que les points recalcul�s
      // et les erreurs.
      PivOrthoPoint[] pts=_prj.getOrthoPoints();
      for (int i=0; i<pts.length; i++) {
        pts[i].setComputeRealPoint(computePts[i].getComputeRealPoint());
        pts[i].setError(computePts[i].getError());
      }


      FuLog.trace(PivResource.getS("Calcul ok."));
    }
    catch (IOException ex) {
      _ana.addError(ex.getMessage());
      return false;
    }
    // Suppression du fichier test
    finally {
      new File(prjOutputs, "GRP_test_ortho.dat").delete();
    }
    if (_task!=null)
      _task.setProgression(90);


    return true;
  }

  /**
   * Lance le filtrage des r�sultats et leur restitution dans l'espace r�el.
   * Le filtrage est fait dans le r�pertoire projet.
   *
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _task La tache en cours d'execution.
   * @return true : Tout s'est bien d�roul�.
   */
  public boolean computeFilteredInstantResultats(CtuluLog _ana, PivProject _prj, PivTaskObserverI _task) {
    _prj.removeInstantFilteredResultsOnFS();
    
    File prjRoot=_prj.getRoot();
    File prjOutputs=new File(prjRoot,OUTPUT_DIR);

    String desc=PivResource.getS("Filtrage des r�sultats...");
    FuLog.trace(desc);
    if (_task!=null) {
      _task.setDesc(desc);
      _task.setProgression(0);
    }

    PivResultsI[] res=null;
    
    try {
      // Cr�ation du fichier contenant la liste de tous les fichiers piv.dat obtenus par calcul.
      _prj.getPersistenceManager().saveInstantResultsList(true);

      // PIV_param.dat
      File paramFile=new File(prjOutputs, "PIV_param.dat");
      Object[] params=new Object[]{_prj.getComputeParameters(), null, _prj.getTransfImageSize()};
      new PivParamWriter().write(params, paramFile, null);

      // filters.dat
      File filtersFile=new File(prjOutputs, "filters.dat");
      Object[] filters = new Object[]{_prj.getComputeParameters().getFilters(), _prj.getStatisticParameters()};
      new PivFiltersWriter().write(filters, filtersFile, null);
      
      // img_ref.dat
      File imgRefFile = new File(prjOutputs, "img_ref.dat");
      params=new Object[]{_prj.getOrthoParameters(),_prj.getSrcImageSize()};
      new PivImgRefWriter().write(params, imgRefFile, _task);
      if (_task!=null)
        _task.setProgression(5);


      ArrayList<String> cmdArray = new ArrayList<>();
      cmdArray.add(exePath+File.separator+EXE.filter + EXE_EXT);
      FuLog.trace("Launched command : "+ String.join(" ", cmdArray));
      
      FilterProcessMessagesObserver obs = new FilterProcessMessagesObserver(_task);
      
      final StringBuffer sberr = new StringBuffer();
      final StringBuffer sbout = new StringBuffer();
      Process proc = FuLib.runProgramAsync(cmdArray.toArray(new String[0]), prjRoot, sbout, sberr, obs);

      KillProcessThread th = new KillProcessThread(_task, proc);
      th.start();
      
      proc.waitFor();

      if (!sberr.toString().isEmpty()) {
        String text=PivUtils.trunkErrorMessage(sberr.toString(),132);
        _ana.addError(PivResource.getS("Erreur dans le filtre des r�sultats:\n")+text);
        return false;
      }

      if (_task!=null && _task.isStopRequested()) {
        _ana.addError(PivResource.getS("Filtrage interrompu"));
        return false;
      }
      if (_task!=null)
        _task.setProgression(80);

      FuLog.trace(PivResource.getS("Filtrage ok."));

      desc = PivResource.getS("Calcul des scalaires instantan�s...");
      FuLog.trace(desc);
      if (_task != null) {
        _task.setDesc(desc);
        _task.setProgression(0);
      }

      String outs = FuLib.runProgram(new String[] { exePath + File.separator + EXE.vel_scal + EXE_EXT }, prjRoot);
      if (!outs.trim().equals("")) {
        _ana.addError(PivResource.getS("Erreur de calcul des scalaires instantan�s:\n") + outs);
        return false;
      }

      if (_task != null && _task.isStopRequested()) {
        _ana.addError(PivResource.getS("Calcul interrompu"));
        return false;
      }
      if (_task != null)
        _task.setProgression(80);

      FuLog.trace(PivResource.getS("Calcul ok."));

      // Chargement des r�sultats
      res=_prj.getPersistenceManager().loadInstantFilteredResults(_task);
    }
    catch (Exception ex) {
      _ana.addError(ex.getMessage());
      return false;
    }
    finally {
      _prj.setInstantFilteredResults(res);
    }

    return true;
  }

  /**
   * Lance la moyenne des resultats et leur restitution dans l'espace r�el. Cette
   * methode doit �tre appel�e juste apr�s le filtrage des vitesses.
   * La moyenne se fait dans le r�pertoire temporaire.
   * 
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _task La tache en cours d'execution.
   * @return true : Tout s'est bien d�roul�.
   * @see #launchFilterVelocities
   */
  public boolean computeAverageResults(CtuluLog _ana, PivProject _prj, PivTaskObserverI _task) {
    _prj.removeAverageResultsOnFS();
    
    File prjRoot=_prj.getRoot();
    File prjOutputs=new File(prjRoot,OUTPUT_DIR);

    PivResultsI res=null;

    try {
      
      // Calcul des vitesses moyennes depuis les resultats caclul�s filtr�s
      // Uniquement si les parametres de calcul ont �t� remplis et que au moins 1 resultat est utilis�.
      
      if (_prj.getComputeParameters().isFilledForComputing() && _prj.getStatisticParameters().getUsedInstantResultsIndexes().length > 0) {
        // Sauvegarde des r�sultats filtr�s qui ont pu �tre modifi�s par l'utilisateur.
        _prj.getPersistenceManager().saveInstantFilteredResults(_prj, prjRoot, _prj.getInstantFilteredResults(), _task);

        // filters.dat
        File filtersFile=new File(prjOutputs, "filters.dat");
        Object[] filters = new Object[]{_prj.getComputeParameters().getFilters(), _prj.getStatisticParameters()};
        new PivFiltersWriter().write(filters, filtersFile, null);

        // Cr�ation du fichier contenant la liste des fichiers piv.dat pour la moyenne.
        _prj.getPersistenceManager().saveInstantResultsList(false);

        String desc = PivResource.getS("Calcul des vitesses moyennes...");
        FuLog.trace(desc);
        if (_task != null) {
          _task.setDesc(desc);
          _task.setProgression(0);
        }

        String outs = FuLib.runProgram(new String[] { exePath + File.separator + EXE.moy_ec + EXE_EXT }, prjRoot);
        if (!outs.trim().equals("")) {
          _ana.addError(PivResource.getS("Erreur de calcul des vitesses moyennes:\n") + outs);
          return false;
        }

        if (_task != null && _task.isStopRequested()) {
          _ana.addError(PivResource.getS("Calcul interrompu"));
          return false;
        }

        if (_task != null)
          _task.setProgression(80);

        FuLog.trace(PivResource.getS("Calcul des vitesses moyennes ok."));

        if (_prj.hasScalInstantResultsFiles()) {
          desc = PivResource.getS("Calcul des r�sultats moyens...");
          FuLog.trace(desc);
          if (_task != null) {
            _task.setDesc(desc);
            _task.setProgression(0);
          }

          outs = FuLib.runProgram(new String[] { exePath + File.separator + EXE.moy_scal + EXE_EXT }, prjRoot);
          if (!outs.trim().equals("")) {
            _ana.addError(PivResource.getS("Erreur de calcul des r�sultats moyens:\n") + outs);
            return false;
          }

          if (_task != null && _task.isStopRequested()) {
            _ana.addError(PivResource.getS("Calcul interrompu"));
            return false;
          }
          if (_task != null)
            _task.setProgression(80);

          FuLog.trace(PivResource.getS("Calcul ok."));
        }

        // Chargement des r�sultats
        res = _prj.getPersistenceManager().loadAverageResults(_task);
      }
      
      else {
        res = new PivResultsDefault(new ResultType[] { ResultType.VX, ResultType.VY, ResultType.NORME, ResultType.CORREL, ResultType.OMEGA, ResultType.DIVERG });
      }
      
      // Les r�sultats manuels sont ajout�s si le mode combinatoire est actif.
      if (_prj.getManualVelocitiesParameters().isActive() && _prj.getStatisticParameters().isManualFieldUsed()) {
        PivResultsI manualRes = _prj.getManualResults();
        ResultType[] manualTypes = manualRes.getResults();
        ResultType[] resAveTypes = res.getResults();

        // Cr�ation des resultats manquants pour les NORME, CORELLATION...
        ResultType[] missingTypes = Arrays.stream(resAveTypes).filter(t-> !Arrays.asList(manualTypes).contains(t)).toArray(ResultType[]::new);
        PivResultsDefault missingManualRes =  new PivResultsDefault(missingTypes);
        double[] dummyVals = new double[missingTypes.length];
        for (int ipt = 0; ipt<manualRes.getNbPoints(); ipt++) {
          missingManualRes.addPoint(manualRes.getX(ipt), manualRes.getY(ipt), dummyVals);
          // On calcule la norme.
          double vx = manualRes.getValue(ipt, ResultType.VX);
          double vy = manualRes.getValue(ipt, ResultType.VY);
          double norme = Math.sqrt(vx * vx + vy * vy);
          missingManualRes.setValue(ipt,  ResultType.NORME, norme);
        }
        
        // Combinaison des resultats manuels vitesses + autres.
        PivCompositeResults fullManualRes = new PivCompositeResults(Arrays.asList(manualRes, missingManualRes));
        // Combinaison des resultats manuels + calcul�s
        res = new PivConcatenatedResults(Arrays.asList(res, fullManualRes));
      }
    }
    catch (IOException ex) {
      _ana.addError(ex.getMessage());
      return false;
    }
    finally {
      _prj.setAverageResults(res);
    }

    return true;
  }

  /**
   * Lance le calcul des parametres par defaut, en fonction des GRPs.dat et de la taille de l'image. 
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _task La tache en cours d'execution.
   * @return true : Tout s'est bien d�roul�.
   */
  public PivOrthoParameters getTransfDefaultParameters(CtuluLog _ana, PivProject _prj, PivTaskObserverI _task) {
    File prjRoot = _prj.getTempDirectory();

    // On travaille dans le repertoire temporaire du projet.
    File prjOutputs = new File(prjRoot, OUTPUT_DIR);
    prjOutputs.mkdirs();
    
    if (_task != null)
      _task.setProgression(0);

    // Le fichier des GRPs
    File grpFile = new File(prjOutputs, "GRP.dat");
    Dimension imgSize = _prj.getSrcImageSize();
    new PivGRPWriter().write(new Object[] { _prj.getOrthoPoints(), imgSize }, grpFile, _task);
    
    if (_task != null)
      _task.setProgression(10);

    // Lancement
    try {
      StringBuffer sbOut = new StringBuffer();
      StringBuffer sbErr = new StringBuffer();
      String outs = FuLib.runProgram(new String[] { exePath + File.separator + EXE.gen_img_ref + EXE_EXT }, prjRoot, sbOut, sbErr);
      if (!sbErr.toString().trim().equals("")) {
        _ana.addError(PivResource.getS("Erreur de calcul de la r�solution :\n") + outs);
        return null;
      }

      if (_task != null && _task.isStopRequested()) {
        _ana.addError(PivResource.getS("Calcul interrompu"));
        return null;
      }

      FuLog.trace(PivResource.getS("Calcul de la r�solution ok."));
      
      if (_task != null)
        _task.setProgression(90);
      
      // Et recup�ration des param�tres de transformation d'image. 
      File imgRefFile = new File(prjOutputs, "img_ref.dat");
      CtuluIOResult<PivOrthoParameters> ret = new PivImgRefReader().read(imgRefFile, _task);
      PivOrthoParameters params = ret.getSource();
//      PivOrthoParameters oldParams = new PivOrthoParameters(_prj.getOrthoParameters());
//      oldParams.setResolution(params.getResolution());
//      oldParams.setXmin(params.getXmin());
//      oldParams.setXmax(params.getXmax());
//      oldParams.setYmin(params.getYmin());
//      oldParams.setYmax(params.getYmax());
//      
//      _prj.setOrthoParameters(oldParams);

      return params;
    }
    catch (IOException ex) {
      _ana.addError(ex.getMessage());
      return null;
    }
  }

  /**
   * Lance le calcul de reconditionnement du transect pass�.
   *
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _task La tache en cours d'execution.
   * @param _trans Le transect a reconditionner. Attention : Le transect en entr�e est modifi�.
   * @param _idSel L'indice du transect a reconditionner.
   * @return true : Tout s'est bien d�roul�.
   */
  public boolean computeTransectRecond(CtuluLog _ana, PivProject _prj, PivTransect _trans, PivTaskObserverI _task) {
    File prjRoot=_prj.getRoot();
    File prjOutputs=new File(prjRoot,OUTPUT_DIR);
    File bathyFile=null;
    File bathypFile=null;
    
    try {
      int prog=10;

      // PIV_param.dat
      File paramFile=new File(prjOutputs, "PIV_param.dat");
      Object[] params=new Object[]{_prj.getComputeParameters(),_trans.getParams(),_prj.getTransfImageSize()};
      new PivParamWriter().write(params, paramFile, null);

      // bathy.dat
      bathyFile=new File(prjOutputs, "bathy.dat");
      new PivBathyWriter().write(_trans, bathyFile, null);

      String desc=PivResource.getS("Reconditionnement du transect...");
      FuLog.trace(desc);
      if (_task != null) {
        _task.setDesc(desc);
        _task.setProgression(prog);
      }

      // Reconditionnement....

      String outs=FuLib.runProgram(new String[]{exePath + File.separator + EXE.bathy_compute + EXE_EXT}, prjRoot);
      if (!outs.trim().equals("")) {
        _ana.addError(PivResource.getS("Erreur de reconditionnement du transect:\n") + outs);
        return false;
      }

      FuLog.trace(PivResource.getS("Reconditionnement du transect ok."));
      if (_task != null) {
        _task.setProgression(90);
      }

      // ... et r�cup�ration de la bathym�trie modifi�e.
      bathypFile=new File(prjOutputs, "bathy_p.dat");
      CtuluIOResult<PivTransect> ret=new PivBathyPReader().read(bathypFile, _task);

      _trans.setStraight(ret.getSource().getStraight());
      _trans.setCoefs(ret.getSource().getCoefs());

      if (_task != null && _task.isStopRequested()) {
        _ana.addError(PivResource.getS("Calcul interrompu"));
        return false;
      }
    }
    catch (IOException ex) {
      _ana.addError(ex.getMessage());
      return false;
    }
    finally {
      if (bathyFile!=null)
        bathyFile.delete();
      if (bathypFile!=null)
        bathypFile.delete();
    }
    return true;
  }
  
  /**
   * Lance le calcul de calcul des r�sultats de d�bit sur les transects.
   * Les calculs sont effectu�s dans l'espace temporaire.
   *
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _recond La g�om�trie des transects est remise � jour depuis les bathys reconditionn�es
   * @param _task La tache en cours d'execution.
   * @return true : Tout s'est bien d�roul�.
   */
  public boolean computeFlowResults(CtuluLog _ana, PivProject _prj, boolean _recond, PivTaskObserverI _task) {
    _prj.removeFlowResultsOnFS();
    
    File prjRoot = _prj.getRoot();
    File prjTmpRoot=_prj.getTempDirectory();
    CtuluLibFile.deleteDir(prjTmpRoot);
    
    File prjTmpOutputs=new File(prjTmpRoot,OUTPUT_DIR);
    prjTmpOutputs.mkdirs();
    File prjTransects=new File(prjRoot,TRANSECTS_DIR);

    PivTransect[] trans=_prj.getTransects();
    // Recopie temporaire des transects.
    PivTransect[] trans2=new PivTransect[trans.length];
    for (int i=0; i < trans.length; i++) {
      trans2[i]=trans[i].clone();
    }
    trans=trans2;
    PivFlowResults[] res=new PivFlowResults[trans.length];

    try {
      int prog=5;
      
      // Les vitesses moyennes.
      File velFile=new File(prjTmpOutputs, "average_vel.out");
      velFile.delete();
      new PivAverageVelWriter().write(_prj.getAverageResults(), velFile, null);
      
      // Hauteurs d'eau
      File hFile = new File(prjTmpOutputs, "h.dat");
      new PivHWriter().write(_prj.getOrthoParameters().getWaterElevation(), hFile, null);
      
      // Pour chaque transect
      for (int i=0; i < trans.length; i++) {

        // PIV_param.dat
        File paramFile=new File(prjTmpOutputs, "PIV_param.dat");
        Object[] params=new Object[]{_prj.getComputeParameters(),trans[i].getParams(),_prj.getTransfImageSize()};
        new PivParamWriter().write(params, paramFile, null);

        // bathy.dat
        File bathyFile=new File(prjTmpOutputs, "bathy.dat");
        new PivBathyWriter().write(trans[i], bathyFile, null);

        String desc=PivResource.getS("Reconditionnement/calcul de d�bit pour le transect {0}...", (i + 1));
        FuLog.trace(desc);
        if (_task != null) {
          _task.setDesc(desc);
          _task.setProgression(prog);
        }
        
        // Reconditionnement....

        String outs=FuLib.runProgram(new String[]{exePath + File.separator + EXE.bathy_compute + EXE_EXT}, prjTmpRoot);
        if (!outs.trim().equals("")) {
          _ana.addError(PivResource.getS("Erreur de reconditionnement du transect {0}:\n", (i + 1)) + outs);
          return false;
        }

        FuLog.trace(PivResource.getS("Reconditionnement du transect {0} ok.", (i + 1)));
//        if (_task != null) {
//          _task.setProgression(100);
//        }

        if (_recond) {
          // ... et r�cup�ration de la bathym�trie modifi�e.
          CtuluIOResult<PivTransect> ret=new PivBathyPReader().read(new File(prjTmpOutputs, "bathy_p.dat"), _task);
          trans[i].setStraight(ret.getSource().getStraight());
          trans[i].setCoefs(ret.getSource().getCoefs());
        }
        
        if (_task != null && _task.isStopRequested()) {
          _ana.addError(PivResource.getS("Calcul interrompu"));
          return false;
        }
        
        // Calcul de vitesses.

        desc=PivResource.getS("Calcul du d�bit...");
        FuLog.trace(desc);
//        if (_task != null) {
//          _task.setDesc(desc);
//          _task.setProgression(prog);
//        }

        StringBuffer sbout=new StringBuffer();
        StringBuffer sberr=new StringBuffer();
        outs=FuLib.runProgram(new String[]{exePath + File.separator + EXE.Q_compute + EXE_EXT}, prjTmpRoot, sbout, sberr);
        if (!sberr.toString().trim().equals("")) {
          _ana.addError(PivResource.getS("Erreur de calcul du d�bit du transect {0}:\n", (i + 1)) + outs);
          return false;
        }
        
        // D�placement du fichier...
        new File(prjTmpOutputs,"Discharge.dat").renameTo(new File(prjTransects,"trans"+PivUtils.formatOn4Chars(i+1)+"."+PivUtils.FILE_FLT_TRANS_RES.getFirstExt()));

        if (_task != null && _task.isStopRequested()) {
          _ana.addError(PivResource.getS("Calcul interrompu"));
          return false;
        }
        
        // ... et lecture
        CtuluIOResult<PivFlowResults> ret=new PivDischargeReader().read(new File(prjTransects,"trans"+PivUtils.formatOn4Chars(i+1)+"."+PivUtils.FILE_FLT_TRANS_RES.getFirstExt()), _task);
        res[i]=ret.getSource();
        FuLog.trace(PivResource.getS("Calcul du d�bit ok."));
        
        prog=5+((i+1)*90/trans.length);
        if (_task != null) {
          _task.setProgression(prog);
        }
      }
    }
    catch (IOException ex) {
      _ana.addError(ex.getMessage());
      return false;
    }

    // On supprime les repertoires temporaires
    finally {
      if (_recond)
        _prj.setTransects(trans);
      _prj.setFlowResults(res);
    }
    
    return true;
  }

  /**
   * Lance le calcul des resultats instantan�s (vitesses de l'espace transform� vers l'espace r�el + 
   * scalaires) et r�cup�re les r�sultats.
   * Important : Cette methode est lanc�e directement dans le r�pertoire du projet.
   *
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _task La tache en cours d'execution.
   * @return true : Tout s'est bien d�roul�.
   * @see #launchPiv
   */
  private boolean computeRealRawResults(CtuluLog _ana, PivProject _prj, PivTaskObserverI _task) {
    File prjRoot=_prj.getRoot();
    File prjVelRaw=new File(prjRoot, VEL_RAW_DIR);
    File prjOutputs=new File(prjRoot, OUTPUT_DIR);
        
    PivResultsI[] res=null;

    try {
      String desc=PivResource.getS("Transformation des vitesses instantan�es vers l'espace r�el...");
      FuLog.trace(desc);
      if (_task!=null) {
        _task.setDesc(desc);
        _task.setProgression(0);
      }

      // Cr�ation du fichier contenant la liste de tous les fichiers piv.dat obtenus par calcul.
      PrintWriter out=new PrintWriter(new File(prjOutputs,"list_avg.dat"));
      for (File f : prjVelRaw.listFiles()) {
        out.println(f.getName());
      }
      out.close();

      // PIV_param.dat
      File paramFile=new File(prjOutputs, "PIV_param.dat");
      Object[] params=new Object[]{_prj.getComputeParameters(), null, _prj.getTransfImageSize()};
      new PivParamWriter().write(params, paramFile, null);

      // img_ref.dat
      File imgRefFile = new File(prjOutputs, "img_ref.dat");
      params=new Object[]{_prj.getOrthoParameters(),_prj.getSrcImageSize()};
      new PivImgRefWriter().write(params, imgRefFile, _task);
      if (_task!=null)
        _task.setProgression(30);


      String outs = FuLib.runProgram(new String[]{exePath+File.separator+EXE.vel_transf + EXE_EXT}, prjRoot);
      if (!outs.trim().equals("")) {
        _ana.addError(PivResource.getS("Erreur de transformation des vitesses instantan�es:\n")+outs);
        return false;
      }

      if (_task!=null && _task.isStopRequested()) {
        _ana.addError(PivResource.getS("Transformation interrompue"));
        return false;
      }
      if (_task!=null)
        _task.setProgression(80);

      FuLog.trace(PivResource.getS("Transformation ok."));
      
      // Chargement des r�sultats
      res=_prj.getPersistenceManager().loadInstantRawResults(_task);
    }
    catch (IOException ex) {
      _ana.addError(ex.getMessage());
      return false;
    }
    finally {
      _prj.setInstantRawResults(res);
    }

    return true;
  }

  /**
   * Lance le calcul manuel des vitesses..
   * Important : Cette methode est lanc�e directement dans le r�pertoire du projet.
   *
   * @param _ana L'analyse pour la tache ex�cut�e.
   * @param _prj Le projet.
   * @param _task La tache en cours d'execution.
   * @return true : Tout s'est bien d�roul�.
   * @see #launchPiv
   */
  public boolean computeManualVelocities(CtuluLog _ana, PivProject _prj, PivTaskObserverI _task) {
    File prjRoot=_prj.getRoot();
    File prjVelReal=new File(prjRoot, VEL_REAL_DIR);
    File prjVelRaw=new File(prjRoot, VEL_RAW_DIR);
    File prjOutputs=new File(prjRoot, OUTPUT_DIR);
        
    PivResultsI res=null;

    try {
      String desc=PivResource.getS("Calcul manuel des vitesses...");
      FuLog.trace(desc);
      if (_task!=null) {
        _task.setDesc(desc);
        _task.setProgression(0);
      }

      // PIV_param.dat
      File manualTrackFile=new File(prjOutputs, "manual_tracking.dat");
      Object[] params=new Object[]{
          _prj.getManualVelocitiesParameters(),
          _prj.getOrthoParameters(),
          _prj.getComputeParameters(),
          _prj.getTransfImageSize()
          };
      
      new PivManualVelWriter().write(params, manualTrackFile, null);

      // #5687 : L'exe doit �tre chang� par un .exe d�s que ca fonctionnera.
      String outs = FuLib.runProgram(new String[] { exePath + File.separator + EXE.manual_vel + EXE_EXT }, prjRoot);
      if (!outs.trim().equals("")) {
        _ana.addError(PivResource.getS("Erreur de transformation des vitesses instantan�es:\n")+outs);
        return false;
      }

      if (_task!=null && _task.isStopRequested()) {
        _ana.addError(PivResource.getS("Calcul interrompu"));
        return false;
      }
      if (_task!=null)
        _task.setProgression(80);

      FuLog.trace(PivResource.getS("Calcul ok."));
      
      // Chargement des r�sultats
      res=_prj.getPersistenceManager().loadManualResults(_task);
    }
    catch (IOException ex) {
      _ana.addError(ex.getMessage());
      return false;
    }
    finally {
      _prj.setManualResults(res);
    }

    return true;
  }
}
