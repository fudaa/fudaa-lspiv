package org.fudaa.fudaa.piv.io;

import java.awt.Dimension;
import java.io.PrintWriter;
import java.util.List;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters;


/**
 * Une classe pour ecrire sur fichier le masque de stabilisation.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivStabMaskWriter extends FileCharSimpleWriterAbstract<Object[]> {

  /**
   * Ecrit les param�tres.
   * 
   * param _o Un tableau contenant 2 objets PivStabilizationParameters et Dimension.
   * 
   * Les objets peuvent �tre null (s'ils n'ont pas �t� d�finis). Dans ce cas, -1 sera
   * inscrit dans les champs correspondant pour indiquer que cette partie des infos
   * n'a pas �t� renseign�e.
   */
  protected void internalWrite(final Object[] _o) {
    if (_o.length != 2 || !(_o[0] instanceof PivStabilizationParameters) || !(_o[1] instanceof Dimension))
      return;
    
    PivStabilizationParameters stabParams = (PivStabilizationParameters)_o[0];
    Dimension imgSize = (Dimension)_o[1];
    
    List<GrPolygone> outlines = stabParams.getOutlines();
    if (outlines.size() == 0)
      return;
    
    final PrintWriter writer = new PrintWriter(out_);

    try {
      
      
      for (int j = imgSize.height-1 ; j>=0 ; j--) {
        for (int i = 0 ; i<imgSize.width ; i++) {
          boolean inMask = false;
          
          // Si on est en zones d'�coulement, on ecrit tous les points a l'interieur d'au moins 1 polygone.
          if (stabParams.isFlowAreas()) {
            for (GrPolygone pgMask : outlines) {
              if (pgMask.contientXY(i, j)) {
                inMask = true;
                break;
              }
            }
          }
          // Si on est en zones fixe, on ecrit les points qui sont a l'exterieur de tous les polygones
          else {
            inMask = true;
            for (GrPolygone pgMask : outlines) {
              if (pgMask.contientXY(i, j)) {
                inMask = false;
              }
            }
          }
          
          if (inMask) {
            writer.println((imgSize.height - 1) - j + " " + i);
          }
        }
      }
    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
  }
}
