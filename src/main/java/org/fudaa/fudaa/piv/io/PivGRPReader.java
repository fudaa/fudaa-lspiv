/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.awt.Dimension;
import java.io.EOFException;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;

/**
 * Un lecteur pour les fichiers PIV de points pour l'orthorectification. Ce
 * lecteur lit les formats standard ou etendu (GRP avec point recalcul�).
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivGRPReader extends FileCharSimpleReaderAbstract<PivOrthoPoint[]> implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;
  /** Format etendu */
  boolean bextFormat_;
  /** La dimension de l'espace ni, nj s'il n'est pas inscrit dans le format (format < 2.0) */
  Dimension imgSize_;

  /**
   * Le constructeur.
   * @param _imgSize La dimension d'image source du projet.
   */
  public PivGRPReader(Dimension _imgSize) {
    imgSize_ = _imgSize;
  }
  
  /**
   * Le constructeur, avec le param�etre d�finissant si le format est �tendu ou non.
   * @param _extendFormat True : Le format est etendu.
   */
  public PivGRPReader(Dimension _imgSize, boolean _extendFormat) {
    bextFormat_=_extendFormat;
    imgSize_ = _imgSize;
  }

  /**
   * Definit si le fichier est au format etendu.
   * @param _b True : Format etendu pour r�cup�ration des v�rification des points recalcul�s
   */
  public void setExtendFormat(boolean _b) {
    bextFormat_=_b;
  }

  /**
   * Lit les points et les retourne.
   * @return La collection de points
   */
  protected PivOrthoPoint[] internalRead() {
    return readPoints();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized PivOrthoPoint[] readPoints() {
    PivOrthoPoint[] pts = null;

    if (super.in_ == null) {
      analyze_.addError(PivResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      int lu = 0;
      boolean afficheAvance = false;
      if (progress_ != null) {
        afficheAvance = true;
        progress_.setProgression(0);
      }
      int pourcentageEnCours = 0;

      in_.setJumpBlankLine(true);

      // Entete
      in_.readFields();
      String entete = in_.stringField(0);
      if (!"GRP".equalsIgnoreCase(entete)) {
        analyze_.addError(PivResource.getS("Format de fichier invalide : Format GRP attendu"), 0);
        return null;
      }
      // GRP Format V2.0 : Ajout des dimensions de l'image.
      if (in_.getNumberOfFields() != 1 && !bextFormat_) {
        if (in_.stringField(1).equalsIgnoreCase("V2.0")) {
          int srcImgWidth = in_.intField(2);
          int srcImgHeight = in_.intField(3);
          imgSize_ = new Dimension(srcImgWidth, srcImgHeight);
        }
      }
      // Nombre de points
      in_.readFields();
      pts = new PivOrthoPoint[in_.intField(0)];
      // Labels des points => Verification qu'on est en format etendu ou non.
      in_.readFields();
      if (!bextFormat_ && in_.getNumberOfFields()!=5) {
        analyze_.addError(PivResource.getS("Format de fichier invalide : Format GRP attendu"), 0);
        return null;
      }
      else if(bextFormat_ && in_.getNumberOfFields() != 8) {
        analyze_.addError(PivResource.getS("Format de fichier invalide : Format GRP �tendu attendu"), 0);
        return null;
      }

      lu += 3; // Si tous les champs remplis au maxi

      // Les points, jusqu'� fin de fichier.
      for (int i = 0; i < pts.length; i++) {
        if (bstop_) {
          return null;
        }

        in_.readFields();
        GrPoint realPt = new GrPoint(in_.doubleField(0), in_.doubleField(1), in_.doubleField(2));
        GrPoint imgPt = new GrPoint(in_.doubleField(3), imgSize_.height - in_.doubleField(4), 0);
        pts[i] = new PivOrthoPoint(realPt, imgPt);
        // Format etendu : Lecture du point recalcul� + error
        if (bextFormat_) {
          GrPoint computeRealPt=new GrPoint(in_.doubleField(5),in_.doubleField(6),0);
          double error=in_.doubleField(7);
          pts[i].setComputeRealPoint(computeRealPt);
          pts[i].setError(error);
        }
        lu += 1; // Approximatif

        if ((afficheAvance) && ((lu * 100 / (pts.length+3)) >= (pourcentageEnCours + 20))) {
          pourcentageEnCours += 20;
          progress_.setProgression(pourcentageEnCours);
        }
      }
    }
    catch (final EOFException e) {
      analyze_.manageException(e);
    }
    catch (final IOException e) {
      analyze_.manageException(e);
    }
    catch (final NumberFormatException e) {
      analyze_.manageException(e);
    }
    
    return pts;
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  public void stop() {
    bstop_ = true;
  }
}
