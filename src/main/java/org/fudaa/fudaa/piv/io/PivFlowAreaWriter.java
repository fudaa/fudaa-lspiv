/*
 * @creation 15 f�vr. 07
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.awt.Dimension;
import java.io.PrintWriter;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.VecteurGrPoint;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;

/**
 * La classe pour ecrire des points d'orthorectification sur fichier. Cette
 * classe ne g�re pas le format �tendu.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivFlowAreaWriter extends FileCharSimpleWriterAbstract<GrPolygone> {

  /**
   * Ecrit les points d'orthorectification.
   * param _o Un tableau de PivOrthoPoint[].
   */
  protected void internalWrite(final GrPolygone _pg) {

    VecteurGrPoint pts = _pg.sommets_;

    final PrintWriter writer = new PrintWriter(out_);

    try {
      // Entete
//      writer.println("GRP");
      // Nombre de points
      writer.println(pts.nombre());

      for (int i = 0; i < pts.nombre(); i++) {
        int x = (int)pts.renvoieX(i);
        int y = (int)pts.renvoieY(i);
        String label = "P" + (i + 1);

        writer.println(x + " " + y + " " + label);
      }

    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
  }
}
