/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.awt.Dimension;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluXmlReaderHelper;
import org.fudaa.ctulu.fileformat.FileOperationReaderAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivFilterParameters;
import org.fudaa.fudaa.piv.metier.PivSamplingVideoParameters;
import org.fudaa.fudaa.piv.metier.PivStatisticParameters;
import org.fudaa.fudaa.piv.metier.PivStatisticParameters.PivStatisticCalcultationMethod;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Un lecteur pour lire les donn�es globales du projet.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivGlobalXmlReader extends FileOperationReaderAbstract<PivGlobalXmlParam>  {

  /** Drapeau d'interruption */
  boolean bstop_;
  InputStream in_;

  @Override
  protected PivGlobalXmlParam internalRead() {
    
    PivGlobalXmlParam params = new PivGlobalXmlParam();

    if (in_ == null) {
      analyze_.addError(PivResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    CtuluXmlReaderHelper reader;
    try {
      reader=new CtuluXmlReaderHelper(in_);
      
      if (progress_ != null) {
        progress_.setProgression(0);
      }

      Document doc=reader.getDoc();
      
      // La version
      String version = "1.0";
      NodeList ndlVersion=doc.getElementsByTagName("version");
      if (ndlVersion.getLength() > 0) {
        Element elVersion = (Element) ndlVersion.item(0);
        version = elVersion.getFirstChild().getNodeValue();
      }
      
      // Coordonn�es du centre IA/SA
      String s1=reader.getTrimTextFor("jCoord");
      String s2=reader.getTrimTextFor("iCoord");
      if (s1!=null && s2!=null) {
        GrPoint pt=new GrPoint();
        pt.x_=Integer.parseInt(s1);
        pt.y_=Integer.parseInt(s2);
        params.iaCenter=pt;
      }

      // La liste des images sources
      NodeList ndlImgs=doc.getElementsByTagName("image");
      params.srcImages=new String[ndlImgs.getLength()];
      for (int i=0; i<ndlImgs.getLength(); i++) {
        Element elImg=(Element)ndlImgs.item(i);
        int ind=Integer.parseInt(elImg.getAttribute("ind"));
        String name=elImg.getFirstChild().getNodeValue();
        params.srcImages[ind]=name;
      }
      
      // La matrice de transformation espace initial => espace de calcul.
      NodeList ndlMat=doc.getElementsByTagName("transfMatrix");
      for (int i=0; i<ndlMat.getLength(); i++) {
        Element elMat=(Element)ndlMat.item(i);
        params.tx=Double.parseDouble(elMat.getAttribute("tx"));
        params.ty=Double.parseDouble(elMat.getAttribute("ty"));
        params.tz=Double.parseDouble(elMat.getAttribute("tz"));
        params.rz=Double.parseDouble(elMat.getAttribute("rz"));
        String xcenter = elMat.getAttribute("xcenter");
        String ycenter = elMat.getAttribute("ycenter");
        // Formats sup�rieurs � 1.4.5
        if (!xcenter.isEmpty() && !ycenter.isEmpty()) {
          params.xcenter=Double.parseDouble(xcenter);
          params.ycenter=Double.parseDouble(ycenter);
        }
      }
      
      // Scaling : Les points de transformation.
      NodeList ndlTransfPoints=doc.getElementsByTagName("transfPoint");
      params.scalingImgPoints=new GrPoint[ndlTransfPoints.getLength()];
      params.scalingRealPoints=new GrPoint[ndlTransfPoints.getLength()];
      for (int ipt=0; ipt < ndlTransfPoints.getLength(); ipt++) {
        Element elPt=(Element) ndlTransfPoints.item(ipt);
        int i=Integer.parseInt(elPt.getAttribute("i"));
        int j=Integer.parseInt(elPt.getAttribute("j"));
        double x=Double.parseDouble(elPt.getAttribute("x"));
        double y=Double.parseDouble(elPt.getAttribute("y"));
        params.scalingImgPoints[ipt]=new GrPoint(i,j,0);
        params.scalingRealPoints[ipt]=new GrPoint(x,y,0);
      }
      
      // Scaling : Le type de resolution
      PivGlobalXmlParam.ResolutionType resType = PivGlobalXmlParam.ResolutionType.DIRECT;
      Double zDrone = null;
      NodeList ndlResolution=doc.getElementsByTagName("resolution");
      for (int ires=0; ires < ndlResolution.getLength(); ires++) {
        Element elRes=(Element) ndlResolution.item(ires);
        resType=PivGlobalXmlParam.ResolutionType.valueOf(elRes.getAttribute("type"));
        String szDrone = elRes.getAttribute("altitudeDrone");
        if (szDrone != "") {
           zDrone = Double.parseDouble(szDrone);
        }
      }
      params.scalingZDrone = zDrone;
      
      params.scalingResolutionImgPoints = new ArrayList<>();
      params.scalingResolutionRealPoints = new ArrayList<>();
      params.scalingResolutionDistances = new ArrayList<>();
      params.scalingZSegments = new ArrayList<>();
      
      // Scaling : Les segments pour la resolution
      NodeList ndlResolutionCouples=doc.getElementsByTagName("resolutionCouple");
      for (int icouple=0; icouple < ndlResolutionCouples.getLength(); icouple++) {
        Element elCouple=(Element) ndlResolutionCouples.item(icouple);
        int i1=Integer.parseInt(elCouple.getAttribute("i1"));
        int j1=Integer.parseInt(elCouple.getAttribute("j1"));
        double x1=Double.parseDouble(elCouple.getAttribute("x1"));
        double y1=Double.parseDouble(elCouple.getAttribute("y1"));
        int i2=Integer.parseInt(elCouple.getAttribute("i2"));
        int j2=Integer.parseInt(elCouple.getAttribute("j2"));
        double x2=Double.parseDouble(elCouple.getAttribute("x2"));
        double y2=Double.parseDouble(elCouple.getAttribute("y2"));
        double distance = Double.parseDouble(elCouple.getAttribute("distance"));
        double zSegment = 0;
        if (zDrone != null && elCouple.getAttribute("zSegment") != null) {
          zSegment = Double.parseDouble(elCouple.getAttribute("zSegment"));
          params.scalingZSegments.add(zSegment);
        }
        
        GrPoint[] imgPts = new GrPoint[2];
        imgPts[0] = new GrPoint(i1, j1, 0);
        imgPts[1] = new GrPoint(i2, j2, 0);
        params.scalingResolutionImgPoints.add(imgPts);
        
        if (resType == PivGlobalXmlParam.ResolutionType.DISTANCES) {
          params.scalingResolutionDistances.add(distance);
        }
        else {
          GrPoint[] realPts = new GrPoint[2];
          realPts[0] = new GrPoint(x1, y1, 0);
          realPts[1] = new GrPoint(x2, y2, 0);
          params.scalingResolutionRealPoints.add(realPts);
        }
      }
      
      // Direct velocities
      boolean isManualVelocitiesUsed = false;
      NodeList ndlDirectVelocities = doc.getElementsByTagName("directVelocities");
      if (ndlDirectVelocities.getLength() > 0) {
        Element elDirectVelocities = (Element)ndlDirectVelocities.item(0);
        
        for (int iel = 0; iel < elDirectVelocities.getChildNodes().getLength(); iel++) {
          
          // Les points de deplacement
          if ("displacementPoints".equals(elDirectVelocities.getChildNodes().item(iel).getNodeName())) {
            Element elDisp = (Element) elDirectVelocities.getChildNodes().item(iel);
            NodeList ndlDisplacementPoints = elDisp.getElementsByTagName("displacementPoint");
            for (int icouple = 0; icouple < ndlDisplacementPoints.getLength(); icouple++) {
              Element elCouple = (Element) ndlDisplacementPoints.item(icouple);
              double i1 = Double.parseDouble(elCouple.getAttribute("i1"));
              double j1 = Double.parseDouble(elCouple.getAttribute("j1"));
              double i2 = Double.parseDouble(elCouple.getAttribute("i2"));
              double j2 = Double.parseDouble(elCouple.getAttribute("j2"));
              int img1 = Integer.parseInt(elCouple.getAttribute("img1"));
              int img2 = Integer.parseInt(elCouple.getAttribute("img2"));

              GrPoint[] imgPts = new GrPoint[2];
              imgPts[0] = new GrPoint(j1, i1, 0);
              imgPts[1] = new GrPoint(j2, i2, 0);
              params.displacementPoints.add(imgPts);

              Integer[] imgIndexes = new Integer[] { img1, img2 };
              params.imgIndexes.add(imgIndexes);
            }
          }
          
          //  Les vitesses calcul�es sont int�gr�es aux r�sultats moyens.
          else if ("combined".equals(elDirectVelocities.getChildNodes().item(iel).getNodeName())) {
            Element elCombined = (Element) elDirectVelocities.getChildNodes().item(iel);
            String sbool=elCombined.getFirstChild().getNodeValue();
            // Conserv� pour plus tard
            isManualVelocitiesUsed = Boolean.parseBoolean(sbool);
          }
        }
      }
      
      // Le calcul par piv
      {
        NodeList ndlPivComputations = doc.getElementsByTagName("pivComputation");
        if (ndlPivComputations.getLength() > 0) {
          Element elPivComputation = (Element)ndlPivComputations.item(0);
          params.isPivActive = Boolean.parseBoolean(elPivComputation.getAttribute("active"));
        }
      }
      
      // Stabilisation : Les points du polygone
      if (version.compareTo("2.0") < 0) {
        NodeList ndlFlowAreaPoints = doc.getElementsByTagName("flowAreaPoint");
        if (ndlFlowAreaPoints.getLength() > 0) {
          params.stabilizationFlowArea = new GrPolygone();
          for (int ipoint = 0; ipoint < ndlFlowAreaPoints.getLength(); ipoint++) {
            Element elPoint = (Element) ndlFlowAreaPoints.item(ipoint);
            int i1 = Integer.parseInt(elPoint.getAttribute("i"));
            int j1 = Integer.parseInt(elPoint.getAttribute("j"));
            params.stabilizationFlowArea.sommets_.ajoute(i1, j1, 0);
          }
        }
      }

      // Sampling
      if (version.compareTo("2.0") < 0) {
        NodeList ndlSampling = doc.getElementsByTagName("sampling");
        if (ndlSampling.getLength() > 0) {
          params.samplingParams = new PivSamplingVideoParameters();

          NodeList nds = ndlSampling.item(0).getChildNodes();
          for (int i = 0; i < nds.getLength(); i++) {
            if (nds.item(i) instanceof Element) {
              Element elParam = (Element) nds.item(i);

              if (elParam.getNodeName().equals("samplingVideoFile")) {
                params.samplingParams.setVideoFile(new File(elParam.getFirstChild().getNodeValue()));
              }
              else if (elParam.getNodeName().equals("samplingPeriod")) {
                int period = Integer.parseInt(elParam.getFirstChild().getNodeValue());
                params.samplingParams.setSamplingPeriod(period);
              }
              else if (elParam.getNodeName().equals("samplingSize")) {
                int width = Integer.parseInt(elParam.getAttribute("width"));
                int height = Integer.parseInt(elParam.getAttribute("height"));
                params.samplingParams.setSamplingSize(new Dimension(width, height));
              }
              else if (elParam.getNodeName().equals("samplingTime")) {
                double begin = Double.parseDouble(elParam.getAttribute("begin"));
                double end = Double.parseDouble(elParam.getAttribute("end"));
                params.samplingParams.setSamplingBeginTime(begin);
                params.samplingParams.setSamplingEndTime(end);
              }
            }
          }
        }
      }
      
      // Filters
      {
        NodeList ndlfilters = doc.getElementsByTagName("filters");
        if (ndlfilters.getLength() > 0) {
          params.filterParams = new PivFilterParameters();

          NodeList nds = ndlfilters.item(0).getChildNodes();
          for (int i = 0; i < nds.getLength(); i++) {
            if (nds.item(i) instanceof Element) {
              Element elParam = (Element) nds.item(i);

              if (elParam.getNodeName().equals("velocityNorm")) {
                boolean active = Boolean.parseBoolean(elParam.getAttribute("active"));
                double min = Double.parseDouble(elParam.getAttribute("min"));
                double max = Double.parseDouble(elParam.getAttribute("max"));
                params.filterParams.setVelocityFilterEnabled(active);
                params.filterParams.setSmin(min);
                params.filterParams.setSmax(max);
              }
              else if (elParam.getNodeName().equals("vx")) {
                boolean active = Boolean.parseBoolean(elParam.getAttribute("active"));
                double min = Double.parseDouble(elParam.getAttribute("min"));
                double max = Double.parseDouble(elParam.getAttribute("max"));
                params.filterParams.setVelocityFilterEnabled(active);
                params.filterParams.setVxmin(min);
                params.filterParams.setVxmax(max);
              }
              else if (elParam.getNodeName().equals("vy")) {
                boolean active = Boolean.parseBoolean(elParam.getAttribute("active"));
                double min = Double.parseDouble(elParam.getAttribute("min"));
                double max = Double.parseDouble(elParam.getAttribute("max"));
                params.filterParams.setVelocityFilterEnabled(active);
                params.filterParams.setVymin(min);
                params.filterParams.setVymax(max);
              }
              else if (elParam.getNodeName().equals("correlation")) {
                boolean active = Boolean.parseBoolean(elParam.getAttribute("active"));
                double min = Double.parseDouble(elParam.getAttribute("min"));
                double max = Double.parseDouble(elParam.getAttribute("max"));
                params.filterParams.setCorrelationFilterEnabled(active);
                params.filterParams.setMinCorrelation(min);
                params.filterParams.setMaxCorrelation(max);
              }
              else if (elParam.getNodeName().equals("medianTest")) {
                boolean active = Boolean.parseBoolean(elParam.getAttribute("active"));
                double epsilon = Double.parseDouble(elParam.getAttribute("epsilon"));
                double r0min = Double.parseDouble(elParam.getAttribute("r0min"));
                boolean advanced = Boolean.parseBoolean(elParam.getAttribute("advanced"));
                int nneighbor = Integer.parseInt(elParam.getAttribute("n_neighbor"));
                double distmax = Double.parseDouble(elParam.getAttribute("dist_max"));
                params.filterParams.setMedianTestFilterEnabled(active);
                params.filterParams.setEpsilon(epsilon);
                params.filterParams.setR0min(r0min);
                params.filterParams.setMedianTestFilterAdvancedEnabled(advanced);
                params.filterParams.setNneighbor(nneighbor);
                params.filterParams.setDistmax(distmax);
              }
              else if (elParam.getNodeName().equals("correlationPeak")) {
                boolean active = Boolean.parseBoolean(elParam.getAttribute("active"));
                double rcut = Double.parseDouble(elParam.getAttribute("rcut"));
                double rh0max = Double.parseDouble(elParam.getAttribute("rh0max"));
                params.filterParams.setCorrelationPeakFilterEnabled(active);
                params.filterParams.setRcut(rcut);
                params.filterParams.setRhomax(rh0max);
              }
              else if (elParam.getNodeName().equals("velocityDistribution")) {
                boolean active = Boolean.parseBoolean(elParam.getAttribute("active"));
                int nstdvel = Integer.parseInt(elParam.getAttribute("nstdvel"));
                params.filterParams.setVelocityDistributionFilterEnabled(active);
                params.filterParams.setNstdvel(nstdvel);
              }
              else if (elParam.getNodeName().equals("velocityDispersion")) {
                boolean active = Boolean.parseBoolean(elParam.getAttribute("active"));
                double covmax = Double.parseDouble(elParam.getAttribute("covmax"));
                params.filterParams.setVelocityDispersionFilterEnabled(active);
                params.filterParams.setCovmax(covmax);
              }
              else if (elParam.getNodeName().equals("angularDispersion")) {
                boolean active = Boolean.parseBoolean(elParam.getAttribute("active"));
                double circvarmax = Double.parseDouble(elParam.getAttribute("circvarmax"));
                params.filterParams.setAngularDispersionFilterEnabled(active);
                params.filterParams.setCircvarmax(circvarmax);
              }
            }
          }
        }
      }

      // Statistics
      {
        params.statisticParams = new PivStatisticParameters();

        NodeList ndlstats = doc.getElementsByTagName("statistics");
        if (ndlstats.getLength() > 0) {

          Element elParam = (Element) ndlstats.item(0);

          PivStatisticCalcultationMethod method;
          if ("median".equals(elParam.getAttribute("method"))) {
            method = PivStatisticCalcultationMethod.MEDIAN;
          }
          else {
            method = PivStatisticCalcultationMethod.MEAN;
          }
          params.statisticParams.setMethod(method);
        }
        
        params.statisticParams.setManualFieldUsed(isManualVelocitiesUsed);
      }

      if (progress_ != null) {
        progress_.setProgression(100);
      }
    }
    catch (final SAXException e) {
      analyze_.manageException(e);
    }
    catch (final ParserConfigurationException e) {
      analyze_.manageException(e);
    }
    catch (final EOFException e) {
      analyze_.manageException(e);
    }
    catch (final IOException e) {
      analyze_.manageException(e);
    }
    catch (final NumberFormatException e) {
      analyze_.manageException(e);
    }
        
    return params;
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  public void stop() {
    bstop_ = true;
  }

  @Override
  protected FortranInterface getFortranInterface() {
    return new FortranInterface() {
      @Override
      public void close() throws IOException {
        if (in_!=null)
          in_.close();
      }
    };
  }

  @Override
  public void setFile(final File _f){
    analyze_ = new CtuluLog();
    analyze_.setResource(_f.getAbsolutePath());
    FileInputStream r = null;
    try {
      r = new FileInputStream(_f);
    }
    catch (final FileNotFoundException _e) {
      analyze_.addSevereError(DodicoLib.getS("Fichier inconnu"));
    }
    in_=r;
  }
}
