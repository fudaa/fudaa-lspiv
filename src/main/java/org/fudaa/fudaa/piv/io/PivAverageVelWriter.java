package org.fudaa.fudaa.piv.io;

import java.io.PrintWriter;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.fudaa.piv.metier.PivResultsI;
import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;


/**
 * Une classe pour ecrire un fichier des r�sultats de vitesses moyenn�es.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivParamWriter.java 9455 2016-11-18 11:05:02Z bmarchan $
 */
public class PivAverageVelWriter extends FileCharSimpleWriterAbstract<PivResultsI> {

  /**
   * Ecrit les resultats.
   */
  @Override
  protected void internalWrite(final PivResultsI _res) {
    if (!_res.hasResult(ResultType.VX)  ||
        !_res.hasResult(ResultType.VY)) {
      donneesInvalides(_res);
      return;
    }

    final PrintWriter writer = new PrintWriter(out_);

    try {
      for (int i=0; i<_res.getNbPoints(); i++) {
        // (est lu par les solveurs en format libre).
        writer.println(_res.getX(i)+" "+_res.getY(i)+" "+_res.getValue(i, ResultType.VX)+" "+_res.getValue(i, ResultType.VY));
      }
    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
  }
}
