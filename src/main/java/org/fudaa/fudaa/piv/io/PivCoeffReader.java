/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.io.EOFException;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;
import org.fudaa.fudaa.piv.PivResource;

import gnu.trove.TDoubleArrayList;

/**
 * Un lecteur pour le fichier des coefficients d'orthorectification.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivHReader.java 9015 2015-02-12 16:45:55Z bmarchan $
 */
public class PivCoeffReader extends FileCharSimpleReaderAbstract<double[]> implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;

  /**
   * Le constructeur.
   */
  public PivCoeffReader() {
  }

  /**
   * Lit les coefficients et les retourne.
   * @return La cote d'eau.
   */
  protected double[] internalRead() {
    return readCoeffs();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized double[] readCoeffs() {
    TDoubleArrayList coeffs=new TDoubleArrayList();

    if (super.in_ == null) {
      analyze_.addError(PivResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      if (progress_ != null) {
        progress_.setProgression(0);
      }

      in_.setJumpBlankLine(true);

      in_.readFields();
      // Nombre de coeffs (les anciens fichiers n'ont pas cette ligne)
      try {
        in_.intField(0);
      }
      catch (NumberFormatException _exc) {
        coeffs.add(in_.doubleField(0));
      }

      // Les coeffs (jusqu'� la fin du fichier)
      try {
        while (true) {
          in_.readFields();
          coeffs.add(in_.doubleField(0));
        }
      }
      catch (EOFException _exc) {}
      
      if (progress_ != null) {
        progress_.setProgression(100);
      }
    }
    catch (final EOFException e) {
      analyze_.manageException(e);
    }
    catch (final IOException e) {
      analyze_.manageException(e);
    }
    catch (final NumberFormatException e) {
      analyze_.manageException(e);
    }
    return coeffs.toNativeArray();
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  public void stop() {
    bstop_ = true;
  }
}
