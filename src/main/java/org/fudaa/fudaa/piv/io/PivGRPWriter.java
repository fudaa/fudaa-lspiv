/*
 * @creation 15 f�vr. 07
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.awt.Dimension;
import java.io.PrintWriter;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;

/**
 * La classe pour ecrire des points d'orthorectification sur fichier. Cette
 * classe ne g�re pas le format �tendu.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivGRPWriter extends FileCharSimpleWriterAbstract<Object[]> {

  /**
   * Ecrit les points d'orthorectification.
   * param _o[0] Un tableau de PivOrthoPoint[], _o[1] La dimension des images, pour que le GRP soit autoportant
   */
  protected void internalWrite(final Object[] _o) {
    if (_o.length!=2 ||
        !(_o[0] instanceof PivOrthoPoint[]) ||
        !(_o[1] instanceof Dimension)) {
      donneesInvalides(_o);
      return;
    }

    PivOrthoPoint[] pts = (PivOrthoPoint[])_o[0];
    int srcImgWidth = ((Dimension)_o[1]).width;
    int srcImgHeight = ((Dimension)_o[1]).height;

    final PrintWriter writer = new PrintWriter(out_);

    try {
      // Entete, version 2 : On ajoute la largeur/hauteur de l'image.
      writer.println("GRP V2.0 " + srcImgWidth + " " + srcImgHeight);
      // Nombre de points
      writer.println(pts.length);
      // Les labels de colonnes
      writer.println("X  Y  Z  i  j");

      for (int i = 0; i < pts.length; i++) {
        double x = pts[i].getRealPoint().x_;
        double y = pts[i].getRealPoint().y_;
        double z = pts[i].getRealPoint().z_;
        int iimg = (int) pts[i].getImgPoint().x_;
        int jimg = (int) (srcImgHeight - pts[i].getImgPoint().y_);

        writer.println(x + " " + y + " " + z + " " + iimg + " " + jimg);
      }

    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
  }
}
