package org.fudaa.fudaa.piv.io;

import java.io.PrintWriter;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.piv.metier.PivTransect;

/**
 * Une classe pour ecrire un transect sur fichier.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivBathyWriter extends FileCharSimpleWriterAbstract<PivTransect> {

  /**
   * Ecrit les paramètres d'orthorectification.
   * param _o Un PivTransect.
   */
  @Override
  protected void internalWrite(final PivTransect _o) {
    GrPolyligne pl = _o.getStraight();
    double[] coefs = _o.getCoefs();

    final PrintWriter writer = new PrintWriter(out_);

    try {

      // Boucle sur tous les points, ecriture des coordonnées en entier (images)
      for (int i=0; i<pl.nombre(); i++) {
        writer.println((double) pl.sommet(i).x_ + " " + (double) pl.sommet(i).y_ + " " + (double) pl.sommet(i).z_ + " " + coefs[i]);
      }

    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
  }
}
