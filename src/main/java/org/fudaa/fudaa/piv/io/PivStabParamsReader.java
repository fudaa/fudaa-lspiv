/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.io.EOFException;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters.StabilizationPointsDensity;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters.StabilizationTransformationModel;

/**
 * Un lecteur pour le fichier stab_params.dat.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivStabParamsReader extends FileCharSimpleReaderAbstract<PivStabilizationParameters> implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;

  /**
   * Le constructeur.
   */
  public PivStabParamsReader() {
  }

  /**
   * @return Les param�tres de stabilisation.
   */
  protected PivStabilizationParameters internalRead() {
    return readParams();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized PivStabilizationParameters readParams() {
    PivStabilizationParameters ret = new PivStabilizationParameters();

    if (super.in_ == null) {
      analyze_.addError(PivResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      if (progress_ != null) {
        progress_.setProgression(0);
      }

      in_.setJumpBlankLine(true);
      in_.setJumpCommentLine(true);
      in_.setCommentInOneField("#");

      // Commentaire
      in_.readFields();
      // Densit�
      in_.readFields();
      ret.setDensity(StabilizationPointsDensity.ordinal(in_.intField(0)));
      // Commentaire
      in_.readFields();
      // Mod�le
      in_.readFields();
      ret.setModel(StabilizationTransformationModel.ordinal(in_.intField(0)));

      if (progress_ != null) {
        progress_.setProgression(100);
      }
      
      return ret;
    }
    catch (final EOFException e) {
      analyze_.manageException(e);
    }
    catch (final IOException e) {
      analyze_.manageException(e);
    }
    catch (final NumberFormatException e) {
      analyze_.manageException(e);
    }
    return null;
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  public void stop() {
    bstop_ = true;
  }
}
