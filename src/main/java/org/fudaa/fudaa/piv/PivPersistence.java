package org.fudaa.fudaa.piv;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.fudaa.commun.save.FudaaSaveLib;
import org.fudaa.fudaa.commun.save.FudaaSaveProject;
import org.fudaa.fudaa.piv.io.PivFlowAreaReader;
import org.fudaa.fudaa.piv.io.PivStabAreasWriter;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.fu.FuLog;

/**
 * Sauve ou charge un fichier projet depuis un fichier ZIP. Le fichier contient
 * des datas et des caract�ristiques graphiques.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivPersistence {
  PivImplementation impl_;
  private PivProject prj_;
  private Map<String,EbliUIProperties> uiprops_;
  
  public PivPersistence(PivImplementation _impl) {
    impl_=_impl;
  }
  
  /**
   * Sauvegarde dans un format zipp� du projet et des propri�t�s graphiques.
   * @param _prjFile Le fichier zip de destination.
   * @param _prog L'interface de progression.
   * @return True : Si la sauvegarde s'est bien d�roul�e.
   */
  public boolean save(File _prjFile, ProgressionInterface _prog) {
    PivProject prj=impl_.getCurrentProject();
    
    // Sauvegarde des infos projet (pour plus tard, compatibilit� avec les autres projets Fudaa).
    try {
      File infoFile=new File(prj.getRoot(), "project.xml");
      FileOutputStream outs = new FileOutputStream(infoFile);
      FudaaSaveProject.saveOnProjectXML(outs, new BuInformationsDocument(), impl_.getInformationsSoftware());
      outs.close();
    }
    catch (IOException ex) {
      return false;
    }
    
    // Sauvegarde des datas dans le r�pertoire temporaire.
    prj.save(_prog);

    // Sauvegarde des propri�t�s graphiques des calques.
    File dbFile=new File(prj.getRoot(), "project.db");
    dbFile.delete();
    FudaaSaveLib.configureDb4o();
    ObjectContainer cont=Db4o.openFile(dbFile.getAbsolutePath());
    for (EbliUIProperties props : impl_.get2dFrame().getVisuPanel().getLayerProperties().values()) {
      cont.set(props);
    }
    cont.commit();
    cont.close();
    
    // Compression du projet en zip ou 7z.
    try {
      CtuluLibFile.archive(prj.getRoot(), _prjFile, _prog);
    }
    catch (IOException ex) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Charge le projet depuis le fichier projet donn�. Le fichier projet est
   * d�zipp�, puis il est charg� s'il est conforme.
   * 
   * @param _prjFile Le fichier contenant le projet.
   * @param _prog L'interface de progression. Peut etre <tt>null</tt>
   * @return True Si le fichier donn� est bien un fichier projet.
   */
  public boolean load(File _prjFile, PivTaskObserverI _prog) {
    if (_prog!=null)
      _prog.setDesc(PivResource.getS("Chargement du projet"));
    
    // Controle que le fichier archive est bien un fichier PIV par recherche
    // d'un repertoire OUTPUT_DIR dans toutes les entries. Suivant le zippeur,
    // il se peut que le r�pertoire en tant qu'entry n'existe pas. On recherche
    // donc ce nom dans toutes les entries.
    if (!CtuluLibFile.findEntryInArchive(_prjFile, new File(PivProject.OUTPUT_DIR)))
      return false;
    
    // Cr�ation du r�pertoire temporaire de travail pour le unzip...
    prj_=new PivProject();
    prj_.init();
    // ... puis unzip
    try {
      CtuluLibFile.unarchive(_prjFile, prj_.getRoot(), _prog);
    }
    catch (IOException ex) {
      FuLog.debug(ex.getMessage());
      return false;
    }
    
    // Lecture de la version
    String version;
    try {
      File infoFile=new File(prj_.getRoot(), "project.xml");
      FileInputStream ins = new FileInputStream(infoFile);
      BuInformationsSoftware infos = FudaaSaveProject.getSoftwareInfos(ins);
      ins.close();
      
      version=infos.version;
    }
    // Pas de fichier => On consid�re que la version est inf�rieure ou �gale � 1.1
    catch (IOException e) {
      version="1.1";
    }
    
    if (compareVersions(impl_.getInformationsSoftware().version, version) > 0) {
      FuLog.trace("Conversion du projet de la version " + version + " vers " + impl_.getInformationsSoftware().version);

      // Conversion des versions < 1.4.2
      if (compareVersions("1.4.2", version) > 0) {
        convertTo1_4_2();
      }
      // Conversion des version < 1.10.0
      if (compareVersions("1.10.0", version) > 0) {
        convertTo_1_10_0();
      }
    }
    else {
      FuLog.trace("Chargement du fichier en version " + version);
    }
    
    // Lecture du projet
    prj_.load(_prog);
    
    
    // Lecture des propri�t�s graphiques des calques.
    File dbFile_=new File(prj_.getRoot(), "project.db");
    FudaaSaveLib.configureDb4o();
    ObjectContainer cont=Db4o.openFile(dbFile_.getAbsolutePath());
    ObjectSet set=cont.query(EbliUIProperties.class);
    uiprops_=new HashMap<String,EbliUIProperties>();
    while (set.hasNext()) {
      EbliUIProperties props=(EbliUIProperties)set.next();
      cont.activate(props, Integer.MAX_VALUE);
      uiprops_.put(props.getLayerName(),props);
    }
    cont.close();
    
    return true;
  }
  
  /**
   * Retourne 1 si version1 > version2, 0 si version1 = version2, -1 si version2 > version1
   * @param version1
   * @param version2
   * @return
   */
  protected int compareVersions(String version1, String version2) {
    String[] snums1 = version1.split("\\.");
    String[] snums2 = version2.split("\\.");
    
    for(int i = 0; i < snums1.length; i++) {
      String snum1 = snums1[i];
      Integer num1 = PivUtils.tryParseInt(snum1);
      
      String snum2 = null;
      Integer num2 = null;
      if (snums2.length > i ) {
        snum2 = snums2[i];
        num2 = PivUtils.tryParseInt(snum2);
      }
      else {
        return 1;
      }

      // On compare suivant les caract�res si un sous numero n'est pas un entier.
      if (num1 == null || num2 == null) {
        if (snum1 == snum2) {
          continue;
        }
        else if (snum1 == null) {
          return -1;
        }
        else if (snum2 == null) {
          return 1;
        }
        else {
          return snum1.compareTo(snum2);
        }
      }

      if (num1 == num2) {
        continue;
      }
      else if (num1 > num2) {
        return 1;
      }
      else {
        return -1;
      }
    }
    
    return 0;
  }
  
  /**
   * @return Le projet apr�s chargement
   */
  public PivProject getProject() {
    return prj_;
  }
  
  /**
   * @return Les propri�t�s graphiques apr�s chargement.
   */
  public Map<String,EbliUIProperties> getLayerProperties() {
    return uiprops_;
  }
  
  /**
   * Conversion vers la version 1.4.2
   * Les modifications sont :
   * - Le fichier outputs.dir/bathy.dat est d�plac� vers transects/bathy0001.dat
   * - les fichiers img_pgm/cache.jpg et img_transf/cache.jpg sont supprim�es.
   */
  private void convertTo1_4_2() {
    File dirImgSrc=new File(prj_.getRoot(),"img_pgm");
    File dirImgTrf=new File(prj_.getRoot(),"img_transf");
    File dirOutputs=new File(prj_.getRoot(),"outputs.dir");
    File dirTransec=new File(prj_.getRoot(),"transects");
    
    new File(dirImgSrc,"cache.jpg").delete();
    new File(dirImgTrf,"cache.jpg").delete();
    
    if (new File(dirOutputs,"bathy.dat").exists()) {
      dirTransec.mkdir();
      new File(dirOutputs,"bathy.dat").renameTo(new File(dirTransec,"trans0001."+PivUtils.FILE_FLT_TRANS_BTH.getFirstExt()));
    }
    if (new File(dirOutputs,"Discharge.dat").exists()) {
      dirTransec.mkdir();
      new File(dirOutputs,"Discharge.dat").renameTo(new File(dirTransec,"trans0001."+PivUtils.FILE_FLT_TRANS_RES.getFirstExt()));
    }
    if (new File(dirOutputs,"PIV_param.dat").exists()) {
      dirTransec.mkdir();
      CtuluLibFile.copyFile(new File(dirOutputs,"PIV_param.dat"),new File(dirTransec,"trans0001."+PivUtils.FILE_FLT_TRANS_PAR.getFirstExt()));
    }
  }
  
  /**
   * Conversion vers la version 1.4.2
   * Les modifications sont :
   * - Le fichier flow_area.dat est converti en stab_areas.dat
   */
  private void convertTo_1_10_0() {
    File dirOutputs=new File(prj_.getRoot(),"outputs.dir");
    File flowAreaFile = new File(dirOutputs,"flow_area.dat");
    File stabAreasFile = new File(dirOutputs,"stab_areas.dat");
    
    if (flowAreaFile.exists()) {
      // Le fichier du polygone de stabilisation.
      CtuluIOResult<GrPolygone> ret = new PivFlowAreaReader().read(flowAreaFile, null);
      flowAreaFile.delete();
      
      PivStabilizationParameters stabParams = new PivStabilizationParameters();
      stabParams.getOutlines().add(ret.getSource());
      
      // Le nouveau fichier de stabilisation.
      stabAreasFile.delete();
      new PivStabAreasWriter().write(stabParams, stabAreasFile, null);
    }
  }
}
