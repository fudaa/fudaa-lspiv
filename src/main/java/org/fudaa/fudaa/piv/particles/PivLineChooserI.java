package org.fudaa.fudaa.piv.particles;

import org.fudaa.fudaa.sig.layer.FSigTempLineInLayer;
import org.locationtech.jts.geom.LineString;

/**
 * Une classe pour l'affichage temporaire d'une ligne dans la vue 2D.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public interface PivLineChooserI {

  FSigTempLineInLayer getTmpLine();

  void setTmpLine(FSigTempLineInLayer tmpLine);

  void close();

  /**
   * @return the initSelected
   */
  LineString getInitSelected();

  void restaurer();

  void update(LineString _s, boolean _zoom);

  void zoomInitial();

}