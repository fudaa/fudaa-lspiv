package org.fudaa.fudaa.piv.particles;

import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.ListModel;

import org.fudaa.ctulu.CtuluDurationDateFormatter;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.layer.PivResultsModel;
import org.fudaa.fudaa.piv.metier.PivResultsI;

/**
 * Une classe impl�mentant une source pour les particules � partir des r�sultats de
 * calcul.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivResultSource implements PivParticlesSourceI {
  DefaultListModel<CtuluVariable> varList_;
  DefaultListModel<PivVectorDefinitionI> flecheList_;
  DefaultListModel<Double> timeList_;
  double[] times_;
  EfGridDataInterpolator interp_;
  PivResultsModel res_;
  String name_;

  public PivResultSource(PivResultsModel _res, String _name) {
    res_=_res;
    name_=_name;
  }
  
  @Override
  public void setResults(PivResultsModel _mdl) {
    varList_=null;
    timeList_=null;
    times_=null;
    interp_=null;
    res_=_mdl;
  }
  
  @Override
  public EfGridDataInterpolator getInterpolator() {
    if (interp_==null)
      interp_=new EfGridDataInterpolator(this);
    return interp_;
  }

  @Override
  public double[] getTimes() {
    if (times_==null) {
      times_=new double[getNbTimeStep()];
      for (int i=0; i<times_.length; i++) {
        times_[i]=getTimeStep(i);
      }
    }
    return times_;
  }

  @Override
  public ListModel<PivVectorDefinitionI> getFlecheListModel() {
    if (flecheList_==null) {
      PivVectorDefinitionI fch;
      flecheList_=new DefaultListModel<>();
      fch=new PivVectorDefinitionI() {
        
        @Override
        public CtuluVariable getVy() {
          return PivResultsI.ResultType.VY;
        }
        
        @Override
        public CtuluVariable getVx() {
          return PivResultsI.ResultType.VX;
        }
        
        @Override
        public String getVar() {
          return PivResource.getS("Vitesse");
        }
        
        @Override
        public String toString() {
          return getVar();
        }
      };
      flecheList_.addElement(fch);
    }
    return flecheList_;
  }

  @Override
  public int getNbTimeStep() {
    return res_==null ? 0:res_.getNbTime();
  }

  @Override
  public CtuluNumberFormatI getTimeFormatter() {
    return new CtuluDurationDateFormatter();
  }

  @Override
  public double getTimeStep(int _i) {
    return _i;
  }

  @Override
  public ListModel<CtuluVariable> getVarListModel() {
    if (varList_==null) {
      varList_=new DefaultListModel<>();
      if (res_ != null) {
        for (CtuluVariable var : res_.getVariables()) {
          varList_.addElement(var);
        }
      }
    }
    return varList_;
  }

  @Override
  public ListModel<Double> getTimeListModel() {
    if (timeList_==null) {
      timeList_=new DefaultListModel<>();
      if (res_ != null) {
        for (int i=0; i < res_.getNbTime(); i++) {
          timeList_.addElement(getTimeStep(i));
        }
      }
    }
    return timeList_;
  }

  @Override
  public boolean isElementVar(CtuluVariable _idxVar) {
    return res_==null ? false:res_.isElementVar(_idxVar);
  }

  @Override
  public boolean isDefined(CtuluVariable _var) {
    return res_==null ? false:res_.isDefined(_var);
  }

  @Override
  public EfData getData(CtuluVariable _var, int _timeIdx) throws IOException {
    return res_==null ? null:res_.getData(_var, _timeIdx);
  }

  @Override
  public double getData(CtuluVariable _var, int _timeIdx, int _idxObjet) throws IOException {
    return res_==null ? null:res_.getData(_var, _timeIdx, _idxObjet);
  }

  @Override
  public EfGridInterface getGrid() {
    return res_==null ? null:res_.getGrid();
  }

  @Override
  public String getSourceName() {
    return name_;
  }

  @Override
  public CtuluVariable getVariableById(String _id) {
    // Initialise la liste.
    getVarListModel();
    if (varList_==null) return null;
    for (int i=0; i<varList_.size(); i++) {
      if (varList_.getElementAt(i).getID().equals(_id))
        return varList_.getElementAt(i);
    }
    return null;
  }
}