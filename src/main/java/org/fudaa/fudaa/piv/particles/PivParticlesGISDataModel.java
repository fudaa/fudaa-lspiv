/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.fudaa.piv.particles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.collection.CtuluArrayBoolean;
import org.fudaa.ctulu.collection.CtuluListDouble;
import org.fudaa.ctulu.gis.GISAttributeBoolean;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.dodico.ef.operation.EfTrajectoireParameters;
import org.fudaa.dodico.ef.operation.EfTrajectoireResultBuilder;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.piv.PivResource;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;

import com.memoire.fu.FuLog;

/**
 * Ce dataModel est utilise pour transformer les resultats des calcul de particules en donnees sig
 * 
 * @author deniger, Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivParticlesGISDataModel implements GISDataModel {
  
  /**
   * Attributs utilis�s dans le calque.
   */
  public static final GISAttributeInterface ATT_MARK=new GISAttributeBoolean("MARK", true);
  public static final GISAttributeInterface ATT_TEMPS=new GISAttributeDouble(PivResource.getS("Temps"), true);


  List<GISAttributeInterface> att_;
  GISPolyligne[] ligne_;
  Map<GISAttributeInterface, List> values_;

  @SuppressWarnings("unchecked")
  protected static PivParticlesGISDataModel build(List<EfTrajectoireResultBuilder> _res, GrMorphisme _toReal, CtuluAnalyze _analyse) {
    List<GISPolyligne> poly=new ArrayList<GISPolyligne>(_res.size());
    EfTrajectoireParameters params=null;
    List<GISAttributeInterface> attrs=null;
    Map<GISAttributeInterface, CtuluVariable> attr2Var=new HashMap<>();

    Map<GISAttributeInterface, List> values=new HashMap<>();
    int nbRemove=0;
    
    for (EfTrajectoireResultBuilder res : _res) {
      if (params==null) {
        params=res.getParameters();
        attrs=buildAttributes(params,attr2Var);
      }
      
      List<Coordinate> coords=res.getCoords();
      if (coords.size() < 2) {
        nbRemove++;
      }
      else {
        // Changement de coordonn�es pour la visualisation (comme pour les autres calques). Ces coordonn�es ne sont pas sauvegard�es.
        for (int i=0; i<coords.size(); i++) {
          GrPoint pt=new GrPoint(coords.get(i)).applique(_toReal);
          coords.set(i, new Coordinate(pt.x_, pt.y_, pt.z_));
        }
        poly.add((GISPolyligne) GISGeometryFactory.INSTANCE.createLineString((Coordinate[]) coords.toArray(new Coordinate[coords.size()])));
        
        for (GISAttributeInterface att : attrs) {

          List dest=values.get(att);
          if (dest == null) {
            dest=new ArrayList(_res.size());
            values.put(att, dest);
          }
          
//          CtuluVariable v=_corresp.get(attribute);
          if (att == ATT_TEMPS) {
            dest.add(new CtuluListDouble(res.getTimes()));
          }
          else if (att == ATT_MARK) {
            dest.add(new CtuluArrayBoolean((Boolean[]) res.getMarqueurs().toArray(new Boolean[res.getMarqueurs().size()])));
          }
          else {
            dest.add(new CtuluListDouble(res.getVarValues().get(attr2Var.get(att))));
          }
        }
      }

    }
    if (poly.isEmpty()) {
      _analyse.addFatalError(PivResource.getS("Aucune ligne � tracer"));
    }
    if (nbRemove > 0) {
      if (params.isTrajectoire()) {
        _analyse.addWarn(PivResource.getS("{0} trajectoire(s) enlev�e(s) car vide(s)", CtuluLibString.getString(nbRemove)), -1);
      }
      else {
        _analyse.addWarn(PivResource.getS("{0} ligne(s) de courant enlev�e(s) car vide(s)", CtuluLibString.getString(nbRemove)), -1);
      }
    }
    PivParticlesGISDataModel model=new PivParticlesGISDataModel();
    model.att_=attrs;
    model.ligne_=(GISPolyligne[]) poly.toArray(new GISPolyligne[poly.size()]);
    model.values_=values;
    return model;

  }
  
  private static GISAttributeInterface createAttributeForVar(CtuluVariable _var) {
    GISAttributeDouble ret=new GISAttributeDouble(_var.getName(), true);
    ret.setID(_var.getName());
    return ret;
  }
  
  private static List<GISAttributeInterface> buildAttributes(EfTrajectoireParameters _params, Map<GISAttributeInterface, CtuluVariable> _attr2Var) {
    List<GISAttributeInterface> list=new ArrayList<>();
    list.add(ATT_TEMPS);
    list.add(ATT_MARK);
//    list.add(ATT_VX);
//    list.add(ATT_VY);
    _attr2Var.put(ATT_MARK, ATT_MARK);
    _attr2Var.put(ATT_TEMPS, ATT_TEMPS);
    
    for (CtuluVariable var : _params.varsASuivre_) {
      GISAttributeInterface attr=createAttributeForVar(var);
      _attr2Var.put(attr,var);
      list.add(attr);
    }
    
    return list;
  }

  private PivParticlesGISDataModel() {

  }
  
  public GISAttributeInterface[] getAttributes() {
    return att_.toArray(new GISAttributeInterface[0]);
  }

  @Override
  public GISAttributeInterface getAttribute(int _idxAtt) {
    return att_.get(_idxAtt);
  }

  @Override
  public double getDoubleValue(int _idxAtt, int _idxGeom) {
    FuLog.error("non possible");
    return 0;
  }

  @Override
  public Envelope getEnvelopeInternal() {
    FuLog.error("pas calcule");
    return null;
  }
  
  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if(xyToAdd==null){
      return this;
    }
    PivParticlesGISDataModel model = new PivParticlesGISDataModel();
    model.att_ = this.att_;
    model.ligne_ = new GISPolyligne[ligne_.length];
    for(int i=0;i<ligne_.length;i++){
      model.ligne_[i]=ligne_[i].createTranslateGeometry(xyToAdd);
    }
    model.values_ = this.values_;
    return model;
  }

  @Override
  public Geometry getGeometry(int _idxGeom) {
    return ligne_[_idxGeom];
  }

  @Override
  public int getIndiceOf(GISAttributeInterface _att) {
    return att_.indexOf(_att);
  }

  @Override
  public int getNbAttributes() {
    return att_.size();
  }

  @Override
  public int getNumGeometries() {
    return ligne_.length;
  }

  @Override
  public Object getValue(int _idxAtt, int _idxGeom) {
    GISAttributeInterface att = att_.get(_idxAtt);
    return values_.get(att).get(_idxGeom);
  }

  @Override
  public void preload(GISAttributeInterface[] _att, ProgressionInterface _prog) {}

}
