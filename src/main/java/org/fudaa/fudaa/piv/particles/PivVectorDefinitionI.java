/**
 *  @creation     16 nov. 2004
 *  @modification $Date: 2007-06-05 09:01:13 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.particles;

import org.fudaa.ctulu.CtuluVariable;

/**
 * D�finit les variables des composantes x,y des vecteurs utilis�s pour le calcul de trajectoire.
 * Ces composantes peuvent �tre vx, vy ou toutes autres variables
 * 
 * @author Fred Deniger, Bertrand Marchand (marchand@deltacad.fr)
 */
public interface PivVectorDefinitionI {

  /**
   * @return Le nom du vecteur
   */
  String getVar();

  /**
   * @return La composante suivant X
   */
  CtuluVariable getVx();

  /**
   * @return La composante suivant Y
   */
  CtuluVariable getVy();
  
  // A implementer pour l'affichage correct dans les listes.
  String toString();
}
