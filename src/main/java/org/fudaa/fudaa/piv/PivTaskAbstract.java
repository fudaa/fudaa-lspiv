package org.fudaa.fudaa.piv;

/**
 * Une tache avec un statut. Cette tache peut �tre observ�e pour avoir un retour d'information sur son avanc�e.
 * @author marchand
 *
 */
public abstract class PivTaskAbstract extends Thread {
  
  /**
   * Les etats pour une tache.
   * @author marchand
   */
  public enum TaskStatus {
    /** La tache n'est pas a lancer (pas de param�tres, ou la tache est saut�e) */
    PASSED,
    /** La tache est a jour, n'est pas sens�e etre relanc�e. */
    UP_TO_DATE,
    /** La tache s'est termin�e avec succes */
    SUCCESS,
    /** La tache a echou�  */
    FAILED,
    /** La tache est en cours d'ex�cution */
    RUNNING,
    /** La tache a �t� abord�e */
    ABORTED,
    /** La tache peut �tre lanc�e. */
    RUNNABLE,
    /** La tache est en attente d'etre execut�e. */
    WAITING;
  }
  
  protected TaskStatus status_;
  protected PivTaskObserverI observer_;

  public PivTaskAbstract() {
    super();
  }
  
  public PivTaskAbstract(String _name) {
    super(_name);
  }

  public void setStatus(TaskStatus _status) {
    status_ = _status;
    if (observer_ != null) {
      observer_.setStatus(_status);
    }
  }
  
  public TaskStatus getStatus() {
    return status_;
  }
  
  @Override
  public void run() {
    try {
      if (observer_ != null) {
        observer_.setStatus(TaskStatus.RUNNING);
        observer_.reset();
      }
      boolean ret = act(observer_);
      if (observer_ != null) {
        if (observer_.isStopRequested()) {
          observer_.setStatus(TaskStatus.ABORTED);
        }
        else if (ret) {
          observer_.setStatus(TaskStatus.SUCCESS);
        }
        else {
          observer_.setStatus(TaskStatus.FAILED);
        }
      }
    }
    finally {
      if (observer_ != null)
        observer_.completed();
    }
  }
  
  /**
   * La m�thode d'ex�cution, a implementer pour la tache
   * @param _observer L'observer de la tache.
   * @return
   */
  public abstract boolean act(PivTaskObserverI _observer);
  
  /**
   * Definit la vue de la tache qui affiche la progression.
   * @param _view LA vue.
   */
  public void setObserver(PivTaskObserverI _view) {
    observer_ = _view;
  }
}
