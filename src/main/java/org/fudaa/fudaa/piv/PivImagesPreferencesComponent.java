package org.fudaa.fudaa.piv;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;

import com.memoire.bu.BuAbstractPreferencesComponent;
import com.memoire.bu.BuVerticalLayout;

/**
 * Un composant pour les pr�f�rences d'images
 * @author marchand@deltacad.fr
 */
public class PivImagesPreferencesComponent extends BuAbstractPreferencesComponent {
  private JCheckBox cbAutoCache_;

  public PivImagesPreferencesComponent() {
    super(PivPreferences.PIV);

    cbAutoCache_=new JCheckBox(PivResource.getS("Cache des images automatique"));
    cbAutoCache_.setPreferredSize(new Dimension(300, cbAutoCache_.getPreferredSize().height));
//    setLayout(new FlowLayout(FlowLayout.LEFT,2,0));
    setLayout(new BuVerticalLayout());
    add(new JLabel(PivResource.getS("<html><i>La cr�ation/mise � jour des caches de toutes les images peut prendre plusieurs minutes. Cette cr�ation sera r�alis�e apr�s chaque op�ration de s�lection d'image ou transformation.</i></html>")));
    add(cbAutoCache_);
    
    updateComponent();
    
    cbAutoCache_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        setSavabled(true);
        setModified(true);
      }
    });
  }
  
  @Override
  public String getTitle() {
    return PivResource.getS("Cr�ation automatique des images cache");
  }
  
  @Override
  public boolean isPreferencesValidable() {
    return true;
  }
  
  @Override
  public boolean isPreferencesCancelable() {
    return true;
  }
  
  /**
   * Mise a jour des composant � partir des info du fichier.
   */
  @Override
  protected void updateComponent() {
    cbAutoCache_.setSelected(PivPreferences.PIV.getBooleanProperty(PivPreferences.PIV_AUTO_CACHE,true));
    
  }
  
  /**
   * Remplit la table a partir des valeurs des combobox.
   */
  @Override
  protected void updateProperties() {
    PivPreferences.PIV.putBooleanProperty(PivPreferences.PIV_AUTO_CACHE,cbAutoCache_.isSelected());
  }
}
