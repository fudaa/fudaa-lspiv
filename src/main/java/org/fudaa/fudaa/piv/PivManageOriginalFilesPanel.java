/*
 * @creation     18 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.piv.metier.PivComputeParameters;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.bu.BuVerticalLayout;

/**
 * Un panneau pour gerer les images d'origine, qu'elles soient pgm ou autres.
 * 
 * @author marchand@deltacad.fr
 * @version $Id$
 */
public class PivManageOriginalFilesPanel extends CtuluDialogPanel {

  public class BooleanCellRenderer extends JCheckBox implements TableCellRenderer  {
    
    public BooleanCellRenderer() {
      setHorizontalAlignment(SwingConstants.CENTER);
      setOpaque(false);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      setSelected((Boolean)value);
      setOpaque(true);
      if (isSelected) {
        setBackground(UIManager.getColor("Table.selectionBackground"));
      }
      else {
        setBackground(UIManager.getColor("Table.background"));
      }
      return this;
    }
  }
  
  public class OriginalFilesModel extends CtuluListEditorModel {
    
    public OriginalFilesModel() {
      super(true);
    }
    
    @Override
    public boolean isCellEditable(int _rowIndex, int _columnIndex) {
      return _columnIndex>=2;
    }
    
    @Override
    public int getColumnCount() {
      return 3;
    }

    /* (non-Javadoc)
     * @see org.fudaa.ctulu.gui.CtuluListEditorModel#getColumnClass(int)
     */
    @Override
    public Class<?> getColumnClass(int _columnIndex) {
      if (_columnIndex == 0) {
        return Integer.class;
      }
      else if (_columnIndex == 1) {
        return String.class;
      }
      else {
        return Boolean.class;
      }
    }

    @Override
    public Object getValueAt(final int _row, final int _col) {
      FileCouple val = (FileCouple)v_.get(_row);
      if (_col == 0) {
        return CtuluLibString.getString(_row + 1);
      }
      else if (_col == 1) {
        return val.file;
      }
      else {
        return val.isBackground;
      }
    }
    
    @Override
    public void setValueAt(Object _aValue, int _row, int _col) {
      FileCouple val = (FileCouple)v_.get(_row);
      if (_col == 2) {
        val.isBackground = (boolean)_aValue;
      }
    }

    @Override
    public String getColumnName(final int _column) {
      switch (_column) {
        case 0:
          return PivResource.getS("N�");
        case 1:
          return PivResource.getS("Fichier");
        case 2:
          return PivResource.getS("Fond");
        default:
          return CtuluLibString.EMPTY_STRING;
      }
    }

    @Override
    public Object createNewObject() {
      return null;
    }
  }
  
  /**
   * Une classe qui associe un fichier � un boolean qui indique si le fichier image est un fichier de fond ou non.
   * @author Bertrand Marchand (marchand@detacad.fr)
   */
  public class FileCouple {
    
    public FileCouple(File _f, boolean _isBackground) {
      file = _f;
      isBackground = _isBackground;
    }
    
    File file;
    boolean isBackground;
  }
  
  private PivImplementation impl_;
  /** Le repertoire racine du projet */
  private File prjRoot_;
  /** Le mod�le pour la liste des images */
  private CtuluListEditorModel mdFiles_;
  /** Le nombre d'images par secondes */
  private JTextField tfFrameRate_;
  /** L'intervalle de temps (s) */
  private JTextField tfTimeStep_;
  
  /**
   * Constructeur.
   * @param _impl Le parent pour la boite de dialogue.
   */
  public PivManageOriginalFilesPanel(PivImplementation _impl) {
    impl_=_impl;
    
    setLayout(new BuVerticalLayout(5, true, true));
    setHelpText(PivResource.getS("Les images de fond ne participent pas au calcul.\nLes autres images seront utilis�es pour le calcul dans l'ordre ou elles sont d�finies.\nUtilisez Monter/Descendre pour modifier cet ordre."));
    
    // L'intervalle de temps
    tfFrameRate_ = new JTextField();
    tfFrameRate_.setColumns(10);
    tfTimeStep_ = new JTextField();
    tfTimeStep_.setColumns(10);
    
    JPanel pnFrameRate = new JPanel();
    pnFrameRate.setLayout(new FlowLayout(FlowLayout.LEFT, 3, 3));
    
    pnFrameRate.add(new JLabel(PivResource.getS("Nombre d'images par secondes:")));
    pnFrameRate.add(tfFrameRate_);
    pnFrameRate.add(new JLabel(PivResource.PIV.getIcon("lie-horizontal")));
    pnFrameRate.add(new JLabel(PivResource.getS("Intervalle de temps")+" (s):"));
    pnFrameRate.add(tfTimeStep_);
    
    new PivLinkNumericalTextFields<Double,Double>(tfFrameRate_, tfTimeStep_) {

      @Override
      public Double calculateFromTF2() {
        double val = Double.parseDouble(tf2_.getText());
        if (val==0)
          return 0.;
        
        return 1./val;
      }

      @Override
      public Double calculateFromTF1() {
        double val = Double.parseDouble(tf1_.getText());
        if (val==0)
          return 0.;
        
        return 1./val;
      }
    };
    
    add(pnFrameRate);
    
    // Label images
    JPanel pnFilesLabel = new JPanel();
    pnFilesLabel.setLayout(new FlowLayout(FlowLayout.LEFT, 3, 3));
    
    pnFilesLabel.add(new JLabel(PivResource.getS("Liste des images"),JLabel.LEFT));
    add(pnFilesLabel);
    
    // Liste des images
    mdFiles_=new OriginalFilesModel();
    
    CtuluListEditorPanel pnFiles=new CtuluListEditorPanel(mdFiles_, true, true, false) {
      @Override
      public void actionAdd() {
        CtuluFileChooser fc = new CtuluFileChooser(true);
        fc.setFileFilter(PivUtils.FILE_FLT_IMAGES);
        fc.setMultiSelectionEnabled(true);
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setAcceptAllFileFilterUsed(false);
        fc.setDialogTitle(PivResource.getS("Ajouter des images"));
        if (fc.showOpenDialog(impl_.getParentComponent()) == JFileChooser.CANCEL_OPTION) {
          return;
        }

        File[] files = fc.getSelectedFiles();
        for (File f : files) {
          mdFiles_.addElement(new FileCouple(f, false));
        }
        
      }
    };

    
    pnFiles.setValueListCellRenderer(new CtuluCellTextRenderer() {
      @Override
      public void setValue(Object _file) {
        File f=(File)_file;
        if (f.getName().startsWith(PivProject.BACKGROUND_IMAGE_NAME_PREFIX)) {
          setText(f.getName().substring(PivProject.BACKGROUND_IMAGE_NAME_PREFIX.length()));
        }
        else {
          setText(f.getName());
        }
        if (f.getPath().startsWith(prjRoot_.getPath())) {
          setToolTipText(PivResource.getS("Dans le projet"));
        }
        else {
          setToolTipText(f.getPath());
        }
      }
    });
//    pnFiles.getTableColumnModel().getColumn(2).setCellEditor(fileMng_.getFmtRowEditor());
    pnFiles.getTableColumnModel().getColumn(2).setCellRenderer(new BooleanCellRenderer());
    pnFiles.getTableColumnModel().getColumn(2).setMaxWidth(70);
    pnFiles.setPreferredSize(new Dimension(pnFiles.getPreferredSize().width,250));


    add(pnFiles);
  }
  
  @Override
  public void setValue(Object _files) {
    if (!(_files instanceof File[]))
      throw new IllegalArgumentException("bad type parameter");
    setFiles((File[])_files);
  }
  
  @Override
  public File[] getValue() {
    return getFiles();
  }
  
  /**
   * Definit la racine du projet, pour savoir si un fichier fait d�j� partie du projet.
   * @param _root La racine du projet.
   */
  public void setRootProject(File _root) {
    prjRoot_=_root;
  }

  /**
   * Rempli le panneau depuis les donn�es du projet.
   * @param _timeStep L'intervalle de temps.
   */
  public void setTimeStep(Double _timeStep) {
    tfTimeStep_.setText("" + (_timeStep!=null ? _timeStep:""));
  }
  
  public double getTimeStep() {
    return Double.parseDouble(tfTimeStep_.getText().trim());
  }
  
  /**
   * D�finit les images sources
   * @param _files Les images
   */
  public void setFiles(File[] _files) {
    if (_files == null)
      return;

    mdFiles_.removeAll();
    for (int i=_files.length - 1; i >= 0; i--) {
      boolean isBackground=_files[i].getName().startsWith(PivProject.BACKGROUND_IMAGE_NAME_PREFIX);
      mdFiles_.add(0, new FileCouple(_files[i], isBackground));
    }
  }
  
  /**
   * @return Le type de chaque fichier (caucl ou fond).
   */
  public PivProject.SRC_IMAGE_TYPE[] getFileTypes() {
    FileCouple[] fcs=new FileCouple[mdFiles_.getRowCount()];
    mdFiles_.getValues(fcs);
    
    ArrayList<PivProject.SRC_IMAGE_TYPE> types = new ArrayList<>();
    for (FileCouple fc : fcs) {
      if (fc.isBackground) {
        types.add(PivProject.SRC_IMAGE_TYPE.BACKGROUND);
      }
      else {
        types.add(PivProject.SRC_IMAGE_TYPE.CALCULATION);
      }
    }
    
    return types.toArray(new PivProject.SRC_IMAGE_TYPE[0]);
  }
  
  /**
   * @return Les fichiers source, comportant les anciens et nouveaux fichiers.
   */
  public File[] getFiles() {
    FileCouple[] fcs=new FileCouple[mdFiles_.getRowCount()];
    mdFiles_.getValues(fcs);
    
    ArrayList<File> files = new ArrayList<>();
    for (FileCouple fc : fcs) {
        files.add(fc.file);
    }
    
    return files.toArray(new File[0]);
  }
  
  @Override
  public boolean isDataValid() {
    boolean bok=PivUtils.isStrictPositiveReal(this, tfFrameRate_.getText(), PivResource.getS("Nombre d'images par secondes"))!=null && 
        PivUtils.isStrictPositiveReal(this, tfTimeStep_.getText(), PivResource.getS("Intervalle de temps"))!=null;
    
    if (!bok) return false;
    
    setErrorText("");
    return true;
  }
}
