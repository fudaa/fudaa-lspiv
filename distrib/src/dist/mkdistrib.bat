@echo off
rem Cr�e une distribution de fudaa-lspiv avec izpack.

if "%1"=="" goto err

set LSPIV_JAR=%1
rem Il doit �tre mis dans le fichier .xml
rem set APP_VER=1.1

pushd izpack
call c:\progra~1\izpack\bin\compile fudaa-lspiv.xml -b . -o ..\fudaa-lspiv-%LSPIV_JAR%-setup.jar -k standard
popd
goto end

:err
echo Usage : mkdistrib {version}
echo Example : mkdistrib 1.1_b20110915
:end