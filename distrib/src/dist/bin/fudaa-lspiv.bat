@echo off
@rem Lancement de Fudaa-LSPIV

@rem A modifier suivant l'installation du JDK.
set JAVA="%{JAVA_HOME}"

@rem A modifier suivant la version de Fudaa-LSPIV
set JAR=fudaa-lspiv.jar

if "%{ISO3_LANG}"=="fra" set PIV_LANG=fr
if "%{ISO3_LANG}"=="eng" set PIV_LANG=en

if not exist %JAVA%\bin\javaw.exe goto error

%JAVA%\bin\javaw -Xmx2048m -Dpiv.lang="%PIV_LANG%" -Dpiv.exe.path="%{INSTALL_PATH}\exes" -Dpiv.doc.path="%{INSTALL_PATH}\doc" -Dpiv.templates.path="%{INSTALL_PATH}\templates" -cp "%{INSTALL_PATH}\lib\%JAR%;%{INSTALL_PATH}\lib\jai_imageio.jar" org.fudaa.fudaa.piv.Piv %1 %2 %3 %4 %5 %6 %7 %8 %9
goto end

@rem Java a ete modifie, obligation de reinstaller.
:error
if "%PIV_LANG%"=="fr" msg %username% Java a probablement ete modifie (ou mis a jour) et Fudaa-LSPIV ne fonctionnera plus. Merci de reinstaller Fudaa-LSPIV.
if "%PIV_LANG%"=="en" msg %username% Java has probably been modified (or updated) and Fudaa-LSPIV will no longer work. Please reinstall Fudaa-LSPIV.

:end