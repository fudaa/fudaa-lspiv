Installation de Fudaa-LSPIV
===========================

Pr�-requis
----------

Fudaa-LSPIV ne necessite plus l'installation de Java.
Aucun autre pr�-requis n'est n�cessaire.

Installation et execution de Fudaa-LSPIV (Windows)
--------------------------------------------------

1. Decompressez le fichier d'installation fudaa-lspiv-{version}-win-x64.zip. {version} est � remplacer par la version du logiciel.
2. Double cliquez sur le fichier Fudaa-LSPIV\fudaa-lspiv.bat pour ex�cuter Fudaa-LSPIV. 

Installation et execution de Fudaa-LSPIV (Linux)
--------------------------------------------------

1. Decompressez le fichier d'installation fudaa-lspiv-{version}-linux-x64.tar.gz. {version} est � remplacer par la version du logiciel.
2. Double cliquez sur le fichier Fudaa-LSPIV\fudaa-lspiv.sh pour ex�cuter Fudaa-LSPIV. 

D�sinstallation
---------------

La desinstallation se fait par la suppression du repertoire Fudaa-LSPIV.
 