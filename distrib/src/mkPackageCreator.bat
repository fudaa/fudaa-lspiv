@echo off

::---------------------------------------------------------
:: Génération du creator de package Fudaa-LSPIV
:: Ce template sert sur Windows et Linux.
::---------------------------------------------------------

if "%1" == "" goto usage

echo Creation de l'archive packageCreator-%1.zip de creation de setup Fudaa-LSPIV...

set TEMPLATE_DIR=packageCreator
set RESOURCES_DIR=.\resources

:: Vérification de la présence du PATH des exes
set EXES_PATH=D:\Projets\Fudaa\devel\exes\lspiv\exes
if not exist "%EXES_PATH%" goto exesPath

:: Le java utilisé pour packager est la version 17.
set JAVA_HOME=C:\Program Files\Java\jdk-17
if not exist "%JAVA_HOME%" goto java17

:: Verification que le jar a été généré pour la version  donnée
set JAR_FILE=..\..\target\fudaa-lspiv-%1-SNAPSHOT-with-dep.jar
if not exist "%JAR_FILE%" goto jarFile

set PATH=%JAVA_HOME%\bin;%PATH%

rmdir /s/q %TEMPLATE_DIR% 2> NUL

xcopy /e/q/i/y %RESOURCES_DIR% "%TEMPLATE_DIR%\%RESOURCES_DIR%"
xcopy /y scripts\create_setup* "%TEMPLATE_DIR%\*"
copy "%JAR_FILE%" "%TEMPLATE_DIR%\%RESOURCES_DIR%\jars\fudaa-lspiv.jar"
xcopy /e/q/i/y "%EXES_PATH%"\linux-x64 "%TEMPLATE_DIR%\%RESOURCES_DIR%\linux-x64\exes"
xcopy /e/q/i/y "%EXES_PATH%"\win-x64 "%TEMPLATE_DIR%\%RESOURCES_DIR%\win-x64\exes"

jar cMf packageCreator-%1.zip "%TEMPLATE_DIR%"
rmdir /s/q %TEMPLATE_DIR% 2> NUL

goto end

:exesPath
echo Erreur : Le chemin %EXES_PATH% est introuvable
goto end

:java17
echo Erreur : Java 17 doit etre installe sur %JAVA_HOME%
goto end

:jarFile
echo Erreur : Le fichier %JAR_FILE% n'a pas ete genere
goto end

:usage
echo Usage : %0 {version}

:end