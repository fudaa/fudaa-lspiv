@echo on

::---------------------------------------------------------
:: Creation du package Fudaa-LSPIV
::---------------------------------------------------------

if "%1" == "" goto usage

echo Creation du package fudaa-lspiv-%1-win-x64.zip...

set DISTRIB_DIR=_distrib
set RESOURCES_DIR=.\resources
set APPLI_DIR=Fudaa-LSPIV

:: Le java embarqué est la version 11, au dela pb avec certaines librairies.
set JAVA_11_DIR=C:\Program Files\Java\jdk-11
if not exist "%JAVA_11_DIR%" goto java11

:: Le java utilisé pour packager est la version 17.
set JAVA_HOME=C:\Program Files\Java\jdk-17
if not exist "%JAVA_HOME%" goto java17

set PATH=%JAVA_HOME%\bin;%PATH%

rmdir /s/q "%DISTRIB_DIR%" 2> NUL
jpackage --type app-image --app-version %1 --description "Fudaa-LSPIV" --name "appli" --dest "%DISTRIB_DIR%\%APPLI_DIR%" --input "%RESOURCES_DIR%\jars" --main-jar fudaa-lspiv.jar --runtime-image "%JAVA_11_DIR%"
::jpackage --type app-image --app-version 10.0 --description "appli Fudaa-LSPIV" --name "%APPLI_DIR%" --dest %DISTRIB_DIR% --input %RESOURCES_DIR%/jars --main-jar fudaa-lspiv-1.10.0.jar

xcopy /e/q/i/y "%RESOURCES_DIR%\doc" "%DISTRIB_DIR%\%APPLI_DIR%\doc"
xcopy /e/q/i/y "%RESOURCES_DIR%\examples" "%DISTRIB_DIR%\%APPLI_DIR%\examples"
xcopy /e/q/i/y "%RESOURCES_DIR%\icons" "%DISTRIB_DIR%\%APPLI_DIR%\icons"
xcopy /e/q/i/y "%RESOURCES_DIR%\templates" "%DISTRIB_DIR%\%APPLI_DIR%\templates"
xcopy /e/q/i/y "%RESOURCES_DIR%\win-x64\*" "%DISTRIB_DIR%\%APPLI_DIR%"
xcopy /y "%RESOURCES_DIR%\*" "%DISTRIB_DIR%\%APPLI_DIR%\*"

jar cMf fudaa-lspiv-%1-win-x64.zip -C "%DISTRIB_DIR%" "%APPLI_DIR%"
rmdir /s/q "%DISTRIB_DIR%" 2> NUL
goto end

:java11
echo Erreur : Java 11 doit etre installe
goto end

:java17
echo Erreur : Java 17 doit etre installe
goto end

:usage
echo Usage : %0 {version}

:end